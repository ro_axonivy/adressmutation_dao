-- =============================================
-- Author:		Brian Streuli, Patrick Balsiger
-- Create date: 08.04.2013
-- Description:	Recursive Queries for Orders having
--				MatchedToOrder Id's
-- =============================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION dbo.GetRelatedOrders
(
	@OrderId int
)
RETURNS 
@Matches TABLE 
(
	OrderId int not null
)
AS
BEGIN
	WITH Parents(orderId, matchedToOrder, dispatchingId)
	AS
	(
		--Anchor
		SELECT 
			orders.orderId,
			CASE WHEN orders.matchedToOrder IS NULL THEN 
				orders.orderId --may matched by dispatchingId
			ELSE 
				orders.matchedToOrder
			END as matchedToOrder, 
			orders.dispatchingId
		FROM 
			tbl_Orders orders
		WHERE 
			orderId = @OrderId
			OR dispatchingId IN (SELECT dispatchingId FROM tbl_Orders WHERE orderId = @OrderId)
		UNION ALL
		--Top Down Childs
		SELECT 
			siblings.orderId,
			siblings.matchedToOrder, 
			siblings.dispatchingId
		FROM tbl_Orders as siblings INNER JOIN Parents 
			ON Parents.matchedToOrder =siblings.orderId
		WHERE 
			siblings.orderId <> siblings.matchedToOrder
			AND NOT siblings.matchedToOrder IS NULL
	)
	INSERT @Matches
	SELECT
		matchedToOrder
	FROM 
		Parents;

	INSERT 
		@Matches
	SELECT 
		dispatches.orderId 
	FROM 
		tbl_Orders orders
		INNER JOIN tbl_Orders dispatches ON orders.dispatchingId = dispatches.dispatchingId
		INNER JOIN @Matches matches ON orders.orderId = matches.orderId;


	WITH ChildsOfParents(OrderId)
	AS (
		SELECT orderId FROM @Matches as parents
		UNION ALL
		--Top Down Childs
		SELECT 
			siblings.orderId as ParentOrderId
		FROM tbl_Orders as siblings INNER JOIN ChildsOfParents 
			ON ChildsOfParents.OrderId =siblings.matchedToOrder
		WHERE 
			siblings.orderId <> siblings.matchedToOrder	
	)
	INSERT @Matches
	SELECT
		OrderId
	FROM 
		ChildsOfParents;

	-- add the linked dispatchingIds
	INSERT @Matches
	SELECT
		OrderId
	FROM 
		tbl_Orders 
	WHERE 
		dispatchingId in (SELECT dispatchingId from tbl_Orders where orderId in (select orderId from @Matches))

	RETURN 
END
GO