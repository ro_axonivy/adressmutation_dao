SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================
-- Author:		Brian Streuli
-- Create date: 19.07.2011
-- Description:	This procedure allows to gather data manipulations from a Table.
--				Manipulated records would be copied into a table named as the source table plus suffix _History
--				After insert, inserted values are appended to the history with HistoryType 'INSERTED'
--				After update, old values are appended with HistoryType 'DELETED'
--				and new values with HistoryType 'INSERTED'
--				After deletion deleted values are appended with HistoryType 'DELETED'
-- ==============================================================================================================
CREATE PROCEDURE [dbo].[sp_create_historyFor] 
	@TABLE_NAME sysname
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @FROM_TABLE sysname;
	DECLARE @TO_TABLE sysname;
	DECLARE @SQL nvarchar(max);
	DECLARE @TO_TABLE_FIELDS nvarchar(max);	
	DECLARE @FROM_TABLE_FIELDS nvarchar(max);
	
	IF(LEFT(@TABLE_NAME, 5)='[dbo]')
		BEGIN
			SET @FROM_TABLE = OBJECT_NAME(OBJECT_ID(@TABLE_NAME));
		END
	ELSE 
		BEGIN
			SET @FROM_TABLE = @TABLE_NAME	
		END
	IF(RIGHT(@FROM_TABLE, 8)='_History')
		RETURN 0;
	ELSE 
		BEGIN
			PRINT 'Adding History table for '+@FROM_TABLE+'...';
		END
	SET @TO_TABLE = @FROM_TABLE+'_History'
			
	--Check if the History table exists
	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].['+@TO_TABLE+']') AND type in (N'U'))
	BEGIN
		SET @SQL = 'DROP TABLE [dbo].['+@TO_TABLE+']'
		EXECUTE (@SQL);
		--PRINT 'Adding History table for '+@FROM_TABLE+'...Old History table dropped.';
	END
	--First we need the fields which are not computed as an update/insert would fail	
	SELECT
		@TO_TABLE_FIELDS = COALESCE(@TO_TABLE_FIELDS+', ['+c.[name]+']', '['+c.[name]+']')
	FROM 
		sys.all_columns AS c
	WHERE
		c.object_id = OBJECT_ID(@FROM_TABLE) 
		AND c.is_computed = 0
		--Cannot use text, ntext, or image columns in the 'inserted' and 'deleted' tables.
		AND c.user_type_id<>34 --image
		AND c.user_type_id<>35 --text
		AND c.user_type_id<>99 --ntext
		
	--Check if the table has an identity Column
	--To get a copy of the original table which is insertable with multiple 
	--identities we need to copy the table without the identity spec.
	--text, ntext and image columns wher not provided by INSERTED/DELETED Trigger tables.
	--so we need to exclude them too.
	DECLARE @COLUMN_NAME sysname;	
	SET @COLUMN_NAME = '';

	SELECT
		@COLUMN_NAME = CASE WHEN c.is_identity=1 THEN '0 as ['+c.[name]+']' ELSE '['+c.[name]+']' END,
		@FROM_TABLE_FIELDS = COALESCE(@FROM_TABLE_FIELDS+', '+@COLUMN_NAME, @COLUMN_NAME)
	FROM 
		sys.all_columns AS c
	WHERE
		c.object_id = OBJECT_ID(@FROM_TABLE) 
			AND (
				-- include identity columns with int types only
				-- as the table creation includes the identity colum as '0 as <Column Name>'
				(c.user_type_id = 56 AND c.is_identity=1)
				-- all other columns which not are identity are allowed
				OR (c.is_identity = 0 
					AND c.user_type_id<>34 --image
					AND c.user_type_id<>35 --text
					AND c.user_type_id<>99 --ntext
					)
			);		
	--Create the table
	SET @SQL = 'SELECT TOP 0 '+@FROM_TABLE_FIELDS+' INTO '+@TO_TABLE+' FROM '+@FROM_TABLE
	EXECUTE (@SQL);	
	--PRINT 'Adding History table for '+@FROM_TABLE+'...New History table created.';
	--Prepare Select From fields
	SET @FROM_TABLE_FIELDS = @TO_TABLE_FIELDS
	--Add History Date
	EXEC ('ALTER TABLE '+@TO_TABLE+' ADD [HistoryDate] datetime not null');				
	SET @TO_TABLE_FIELDS = @TO_TABLE_FIELDS+', HistoryDate'
	SET @FROM_TABLE_FIELDS = @FROM_TABLE_FIELDS+', GetDate() as HistoryDate'
	EXEC ('ALTER TABLE '+@TO_TABLE+' ADD [HistoryYear] int not null');				
	SET @TO_TABLE_FIELDS = @TO_TABLE_FIELDS+', HistoryYear'
	SET @FROM_TABLE_FIELDS = @FROM_TABLE_FIELDS+', YEAR(GetDate()) as HistoryYear'
	EXEC ('ALTER TABLE '+@TO_TABLE+' ADD [HistoryMonth] int not null');				
	SET @TO_TABLE_FIELDS = @TO_TABLE_FIELDS+', HistoryMonth'
	SET @FROM_TABLE_FIELDS = @FROM_TABLE_FIELDS+', MONTH(GetDate()) as HistoryMonth'
	EXEC ('ALTER TABLE '+@TO_TABLE+' ADD [HistoryDay] int not null');				
	SET @TO_TABLE_FIELDS = @TO_TABLE_FIELDS+', HistoryDay'
	SET @FROM_TABLE_FIELDS = @FROM_TABLE_FIELDS+', DAY(GetDate()) as HistoryDay'

	--Add History Type
	DECLARE @FROM_INSERTED_TYPE nvarchar(max);
	DECLARE @FROM_DELETED_TYPE nvarchar(max);
	EXEC ('ALTER TABLE '+@TO_TABLE+' ADD [HistoryType] nvarchar(10) not null');
	SET @TO_TABLE_FIELDS = @TO_TABLE_FIELDS+', HistoryType'				
	SET @FROM_INSERTED_TYPE = @FROM_TABLE_FIELDS+', ''INSERTED'' as HistoryType'
	SET @FROM_DELETED_TYPE = @FROM_TABLE_FIELDS+', ''DELETED'' as HistoryType'
	--Add History id
	EXEC ('ALTER TABLE '+@TO_TABLE+' ADD [HistoryId] int primary key identity(1,1) not null');
	--PRINT 'Adding History table for '+@FROM_TABLE+'...Added History specific fields.';

	--Now we copy the table data as initial values
	SET @SQL = 'INSERT INTO '+@TO_TABLE+' SELECT '+@FROM_INSERTED_TYPE+' FROM '+@FROM_TABLE
	--PRINT 'Adding History table for '+@FROM_TABLE+'...Insert existing data with:'+CHAR(13)+CHAR(10)+@SQL;
	EXECUTE (@SQL);
	--PRINT 'Adding History table for '+@FROM_TABLE+'...Inserted existing data.';

	--Now we can create the triggers
	--Check if it already exists
	DECLARE @TRIGGER_NAME sysname
	SET @TRIGGER_NAME = 'tr_'+@FROM_TABLE
	IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].['+@TRIGGER_NAME+']'))
	BEGIN
		SET @SQL = 'DROP TRIGGER [dbo].['+@TRIGGER_NAME+']'
		EXEC(@SQL);
		--PRINT 'Adding History table for '+@FROM_TABLE+'...Old History Trigger dropped.';
	END
	--Build Trigger Create Statement	
	SET @SQL = 'CREATE TRIGGER [dbo].['+@TRIGGER_NAME+']
	   ON  [dbo].['+@FROM_TABLE+']
	   AFTER INSERT,DELETE,UPDATE
	AS 
	BEGIN
		SET NOCOUNT ON;
		
		INSERT INTO [dbo].['+@TO_TABLE+'] ('+@TO_TABLE_FIELDS+')
		SELECT '+@FROM_DELETED_TYPE+' FROM DELETED
	    
		INSERT INTO [dbo].['+@TO_TABLE+'] ('+@TO_TABLE_FIELDS+')
		SELECT '+@FROM_INSERTED_TYPE+' FROM INSERTED
	END
	'	
	--PRINT 'Adding History table for '+@FROM_TABLE+'...Create History Trigger:'+CHAR(13)+CHAR(10)+@SQL;
	EXEC (@SQL);
	--PRINT 'Adding History table for '+@FROM_TABLE+'...New History Trigger created.';
	PRINT 'Adding History table for '+@FROM_TABLE+'...finished.';

END

GO


