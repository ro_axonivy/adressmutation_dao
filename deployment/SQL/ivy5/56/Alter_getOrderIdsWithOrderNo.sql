IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[getOrderIdsWithOrderNo]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[getOrderIdsWithOrderNo]
GO


-- =======================================================
-- Author:		Brian Streuli
-- Create date: 31.10.2014
-- Description:	Returns Order Id's having given orderNo
-- =======================================================
CREATE FUNCTION [dbo].[getOrderIdsWithOrderNo] 
(
	@OrderNo nvarchar(255)
)
RETURNS @return TABLE (
	orderId int not null
)
AS
BEGIN 
	INSERT INTO @return 
	SELECT 
		SignatureRequestOrderId
	FROM 
		tbl_Contact_SignatureRequest 
	WHERE 
		signatureOrderNo = @OrderNo

	INSERT INTO @return
	SELECT 
		ticket.orderId
	FROM 		
		dbo.tbl_PendencyTickets ticket
		LEFT OUTER JOIN @return ret ON ticket.orderId = ret.orderId
	WHERE
		ret.orderId IS NULL AND
		ticket.CRMIssueOrderNo = @OrderNo 

	INSERT INTO @return
	SELECT 
		contact.orderId
	FROM 
		dbo.tbl_Contacts contact  
		INNER JOIN dbo.tbl_Contact_AddressChecks adrCheck ON contact.contactId = adrCheck.contactId 
		LEFT OUTER JOIN @return ret ON contact.orderId = ret.orderId
	WHERE
		ret.orderId IS NULL AND
		adrCheck.addressCheckOrderNo = @OrderNo  

	INSERT INTO @return
	SELECT 
		contact.orderId
	FROM 
		dbo.tbl_Contacts contact  
		INNER JOIN dbo.tbl_Contact_Letters lettr ON contact.contactId = lettr.contactId 
		LEFT OUTER JOIN @return ret ON contact.orderId = ret.orderId
	WHERE
		ret.orderId IS NULL AND
		lettr.letterOrderNo = @OrderNo 
		
	INSERT INTO @return
	SELECT 
		o.orderId 
	FROM tbl_Request_BusinessPartner bp
		INNER JOIN tbl_Recordings rec ON rec.businessPartner_businessPartnerId = bp.businessPartnerId
		INNER JOIN tbl_Orders o ON o.recordingId = rec.recordingId
		LEFT OUTER JOIN @return ret ON o.orderId = ret.orderId
	WHERE
		ret.orderId IS NULL AND 
		bp.bpOrderNo = @OrderNo
	
	INSERT INTO @return
	SELECT 
		o.orderId 
	FROM tbl_Recording_DomicileOrOwner doo
		INNER JOIN tbl_Orders o ON o.recordingId = doo.recordingId
		LEFT OUTER JOIN @return ret ON o.orderId = ret.orderId
	WHERE
		ret.orderId IS NULL AND  
		doo.orderNo =  @OrderNo
		
	INSERT INTO @return
	SELECT 
		o.orderId 
	FROM tbl_Recording_Correspondence cor
		INNER JOIN tbl_Recordings rec ON rec.correspondence_correspondenceId = cor.correspondenceId
		INNER JOIN tbl_Orders o ON o.recordingId = rec.recordingId
		LEFT OUTER JOIN @return ret ON o.orderId = ret.orderId
	WHERE
		ret.orderId IS NULL AND  (
			changeAddressOrderNo = @OrderNo 
			OR newAddressOrderNo = @OrderNo 
		)
		
	INSERT INTO @return
	SELECT 
		o.orderId 
	FROM tbl_Recording_CorrespondenceContainer corCont
		INNER JOIN tbl_Recordings rec ON rec.correspondence_correspondenceId = corCont.correspondenceId
		INNER JOIN tbl_Orders o ON o.recordingId = rec.recordingId
		LEFT OUTER JOIN @return ret ON o.orderId = ret.orderId
	WHERE
		ret.orderId IS NULL AND  
		corCont.containerOrderNo =  @OrderNo
		
	INSERT INTO @return
	SELECT 
		o.orderId 
	FROM tbl_Recording_LSV lsv
		INNER JOIN tbl_Orders o ON o.recordingId = lsv.recordingId
		LEFT OUTER JOIN @return ret ON o.orderId = ret.orderId
	WHERE
		ret.orderId IS NULL AND  
		lsv.lsvOrderNo  = @OrderNo
	
	INSERT INTO @return
	SELECT 
		o.orderId 
	FROM tbl_Recording_Affirmation aff
		INNER JOIN tbl_Recordings rec ON rec.affirmationId = aff.affirmationId
		INNER JOIN tbl_Orders o ON o.recordingId = rec.recordingId
		LEFT OUTER JOIN @return ret ON o.orderId = ret.orderId
	WHERE
		ret.orderId IS NULL AND 
		aff.orderNo = @OrderNo

	INSERT INTO @return
	SELECT 
		o.orderId 
	FROM dbo.tbl_Recording_Meta meta
		INNER JOIN tbl_Orders o ON o.recordingId = meta.recordingId
		LEFT OUTER JOIN @return ret ON o.orderId = ret.orderId
	WHERE
		ret.orderId IS NULL AND  
		meta.recordingMetaOrderNo = @OrderNo
	RETURN
END
