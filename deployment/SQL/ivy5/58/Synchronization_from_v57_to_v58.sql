/*
Script created by SQL Compare version 7.1.0 from Red Gate Software Ltd at 13.08.2015 12:30:48
Run this script on swbtstsrvweb.bzp_ivy_data_IT to make it the same as swbtstsrvweb.bzp_ivy_data_DEV
Please back up your database before running this script
*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[btbl_MailBoxConfigurations]'
GO
CREATE TABLE [dbo].[btbl_MailBoxConfigurations]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[Host] [varchar] (255) NOT NULL,
[IsRunnable] [bit] NOT NULL,
[OnExceptionMailFrom] [varchar] (255) NOT NULL,
[OnExceptionMailTo] [varchar] (255) NOT NULL,
[Password] [varchar] (255) NOT NULL,
[Port] [int] NOT NULL,
[Username] [varchar] (255) NOT NULL,
[OrderMatchingId] [int] NOT NULL,
[NextScan] [datetime2] NULL,
[ScanIntervalLiteral] [varchar] (255) NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__tbl_Mail__3213E83F7AA72534] on [dbo].[btbl_MailBoxConfigurations]'
GO
ALTER TABLE [dbo].[btbl_MailBoxConfigurations] ADD CONSTRAINT [PK__tbl_Mail__3213E83F7AA72534] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tbl_Attachment]'
GO
CREATE TABLE [dbo].[tbl_Attachment]
(
[attachmentId] [int] NOT NULL IDENTITY(1, 1),
[scanningId] [int] NOT NULL,
[fileId] [int] NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__tbl_Atta__C417BD8174EE4BDE] on [dbo].[tbl_Attachment]'
GO
ALTER TABLE [dbo].[tbl_Attachment] ADD CONSTRAINT [PK__tbl_Atta__C417BD8174EE4BDE] PRIMARY KEY CLUSTERED ([attachmentId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[tbl_Attachment]'
GO
ALTER TABLE [dbo].[tbl_Attachment] ADD CONSTRAINT [UK_bgk1qj0wl9byw6km6bj7va1uu] UNIQUE NONCLUSTERED ([fileId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[btbl_MailBoxConfigurations]'
GO
ALTER TABLE [dbo].[btbl_MailBoxConfigurations] ADD
CONSTRAINT [FK_6282sqcn0i0ky6mv0b6xx9o6u] FOREIGN KEY ([OrderMatchingId]) REFERENCES [dbo].[btbl_OrderMachings] ([orderMatchId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[tbl_Attachment]'
GO
ALTER TABLE [dbo].[tbl_Attachment] ADD
CONSTRAINT [FK_bgk1qj0wl9byw6km6bj7va1uu] FOREIGN KEY ([fileId]) REFERENCES [dbo].[tbl_Files] ([fileId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
