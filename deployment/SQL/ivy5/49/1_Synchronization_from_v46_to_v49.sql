/*
Script created by SQL Compare version 7.1.0 from Red Gate Software Ltd at 18.12.2013 15:41:59
Run this script on b2p.soreco.ch.bzp_ivy_data to make it the same as swbnbbst001.bzp_ivy_data
Please back up your database before running this script
*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[tbl_OrderBookTypes]'
GO
CREATE TABLE [dbo].[tbl_OrderBookTypes]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[orderBookId] [int] NOT NULL,
[orderType] [varchar] (255) NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tbl_OrderBookTypes] on [dbo].[tbl_OrderBookTypes]'
GO
ALTER TABLE [dbo].[tbl_OrderBookTypes] ADD CONSTRAINT [PK_tbl_OrderBookTypes] PRIMARY KEY CLUSTERED ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IDX_OrderBookId] on [dbo].[tbl_OrderBookTypes]'
GO
CREATE NONCLUSTERED INDEX [IDX_OrderBookId] ON [dbo].[tbl_OrderBookTypes] ([orderBookId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IDX_OrderType] on [dbo].[tbl_OrderBookTypes]'
GO
CREATE NONCLUSTERED INDEX [IDX_OrderType] ON [dbo].[tbl_OrderBookTypes] ([orderType])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tbl_OrderBookRelations]'
GO
CREATE TABLE [dbo].[tbl_OrderBookRelations]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[OrderBookId] [int] NOT NULL,
[RelatedOrderBookId] [int] NOT NULL,
[RelatedParentOrderBookId] [int] NULL,
[IsHerited] [bit] NOT NULL,
[RelatedOrderBookLevel] [int] NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tbl_OrderBookRelations] on [dbo].[tbl_OrderBookRelations]'
GO
ALTER TABLE [dbo].[tbl_OrderBookRelations] ADD CONSTRAINT [PK_tbl_OrderBookRelations] PRIMARY KEY CLUSTERED ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IDX_OrderBookId] on [dbo].[tbl_OrderBookRelations]'
GO
CREATE NONCLUSTERED INDEX [IDX_OrderBookId] ON [dbo].[tbl_OrderBookRelations] ([OrderBookId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IDX_RelatedOrderBookId] on [dbo].[tbl_OrderBookRelations]'
GO
CREATE NONCLUSTERED INDEX [IDX_RelatedOrderBookId] ON [dbo].[tbl_OrderBookRelations] ([RelatedOrderBookId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[btbl_AuthorityGroupUsers]'
GO
CREATE TABLE [dbo].[btbl_AuthorityGroupUsers]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[authorityGroupId] [int] NOT NULL,
[orderBookId] [int] NOT NULL,
[userSign] [nvarchar] (50) NOT NULL,
[canRead] [smallint] NOT NULL,
[canWrite] [smallint] NOT NULL,
[canDispatch] [smallint] NOT NULL,
[canTeamDisp] [smallint] NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_btbl_AuthorityGroupUsers] on [dbo].[btbl_AuthorityGroupUsers]'
GO
ALTER TABLE [dbo].[btbl_AuthorityGroupUsers] ADD CONSTRAINT [PK_btbl_AuthorityGroupUsers] PRIMARY KEY CLUSTERED ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IDX_OrderBookId] on [dbo].[btbl_AuthorityGroupUsers]'
GO
CREATE NONCLUSTERED INDEX [IDX_OrderBookId] ON [dbo].[btbl_AuthorityGroupUsers] ([orderBookId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IDX_UserSign] on [dbo].[btbl_AuthorityGroupUsers]'
GO
CREATE NONCLUSTERED INDEX [IDX_UserSign] ON [dbo].[btbl_AuthorityGroupUsers] ([userSign])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[getSplittedTextTable]'
GO
/**
* Description: this function splits varchar(8000) text by given delimiter
* Author: Jeff Moden
* Have a look at:http://www.sqlservercentral.com/articles/Tally+Table/72993/
*/
CREATE FUNCTION [dbo].[getSplittedTextTable]
--===== Define I/O parameters
        (@text VARCHAR(8000), @delimiter CHAR(1))
--WARNING!!! DO NOT USE MAX DATA-TYPES HERE!  IT WILL KILL PERFORMANCE!
RETURNS TABLE WITH SCHEMABINDING AS
 RETURN
--===== "Inline" CTE Driven "Tally Table" produces values from 1 up to 10,000...
     -- enough to cover VARCHAR(8000)
  WITH E1(N) AS (
                 SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL
                 SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL
                 SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1
                ),                          --10E+1 or 10 rows
       E2(N) AS (SELECT 1 FROM E1 a, E1 b), --10E+2 or 100 rows
       E4(N) AS (SELECT 1 FROM E2 a, E2 b), --10E+4 or 10,000 rows max
 cteTally(N) AS (--==== This provides the "base" CTE and limits the number of rows right up front
                     -- for both a performance gain and prevention of accidental "overruns"
                 SELECT TOP (ISNULL(DATALENGTH(@text),0)) ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) FROM E4
                ),
cteStart(N1) AS (--==== This returns N+1 (starting position of each "element" just once for each delimiter)
                 SELECT 1 UNION ALL
                 SELECT t.N+1 FROM cteTally t WHERE SUBSTRING(@text,t.N,1) = @delimiter
                ),
cteLen(N1,L1) AS(--==== Return start and length (for use in substring)
                 SELECT s.N1,
                        ISNULL(NULLIF(CHARINDEX(@delimiter,@text,s.N1),0)-s.N1,8000)
                   FROM cteStart s
                )
--===== Do the actual split. The ISNULL/NULLIF combo handles the length for the final element when no delimiter is found.
 SELECT ItemNumber = ROW_NUMBER() OVER(ORDER BY l.N1),
        Item       = SUBSTRING(@text, l.N1, l.L1)
   FROM cteLen l
;
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[v_Authored_OrderTypesPerUser]'
GO

CREATE VIEW [dbo].[v_Authored_OrderTypesPerUser]
AS
SELECT 
	authoredUsers.userSign
	, relatedOrderTypes.orderType
	, MAX(authoredUsers.canRead) as canRead
	, MAX(authoredUsers.canWrite) as canWrite
	, MAX(authoredUsers.canDispatch) as canDispatch
	, MAX(authoredUsers.canTeamDisp) as canTeamDisp
FROM 
	btbl_AuthorityGroupUsers authoredUsers
	INNER JOIN tbl_orderBookRelations relatedBooks ON authoredUsers.orderBookId=relatedBooks.orderBookId
	INNER JOIN tbl_OrderBookTypes relatedOrderTypes ON relatedBooks.RelatedOrderBookId=relatedOrderTypes.OrderBookId
GROUP BY
	authoredUsers.userSign
	, relatedOrderTypes.orderType	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[getOrderIdsWithOrderNo]'
GO
-- =============================================
-- Author:		Brian Streuli
-- Create date: 18.12.2013
-- Description:	Returns Order Id's having underlying given orderNo
-- =============================================
CREATE FUNCTION getOrderIdsWithOrderNo 
(	
	@OrderNo nvarchar(255)
)
RETURNS TABLE 
AS
RETURN 
(
SELECT DISTINCT
	o.orderId 
FROM    
	tbl_Orders o 
	--ContactSignature 
	LEFT OUTER JOIN dbo.tbl_Contact_SignatureRequest signR ON o.orderId = signR.SignatureRequestOrderId 
		AND signR.signatureOrderNo = @OrderNo 
	--Contacts  
	LEFT OUTER JOIN dbo.tbl_Contacts contact ON o.orderId = contact.orderId  
		--Letters  
		LEFT OUTER JOIN dbo.tbl_Contact_Letters lettr ON contact.contactId = lettr.contactId 
			AND lettr.letterOrderNo = @OrderNo  
		--PendencyTickets  
		LEFT OUTER JOIN dbo.tbl_PendencyTickets ticket ON o.orderId = ticket.orderId 
			AND ticket.CRMIssueOrderNo = @OrderNo  
	--Recordings 
	LEFT OUTER JOIN tbl_Recordings rec ON o.recordingId = rec.recordingId 
		LEFT OUTER JOIN tbl_Recording_Correspondence cor ON rec.correspondence_correspondenceId = cor.correspondenceId 
			--Correspondence has No Order no 
			LEFT OUTER JOIN tbl_Recording_CorrespondenceContainer cont ON cont.correspondenceId = cor.correspondenceId 
				AND 
						( 
								cor.changeAddressOrderNo = @OrderNo 
						OR      cor.newAddressOrderNo    = @OrderNo 
						) 
		LEFT OUTER JOIN dbo.tbl_Recording_DomicileOrOwner doo ON o.recordingId = doo.recordingId 
			AND doo.orderNo = @OrderNo 
		LEFT OUTER JOIN tbl_Request_BusinessPartner bp ON rec.businessPartner_businessPartnerId = bp.businessPartnerId 
			AND bp.bpOrderNo = @OrderNo 
		LEFT OUTER JOIN dbo.tbl_Recording_Affirmation aff ON rec.affirmationId = aff.affirmationId 
			AND aff.orderNo = @OrderNo 
		LEFT OUTER JOIN dbo.tbl_Recording_LSV lsv ON rec.recordingId = lsv.recordingId 
			AND lsv.lsvOrderNo  = @OrderNo 
		--MetaTypes 
		LEFT OUTER JOIN dbo.tbl_Recording_Meta meta  ON  o.recordingId = meta.recordingId 
			AND meta.recordingMetaOrderNo = @OrderNo 
WHERE
	NOT (CASE 
	WHEN signr.signatureOrderNo<>'' THEN signr.signatureOrderNo 
	WHEN cor.changeAddressOrderNo= @OrderNo THEN cor.changeAddressOrderNo 
	WHEN cor.newAddressOrderNo = @OrderNo THEN cor.newAddressOrderNo 
	WHEN cont.containerOrderNo<>'' THEN cont.containerOrderNo 
	WHEN doo.orderNo<>'' THEN doo.orderNo 
	WHEN aff.orderNo<>'' THEN aff.orderNo 
	WHEN bp.bpOrderNo<>'' THEN bp.bpOrderNo 
	WHEN lsv.lsvOrderNo<>'' THEN lsv.lsvOrderNo 
	WHEN meta.recordingMetaOrderNo<>'' THEN meta.recordingMetaOrderNo 
	ELSE NULL END) IS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[tr_btbl_AuthorityGroup_Normalisation] on [dbo].[btbl_AuthorityGroup]'
GO


CREATE TRIGGER [dbo].[tr_btbl_AuthorityGroup_Normalisation]
	   ON  [dbo].[btbl_AuthorityGroup]
	   AFTER INSERT,DELETE,UPDATE
	AS 
	BEGIN

	DECLARE @size int
	DECLARE @increment int
	DECLARE @authorityId int
	DECLARE @orderBookId int
	DECLARE @users varchar(2500)
	DECLARE @canRead smallint
	DECLARE @canWrite smallint
	DECLARE @canDispatch smallint
	DECLARE @canTeamDisp smallint
	DECLARE @authorities TABLE (authorityGroupId int not null, orderBookId int not null, ordinal int identity(1,1), Users varchar(2500), GroupRole varchar(255))
	
	/**
	* Collect the new AuthorityGroups having Users and corresponding groupRole
	*/
	INSERT @authorities(authorityGroupId, orderBookId, Users, GroupRole)
	SELECT 
		auth.id
		, auth.OrderBook_orderBookId
		, Users
		, GroupRole
	FROM 
		inserted auth
	WHERE 
		NOT auth.Users IS NULL
		AND auth.Users<>''
		AND NOT auth.GroupRole IS NULL
		AND auth.GroupRole <>''
	ORDER BY auth.id
	
	/**
	* Delete may existing user mappings
	*/
	DELETE 
	FROM btbl_AuthorityGroupUsers 
	WHERE 
		authorityGroupId IN (SELECT ID FROM inserted)
		OR authorityGroupId IN (SELECT ID FROM deleted)
		;

	SET @size = (SELECT COUNT(*) FROM @authorities);
	SET @increment = 0;

	/**
	* Loop each authority group, split the users list and save
	* user Permission in mapping table btbl_AuthorityGroupUsers
	*/
	WHILE @increment<@size
		BEGIN
			SET @increment=@increment+1
			SELECT 
				@authorityId=authorityGroupId
				, @orderBookId = orderBookId
				, @users = Users
				, @canRead = 1 --all users with a role can Read
				, @canWrite = CASE WHEN GroupRole ='Write' THEN 1 ELSE 0 END			
				, @canDispatch = CASE WHEN GroupRole ='Dispatch' THEN 1 ELSE 0 END			
				, @canTeamDisp = CASE WHEN GroupRole ='TeamDisp' THEN 1 ELSE 0 END			
			FROM 
				@authorities
			WHERE
				ordinal = @increment
			
			INSERT btbl_AuthorityGroupUsers(authorityGroupId, orderBookId, userSign, canRead, canWrite, canDispatch, canTeamDisp)
			SELECT 
				@authorityId
				, @orderBookId
				, RTRIM(LTRIM(Item)) as UserSign
				, @canRead
				, @canWrite
				, @canDispatch
				, @canTeamDisp
			FROM getSplittedTextTable(@users, ',')
		END
	
	END
	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[tr_tbl_OrderBooks_Normalisation] on [dbo].[tbl_OrderBooks]'
GO

CREATE TRIGGER [dbo].[tr_tbl_OrderBooks_Normalisation]
	   ON  [dbo].[tbl_OrderBooks]
	   AFTER INSERT,DELETE,UPDATE
	AS 
	BEGIN
		DECLARE @size int
	DECLARE @increment int
	DECLARE @orderBooks TABLE (orderBookId int not null, ordinal int identity(1,1), orderTypes varchar(2500))
	DECLARE @orderBookId int
	DECLARE @orderTypes varchar(2500)
	/**
	* Collect the OrderBooks having orderTypes 
	*/
	INSERT @orderBooks(orderBookId, orderTypes)
	SELECT 
		orderBooks.orderBookId
		, orderTypes
	FROM 
		inserted orderBooks
	WHERE 
		NOT orderBooks.orderTypes IS NULL
		AND orderBooks.orderTypes<>''
	ORDER BY orderBooks.orderBookId
	/**
	* Delete may existing Mappings
	*/
	DELETE 
	FROM tbl_OrderBookTypes 
	WHERE 
		orderBookId IN (SELECT orderBookId FROM inserted)
		OR orderBookId IN (SELECT orderBookId FROM deleted)
		;
	DELETE 
	FROM tbl_OrderBookRelations 	
	WHERE 
		orderBookId IN (SELECT orderBookId FROM inserted)
		OR orderBookId IN (SELECT orderBookId FROM deleted)
		;

	/**
	* Loop for each OrderBook of INSERTED Table
	* to get the OrderTypes and the Child OrderBooks in a normalized Structure
	*/	
	SET @size = (SELECT COUNT(*) FROM @orderBooks);
	SET @increment = 0;
	WHILE @increment<@size
		BEGIN
			SET @increment=@increment+1
			/**
			* Get the current orderBook values into variables
			*/
			SELECT 
				@orderBookId = orderBookId
				, @orderTypes = LTRIM(RTRIM(orderTypes))
			FROM 
				@orderBooks
			WHERE
				ordinal = @increment
			/**
			* Truncate brackets from orderTypes String
			*/
			IF LEFT(@orderTypes, 1)='[' 
				BEGIN
					SET @orderTypes = RIGHT(@orderTypes, LEN(@orderTypes)-1)
				END
			IF RIGHT(@orderTypes, 1)=']' 
				BEGIN
					SET @orderTypes = LEFT(@orderTypes, LEN(@orderTypes)-1)
				END
			/**
			* Split the comma separated list of orderTypes for normalization
			* of orderBook->OrderType relation
			*/
			INSERT tbl_OrderBookTypes(orderBookId, orderType)
			SELECT 
				@orderBookId
				, RTRIM(LTRIM(Item)) as Item 
			FROM getSplittedTextTable(@orderTypes, ',')
			
			/**
			* Users have rights to all child orderbooks so we get them into the mapping table tbl_OrderBookRelations
			* which resolves that relation for each orderBook
			*/
			;WITH
			  cteOrderBooks (orderBookId, parentOrderBookId, OrderBookName, orderBookLevel, isHerited)
			  AS(
				SELECT 
					orderBookId
					, parentOrderBook_orderBookId as parentOrderBookId
					, name as OrderBookName
					, 1 as orderBookLevel
					, 0 as isHerited
				FROM tbl_OrderBooks
				WHERE orderBookId =@orderBookId
				UNION ALL
				SELECT 
					e.orderBookId
					, e.parentOrderBook_orderBookId as parentOrderBookId
					, e.name as OrderBookName
					, r.orderBookLevel + 1
					, 1 as isHerited
				FROM tbl_OrderBooks e
					INNER JOIN cteOrderBooks r ON e.parentOrderBook_orderBookId = r.orderBookId
			  )
			INSERT dbo.tbl_OrderBookRelations(OrderBookId, RelatedOrderBookId, RelatedParentOrderBookId, IsHerited, RelatedOrderBookLevel)
			SELECT
				@orderBookId as OrderBookId
			  , orderBookId as RelatedOrderBookId
			  , parentOrderBookId as RelatedParentOrderBookId
			  , IsHerited
			  , orderBookLevel as RelatedOrderBookLevel
			FROM cteOrderBooks
			ORDER BY 
				parentOrderBookId

		END
	END
	



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
