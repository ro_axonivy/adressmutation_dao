package ch.soreco.orderbook.view.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
/**
 * Test For {@link SqlQueryBuilder}
 */
public class SqlQueryBuilderTest {
	@Test
	public void testSelectTop() {
		SqlQueryBuilder query = new SqlQueryBuilder();
		query
			.top(200)
			.select(new SqlName("o", "*"))
			.from(new SqlNameAlias("dbo", "tbl_Orders", "o"))
		;
		assertEquals("Test Select Top o.*", "SELECT TOP 200 \n\to.*\nFROM \n\tdbo.tbl_Orders AS o", query.getQuery());
	}
	@Test
	public void testFrom() {
		SqlQueryBuilder query = new SqlQueryBuilder();
		query.from(new SqlNameAlias("dbo", "tbl_Orders", "o"));
		assertEquals("Test From", "SELECT *\nFROM \n\tdbo.tbl_Orders AS o", query.getQuery());
	}
	@Test
	public void testInnerJoin() {
		SqlQueryBuilder query = new SqlQueryBuilder();
		SqlNameAlias auth = new SqlNameAlias("dbo", "v_Authored_OrderTypesPerUser", "auth");
		SqlClause authUserSign = new SqlClause(new SqlName("auth", "userSign"), SqlOperator.EQUAL, "PB");
		SqlClause authOrderType = new SqlClause(new SqlName("auth", "orderType"), SqlOperator.EQUAL, new SqlName("o", "orderType"));
		query
			.from(new SqlNameAlias("dbo", "tbl_Orders", "o"))
			.innerJoin(new SqlNameAlias("dbo", "tbl_Recording_Meta", "c")
						, SqlOperator.AND
						, new SqlClause(new SqlName("o", "recordingId"), SqlOperator.EQUAL, new SqlName("c", "recordingId"))
					)
			.innerJoin(auth, SqlOperator.AND, authUserSign, authOrderType)
		;
		assertEquals("Test innerJoin", "SELECT *\nFROM \n\tdbo.tbl_Orders AS o\n\tINNER JOIN dbo.tbl_Recording_Meta AS c ON o.recordingId = c.recordingId\n\tINNER JOIN dbo.v_Authored_OrderTypesPerUser AS auth ON auth.userSign = 'PB' AND auth.orderType = o.orderType", query.getQuery());
	}
	@Test
	public void testWhere() {
		SqlQueryBuilder query = new SqlQueryBuilder();
		query
			.from(new SqlNameAlias("dbo", "tbl_Orders", "o"))
			.where(new SqlClause(new SqlName("o", "editorUserId"), SqlOperator.EQUAL, "PB"))
			.where(new SqlClause(new SqlName("o", "priority"), SqlOperator.EQUAL, 1))
			.where(new SqlExpression("NOT orderState IS NULL"))
			.whereStringFieldIsEmpty(new SqlName("o", "checkerUserId"));
		;
		assertEquals("Test Where Clauses", "SELECT *\nFROM \n\tdbo.tbl_Orders AS o\nWHERE \n\to.editorUserId = 'PB'\n\tAND o.priority = 1\n\tAND NOT orderState IS NULL\n\tAND (o.checkerUserId IS NULL OR o.checkerUserId ='' )", query.getQuery());
	}
	@Test
	public void testOrderBy() {
		SqlQueryBuilder query = new SqlQueryBuilder();
		query.from(new SqlNameAlias("dbo", "tbl_Orders", "o"))
			.orderBy(new SqlName("o", "orderId"), true);
		assertEquals("Test From", "SELECT *\nFROM \n\tdbo.tbl_Orders AS o\nORDER BY \n\to.orderId ASC", query.getQuery());	
	}
	/**
	 * For Testing NativeQueryBuilder
	 * @param args
	 */
	@Test
	public void testAQuery() {
		SqlQueryBuilder query = new SqlQueryBuilder();
		SqlNameAlias auth = new SqlNameAlias("dbo", "v_Authored_OrderTypesPerUser", "auth");
		SqlClause authUserSign = new SqlClause(new SqlName("auth", "userSign"), SqlOperator.EQUAL, "PB");
		SqlClause authOrderType = new SqlClause(new SqlName("auth", "orderType"), SqlOperator.EQUAL, new SqlName("o", "orderType"));
		query
			.top(200)
			.select(new SqlName("o", "*"))
			.from(new SqlNameAlias("dbo", "tbl_Orders", "o"))
			.innerJoin(auth, SqlOperator.AND, authUserSign, authOrderType)
			.innerJoin(new SqlNameAlias("dbo", "tbl_Recording_Meta", "c")
				, SqlOperator.AND
				, new SqlClause(
						new SqlName("o", "recordingId")
						, SqlOperator.EQUAL
						, new SqlName("c", "recordingId")
					)
			)
			.innerJoin(new SqlNameAlias("dbo", "getOrderIdsWithOrderNo('12')", "orderNoOrders")
					, SqlOperator.AND
					, new SqlClause(new SqlName("o", "orderId"), SqlOperator.EQUAL, new SqlName("orderNoOrders", "orderId"))
			)
			.where(new SqlClause(new SqlName("o", "orderState"), SqlOperator.NOT_EQUAL, ""))
			.where(new SqlClause(new SqlName("o", "BPId"), SqlOperator.EQUAL, "1235467"))
			.where(new SqlClause(new SqlName("o", "checkerUserId"), SqlOperator.EQUAL, "ZROHO"))
			.where(new SqlClause(new SqlName("o", "editorUserId"), SqlOperator.EQUAL, "PB"))
			.where(new SqlClause(new SqlName("c", "recordingMetaComment"), SqlOperator.LIKE, "Hello Comment"))
			.where(new SqlClause(new SqlName("o", "orderId"), SqlOperator.EQUAL, 1647))
			.where(new SqlClause(new SqlName("o", "orderState"), SqlOperator.EQUAL, "Recording"))
			.where(new SqlExpression("(DATEDIFF(dd,o.orderEnd, '2014-01-06')<3 OR (o.orderState<>'Merged' AND o.orderState<>'Aborted'))"))
			.where(new SqlClause(new SqlName("o", "orderType"), SqlOperator.EQUAL, "Disinvestment"))
			.where(new SqlClause(new SqlName("o", "priority"), SqlOperator.EQUAL, 1))
			.where(new SqlClause(new SqlName("o", "lastEdit"), SqlOperator.GREATER_OR_EQUAL, "2014-01-08"))
			.where(new SqlClause(new SqlName("o", "lastEdit"), SqlOperator.LESS_OR_EQUAL, "2014-01-08 23:59:59"))
			.where(new SqlClause(new SqlName("o", "orderStart"), SqlOperator.GREATER_OR_EQUAL, "2012-03-15"))
			.where(new SqlClause(new SqlName("o", "orderStart"), SqlOperator.LESS_OR_EQUAL, "2012-03-15 23:59:59"))
			.orderBy(new SqlName("CASE o.orderQuantityType WHEN 'A' THEN 0 ELSE 3 END"), true)
			.orderBy(new SqlName("o", "orderId"), false)
		;
		StringBuilder expected = new StringBuilder("");
		expected.append("SELECT TOP 200 \n");
		expected.append("	o.*\n");
		expected.append("FROM \n");
		expected.append("	dbo.tbl_Orders AS o\n");
		expected.append("	INNER JOIN dbo.v_Authored_OrderTypesPerUser AS auth ON auth.userSign = 'PB' AND auth.orderType = o.orderType\n");
		expected.append("	INNER JOIN dbo.tbl_Recording_Meta AS c ON o.recordingId = c.recordingId\n");
		expected.append("	INNER JOIN dbo.getOrderIdsWithOrderNo('12') AS orderNoOrders ON o.orderId = orderNoOrders.orderId\n");
		expected.append("WHERE \n");
		expected.append("	o.orderState <> ''\n");
		expected.append("	AND o.BPId = '1235467'\n");
		expected.append("	AND o.checkerUserId = 'ZROHO'\n");
		expected.append("	AND o.editorUserId = 'PB'\n");
		expected.append("	AND c.recordingMetaComment LIKE 'Hello Comment'\n");
		expected.append("	AND o.orderId = 1647\n");
		expected.append("	AND o.orderState = 'Recording'\n");
		expected.append("	AND (DATEDIFF(dd,o.orderEnd, '2014-01-06')<3 OR (o.orderState<>'Merged' AND o.orderState<>'Aborted'))\n");
		expected.append("	AND o.orderType = 'Disinvestment'\n");
		expected.append("	AND o.priority = 1\n");
		expected.append("	AND o.lastEdit >= '2014-01-08'\n");
		expected.append("	AND o.lastEdit <= '2014-01-08 23:59:59'\n");
		expected.append("	AND o.orderStart >= '2012-03-15'\n");
		expected.append("	AND o.orderStart <= '2012-03-15 23:59:59'\n");
		expected.append("ORDER BY \n");
		expected.append("	CASE o.orderQuantityType WHEN 'A' THEN 0 ELSE 3 END ASC\n");
		expected.append("	, o.orderId DESC");

		assertEquals("Test A Query", expected.toString(), query.getQuery());
	}
}
