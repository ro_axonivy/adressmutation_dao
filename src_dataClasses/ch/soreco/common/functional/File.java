package ch.soreco.common.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class File", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class File extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 2891189136191272362L;

  private ch.soreco.common.bo.File filter;

  /**
   * Gets the field filter.
   * @return the value of the field filter; may be null.
   */
  public ch.soreco.common.bo.File getFilter()
  {
    return filter;
  }

  /**
   * Sets the field filter.
   * @param _filter the new value of the field filter.
   */
  public void setFilter(ch.soreco.common.bo.File _filter)
  {
    filter = _filter;
  }

  private ch.soreco.common.bo.File file;

  /**
   * Gets the field file.
   * @return the value of the field file; may be null.
   */
  public ch.soreco.common.bo.File getFile()
  {
    return file;
  }

  /**
   * Sets the field file.
   * @param _file the new value of the field file.
   */
  public void setFile(ch.soreco.common.bo.File _file)
  {
    file = _file;
  }

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private java.lang.String fileContent;

  /**
   * Gets the field fileContent.
   * @return the value of the field fileContent; may be null.
   */
  public java.lang.String getFileContent()
  {
    return fileContent;
  }

  /**
   * Sets the field fileContent.
   * @param _fileContent the new value of the field fileContent.
   */
  public void setFileContent(java.lang.String _fileContent)
  {
    fileContent = _fileContent;
  }

  private java.lang.String fileUrl;

  /**
   * Gets the field fileUrl.
   * @return the value of the field fileUrl; may be null.
   */
  public java.lang.String getFileUrl()
  {
    return fileUrl;
  }

  /**
   * Sets the field fileUrl.
   * @param _fileUrl the new value of the field fileUrl.
   */
  public void setFileUrl(java.lang.String _fileUrl)
  {
    fileUrl = _fileUrl;
  }

  private java.lang.Boolean deleteFromFileSystem;

  /**
   * Gets the field deleteFromFileSystem.
   * @return the value of the field deleteFromFileSystem; may be null.
   */
  public java.lang.Boolean getDeleteFromFileSystem()
  {
    return deleteFromFileSystem;
  }

  /**
   * Sets the field deleteFromFileSystem.
   * @param _deleteFromFileSystem the new value of the field deleteFromFileSystem.
   */
  public void setDeleteFromFileSystem(java.lang.Boolean _deleteFromFileSystem)
  {
    deleteFromFileSystem = _deleteFromFileSystem;
  }

  private java.lang.Boolean keepFile;

  /**
   * Gets the field keepFile.
   * @return the value of the field keepFile; may be null.
   */
  public java.lang.Boolean getKeepFile()
  {
    return keepFile;
  }

  /**
   * Sets the field keepFile.
   * @param _keepFile the new value of the field keepFile.
   */
  public void setKeepFile(java.lang.Boolean _keepFile)
  {
    keepFile = _keepFile;
  }

}
