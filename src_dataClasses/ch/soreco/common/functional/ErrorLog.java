package ch.soreco.common.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class ErrorLog", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class ErrorLog extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 7183063820320714642L;

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private ch.soreco.common.bo.ErrorLog filter;

  /**
   * Gets the field filter.
   * @return the value of the field filter; may be null.
   */
  public ch.soreco.common.bo.ErrorLog getFilter()
  {
    return filter;
  }

  /**
   * Sets the field filter.
   * @param _filter the new value of the field filter.
   */
  public void setFilter(ch.soreco.common.bo.ErrorLog _filter)
  {
    filter = _filter;
  }

  private ch.soreco.common.bo.ErrorLog ErrorLog;

  /**
   * Gets the field ErrorLog.
   * @return the value of the field ErrorLog; may be null.
   */
  public ch.soreco.common.bo.ErrorLog getErrorLog()
  {
    return ErrorLog;
  }

  /**
   * Sets the field ErrorLog.
   * @param _ErrorLog the new value of the field ErrorLog.
   */
  public void setErrorLog(ch.soreco.common.bo.ErrorLog _ErrorLog)
  {
    ErrorLog = _ErrorLog;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.common.bo.ErrorLog> ErrorLogs;

  /**
   * Gets the field ErrorLogs.
   * @return the value of the field ErrorLogs; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.common.bo.ErrorLog> getErrorLogs()
  {
    return ErrorLogs;
  }

  /**
   * Sets the field ErrorLogs.
   * @param _ErrorLogs the new value of the field ErrorLogs.
   */
  public void setErrorLogs(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.common.bo.ErrorLog> _ErrorLogs)
  {
    ErrorLogs = _ErrorLogs;
  }

}
