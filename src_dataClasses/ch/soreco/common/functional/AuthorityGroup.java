package ch.soreco.common.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class AuthorityGroup", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class AuthorityGroup extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 3174674502075423506L;

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private ch.soreco.common.bo.AuthorityGroup filter;

  /**
   * Gets the field filter.
   * @return the value of the field filter; may be null.
   */
  public ch.soreco.common.bo.AuthorityGroup getFilter()
  {
    return filter;
  }

  /**
   * Sets the field filter.
   * @param _filter the new value of the field filter.
   */
  public void setFilter(ch.soreco.common.bo.AuthorityGroup _filter)
  {
    filter = _filter;
  }

  private ch.soreco.common.bo.AuthorityGroup authorityGroup;

  /**
   * Gets the field authorityGroup.
   * @return the value of the field authorityGroup; may be null.
   */
  public ch.soreco.common.bo.AuthorityGroup getAuthorityGroup()
  {
    return authorityGroup;
  }

  /**
   * Sets the field authorityGroup.
   * @param _authorityGroup the new value of the field authorityGroup.
   */
  public void setAuthorityGroup(ch.soreco.common.bo.AuthorityGroup _authorityGroup)
  {
    authorityGroup = _authorityGroup;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.common.bo.AuthorityGroup> authorityGroups;

  /**
   * Gets the field authorityGroups.
   * @return the value of the field authorityGroups; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.common.bo.AuthorityGroup> getAuthorityGroups()
  {
    return authorityGroups;
  }

  /**
   * Sets the field authorityGroups.
   * @param _authorityGroups the new value of the field authorityGroups.
   */
  public void setAuthorityGroups(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.common.bo.AuthorityGroup> _authorityGroups)
  {
    authorityGroups = _authorityGroups;
  }

}
