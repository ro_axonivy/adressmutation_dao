package ch.soreco.common.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Persistence", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class Persistence extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 6856758249591657516L;

  private java.lang.Object object;

  /**
   * Gets the field object.
   * @return the value of the field object; may be null.
   */
  public java.lang.Object getObject()
  {
    return object;
  }

  /**
   * Sets the field object.
   * @param _object the new value of the field object.
   */
  public void setObject(java.lang.Object _object)
  {
    object = _object;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.Object> objectList;

  /**
   * Gets the field objectList.
   * @return the value of the field objectList; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.Object> getObjectList()
  {
    return objectList;
  }

  /**
   * Sets the field objectList.
   * @param _objectList the new value of the field objectList.
   */
  public void setObjectList(ch.ivyteam.ivy.scripting.objects.List<java.lang.Object> _objectList)
  {
    objectList = _objectList;
  }

  private ch.ivyteam.ivy.scripting.objects.CompositeObject filter;

  /**
   * Gets the field filter.
   * @return the value of the field filter; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.CompositeObject getFilter()
  {
    return filter;
  }

  /**
   * Sets the field filter.
   * @param _filter the new value of the field filter.
   */
  public void setFilter(ch.ivyteam.ivy.scripting.objects.CompositeObject _filter)
  {
    filter = _filter;
  }

  private ch.ivyteam.ivy.process.data.persistence.IIvyEntityManager persistenceUnit;

  /**
   * Gets the field persistenceUnit.
   * @return the value of the field persistenceUnit; may be null.
   */
  public ch.ivyteam.ivy.process.data.persistence.IIvyEntityManager getPersistenceUnit()
  {
    return persistenceUnit;
  }

  /**
   * Sets the field persistenceUnit.
   * @param _persistenceUnit the new value of the field persistenceUnit.
   */
  public void setPersistenceUnit(ch.ivyteam.ivy.process.data.persistence.IIvyEntityManager _persistenceUnit)
  {
    persistenceUnit = _persistenceUnit;
  }

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

}
