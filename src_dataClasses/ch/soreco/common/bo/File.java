package ch.soreco.common.bo;

/**
 * Datei, Speicherort aller importierten Dokumente
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class File", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_Files")
public class File extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -7829621482184604440L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer fileId;

  /**
   * Gets the field fileId.
   * @return the value of the field fileId; may be null.
   */
  public java.lang.Integer getFileId()
  {
    return fileId;
  }

  /**
   * Sets the field fileId.
   * @param _fileId the new value of the field fileId.
   */
  public void setFileId(java.lang.Integer _fileId)
  {
    fileId = _fileId;
  }

  /**
   * Deprecated
   */
  @javax.persistence.Column(name="fileGroupId")
  private java.lang.Integer fileGroupId;

  /**
   * Gets the field fileGroupId.
   * @return the value of the field fileGroupId; may be null.
   */
  public java.lang.Integer getFileGroupId()
  {
    return fileGroupId;
  }

  /**
   * Sets the field fileGroupId.
   * @param _fileGroupId the new value of the field fileGroupId.
   */
  public void setFileGroupId(java.lang.Integer _fileGroupId)
  {
    fileGroupId = _fileGroupId;
  }

  /**
   * Dateiname
   */
  private java.lang.String fileName;

  /**
   * Gets the field fileName.
   * @return the value of the field fileName; may be null.
   */
  public java.lang.String getFileName()
  {
    return fileName;
  }

  /**
   * Sets the field fileName.
   * @param _fileName the new value of the field fileName.
   */
  public void setFileName(java.lang.String _fileName)
  {
    fileName = _fileName;
  }

  /**
   * Dateiendung
   */
  private java.lang.String fileExtension;

  /**
   * Gets the field fileExtension.
   * @return the value of the field fileExtension; may be null.
   */
  public java.lang.String getFileExtension()
  {
    return fileExtension;
  }

  /**
   * Sets the field fileExtension.
   * @param _fileExtension the new value of the field fileExtension.
   */
  public void setFileExtension(java.lang.String _fileExtension)
  {
    fileExtension = _fileExtension;
  }

  /**
   * Dateityp
   */
  private java.lang.String contentType;

  /**
   * Gets the field contentType.
   * @return the value of the field contentType; may be null.
   */
  public java.lang.String getContentType()
  {
    return contentType;
  }

  /**
   * Sets the field contentType.
   * @param _contentType the new value of the field contentType.
   */
  public void setContentType(java.lang.String _contentType)
  {
    contentType = _contentType;
  }

  /**
   * Gr�sse
   */
  private java.lang.Integer fileSize;

  /**
   * Gets the field fileSize.
   * @return the value of the field fileSize; may be null.
   */
  public java.lang.Integer getFileSize()
  {
    return fileSize;
  }

  /**
   * Sets the field fileSize.
   * @param _fileSize the new value of the field fileSize.
   */
  public void setFileSize(java.lang.Integer _fileSize)
  {
    fileSize = _fileSize;
  }

  private transient java.io.File file;

  /**
   * Gets the field file.
   * @return the value of the field file; may be null.
   */
  public java.io.File getFile()
  {
    return file;
  }

  /**
   * Sets the field file.
   * @param _file the new value of the field file.
   */
  public void setFile(java.io.File _file)
  {
    file = _file;
  }

  /**
   * Datei Inhalt
   */
  @javax.persistence.Column(length=8000)
  private java.lang.String fileContent;

  /**
   * Gets the field fileContent.
   * @return the value of the field fileContent; may be null.
   */
  public java.lang.String getFileContent()
  {
    return fileContent;
  }

  /**
   * Sets the field fileContent.
   * @param _fileContent the new value of the field fileContent.
   */
  public void setFileContent(java.lang.String _fileContent)
  {
    fileContent = _fileContent;
  }

  /**
   * Name der Herkunftsapplikation
   */
  private java.lang.String webAppName;

  /**
   * Gets the field webAppName.
   * @return the value of the field webAppName; may be null.
   */
  public java.lang.String getWebAppName()
  {
    return webAppName;
  }

  /**
   * Sets the field webAppName.
   * @param _webAppName the new value of the field webAppName.
   */
  public void setWebAppName(java.lang.String _webAppName)
  {
    webAppName = _webAppName;
  }

  /**
   * Prozessmodelname
   */
  private java.lang.String processModelName;

  /**
   * Gets the field processModelName.
   * @return the value of the field processModelName; may be null.
   */
  public java.lang.String getProcessModelName()
  {
    return processModelName;
  }

  /**
   * Sets the field processModelName.
   * @param _processModelName the new value of the field processModelName.
   */
  public void setProcessModelName(java.lang.String _processModelName)
  {
    processModelName = _processModelName;
  }

  /**
   * Prozessmodelversion
   */
  private java.lang.String processModelVersion;

  /**
   * Gets the field processModelVersion.
   * @return the value of the field processModelVersion; may be null.
   */
  public java.lang.String getProcessModelVersion()
  {
    return processModelVersion;
  }

  /**
   * Sets the field processModelVersion.
   * @param _processModelVersion the new value of the field processModelVersion.
   */
  public void setProcessModelVersion(java.lang.String _processModelVersion)
  {
    processModelVersion = _processModelVersion;
  }

  /**
   * Servername
   */
  private java.lang.String hostName;

  /**
   * Gets the field hostName.
   * @return the value of the field hostName; may be null.
   */
  public java.lang.String getHostName()
  {
    return hostName;
  }

  /**
   * Sets the field hostName.
   * @param _hostName the new value of the field hostName.
   */
  public void setHostName(java.lang.String _hostName)
  {
    hostName = _hostName;
  }

  /**
   * Ersteller UserId
   */
  private java.lang.String createUserId;

  /**
   * Gets the field createUserId.
   * @return the value of the field createUserId; may be null.
   */
  public java.lang.String getCreateUserId()
  {
    return createUserId;
  }

  /**
   * Sets the field createUserId.
   * @param _createUserId the new value of the field createUserId.
   */
  public void setCreateUserId(java.lang.String _createUserId)
  {
    createUserId = _createUserId;
  }

  /**
   * Ersteller Name
   */
  private java.lang.String createUserName;

  /**
   * Gets the field createUserName.
   * @return the value of the field createUserName; may be null.
   */
  public java.lang.String getCreateUserName()
  {
    return createUserName;
  }

  /**
   * Sets the field createUserName.
   * @param _createUserName the new value of the field createUserName.
   */
  public void setCreateUserName(java.lang.String _createUserName)
  {
    createUserName = _createUserName;
  }

  /**
   * Erstelldatum
   */
  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateTimeUserType")
  private ch.ivyteam.ivy.scripting.objects.DateTime createDate;

  /**
   * Gets the field createDate.
   * @return the value of the field createDate; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.DateTime getCreateDate()
  {
    return createDate;
  }

  /**
   * Sets the field createDate.
   * @param _createDate the new value of the field createDate.
   */
  public void setCreateDate(ch.ivyteam.ivy.scripting.objects.DateTime _createDate)
  {
    createDate = _createDate;
  }

  /**
   * Gel�scht Flag
   */
  private java.lang.Boolean isDeleted;

  /**
   * Gets the field isDeleted.
   * @return the value of the field isDeleted; may be null.
   */
  public java.lang.Boolean getIsDeleted()
  {
    return isDeleted;
  }

  /**
   * Sets the field isDeleted.
   * @param _isDeleted the new value of the field isDeleted.
   */
  public void setIsDeleted(java.lang.Boolean _isDeleted)
  {
    isDeleted = _isDeleted;
  }

  private transient ch.ivyteam.ivy.scripting.objects.File fileIvy;

  /**
   * Gets the field fileIvy.
   * @return the value of the field fileIvy; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.File getFileIvy()
  {
    return fileIvy;
  }

  /**
   * Sets the field fileIvy.
   * @param _fileIvy the new value of the field fileIvy.
   */
  public void setFileIvy(ch.ivyteam.ivy.scripting.objects.File _fileIvy)
  {
    fileIvy = _fileIvy;
  }

  /**
   * Blob File
   */
  private transient ch.soreco.webbies.common.db.BlobFile blobFile;

  /**
   * Gets the field blobFile.
   * @return the value of the field blobFile; may be null.
   */
  public ch.soreco.webbies.common.db.BlobFile getBlobFile()
  {
    return blobFile;
  }

  /**
   * Sets the field blobFile.
   * @param _blobFile the new value of the field blobFile.
   */
  public void setBlobFile(ch.soreco.webbies.common.db.BlobFile _blobFile)
  {
    blobFile = _blobFile;
  }

}
