package ch.soreco.common.bo;

/**
 * Deprecated
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class ErrorLog", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_ErrorLog")
public class ErrorLog extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 8320417401940136465L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer id;

  /**
   * Gets the field id.
   * @return the value of the field id; may be null.
   */
  public java.lang.Integer getId()
  {
    return id;
  }

  /**
   * Sets the field id.
   * @param _id the new value of the field id.
   */
  public void setId(java.lang.Integer _id)
  {
    id = _id;
  }

  private java.lang.Integer ExtId;

  /**
   * Gets the field ExtId.
   * @return the value of the field ExtId; may be null.
   */
  public java.lang.Integer getExtId()
  {
    return ExtId;
  }

  /**
   * Sets the field ExtId.
   * @param _ExtId the new value of the field ExtId.
   */
  public void setExtId(java.lang.Integer _ExtId)
  {
    ExtId = _ExtId;
  }

  private java.lang.String LogType;

  /**
   * Gets the field LogType.
   * @return the value of the field LogType; may be null.
   */
  public java.lang.String getLogType()
  {
    return LogType;
  }

  /**
   * Sets the field LogType.
   * @param _LogType the new value of the field LogType.
   */
  public void setLogType(java.lang.String _LogType)
  {
    LogType = _LogType;
  }

  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateTimeUserType")
  private ch.ivyteam.ivy.scripting.objects.DateTime LogTime;

  /**
   * Gets the field LogTime.
   * @return the value of the field LogTime; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.DateTime getLogTime()
  {
    return LogTime;
  }

  /**
   * Sets the field LogTime.
   * @param _LogTime the new value of the field LogTime.
   */
  public void setLogTime(ch.ivyteam.ivy.scripting.objects.DateTime _LogTime)
  {
    LogTime = _LogTime;
  }

  private java.lang.String LogObject;

  /**
   * Gets the field LogObject.
   * @return the value of the field LogObject; may be null.
   */
  public java.lang.String getLogObject()
  {
    return LogObject;
  }

  /**
   * Sets the field LogObject.
   * @param _LogObject the new value of the field LogObject.
   */
  public void setLogObject(java.lang.String _LogObject)
  {
    LogObject = _LogObject;
  }

  private java.lang.String ErrorMassage;

  /**
   * Gets the field ErrorMassage.
   * @return the value of the field ErrorMassage; may be null.
   */
  public java.lang.String getErrorMassage()
  {
    return ErrorMassage;
  }

  /**
   * Sets the field ErrorMassage.
   * @param _ErrorMassage the new value of the field ErrorMassage.
   */
  public void setErrorMassage(java.lang.String _ErrorMassage)
  {
    ErrorMassage = _ErrorMassage;
  }

  private java.lang.String ErrorDetail;

  /**
   * Gets the field ErrorDetail.
   * @return the value of the field ErrorDetail; may be null.
   */
  public java.lang.String getErrorDetail()
  {
    return ErrorDetail;
  }

  /**
   * Sets the field ErrorDetail.
   * @param _ErrorDetail the new value of the field ErrorDetail.
   */
  public void setErrorDetail(java.lang.String _ErrorDetail)
  {
    ErrorDetail = _ErrorDetail;
  }

  private java.lang.Integer LogLevel;

  /**
   * Gets the field LogLevel.
   * @return the value of the field LogLevel; may be null.
   */
  public java.lang.Integer getLogLevel()
  {
    return LogLevel;
  }

  /**
   * Sets the field LogLevel.
   * @param _LogLevel the new value of the field LogLevel.
   */
  public void setLogLevel(java.lang.Integer _LogLevel)
  {
    LogLevel = _LogLevel;
  }

}
