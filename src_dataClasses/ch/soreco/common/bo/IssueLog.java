package ch.soreco.common.bo;

/**
 * Deprecated
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class IssueLog", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_IssueLogs")
public class IssueLog extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -6966436830607383483L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer id;

  /**
   * Gets the field id.
   * @return the value of the field id; may be null.
   */
  public java.lang.Integer getId()
  {
    return id;
  }

  /**
   * Sets the field id.
   * @param _id the new value of the field id.
   */
  public void setId(java.lang.Integer _id)
  {
    id = _id;
  }

  private java.lang.String screen;

  /**
   * Gets the field screen.
   * @return the value of the field screen; may be null.
   */
  public java.lang.String getScreen()
  {
    return screen;
  }

  /**
   * Sets the field screen.
   * @param _screen the new value of the field screen.
   */
  public void setScreen(java.lang.String _screen)
  {
    screen = _screen;
  }

  @javax.persistence.Column(name="issueRole")
  private java.lang.String issueRole;

  /**
   * Gets the field issueRole.
   * @return the value of the field issueRole; may be null.
   */
  public java.lang.String getIssueRole()
  {
    return issueRole;
  }

  /**
   * Sets the field issueRole.
   * @param _issueRole the new value of the field issueRole.
   */
  public void setIssueRole(java.lang.String _issueRole)
  {
    issueRole = _issueRole;
  }

  @javax.persistence.Column(name="issuePriority", length=1)
  private java.lang.String issuePriority;

  /**
   * Gets the field issuePriority.
   * @return the value of the field issuePriority; may be null.
   */
  public java.lang.String getIssuePriority()
  {
    return issuePriority;
  }

  /**
   * Sets the field issuePriority.
   * @param _issuePriority the new value of the field issuePriority.
   */
  public void setIssuePriority(java.lang.String _issuePriority)
  {
    issuePriority = _issuePriority;
  }

  @javax.persistence.Column(length=10)
  private java.lang.String release;

  /**
   * Gets the field release.
   * @return the value of the field release; may be null.
   */
  public java.lang.String getRelease()
  {
    return release;
  }

  /**
   * Sets the field release.
   * @param _release the new value of the field release.
   */
  public void setRelease(java.lang.String _release)
  {
    release = _release;
  }

  private java.lang.String issueState;

  /**
   * Gets the field issueState.
   * @return the value of the field issueState; may be null.
   */
  public java.lang.String getIssueState()
  {
    return issueState;
  }

  /**
   * Sets the field issueState.
   * @param _issueState the new value of the field issueState.
   */
  public void setIssueState(java.lang.String _issueState)
  {
    issueState = _issueState;
  }

  @javax.persistence.Column(length=800)
  private java.lang.String description;

  /**
   * Gets the field description.
   * @return the value of the field description; may be null.
   */
  public java.lang.String getDescription()
  {
    return description;
  }

  /**
   * Sets the field description.
   * @param _description the new value of the field description.
   */
  public void setDescription(java.lang.String _description)
  {
    description = _description;
  }

  @javax.persistence.Column(length=800)
  private java.lang.String expectedResult;

  /**
   * Gets the field expectedResult.
   * @return the value of the field expectedResult; may be null.
   */
  public java.lang.String getExpectedResult()
  {
    return expectedResult;
  }

  /**
   * Sets the field expectedResult.
   * @param _expectedResult the new value of the field expectedResult.
   */
  public void setExpectedResult(java.lang.String _expectedResult)
  {
    expectedResult = _expectedResult;
  }

  @javax.persistence.Column(length=800)
  private java.lang.String conceptReference;

  /**
   * Gets the field conceptReference.
   * @return the value of the field conceptReference; may be null.
   */
  public java.lang.String getConceptReference()
  {
    return conceptReference;
  }

  /**
   * Sets the field conceptReference.
   * @param _conceptReference the new value of the field conceptReference.
   */
  public void setConceptReference(java.lang.String _conceptReference)
  {
    conceptReference = _conceptReference;
  }

  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateUserType")
  private ch.ivyteam.ivy.scripting.objects.Date issueDate;

  /**
   * Gets the field issueDate.
   * @return the value of the field issueDate; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Date getIssueDate()
  {
    return issueDate;
  }

  /**
   * Sets the field issueDate.
   * @param _issueDate the new value of the field issueDate.
   */
  public void setIssueDate(ch.ivyteam.ivy.scripting.objects.Date _issueDate)
  {
    issueDate = _issueDate;
  }

  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateUserType")
  private ch.ivyteam.ivy.scripting.objects.Date issueSolvedDate;

  /**
   * Gets the field issueSolvedDate.
   * @return the value of the field issueSolvedDate; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Date getIssueSolvedDate()
  {
    return issueSolvedDate;
  }

  /**
   * Sets the field issueSolvedDate.
   * @param _issueSolvedDate the new value of the field issueSolvedDate.
   */
  public void setIssueSolvedDate(ch.ivyteam.ivy.scripting.objects.Date _issueSolvedDate)
  {
    issueSolvedDate = _issueSolvedDate;
  }

  private java.lang.String issuedBy;

  /**
   * Gets the field issuedBy.
   * @return the value of the field issuedBy; may be null.
   */
  public java.lang.String getIssuedBy()
  {
    return issuedBy;
  }

  /**
   * Sets the field issuedBy.
   * @param _issuedBy the new value of the field issuedBy.
   */
  public void setIssuedBy(java.lang.String _issuedBy)
  {
    issuedBy = _issuedBy;
  }

  @javax.persistence.Column(length=800)
  private java.lang.String providerComment;

  /**
   * Gets the field providerComment.
   * @return the value of the field providerComment; may be null.
   */
  public java.lang.String getProviderComment()
  {
    return providerComment;
  }

  /**
   * Sets the field providerComment.
   * @param _providerComment the new value of the field providerComment.
   */
  public void setProviderComment(java.lang.String _providerComment)
  {
    providerComment = _providerComment;
  }

  private java.lang.String providerState;

  /**
   * Gets the field providerState.
   * @return the value of the field providerState; may be null.
   */
  public java.lang.String getProviderState()
  {
    return providerState;
  }

  /**
   * Sets the field providerState.
   * @param _providerState the new value of the field providerState.
   */
  public void setProviderState(java.lang.String _providerState)
  {
    providerState = _providerState;
  }

}
