package ch.soreco.common.bo;

/**
 * Berechtigungsgruppe
 * Diese Entit�t steuert in Verbindung mit dem OrderBook / Auftragsbuch welche User wie (GroupRole) auf ein Auftragsbuch berechtigt sind.
 * Wird unter Administration / Konfiguration parametrisiert. 
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class AuthorityGroup", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="btbl_AuthorityGroup")
public class AuthorityGroup extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 4100849704661520283L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer id;

  /**
   * Gets the field id.
   * @return the value of the field id; may be null.
   */
  public java.lang.Integer getId()
  {
    return id;
  }

  /**
   * Sets the field id.
   * @param _id the new value of the field id.
   */
  public void setId(java.lang.Integer _id)
  {
    id = _id;
  }

  /**
   * Name der Gruppe, Freitext
   */
  private java.lang.String GroupName;

  /**
   * Gets the field GroupName.
   * @return the value of the field GroupName; may be null.
   */
  public java.lang.String getGroupName()
  {
    return GroupName;
  }

  /**
   * Sets the field GroupName.
   * @param _GroupName the new value of the field GroupName.
   */
  public void setGroupName(java.lang.String _GroupName)
  {
    GroupName = _GroupName;
  }

  /**
   * Beschreibung, Freitext
   */
  @javax.persistence.Column(length=1500)
  private java.lang.String Description;

  /**
   * Gets the field Description.
   * @return the value of the field Description; may be null.
   */
  public java.lang.String getDescription()
  {
    return Description;
  }

  /**
   * Sets the field Description.
   * @param _Description the new value of the field Description.
   */
  public void setDescription(java.lang.String _Description)
  {
    Description = _Description;
  }

  /**
   * Verlinkung zum Auftragsbuch
   */
  @javax.persistence.ManyToOne(cascade={javax.persistence.CascadeType.PERSIST, javax.persistence.CascadeType.MERGE}, fetch=javax.persistence.FetchType.EAGER)
  private ch.soreco.orderbook.bo.OrderBook OrderBook;

  /**
   * Gets the field OrderBook.
   * @return the value of the field OrderBook; may be null.
   */
  public ch.soreco.orderbook.bo.OrderBook getOrderBook()
  {
    return OrderBook;
  }

  /**
   * Sets the field OrderBook.
   * @param _OrderBook the new value of the field OrderBook.
   */
  public void setOrderBook(ch.soreco.orderbook.bo.OrderBook _OrderBook)
  {
    OrderBook = _OrderBook;
  }

  /**
   * Rolle: Read (nicht mehr ben�tigt), Write, Disp, TeamDisp 
   */
  private java.lang.String GroupRole;

  /**
   * Gets the field GroupRole.
   * @return the value of the field GroupRole; may be null.
   */
  public java.lang.String getGroupRole()
  {
    return GroupRole;
  }

  /**
   * Sets the field GroupRole.
   * @param _GroupRole the new value of the field GroupRole.
   */
  public void setGroupRole(java.lang.String _GroupRole)
  {
    GroupRole = _GroupRole;
  }

  private transient java.util.List<ch.ivyteam.ivy.security.IUser> UserList;

  /**
   * Gets the field UserList.
   * @return the value of the field UserList; may be null.
   */
  public java.util.List<ch.ivyteam.ivy.security.IUser> getUserList()
  {
    return UserList;
  }

  /**
   * Sets the field UserList.
   * @param _UserList the new value of the field UserList.
   */
  public void setUserList(java.util.List<ch.ivyteam.ivy.security.IUser> _UserList)
  {
    UserList = _UserList;
  }

  /**
   * Liste aller Benutzer, K�rzel, Kommesepariert
   */
  @javax.persistence.Column(length=2500)
  private java.lang.String Users;

  /**
   * Gets the field Users.
   * @return the value of the field Users; may be null.
   */
  public java.lang.String getUsers()
  {
    return Users;
  }

  /**
   * Sets the field Users.
   * @param _Users the new value of the field Users.
   */
  public void setUsers(java.lang.String _Users)
  {
    Users = _Users;
  }

  private transient ch.ivyteam.ivy.security.IUser User;

  /**
   * Gets the field User.
   * @return the value of the field User; may be null.
   */
  public ch.ivyteam.ivy.security.IUser getUser()
  {
    return User;
  }

  /**
   * Sets the field User.
   * @param _User the new value of the field User.
   */
  public void setUser(ch.ivyteam.ivy.security.IUser _User)
  {
    User = _User;
  }

}
