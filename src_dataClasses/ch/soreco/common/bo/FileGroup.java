package ch.soreco.common.bo;

/**
 * Deprecated
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class FileGroup", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_FileGroups")
public class FileGroup extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 8922934895205298643L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer fileGroupId;

  /**
   * Gets the field fileGroupId.
   * @return the value of the field fileGroupId; may be null.
   */
  public java.lang.Integer getFileGroupId()
  {
    return fileGroupId;
  }

  /**
   * Sets the field fileGroupId.
   * @param _fileGroupId the new value of the field fileGroupId.
   */
  public void setFileGroupId(java.lang.Integer _fileGroupId)
  {
    fileGroupId = _fileGroupId;
  }

  private java.lang.String fileGroupName;

  /**
   * Gets the field fileGroupName.
   * @return the value of the field fileGroupName; may be null.
   */
  public java.lang.String getFileGroupName()
  {
    return fileGroupName;
  }

  /**
   * Sets the field fileGroupName.
   * @param _fileGroupName the new value of the field fileGroupName.
   */
  public void setFileGroupName(java.lang.String _fileGroupName)
  {
    fileGroupName = _fileGroupName;
  }

  private java.lang.String fileGroupDescription;

  /**
   * Gets the field fileGroupDescription.
   * @return the value of the field fileGroupDescription; may be null.
   */
  public java.lang.String getFileGroupDescription()
  {
    return fileGroupDescription;
  }

  /**
   * Sets the field fileGroupDescription.
   * @param _fileGroupDescription the new value of the field fileGroupDescription.
   */
  public void setFileGroupDescription(java.lang.String _fileGroupDescription)
  {
    fileGroupDescription = _fileGroupDescription;
  }

  private java.lang.String security;

  /**
   * Gets the field security.
   * @return the value of the field security; may be null.
   */
  public java.lang.String getSecurity()
  {
    return security;
  }

  /**
   * Sets the field security.
   * @param _security the new value of the field security.
   */
  public void setSecurity(java.lang.String _security)
  {
    security = _security;
  }

  private java.lang.String webAppName;

  /**
   * Gets the field webAppName.
   * @return the value of the field webAppName; may be null.
   */
  public java.lang.String getWebAppName()
  {
    return webAppName;
  }

  /**
   * Sets the field webAppName.
   * @param _webAppName the new value of the field webAppName.
   */
  public void setWebAppName(java.lang.String _webAppName)
  {
    webAppName = _webAppName;
  }

  private java.lang.String processModelName;

  /**
   * Gets the field processModelName.
   * @return the value of the field processModelName; may be null.
   */
  public java.lang.String getProcessModelName()
  {
    return processModelName;
  }

  /**
   * Sets the field processModelName.
   * @param _processModelName the new value of the field processModelName.
   */
  public void setProcessModelName(java.lang.String _processModelName)
  {
    processModelName = _processModelName;
  }

  private java.lang.String processModelVersion;

  /**
   * Gets the field processModelVersion.
   * @return the value of the field processModelVersion; may be null.
   */
  public java.lang.String getProcessModelVersion()
  {
    return processModelVersion;
  }

  /**
   * Sets the field processModelVersion.
   * @param _processModelVersion the new value of the field processModelVersion.
   */
  public void setProcessModelVersion(java.lang.String _processModelVersion)
  {
    processModelVersion = _processModelVersion;
  }

  private java.lang.String hostName;

  /**
   * Gets the field hostName.
   * @return the value of the field hostName; may be null.
   */
  public java.lang.String getHostName()
  {
    return hostName;
  }

  /**
   * Sets the field hostName.
   * @param _hostName the new value of the field hostName.
   */
  public void setHostName(java.lang.String _hostName)
  {
    hostName = _hostName;
  }

  private java.lang.String createUserId;

  /**
   * Gets the field createUserId.
   * @return the value of the field createUserId; may be null.
   */
  public java.lang.String getCreateUserId()
  {
    return createUserId;
  }

  /**
   * Sets the field createUserId.
   * @param _createUserId the new value of the field createUserId.
   */
  public void setCreateUserId(java.lang.String _createUserId)
  {
    createUserId = _createUserId;
  }

  private java.lang.String createUserName;

  /**
   * Gets the field createUserName.
   * @return the value of the field createUserName; may be null.
   */
  public java.lang.String getCreateUserName()
  {
    return createUserName;
  }

  /**
   * Sets the field createUserName.
   * @param _createUserName the new value of the field createUserName.
   */
  public void setCreateUserName(java.lang.String _createUserName)
  {
    createUserName = _createUserName;
  }

  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateTimeUserType")
  private ch.ivyteam.ivy.scripting.objects.DateTime createDate;

  /**
   * Gets the field createDate.
   * @return the value of the field createDate; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.DateTime getCreateDate()
  {
    return createDate;
  }

  /**
   * Sets the field createDate.
   * @param _createDate the new value of the field createDate.
   */
  public void setCreateDate(ch.ivyteam.ivy.scripting.objects.DateTime _createDate)
  {
    createDate = _createDate;
  }

  private java.lang.String modifyUserId;

  /**
   * Gets the field modifyUserId.
   * @return the value of the field modifyUserId; may be null.
   */
  public java.lang.String getModifyUserId()
  {
    return modifyUserId;
  }

  /**
   * Sets the field modifyUserId.
   * @param _modifyUserId the new value of the field modifyUserId.
   */
  public void setModifyUserId(java.lang.String _modifyUserId)
  {
    modifyUserId = _modifyUserId;
  }

  private java.lang.String modifyUserName;

  /**
   * Gets the field modifyUserName.
   * @return the value of the field modifyUserName; may be null.
   */
  public java.lang.String getModifyUserName()
  {
    return modifyUserName;
  }

  /**
   * Sets the field modifyUserName.
   * @param _modifyUserName the new value of the field modifyUserName.
   */
  public void setModifyUserName(java.lang.String _modifyUserName)
  {
    modifyUserName = _modifyUserName;
  }

  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateTimeUserType")
  private ch.ivyteam.ivy.scripting.objects.DateTime modifyDate;

  /**
   * Gets the field modifyDate.
   * @return the value of the field modifyDate; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.DateTime getModifyDate()
  {
    return modifyDate;
  }

  /**
   * Sets the field modifyDate.
   * @param _modifyDate the new value of the field modifyDate.
   */
  public void setModifyDate(ch.ivyteam.ivy.scripting.objects.DateTime _modifyDate)
  {
    modifyDate = _modifyDate;
  }

}
