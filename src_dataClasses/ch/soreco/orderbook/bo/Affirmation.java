package ch.soreco.orderbook.bo;

/**
 * Adressmutation Best�tigung
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Affirmation", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_Recording_Affirmation")
public class Affirmation extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -2728414138274681381L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer affirmationId;

  /**
   * Gets the field affirmationId.
   * @return the value of the field affirmationId; may be null.
   */
  public java.lang.Integer getAffirmationId()
  {
    return affirmationId;
  }

  /**
   * Sets the field affirmationId.
   * @param _affirmationId the new value of the field affirmationId.
   */
  public void setAffirmationId(java.lang.Integer _affirmationId)
  {
    affirmationId = _affirmationId;
  }

  /**
   * Adressmutation best�tigt
   */
  private java.lang.Boolean isAffirmed;

  /**
   * Gets the field isAffirmed.
   * @return the value of the field isAffirmed; may be null.
   */
  public java.lang.Boolean getIsAffirmed()
  {
    return isAffirmed;
  }

  /**
   * Sets the field isAffirmed.
   * @param _isAffirmed the new value of the field isAffirmed.
   */
  public void setIsAffirmed(java.lang.Boolean _isAffirmed)
  {
    isAffirmed = _isAffirmed;
  }

  /**
   * Best�tigung gepr�ft
   */
  private java.lang.Boolean affirmationIsChecked;

  /**
   * Gets the field affirmationIsChecked.
   * @return the value of the field affirmationIsChecked; may be null.
   */
  public java.lang.Boolean getAffirmationIsChecked()
  {
    return affirmationIsChecked;
  }

  /**
   * Sets the field affirmationIsChecked.
   * @param _affirmationIsChecked the new value of the field affirmationIsChecked.
   */
  public void setAffirmationIsChecked(java.lang.Boolean _affirmationIsChecked)
  {
    affirmationIsChecked = _affirmationIsChecked;
  }

  /**
   * Order#
   */
  private java.lang.String orderNo;

  /**
   * Gets the field orderNo.
   * @return the value of the field orderNo; may be null.
   */
  public java.lang.String getOrderNo()
  {
    return orderNo;
  }

  /**
   * Sets the field orderNo.
   * @param _orderNo the new value of the field orderNo.
   */
  public void setOrderNo(java.lang.String _orderNo)
  {
    orderNo = _orderNo;
  }

  /**
   * Pr�fer UserId
   */
  private java.lang.String affirmationCheckedBy;

  /**
   * Gets the field affirmationCheckedBy.
   * @return the value of the field affirmationCheckedBy; may be null.
   */
  public java.lang.String getAffirmationCheckedBy()
  {
    return affirmationCheckedBy;
  }

  /**
   * Sets the field affirmationCheckedBy.
   * @param _affirmationCheckedBy the new value of the field affirmationCheckedBy.
   */
  public void setAffirmationCheckedBy(java.lang.String _affirmationCheckedBy)
  {
    affirmationCheckedBy = _affirmationCheckedBy;
  }

}
