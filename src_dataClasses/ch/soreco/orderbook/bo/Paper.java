package ch.soreco.orderbook.bo;

/**
 * Unterlagen
 * 1:n Beziehung zu Contact / Kontakt
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Paper", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_Papers")
public class Paper extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -7578688224711460454L;

  @javax.persistence.Column(name="orderId")
  private java.lang.Integer paperOrderId;

  /**
   * Gets the field paperOrderId.
   * @return the value of the field paperOrderId; may be null.
   */
  public java.lang.Integer getPaperOrderId()
  {
    return paperOrderId;
  }

  /**
   * Sets the field paperOrderId.
   * @param _paperOrderId the new value of the field paperOrderId.
   */
  public void setPaperOrderId(java.lang.Integer _paperOrderId)
  {
    paperOrderId = _paperOrderId;
  }

  /**
   * Id von Contact / Kontakt
   */
  @javax.persistence.Column(name="contactId")
  private java.lang.Integer contactId;

  /**
   * Gets the field contactId.
   * @return the value of the field contactId; may be null.
   */
  public java.lang.Integer getContactId()
  {
    return contactId;
  }

  /**
   * Sets the field contactId.
   * @param _contactId the new value of the field contactId.
   */
  public void setContactId(java.lang.Integer _contactId)
  {
    contactId = _contactId;
  }

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer paperId;

  /**
   * Gets the field paperId.
   * @return the value of the field paperId; may be null.
   */
  public java.lang.Integer getPaperId()
  {
    return paperId;
  }

  /**
   * Sets the field paperId.
   * @param _paperId the new value of the field paperId.
   */
  public void setPaperId(java.lang.Integer _paperId)
  {
    paperId = _paperId;
  }

  /**
   * Brief erstellt am
   */
  private java.lang.Boolean isPaper;

  /**
   * Gets the field isPaper.
   * @return the value of the field isPaper; may be null.
   */
  public java.lang.Boolean getIsPaper()
  {
    return isPaper;
  }

  /**
   * Sets the field isPaper.
   * @param _isPaper the new value of the field isPaper.
   */
  public void setIsPaper(java.lang.Boolean _isPaper)
  {
    isPaper = _isPaper;
  }

  /**
   * Unterlagen angefordert am
   */
  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateUserType")
  private ch.ivyteam.ivy.scripting.objects.Date paperRequestDate;

  /**
   * Gets the field paperRequestDate.
   * @return the value of the field paperRequestDate; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Date getPaperRequestDate()
  {
    return paperRequestDate;
  }

  /**
   * Sets the field paperRequestDate.
   * @param _paperRequestDate the new value of the field paperRequestDate.
   */
  public void setPaperRequestDate(ch.ivyteam.ivy.scripting.objects.Date _paperRequestDate)
  {
    paperRequestDate = _paperRequestDate;
  }

  /**
   * Brief Unterschrift angefordert
   */
  private java.lang.String paperType;

  /**
   * Gets the field paperType.
   * @return the value of the field paperType; may be null.
   */
  public java.lang.String getPaperType()
  {
    return paperType;
  }

  /**
   * Sets the field paperType.
   * @param _paperType the new value of the field paperType.
   */
  public void setPaperType(java.lang.String _paperType)
  {
    paperType = _paperType;
  }

  /**
   * Standartvorlagetyp
   */
  private java.lang.String standardPaperType;

  /**
   * Gets the field standardPaperType.
   * @return the value of the field standardPaperType; may be null.
   */
  public java.lang.String getStandardPaperType()
  {
    return standardPaperType;
  }

  /**
   * Sets the field standardPaperType.
   * @param _standardPaperType the new value of the field standardPaperType.
   */
  public void setStandardPaperType(java.lang.String _standardPaperType)
  {
    standardPaperType = _standardPaperType;
  }

  /**
   * Individuell
   */
  private java.lang.String individualPaperType;

  /**
   * Gets the field individualPaperType.
   * @return the value of the field individualPaperType; may be null.
   */
  public java.lang.String getIndividualPaperType()
  {
    return individualPaperType;
  }

  /**
   * Sets the field individualPaperType.
   * @param _individualPaperType the new value of the field individualPaperType.
   */
  public void setIndividualPaperType(java.lang.String _individualPaperType)
  {
    individualPaperType = _individualPaperType;
  }

  /**
   * CRM Issue erstellt
   */
  private java.lang.Boolean isCRMIssued;

  /**
   * Gets the field isCRMIssued.
   * @return the value of the field isCRMIssued; may be null.
   */
  public java.lang.Boolean getIsCRMIssued()
  {
    return isCRMIssued;
  }

  /**
   * Sets the field isCRMIssued.
   * @param _isCRMIssued the new value of the field isCRMIssued.
   */
  public void setIsCRMIssued(java.lang.Boolean _isCRMIssued)
  {
    isCRMIssued = _isCRMIssued;
  }

  /**
   * Unterlagen gepr�ft
   */
  private java.lang.Boolean paperIsChecked;

  /**
   * Gets the field paperIsChecked.
   * @return the value of the field paperIsChecked; may be null.
   */
  public java.lang.Boolean getPaperIsChecked()
  {
    return paperIsChecked;
  }

  /**
   * Sets the field paperIsChecked.
   * @param _paperIsChecked the new value of the field paperIsChecked.
   */
  public void setPaperIsChecked(java.lang.Boolean _paperIsChecked)
  {
    paperIsChecked = _paperIsChecked;
  }

  /**
   * Pr�fer UserId
   */
  private java.lang.String paperCheckedBy;

  /**
   * Gets the field paperCheckedBy.
   * @return the value of the field paperCheckedBy; may be null.
   */
  public java.lang.String getPaperCheckedBy()
  {
    return paperCheckedBy;
  }

  /**
   * Sets the field paperCheckedBy.
   * @param _paperCheckedBy the new value of the field paperCheckedBy.
   */
  public void setPaperCheckedBy(java.lang.String _paperCheckedBy)
  {
    paperCheckedBy = _paperCheckedBy;
  }

}
