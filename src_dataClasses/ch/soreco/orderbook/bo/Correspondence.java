package ch.soreco.orderbook.bo;

/**
 * Korrespondenz in Adressmutation
 * 1:n Beziehung zu CorrespondenceContainer / Korrespondenz Container
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Correspondence", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_Recording_Correspondence")
public class Correspondence extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -7980286189645057032L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer correspondenceId;

  /**
   * Gets the field correspondenceId.
   * @return the value of the field correspondenceId; may be null.
   */
  public java.lang.Integer getCorrespondenceId()
  {
    return correspondenceId;
  }

  /**
   * Sets the field correspondenceId.
   * @param _correspondenceId the new value of the field correspondenceId.
   */
  public void setCorrespondenceId(java.lang.Integer _correspondenceId)
  {
    correspondenceId = _correspondenceId;
  }

  /**
   * Order# der neuen Adresse
   */
  private java.lang.String newAddressOrderNo;

  /**
   * Gets the field newAddressOrderNo.
   * @return the value of the field newAddressOrderNo; may be null.
   */
  public java.lang.String getNewAddressOrderNo()
  {
    return newAddressOrderNo;
  }

  /**
   * Sets the field newAddressOrderNo.
   * @param _newAddressOrderNo the new value of the field newAddressOrderNo.
   */
  public void setNewAddressOrderNo(java.lang.String _newAddressOrderNo)
  {
    newAddressOrderNo = _newAddressOrderNo;
  }

  /**
   * Order# der ge�nderten Adresse
   */
  private java.lang.String changeAddressOrderNo;

  /**
   * Gets the field changeAddressOrderNo.
   * @return the value of the field changeAddressOrderNo; may be null.
   */
  public java.lang.String getChangeAddressOrderNo()
  {
    return changeAddressOrderNo;
  }

  /**
   * Sets the field changeAddressOrderNo.
   * @param _changeAddressOrderNo the new value of the field changeAddressOrderNo.
   */
  public void setChangeAddressOrderNo(java.lang.String _changeAddressOrderNo)
  {
    changeAddressOrderNo = _changeAddressOrderNo;
  }

  /**
   * Neue Adresse gepr�ft
   */
  private java.lang.Boolean newAddressIsChecked;

  /**
   * Gets the field newAddressIsChecked.
   * @return the value of the field newAddressIsChecked; may be null.
   */
  public java.lang.Boolean getNewAddressIsChecked()
  {
    return newAddressIsChecked;
  }

  /**
   * Sets the field newAddressIsChecked.
   * @param _newAddressIsChecked the new value of the field newAddressIsChecked.
   */
  public void setNewAddressIsChecked(java.lang.Boolean _newAddressIsChecked)
  {
    newAddressIsChecked = _newAddressIsChecked;
  }

  /**
   * Neue Adresse Pr�fer UserId
   */
  private java.lang.String newAdressCheckedBy;

  /**
   * Gets the field newAdressCheckedBy.
   * @return the value of the field newAdressCheckedBy; may be null.
   */
  public java.lang.String getNewAdressCheckedBy()
  {
    return newAdressCheckedBy;
  }

  /**
   * Sets the field newAdressCheckedBy.
   * @param _newAdressCheckedBy the new value of the field newAdressCheckedBy.
   */
  public void setNewAdressCheckedBy(java.lang.String _newAdressCheckedBy)
  {
    newAdressCheckedBy = _newAdressCheckedBy;
  }

  /**
   * Adress�nderung gepr�ft
   */
  private java.lang.Boolean changeAddressIsChecked;

  /**
   * Gets the field changeAddressIsChecked.
   * @return the value of the field changeAddressIsChecked; may be null.
   */
  public java.lang.Boolean getChangeAddressIsChecked()
  {
    return changeAddressIsChecked;
  }

  /**
   * Sets the field changeAddressIsChecked.
   * @param _changeAddressIsChecked the new value of the field changeAddressIsChecked.
   */
  public void setChangeAddressIsChecked(java.lang.Boolean _changeAddressIsChecked)
  {
    changeAddressIsChecked = _changeAddressIsChecked;
  }

  /**
   * Adress�nderung Pr�fer UserId
   */
  private java.lang.String changeAddressCheckedBy;

  /**
   * Gets the field changeAddressCheckedBy.
   * @return the value of the field changeAddressCheckedBy; may be null.
   */
  public java.lang.String getChangeAddressCheckedBy()
  {
    return changeAddressCheckedBy;
  }

  /**
   * Sets the field changeAddressCheckedBy.
   * @param _changeAddressCheckedBy the new value of the field changeAddressCheckedBy.
   */
  public void setChangeAddressCheckedBy(java.lang.String _changeAddressCheckedBy)
  {
    changeAddressCheckedBy = _changeAddressCheckedBy;
  }

  /**
   * Korrespondenz Container
   */
  @javax.persistence.OneToMany(cascade={javax.persistence.CascadeType.PERSIST, javax.persistence.CascadeType.MERGE}, fetch=javax.persistence.FetchType.EAGER, mappedBy="correspondence", orphanRemoval=false)
  private java.util.List<ch.soreco.orderbook.bo.CorrespondenceContainer> correspondenceContainer;

  /**
   * Gets the field correspondenceContainer.
   * @return the value of the field correspondenceContainer; may be null.
   */
  public java.util.List<ch.soreco.orderbook.bo.CorrespondenceContainer> getCorrespondenceContainer()
  {
    return correspondenceContainer;
  }

  /**
   * Sets the field correspondenceContainer.
   * @param _correspondenceContainer the new value of the field correspondenceContainer.
   */
  public void setCorrespondenceContainer(java.util.List<ch.soreco.orderbook.bo.CorrespondenceContainer> _correspondenceContainer)
  {
    correspondenceContainer = _correspondenceContainer;
  }

  /**
   * Korrespondenz Typ:  newAddress (Neue Adresse erfassen), deleteAddress (Adresse l�-schen), replaceAddress (Adresse durch vorhandene ersetzen), changeAddress (Korrespondenzadresse �ndern)
   */
  private java.lang.String correspondenceType;

  /**
   * Gets the field correspondenceType.
   * @return the value of the field correspondenceType; may be null.
   */
  public java.lang.String getCorrespondenceType()
  {
    return correspondenceType;
  }

  /**
   * Sets the field correspondenceType.
   * @param _correspondenceType the new value of the field correspondenceType.
   */
  public void setCorrespondenceType(java.lang.String _correspondenceType)
  {
    correspondenceType = _correspondenceType;
  }

}
