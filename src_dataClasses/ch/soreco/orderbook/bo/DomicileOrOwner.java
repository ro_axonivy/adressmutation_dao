package ch.soreco.orderbook.bo;

/**
 * Domicil/Owner in Adressmutation
 * n:1 Beziehung zu Recording / Erfassung
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class DomicileOrOwner", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_Recording_DomicileOrOwner")
public class DomicileOrOwner extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -7298793351482964879L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer domicileOrOwnerId;

  /**
   * Gets the field domicileOrOwnerId.
   * @return the value of the field domicileOrOwnerId; may be null.
   */
  public java.lang.Integer getDomicileOrOwnerId()
  {
    return domicileOrOwnerId;
  }

  /**
   * Sets the field domicileOrOwnerId.
   * @param _domicileOrOwnerId the new value of the field domicileOrOwnerId.
   */
  public void setDomicileOrOwnerId(java.lang.Integer _domicileOrOwnerId)
  {
    domicileOrOwnerId = _domicileOrOwnerId;
  }

  /**
   * Flag ob Adressmutation gew�hlt wurde
   */
  private java.lang.Boolean isAddressmutation;

  /**
   * Gets the field isAddressmutation.
   * @return the value of the field isAddressmutation; may be null.
   */
  public java.lang.Boolean getIsAddressmutation()
  {
    return isAddressmutation;
  }

  /**
   * Sets the field isAddressmutation.
   * @param _isAddressmutation the new value of the field isAddressmutation.
   */
  public void setIsAddressmutation(java.lang.Boolean _isAddressmutation)
  {
    isAddressmutation = _isAddressmutation;
  }

  /**
   * Level des BPs / Combobox
   */
  private java.lang.String bpLevel;

  /**
   * Gets the field bpLevel.
   * @return the value of the field bpLevel; may be null.
   */
  public java.lang.String getBpLevel()
  {
    return bpLevel;
  }

  /**
   * Sets the field bpLevel.
   * @param _bpLevel the new value of the field bpLevel.
   */
  public void setBpLevel(java.lang.String _bpLevel)
  {
    bpLevel = _bpLevel;
  }

  /**
   * Order#
   */
  private java.lang.String orderNo;

  /**
   * Gets the field orderNo.
   * @return the value of the field orderNo; may be null.
   */
  public java.lang.String getOrderNo()
  {
    return orderNo;
  }

  /**
   * Sets the field orderNo.
   * @param _orderNo the new value of the field orderNo.
   */
  public void setOrderNo(java.lang.String _orderNo)
  {
    orderNo = _orderNo;
  }

  /**
   * Domicil/Owner gepr�ft
   */
  private java.lang.Boolean domicileOrOwnerIsChecked;

  /**
   * Gets the field domicileOrOwnerIsChecked.
   * @return the value of the field domicileOrOwnerIsChecked; may be null.
   */
  public java.lang.Boolean getDomicileOrOwnerIsChecked()
  {
    return domicileOrOwnerIsChecked;
  }

  /**
   * Sets the field domicileOrOwnerIsChecked.
   * @param _domicileOrOwnerIsChecked the new value of the field domicileOrOwnerIsChecked.
   */
  public void setDomicileOrOwnerIsChecked(java.lang.Boolean _domicileOrOwnerIsChecked)
  {
    domicileOrOwnerIsChecked = _domicileOrOwnerIsChecked;
  }

  /**
   * Pr�fer UserId
   */
  private java.lang.String domicileOrOwnerCheckedBy;

  /**
   * Gets the field domicileOrOwnerCheckedBy.
   * @return the value of the field domicileOrOwnerCheckedBy; may be null.
   */
  public java.lang.String getDomicileOrOwnerCheckedBy()
  {
    return domicileOrOwnerCheckedBy;
  }

  /**
   * Sets the field domicileOrOwnerCheckedBy.
   * @param _domicileOrOwnerCheckedBy the new value of the field domicileOrOwnerCheckedBy.
   */
  public void setDomicileOrOwnerCheckedBy(java.lang.String _domicileOrOwnerCheckedBy)
  {
    domicileOrOwnerCheckedBy = _domicileOrOwnerCheckedBy;
  }

  /**
   * RecordingId / Erfassungs Id
   */
  @javax.persistence.ManyToOne(cascade={javax.persistence.CascadeType.PERSIST, javax.persistence.CascadeType.MERGE}, fetch=javax.persistence.FetchType.EAGER)
  @javax.persistence.JoinColumn(name="recordingId")
  private ch.soreco.orderbook.bo.Recording recording;

  /**
   * Gets the field recording.
   * @return the value of the field recording; may be null.
   */
  public ch.soreco.orderbook.bo.Recording getRecording()
  {
    return recording;
  }

  /**
   * Sets the field recording.
   * @param _recording the new value of the field recording.
   */
  public void setRecording(ch.soreco.orderbook.bo.Recording _recording)
  {
    recording = _recording;
  }

}
