package ch.soreco.orderbook.bo;

/**
 * Pendenzticket
 * n:1 Beziehung zu Order / Auftrag
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class PendencyTicket", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_PendencyTickets")
public class PendencyTicket extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -8562342744409574907L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer id;

  /**
   * Gets the field id.
   * @return the value of the field id; may be null.
   */
  public java.lang.Integer getId()
  {
    return id;
  }

  /**
   * Sets the field id.
   * @param _id the new value of the field id.
   */
  public void setId(java.lang.Integer _id)
  {
    id = _id;
  }

  /**
   * Id des Auftrags
   */
  @javax.persistence.ManyToOne(cascade={javax.persistence.CascadeType.PERSIST, javax.persistence.CascadeType.MERGE}, fetch=javax.persistence.FetchType.EAGER)
  @javax.persistence.JoinColumn(name="orderId")
  private ch.soreco.orderbook.bo.Order order;

  /**
   * Gets the field order.
   * @return the value of the field order; may be null.
   */
  public ch.soreco.orderbook.bo.Order getOrder()
  {
    return order;
  }

  /**
   * Sets the field order.
   * @param _order the new value of the field order.
   */
  public void setOrder(ch.soreco.orderbook.bo.Order _order)
  {
    order = _order;
  }

  /**
   * Pendenztickettyp
   */
  private java.lang.String pendencyTicketType;

  /**
   * Gets the field pendencyTicketType.
   * @return the value of the field pendencyTicketType; may be null.
   */
  public java.lang.String getPendencyTicketType()
  {
    return pendencyTicketType;
  }

  /**
   * Sets the field pendencyTicketType.
   * @param _pendencyTicketType the new value of the field pendencyTicketType.
   */
  public void setPendencyTicketType(java.lang.String _pendencyTicketType)
  {
    pendencyTicketType = _pendencyTicketType;
  }

  /**
   * Warte auf R�ckantwort
   */
  private java.lang.Boolean isWaitingOnResponse;

  /**
   * Gets the field isWaitingOnResponse.
   * @return the value of the field isWaitingOnResponse; may be null.
   */
  public java.lang.Boolean getIsWaitingOnResponse()
  {
    return isWaitingOnResponse;
  }

  /**
   * Sets the field isWaitingOnResponse.
   * @param _isWaitingOnResponse the new value of the field isWaitingOnResponse.
   */
  public void setIsWaitingOnResponse(java.lang.Boolean _isWaitingOnResponse)
  {
    isWaitingOnResponse = _isWaitingOnResponse;
  }

  /**
   * Wiedervorlage
   */
  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateUserType")
  private ch.ivyteam.ivy.scripting.objects.Date remindAt;

  /**
   * Gets the field remindAt.
   * @return the value of the field remindAt; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Date getRemindAt()
  {
    return remindAt;
  }

  /**
   * Sets the field remindAt.
   * @param _remindAt the new value of the field remindAt.
   */
  public void setRemindAt(ch.ivyteam.ivy.scripting.objects.Date _remindAt)
  {
    remindAt = _remindAt;
  }

  /**
   * Weiterleitung an
   */
  private java.lang.String delegateTo;

  /**
   * Gets the field delegateTo.
   * @return the value of the field delegateTo; may be null.
   */
  public java.lang.String getDelegateTo()
  {
    return delegateTo;
  }

  /**
   * Sets the field delegateTo.
   * @param _delegateTo the new value of the field delegateTo.
   */
  public void setDelegateTo(java.lang.String _delegateTo)
  {
    delegateTo = _delegateTo;
  }

  /**
   * Erstelldatum
   */
  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateTimeUserType")
  private ch.ivyteam.ivy.scripting.objects.DateTime createDateTime;

  /**
   * Gets the field createDateTime.
   * @return the value of the field createDateTime; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.DateTime getCreateDateTime()
  {
    return createDateTime;
  }

  /**
   * Sets the field createDateTime.
   * @param _createDateTime the new value of the field createDateTime.
   */
  public void setCreateDateTime(ch.ivyteam.ivy.scripting.objects.DateTime _createDateTime)
  {
    createDateTime = _createDateTime;
  }

  /**
   * Erstell UserId
   */
  private java.lang.String createUser;

  /**
   * Gets the field createUser.
   * @return the value of the field createUser; may be null.
   */
  public java.lang.String getCreateUser()
  {
    return createUser;
  }

  /**
   * Sets the field createUser.
   * @param _createUser the new value of the field createUser.
   */
  public void setCreateUser(java.lang.String _createUser)
  {
    createUser = _createUser;
  }

  /**
   * Gel�st
   */
  private java.lang.Boolean solved;

  /**
   * Gets the field solved.
   * @return the value of the field solved; may be null.
   */
  public java.lang.Boolean getSolved()
  {
    return solved;
  }

  /**
   * Sets the field solved.
   * @param _solved the new value of the field solved.
   */
  public void setSolved(java.lang.Boolean _solved)
  {
    solved = _solved;
  }

  /**
   * Antwort User
   */
  private java.lang.String respondUser;

  /**
   * Gets the field respondUser.
   * @return the value of the field respondUser; may be null.
   */
  public java.lang.String getRespondUser()
  {
    return respondUser;
  }

  /**
   * Sets the field respondUser.
   * @param _respondUser the new value of the field respondUser.
   */
  public void setRespondUser(java.lang.String _respondUser)
  {
    respondUser = _respondUser;
  }

  /**
   * Department
   */
  private java.lang.String department;

  /**
   * Gets the field department.
   * @return the value of the field department; may be null.
   */
  public java.lang.String getDepartment()
  {
    return department;
  }

  /**
   * Sets the field department.
   * @param _department the new value of the field department.
   */
  public void setDepartment(java.lang.String _department)
  {
    department = _department;
  }

  /**
   * CRM Issue Auftragsnummer
   */
  private java.lang.String CRMIssueOrderNo;

  /**
   * Gets the field CRMIssueOrderNo.
   * @return the value of the field CRMIssueOrderNo; may be null.
   */
  public java.lang.String getCRMIssueOrderNo()
  {
    return CRMIssueOrderNo;
  }

  /**
   * Sets the field CRMIssueOrderNo.
   * @param _CRMIssueOrderNo the new value of the field CRMIssueOrderNo.
   */
  public void setCRMIssueOrderNo(java.lang.String _CRMIssueOrderNo)
  {
    CRMIssueOrderNo = _CRMIssueOrderNo;
  }

  /**
   * Datum der L�sung
   */
  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateTimeUserType")
  private ch.ivyteam.ivy.scripting.objects.DateTime solveDateTime;

  /**
   * Gets the field solveDateTime.
   * @return the value of the field solveDateTime; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.DateTime getSolveDateTime()
  {
    return solveDateTime;
  }

  /**
   * Sets the field solveDateTime.
   * @param _solveDateTime the new value of the field solveDateTime.
   */
  public void setSolveDateTime(ch.ivyteam.ivy.scripting.objects.DateTime _solveDateTime)
  {
    solveDateTime = _solveDateTime;
  }

  /**
   * F�lligkeit
   */
  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateUserType")
  private ch.ivyteam.ivy.scripting.objects.Date dueDate;

  /**
   * Gets the field dueDate.
   * @return the value of the field dueDate; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Date getDueDate()
  {
    return dueDate;
  }

  /**
   * Sets the field dueDate.
   * @param _dueDate the new value of the field dueDate.
   */
  public void setDueDate(ch.ivyteam.ivy.scripting.objects.Date _dueDate)
  {
    dueDate = _dueDate;
  }

  /**
   * Manuelle Wiedervorlage am
   */
  private java.lang.Boolean allowManualRemindAt;

  /**
   * Gets the field allowManualRemindAt.
   * @return the value of the field allowManualRemindAt; may be null.
   */
  public java.lang.Boolean getAllowManualRemindAt()
  {
    return allowManualRemindAt;
  }

  /**
   * Sets the field allowManualRemindAt.
   * @param _allowManualRemindAt the new value of the field allowManualRemindAt.
   */
  public void setAllowManualRemindAt(java.lang.Boolean _allowManualRemindAt)
  {
    allowManualRemindAt = _allowManualRemindAt;
  }

  /**
   * Wiedervorlage in Tagen
   */
  private java.lang.Integer remindInDays;

  /**
   * Gets the field remindInDays.
   * @return the value of the field remindInDays; may be null.
   */
  public java.lang.Integer getRemindInDays()
  {
    return remindInDays;
  }

  /**
   * Sets the field remindInDays.
   * @param _remindInDays the new value of the field remindInDays.
   */
  public void setRemindInDays(java.lang.Integer _remindInDays)
  {
    remindInDays = _remindInDays;
  }

}
