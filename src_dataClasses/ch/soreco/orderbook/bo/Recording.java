package ch.soreco.orderbook.bo;

/**
 * Erfassung
 * 1:1 Beziehung zu BusinessPartner
 * 1:1 Beziehung zu Korrespondenz
 * 1:1 Beziehung zu Bestätigung
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Recording", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_Recordings")
public class Recording extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 7566116863260413621L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer recordingId;

  /**
   * Gets the field recordingId.
   * @return the value of the field recordingId; may be null.
   */
  public java.lang.Integer getRecordingId()
  {
    return recordingId;
  }

  /**
   * Sets the field recordingId.
   * @param _recordingId the new value of the field recordingId.
   */
  public void setRecordingId(java.lang.Integer _recordingId)
  {
    recordingId = _recordingId;
  }

  /**
   * Id des BusinessPartner Eintrags
   */
  @javax.persistence.OneToOne(cascade={javax.persistence.CascadeType.PERSIST, javax.persistence.CascadeType.MERGE}, fetch=javax.persistence.FetchType.EAGER, orphanRemoval=false)
  private ch.soreco.orderbook.bo.BusinessPartner businessPartner;

  /**
   * Gets the field businessPartner.
   * @return the value of the field businessPartner; may be null.
   */
  public ch.soreco.orderbook.bo.BusinessPartner getBusinessPartner()
  {
    return businessPartner;
  }

  /**
   * Sets the field businessPartner.
   * @param _businessPartner the new value of the field businessPartner.
   */
  public void setBusinessPartner(ch.soreco.orderbook.bo.BusinessPartner _businessPartner)
  {
    businessPartner = _businessPartner;
  }

  private transient java.util.List<ch.soreco.orderbook.bo.DomicileOrOwner> domicileOrOwner;

  /**
   * Gets the field domicileOrOwner.
   * @return the value of the field domicileOrOwner; may be null.
   */
  public java.util.List<ch.soreco.orderbook.bo.DomicileOrOwner> getDomicileOrOwner()
  {
    return domicileOrOwner;
  }

  /**
   * Sets the field domicileOrOwner.
   * @param _domicileOrOwner the new value of the field domicileOrOwner.
   */
  public void setDomicileOrOwner(java.util.List<ch.soreco.orderbook.bo.DomicileOrOwner> _domicileOrOwner)
  {
    domicileOrOwner = _domicileOrOwner;
  }

  /**
   * Id des Correspondence / Korrespondenz Eintrags
   */
  @javax.persistence.OneToOne(cascade={javax.persistence.CascadeType.PERSIST, javax.persistence.CascadeType.MERGE}, fetch=javax.persistence.FetchType.EAGER, orphanRemoval=false)
  private ch.soreco.orderbook.bo.Correspondence correspondence;

  /**
   * Gets the field correspondence.
   * @return the value of the field correspondence; may be null.
   */
  public ch.soreco.orderbook.bo.Correspondence getCorrespondence()
  {
    return correspondence;
  }

  /**
   * Sets the field correspondence.
   * @param _correspondence the new value of the field correspondence.
   */
  public void setCorrespondence(ch.soreco.orderbook.bo.Correspondence _correspondence)
  {
    correspondence = _correspondence;
  }

  private transient java.util.List<ch.soreco.orderbook.bo.RecordingLSV> lsv;

  /**
   * Gets the field lsv.
   * @return the value of the field lsv; may be null.
   */
  public java.util.List<ch.soreco.orderbook.bo.RecordingLSV> getLsv()
  {
    return lsv;
  }

  /**
   * Sets the field lsv.
   * @param _lsv the new value of the field lsv.
   */
  public void setLsv(java.util.List<ch.soreco.orderbook.bo.RecordingLSV> _lsv)
  {
    lsv = _lsv;
  }

  /**
   * Id des Affirmation / Bestätigungs Eintrags
   */
  @javax.persistence.OneToOne(cascade={javax.persistence.CascadeType.PERSIST, javax.persistence.CascadeType.MERGE}, fetch=javax.persistence.FetchType.EAGER, orphanRemoval=false)
  @javax.persistence.JoinColumn(name="affirmationId")
  private ch.soreco.orderbook.bo.Affirmation affirmation;

  /**
   * Gets the field affirmation.
   * @return the value of the field affirmation; may be null.
   */
  public ch.soreco.orderbook.bo.Affirmation getAffirmation()
  {
    return affirmation;
  }

  /**
   * Sets the field affirmation.
   * @param _affirmation the new value of the field affirmation.
   */
  public void setAffirmation(ch.soreco.orderbook.bo.Affirmation _affirmation)
  {
    affirmation = _affirmation;
  }

  private transient java.util.List<ch.soreco.orderbook.bo.RecordingMeta> metaTypes;

  /**
   * Gets the field metaTypes.
   * @return the value of the field metaTypes; may be null.
   */
  public java.util.List<ch.soreco.orderbook.bo.RecordingMeta> getMetaTypes()
  {
    return metaTypes;
  }

  /**
   * Sets the field metaTypes.
   * @param _metaTypes the new value of the field metaTypes.
   */
  public void setMetaTypes(java.util.List<ch.soreco.orderbook.bo.RecordingMeta> _metaTypes)
  {
    metaTypes = _metaTypes;
  }

}
