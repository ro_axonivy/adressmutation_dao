package ch.soreco.orderbook.bo;

/**
 * Auftrags Matching
 * Diese Entit�t definiert das Mapping der Dokumenttypen (vom Scanning) auf die effektiven Auftragsarten im Prozess
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class OrderMatching", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="btbl_OrderMachings")
public class OrderMatching extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -2439200041725005651L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer orderMatchId;

  /**
   * Gets the field orderMatchId.
   * @return the value of the field orderMatchId; may be null.
   */
  public java.lang.Integer getOrderMatchId()
  {
    return orderMatchId;
  }

  /**
   * Sets the field orderMatchId.
   * @param _orderMatchId the new value of the field orderMatchId.
   */
  public void setOrderMatchId(java.lang.Integer _orderMatchId)
  {
    orderMatchId = _orderMatchId;
  }

  /**
   * Dokumenttyp
   */
  private java.lang.String docType;

  /**
   * Gets the field docType.
   * @return the value of the field docType; may be null.
   */
  public java.lang.String getDocType()
  {
    return docType;
  }

  /**
   * Sets the field docType.
   * @param _docType the new value of the field docType.
   */
  public void setDocType(java.lang.String _docType)
  {
    docType = _docType;
  }

  /**
   * Auftragsart
   */
  private java.lang.String orderType;

  /**
   * Gets the field orderType.
   * @return the value of the field orderType; may be null.
   */
  public java.lang.String getOrderType()
  {
    return orderType;
  }

  /**
   * Sets the field orderType.
   * @param _orderType the new value of the field orderType.
   */
  public void setOrderType(java.lang.String _orderType)
  {
    orderType = _orderType;
  }

  /**
   * Frist zwischen Bereitstellung und Archivierung in Sekunden
   */
  private java.lang.Integer archiveDuration;

  /**
   * Gets the field archiveDuration.
   * @return the value of the field archiveDuration; may be null.
   */
  public java.lang.Integer getArchiveDuration()
  {
    return archiveDuration;
  }

  /**
   * Sets the field archiveDuration.
   * @param _archiveDuration the new value of the field archiveDuration.
   */
  public void setArchiveDuration(java.lang.Integer _archiveDuration)
  {
    archiveDuration = _archiveDuration;
  }

  /**
   * Priorit�t der Verarbeitungsreihenfolge
   */
  private java.lang.Integer priority;

  /**
   * Gets the field priority.
   * @return the value of the field priority; may be null.
   */
  public java.lang.Integer getPriority()
  {
    return priority;
  }

  /**
   * Sets the field priority.
   * @param _priority the new value of the field priority.
   */
  public void setPriority(java.lang.Integer _priority)
  {
    priority = _priority;
  }

  /**
   * Automatische Auftrags-Priorit�t nach Dispatching
   */
  private java.lang.Integer orderPriority;

  /**
   * Gets the field orderPriority.
   * @return the value of the field orderPriority; may be null.
   */
  public java.lang.Integer getOrderPriority()
  {
    return orderPriority;
  }

  /**
   * Sets the field orderPriority.
   * @param _orderPriority the new value of the field orderPriority.
   */
  public void setOrderPriority(java.lang.Integer _orderPriority)
  {
    orderPriority = _orderPriority;
  }

  /**
   * Mapping auf Dokumenttyp nach Dispatching
   */
  private java.lang.String mapToDocType;

  /**
   * Gets the field mapToDocType.
   * @return the value of the field mapToDocType; may be null.
   */
  public java.lang.String getMapToDocType()
  {
    return mapToDocType;
  }

  /**
   * Sets the field mapToDocType.
   * @param _mapToDocType the new value of the field mapToDocType.
   */
  public void setMapToDocType(java.lang.String _mapToDocType)
  {
    mapToDocType = _mapToDocType;
  }

  /**
   * Auftragsstatus nach Dispatching
   */
  private java.lang.String stateAfterCreate;

  /**
   * Gets the field stateAfterCreate.
   * @return the value of the field stateAfterCreate; may be null.
   */
  public java.lang.String getStateAfterCreate()
  {
    return stateAfterCreate;
  }

  /**
   * Sets the field stateAfterCreate.
   * @param _stateAfterCreate the new value of the field stateAfterCreate.
   */
  public void setStateAfterCreate(java.lang.String _stateAfterCreate)
  {
    stateAfterCreate = _stateAfterCreate;
  }

  /**
   * Mapping auf das gegebene Team f�r Dispatching.
   */
  private java.lang.String mapToTeams;

  /**
   * Gets the field mapToTeams.
   * @return the value of the field mapToTeams; may be null.
   */
  public java.lang.String getMapToTeams()
  {
    return mapToTeams;
  }

  /**
   * Sets the field mapToTeams.
   * @param _mapToTeams the new value of the field mapToTeams.
   */
  public void setMapToTeams(java.lang.String _mapToTeams)
  {
    mapToTeams = _mapToTeams;
  }

}
