package ch.soreco.orderbook.bo;

/**
 * Auftrag
 * Hauptentit�t zur Speicherung der Auftragsdaten
 * 1:1 Beziehung zu Recording / Erfassung
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Order", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_Orders")
public class Order extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 8591084149874452950L;

  /**
   * Auftragsart
   */
  private java.lang.String orderType;

  /**
   * Gets the field orderType.
   * @return the value of the field orderType; may be null.
   */
  public java.lang.String getOrderType()
  {
    return orderType;
  }

  /**
   * Sets the field orderType.
   * @param _orderType the new value of the field orderType.
   */
  public void setOrderType(java.lang.String _orderType)
  {
    orderType = _orderType;
  }

  /**
   * Auftragsstatus
   */
  private java.lang.String orderState;

  /**
   * Gets the field orderState.
   * @return the value of the field orderState; may be null.
   */
  public java.lang.String getOrderState()
  {
    return orderState;
  }

  /**
   * Sets the field orderState.
   * @param _orderState the new value of the field orderState.
   */
  public void setOrderState(java.lang.String _orderState)
  {
    orderState = _orderState;
  }

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer orderId;

  /**
   * Gets the field orderId.
   * @return the value of the field orderId; may be null.
   */
  public java.lang.Integer getOrderId()
  {
    return orderId;
  }

  /**
   * Sets the field orderId.
   * @param _orderId the new value of the field orderId.
   */
  public void setOrderId(java.lang.Integer _orderId)
  {
    orderId = _orderId;
  }

  /**
   * Id des Scannings
   */
  private java.lang.String scanId;

  /**
   * Gets the field scanId.
   * @return the value of the field scanId; may be null.
   */
  public java.lang.String getScanId()
  {
    return scanId;
  }

  /**
   * Sets the field scanId.
   * @param _scanId the new value of the field scanId.
   */
  public void setScanId(java.lang.String _scanId)
  {
    scanId = _scanId;
  }

  /**
   * Id des Files 
   */
  private java.lang.Integer scanFileId;

  /**
   * Gets the field scanFileId.
   * @return the value of the field scanFileId; may be null.
   */
  public java.lang.Integer getScanFileId()
  {
    return scanFileId;
  }

  /**
   * Sets the field scanFileId.
   * @param _scanFileId the new value of the field scanFileId.
   */
  public void setScanFileId(java.lang.Integer _scanFileId)
  {
    scanFileId = _scanFileId;
  }

  /**
   * Id des Mono Files
   */
  private java.lang.Integer scanMonoFileId;

  /**
   * Gets the field scanMonoFileId.
   * @return the value of the field scanMonoFileId; may be null.
   */
  public java.lang.Integer getScanMonoFileId()
  {
    return scanMonoFileId;
  }

  /**
   * Sets the field scanMonoFileId.
   * @param _scanMonoFileId the new value of the field scanMonoFileId.
   */
  public void setScanMonoFileId(java.lang.Integer _scanMonoFileId)
  {
    scanMonoFileId = _scanMonoFileId;
  }

  /**
   * Auftragsvolumen, in TRX Auftr�gen required
   */
  @javax.persistence.Column(length=1)
  private java.lang.String orderQuantityType;

  /**
   * Gets the field orderQuantityType.
   * @return the value of the field orderQuantityType; may be null.
   */
  public java.lang.String getOrderQuantityType()
  {
    return orderQuantityType;
  }

  /**
   * Sets the field orderQuantityType.
   * @param _orderQuantityType the new value of the field orderQuantityType.
   */
  public void setOrderQuantityType(java.lang.String _orderQuantityType)
  {
    orderQuantityType = _orderQuantityType;
  }

  /**
   * UserId des Bearbeiters
   */
  private java.lang.String editorUserId;

  /**
   * Gets the field editorUserId.
   * @return the value of the field editorUserId; may be null.
   */
  public java.lang.String getEditorUserId()
  {
    return editorUserId;
  }

  /**
   * Sets the field editorUserId.
   * @param _editorUserId the new value of the field editorUserId.
   */
  public void setEditorUserId(java.lang.String _editorUserId)
  {
    editorUserId = _editorUserId;
  }

  /**
   * UserId des Pr�fers
   */
  private java.lang.String checkerUserId;

  /**
   * Gets the field checkerUserId.
   * @return the value of the field checkerUserId; may be null.
   */
  public java.lang.String getCheckerUserId()
  {
    return checkerUserId;
  }

  /**
   * Sets the field checkerUserId.
   * @param _checkerUserId the new value of the field checkerUserId.
   */
  public void setCheckerUserId(java.lang.String _checkerUserId)
  {
    checkerUserId = _checkerUserId;
  }

  /**
   * UserID des Dispatchers
   */
  private java.lang.String dispatcherUserId;

  /**
   * Gets the field dispatcherUserId.
   * @return the value of the field dispatcherUserId; may be null.
   */
  public java.lang.String getDispatcherUserId()
  {
    return dispatcherUserId;
  }

  /**
   * Sets the field dispatcherUserId.
   * @param _dispatcherUserId the new value of the field dispatcherUserId.
   */
  public void setDispatcherUserId(java.lang.String _dispatcherUserId)
  {
    dispatcherUserId = _dispatcherUserId;
  }

  private transient java.util.List<ch.soreco.orderbook.bo.Scanning> scannings;

  /**
   * Gets the field scannings.
   * @return the value of the field scannings; may be null.
   */
  public java.util.List<ch.soreco.orderbook.bo.Scanning> getScannings()
  {
    return scannings;
  }

  /**
   * Sets the field scannings.
   * @param _scannings the new value of the field scannings.
   */
  public void setScannings(java.util.List<ch.soreco.orderbook.bo.Scanning> _scannings)
  {
    scannings = _scannings;
  }

  /**
   * Restzeit, nicht aktiv
   */
  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateTimeUserType")
  @javax.persistence.Column(updatable=false, insertable=false)
  private ch.ivyteam.ivy.scripting.objects.DateTime timeRemaining;

  /**
   * Gets the field timeRemaining.
   * @return the value of the field timeRemaining; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.DateTime getTimeRemaining()
  {
    return timeRemaining;
  }

  /**
   * Sets the field timeRemaining.
   * @param _timeRemaining the new value of the field timeRemaining.
   */
  public void setTimeRemaining(ch.ivyteam.ivy.scripting.objects.DateTime _timeRemaining)
  {
    timeRemaining = _timeRemaining;
  }

  /**
   * Start des Auftrags = Scandatum
   */
  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateTimeUserType")
  @javax.persistence.Column(updatable=false)
  private ch.ivyteam.ivy.scripting.objects.DateTime orderStart;

  /**
   * Gets the field orderStart.
   * @return the value of the field orderStart; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.DateTime getOrderStart()
  {
    return orderStart;
  }

  /**
   * Sets the field orderStart.
   * @param _orderStart the new value of the field orderStart.
   */
  public void setOrderStart(ch.ivyteam.ivy.scripting.objects.DateTime _orderStart)
  {
    orderStart = _orderStart;
  }

  /**
   * Ende des Auftrags = Archivierungsdatum
   */
  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateTimeUserType")
  private ch.ivyteam.ivy.scripting.objects.DateTime orderEnd;

  /**
   * Gets the field orderEnd.
   * @return the value of the field orderEnd; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.DateTime getOrderEnd()
  {
    return orderEnd;
  }

  /**
   * Sets the field orderEnd.
   * @param _orderEnd the new value of the field orderEnd.
   */
  public void setOrderEnd(ch.ivyteam.ivy.scripting.objects.DateTime _orderEnd)
  {
    orderEnd = _orderEnd;
  }

  /**
   * Id von Recording / Erfassung
   */
  @javax.persistence.OneToOne(cascade={javax.persistence.CascadeType.PERSIST, javax.persistence.CascadeType.MERGE}, fetch=javax.persistence.FetchType.EAGER, orphanRemoval=false)
  @javax.persistence.JoinColumn(name="recordingId")
  private ch.soreco.orderbook.bo.Recording recording;

  /**
   * Gets the field recording.
   * @return the value of the field recording; may be null.
   */
  public ch.soreco.orderbook.bo.Recording getRecording()
  {
    return recording;
  }

  /**
   * Sets the field recording.
   * @param _recording the new value of the field recording.
   */
  public void setRecording(ch.soreco.orderbook.bo.Recording _recording)
  {
    recording = _recording;
  }

  /**
   * BP Id
   */
  private java.lang.String BPId;

  /**
   * Gets the field BPId.
   * @return the value of the field BPId; may be null.
   */
  public java.lang.String getBPId()
  {
    return BPId;
  }

  /**
   * Sets the field BPId.
   * @param _BPId the new value of the field BPId.
   */
  public void setBPId(java.lang.String _BPId)
  {
    BPId = _BPId;
  }

  /**
   * BP Id gepr�ft
   */
  private java.lang.Boolean bpIdIsChecked;

  /**
   * Gets the field bpIdIsChecked.
   * @return the value of the field bpIdIsChecked; may be null.
   */
  public java.lang.Boolean getBpIdIsChecked()
  {
    return bpIdIsChecked;
  }

  /**
   * Sets the field bpIdIsChecked.
   * @param _bpIdIsChecked the new value of the field bpIdIsChecked.
   */
  public void setBpIdIsChecked(java.lang.Boolean _bpIdIsChecked)
  {
    bpIdIsChecked = _bpIdIsChecked;
  }

  private transient java.util.List<ch.soreco.orderbook.bo.Contact> contacts;

  /**
   * Gets the field contacts.
   * @return the value of the field contacts; may be null.
   */
  public java.util.List<ch.soreco.orderbook.bo.Contact> getContacts()
  {
    return contacts;
  }

  /**
   * Sets the field contacts.
   * @param _contacts the new value of the field contacts.
   */
  public void setContacts(java.util.List<ch.soreco.orderbook.bo.Contact> _contacts)
  {
    contacts = _contacts;
  }

  /**
   * Doublette
   */
  private java.lang.Boolean isDouble;

  /**
   * Gets the field isDouble.
   * @return the value of the field isDouble; may be null.
   */
  public java.lang.Boolean getIsDouble()
  {
    return isDouble;
  }

  /**
   * Sets the field isDouble.
   * @param _isDouble the new value of the field isDouble.
   */
  public void setIsDouble(java.lang.Boolean _isDouble)
  {
    isDouble = _isDouble;
  }

  /**
   * Deprecated
   */
  private java.lang.Integer archiveId;

  /**
   * Gets the field archiveId.
   * @return the value of the field archiveId; may be null.
   */
  public java.lang.Integer getArchiveId()
  {
    return archiveId;
  }

  /**
   * Sets the field archiveId.
   * @param _archiveId the new value of the field archiveId.
   */
  public void setArchiveId(java.lang.Integer _archiveId)
  {
    archiveId = _archiveId;
  }

  /**
   * Typ der Doublette
   */
  private java.lang.String doubletteType;

  /**
   * Gets the field doubletteType.
   * @return the value of the field doubletteType; may be null.
   */
  public java.lang.String getDoubletteType()
  {
    return doubletteType;
  }

  /**
   * Sets the field doubletteType.
   * @param _doubletteType the new value of the field doubletteType.
   */
  public void setDoubletteType(java.lang.String _doubletteType)
  {
    doubletteType = _doubletteType;
  }

  /**
   * Wenn Auftrag gemerged ist, Id des Hauptauftrages
   */
  private java.lang.Integer matchedToOrder;

  /**
   * Gets the field matchedToOrder.
   * @return the value of the field matchedToOrder; may be null.
   */
  public java.lang.Integer getMatchedToOrder()
  {
    return matchedToOrder;
  }

  /**
   * Sets the field matchedToOrder.
   * @param _matchedToOrder the new value of the field matchedToOrder.
   */
  public void setMatchedToOrder(java.lang.Integer _matchedToOrder)
  {
    matchedToOrder = _matchedToOrder;
  }

  /**
   * Flag ob BP nicht feststellbar ist
   */
  private java.lang.Boolean bpNotMatchable;

  /**
   * Gets the field bpNotMatchable.
   * @return the value of the field bpNotMatchable; may be null.
   */
  public java.lang.Boolean getBpNotMatchable()
  {
    return bpNotMatchable;
  }

  /**
   * Sets the field bpNotMatchable.
   * @param _bpNotMatchable the new value of the field bpNotMatchable.
   */
  public void setBpNotMatchable(java.lang.Boolean _bpNotMatchable)
  {
    bpNotMatchable = _bpNotMatchable;
  }

  /**
   * Kommentar, Freitext
   */
  @javax.persistence.Column(length=8000)
  private java.lang.String comment;

  /**
   * Gets the field comment.
   * @return the value of the field comment; may be null.
   */
  public java.lang.String getComment()
  {
    return comment;
  }

  /**
   * Sets the field comment.
   * @param _comment the new value of the field comment.
   */
  public void setComment(java.lang.String _comment)
  {
    comment = _comment;
  }

  /**
   * Flag ob Aufgtrag abgebrochen wird
   */
  private java.lang.Boolean discard;

  /**
   * Gets the field discard.
   * @return the value of the field discard; may be null.
   */
  public java.lang.Boolean getDiscard()
  {
    return discard;
  }

  /**
   * Sets the field discard.
   * @param _discard the new value of the field discard.
   */
  public void setDiscard(java.lang.Boolean _discard)
  {
    discard = _discard;
  }

  /**
   * FAClanId
   */
  private java.lang.String FAClanId;

  /**
   * Gets the field FAClanId.
   * @return the value of the field FAClanId; may be null.
   */
  public java.lang.String getFAClanId()
  {
    return FAClanId;
  }

  /**
   * Sets the field FAClanId.
   * @param _FAClanId the new value of the field FAClanId.
   */
  public void setFAClanId(java.lang.String _FAClanId)
  {
    FAClanId = _FAClanId;
  }

  /**
   * Id des Scannings
   */
  @javax.persistence.Column(name="scanningId")
  private java.lang.Integer scanningId;

  /**
   * Gets the field scanningId.
   * @return the value of the field scanningId; may be null.
   */
  public java.lang.Integer getScanningId()
  {
    return scanningId;
  }

  /**
   * Sets the field scanningId.
   * @param _scanningId the new value of the field scanningId.
   */
  public void setScanningId(java.lang.Integer _scanningId)
  {
    scanningId = _scanningId;
  }

  private transient ch.soreco.orderbook.bo.Scanning scanning;

  /**
   * Gets the field scanning.
   * @return the value of the field scanning; may be null.
   */
  public ch.soreco.orderbook.bo.Scanning getScanning()
  {
    return scanning;
  }

  /**
   * Sets the field scanning.
   * @param _scanning the new value of the field scanning.
   */
  public void setScanning(ch.soreco.orderbook.bo.Scanning _scanning)
  {
    scanning = _scanning;
  }

  /**
   * Doublette gepr�ft
   */
  private java.lang.Boolean doubletteIsChecked;

  /**
   * Gets the field doubletteIsChecked.
   * @return the value of the field doubletteIsChecked; may be null.
   */
  public java.lang.Boolean getDoubletteIsChecked()
  {
    return doubletteIsChecked;
  }

  /**
   * Sets the field doubletteIsChecked.
   * @param _doubletteIsChecked the new value of the field doubletteIsChecked.
   */
  public void setDoubletteIsChecked(java.lang.Boolean _doubletteIsChecked)
  {
    doubletteIsChecked = _doubletteIsChecked;
  }

  private transient java.util.List<ch.soreco.orderbook.bo.PendencyTicket> pendencyTickets;

  /**
   * Gets the field pendencyTickets.
   * @return the value of the field pendencyTickets; may be null.
   */
  public java.util.List<ch.soreco.orderbook.bo.PendencyTicket> getPendencyTickets()
  {
    return pendencyTickets;
  }

  /**
   * Sets the field pendencyTickets.
   * @param _pendencyTickets the new value of the field pendencyTickets.
   */
  public void setPendencyTickets(java.util.List<ch.soreco.orderbook.bo.PendencyTicket> _pendencyTickets)
  {
    pendencyTickets = _pendencyTickets;
  }

  /**
   * Priorit�t
   */
  private java.lang.Integer priority;

  /**
   * Gets the field priority.
   * @return the value of the field priority; may be null.
   */
  public java.lang.Integer getPriority()
  {
    return priority;
  }

  /**
   * Sets the field priority.
   * @param _priority the new value of the field priority.
   */
  public void setPriority(java.lang.Integer _priority)
  {
    priority = _priority;
  }

  /**
   * Id des Dispatchings
   */
  private java.lang.Integer dispatchingId;

  /**
   * Gets the field dispatchingId.
   * @return the value of the field dispatchingId; may be null.
   */
  public java.lang.Integer getDispatchingId()
  {
    return dispatchingId;
  }

  /**
   * Sets the field dispatchingId.
   * @param _dispatchingId the new value of the field dispatchingId.
   */
  public void setDispatchingId(java.lang.Integer _dispatchingId)
  {
    dispatchingId = _dispatchingId;
  }

  /**
   * alle Teams, relevant falls Kombiauftrag
   */
  @javax.persistence.Column(length=8000)
  private java.lang.String teams;

  /**
   * Gets the field teams.
   * @return the value of the field teams; may be null.
   */
  public java.lang.String getTeams()
  {
    return teams;
  }

  /**
   * Sets the field teams.
   * @param _teams the new value of the field teams.
   */
  public void setTeams(java.lang.String _teams)
  {
    teams = _teams;
  }

  /**
   * Pendenzticket Typ / Wiedervorlagegrund des neuesten Pendenztickets
   */
  private java.lang.String pendencyTicketType;

  /**
   * Gets the field pendencyTicketType.
   * @return the value of the field pendencyTicketType; may be null.
   */
  public java.lang.String getPendencyTicketType()
  {
    return pendencyTicketType;
  }

  /**
   * Sets the field pendencyTicketType.
   * @param _pendencyTicketType the new value of the field pendencyTicketType.
   */
  public void setPendencyTicketType(java.lang.String _pendencyTicketType)
  {
    pendencyTicketType = _pendencyTicketType;
  }

  /**
   * N�chste Wiedervorlage
   */
  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateUserType")
  private ch.ivyteam.ivy.scripting.objects.Date nextDueDate;

  /**
   * Gets the field nextDueDate.
   * @return the value of the field nextDueDate; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Date getNextDueDate()
  {
    return nextDueDate;
  }

  /**
   * Sets the field nextDueDate.
   * @param _nextDueDate the new value of the field nextDueDate.
   */
  public void setNextDueDate(ch.ivyteam.ivy.scripting.objects.Date _nextDueDate)
  {
    nextDueDate = _nextDueDate;
  }

  /**
   * Datum der letzten Bearbeitung / Pr�fung
   */
  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateTimeUserType")
  private ch.ivyteam.ivy.scripting.objects.DateTime lastEdit;

  /**
   * Gets the field lastEdit.
   * @return the value of the field lastEdit; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.DateTime getLastEdit()
  {
    return lastEdit;
  }

  /**
   * Sets the field lastEdit.
   * @param _lastEdit the new value of the field lastEdit.
   */
  public void setLastEdit(ch.ivyteam.ivy.scripting.objects.DateTime _lastEdit)
  {
    lastEdit = _lastEdit;
  }

  /**
   * The Order Type Enum value. May be null as set transient.
   */
  private transient ch.soreco.orderbook.enums.OrderType orderTypeEnum;

  /**
   * Gets the field orderTypeEnum.
   * @return the value of the field orderTypeEnum; may be null.
   */
  public ch.soreco.orderbook.enums.OrderType getOrderTypeEnum()
  {
    return orderTypeEnum;
  }

  /**
   * Sets the field orderTypeEnum.
   * @param _orderTypeEnum the new value of the field orderTypeEnum.
   */
  public void setOrderTypeEnum(ch.soreco.orderbook.enums.OrderType _orderTypeEnum)
  {
    orderTypeEnum = _orderTypeEnum;
  }

  private java.lang.String FDL;

  /**
   * Gets the field FDL.
   * @return the value of the field FDL; may be null.
   */
  public java.lang.String getFDL()
  {
    return FDL;
  }

  /**
   * Sets the field FDL.
   * @param _FDL the new value of the field FDL.
   */
  public void setFDL(java.lang.String _FDL)
  {
    FDL = _FDL;
  }

}
