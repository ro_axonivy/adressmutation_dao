package ch.soreco.orderbook.bo;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class MailBoxConfiguration", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="btbl_MailBoxConfigurations")
public class MailBoxConfiguration extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -170415655674529233L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer id;

  /**
   * Gets the field id.
   * @return the value of the field id; may be null.
   */
  public java.lang.Integer getId()
  {
    return id;
  }

  /**
   * Sets the field id.
   * @param _id the new value of the field id.
   */
  public void setId(java.lang.Integer _id)
  {
    id = _id;
  }

  /**
   * Wheter the Mail Scan is runnable or not.
   */
  @javax.persistence.Column(name="IsRunnable", nullable=false)
  private java.lang.Boolean isRunnable;

  /**
   * Gets the field isRunnable.
   * @return the value of the field isRunnable; may be null.
   */
  public java.lang.Boolean getIsRunnable()
  {
    return isRunnable;
  }

  /**
   * Sets the field isRunnable.
   * @param _isRunnable the new value of the field isRunnable.
   */
  public void setIsRunnable(java.lang.Boolean _isRunnable)
  {
    isRunnable = _isRunnable;
  }

  /**
   * Defines the mail sender address
   */
  @javax.persistence.Column(name="OnExceptionMailFrom", nullable=false)
  private java.lang.String onExceptionMailFrom;

  /**
   * Gets the field onExceptionMailFrom.
   * @return the value of the field onExceptionMailFrom; may be null.
   */
  public java.lang.String getOnExceptionMailFrom()
  {
    return onExceptionMailFrom;
  }

  /**
   * Sets the field onExceptionMailFrom.
   * @param _onExceptionMailFrom the new value of the field onExceptionMailFrom.
   */
  public void setOnExceptionMailFrom(java.lang.String _onExceptionMailFrom)
  {
    onExceptionMailFrom = _onExceptionMailFrom;
  }

  /**
   * Defines the mail recipient
   */
  @javax.persistence.Column(name="OnExceptionMailTo", nullable=false)
  private java.lang.String onExceptionMailTo;

  /**
   * Gets the field onExceptionMailTo.
   * @return the value of the field onExceptionMailTo; may be null.
   */
  public java.lang.String getOnExceptionMailTo()
  {
    return onExceptionMailTo;
  }

  /**
   * Sets the field onExceptionMailTo.
   * @param _onExceptionMailTo the new value of the field onExceptionMailTo.
   */
  public void setOnExceptionMailTo(java.lang.String _onExceptionMailTo)
  {
    onExceptionMailTo = _onExceptionMailTo;
  }

  /**
   * Defines the mapping of initial values for an Order
   */
  @javax.persistence.ManyToOne(cascade={javax.persistence.CascadeType.PERSIST, javax.persistence.CascadeType.MERGE}, fetch=javax.persistence.FetchType.EAGER, optional=false)
  @javax.persistence.JoinColumn(name="OrderMatchingId", nullable=false)
  private ch.soreco.orderbook.bo.OrderMatching orderMatching;

  /**
   * Gets the field orderMatching.
   * @return the value of the field orderMatching; may be null.
   */
  public ch.soreco.orderbook.bo.OrderMatching getOrderMatching()
  {
    return orderMatching;
  }

  /**
   * Sets the field orderMatching.
   * @param _orderMatching the new value of the field orderMatching.
   */
  public void setOrderMatching(ch.soreco.orderbook.bo.OrderMatching _orderMatching)
  {
    orderMatching = _orderMatching;
  }

  /**
   * The host name where mail box resides
   */
  @javax.persistence.Column(name="Host", nullable=false)
  private java.lang.String host;

  /**
   * Gets the field host.
   * @return the value of the field host; may be null.
   */
  public java.lang.String getHost()
  {
    return host;
  }

  /**
   * Sets the field host.
   * @param _host the new value of the field host.
   */
  public void setHost(java.lang.String _host)
  {
    host = _host;
  }

  /**
   * The port number. Set -1 to ignore.
   */
  @javax.persistence.Column(name="Port", nullable=false)
  private java.lang.Integer port;

  /**
   * Gets the field port.
   * @return the value of the field port; may be null.
   */
  public java.lang.Integer getPort()
  {
    return port;
  }

  /**
   * Sets the field port.
   * @param _port the new value of the field port.
   */
  public void setPort(java.lang.Integer _port)
  {
    port = _port;
  }

  /**
   * The username of the mailbox owner
   */
  @javax.persistence.Column(name="Username", nullable=false)
  private java.lang.String username;

  /**
   * Gets the field username.
   * @return the value of the field username; may be null.
   */
  public java.lang.String getUsername()
  {
    return username;
  }

  /**
   * Sets the field username.
   * @param _username the new value of the field username.
   */
  public void setUsername(java.lang.String _username)
  {
    username = _username;
  }

  /**
   * The password of the mailbox owner
   */
  @javax.persistence.Column(name="Password", nullable=false)
  private java.lang.String password;

  /**
   * Gets the field password.
   * @return the value of the field password; may be null.
   */
  public java.lang.String getPassword()
  {
    return password;
  }

  /**
   * Sets the field password.
   * @param _password the new value of the field password.
   */
  public void setPassword(java.lang.String _password)
  {
    password = _password;
  }

  /**
   * When the next scan is to be invoked.
   */
  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateTimeUserType")
  @javax.persistence.Column(name="NextScan")
  private ch.ivyteam.ivy.scripting.objects.DateTime nextScanAt;

  /**
   * Gets the field nextScanAt.
   * @return the value of the field nextScanAt; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.DateTime getNextScanAt()
  {
    return nextScanAt;
  }

  /**
   * Sets the field nextScanAt.
   * @param _nextScanAt the new value of the field nextScanAt.
   */
  public void setNextScanAt(ch.ivyteam.ivy.scripting.objects.DateTime _nextScanAt)
  {
    nextScanAt = _nextScanAt;
  }

  /**
   * The duration literal used to calculate next scan at. A duration literal literal has the following format: P[n]Y[n]M[n]DT[n]H[n]M[n]S. Elements may be omitted if their value is zero.
   */
  @javax.persistence.Column(name="ScanIntervalLiteral")
  private java.lang.String scanIntervalLiteral;

  /**
   * Gets the field scanIntervalLiteral.
   * @return the value of the field scanIntervalLiteral; may be null.
   */
  public java.lang.String getScanIntervalLiteral()
  {
    return scanIntervalLiteral;
  }

  /**
   * Sets the field scanIntervalLiteral.
   * @param _scanIntervalLiteral the new value of the field scanIntervalLiteral.
   */
  public void setScanIntervalLiteral(java.lang.String _scanIntervalLiteral)
  {
    scanIntervalLiteral = _scanIntervalLiteral;
  }

  /**
   * whether the first attached pdf is used as an {@link Order} or the order pdf is concatenated with all content which is renderable as PDF.
   */
  @javax.persistence.Column(name="UseFirstPdfAsOrder")
  private java.lang.Boolean useFirstPdfAsOrder;

  /**
   * Gets the field useFirstPdfAsOrder.
   * @return the value of the field useFirstPdfAsOrder; may be null.
   */
  public java.lang.Boolean getUseFirstPdfAsOrder()
  {
    return useFirstPdfAsOrder;
  }

  /**
   * Sets the field useFirstPdfAsOrder.
   * @param _useFirstPdfAsOrder the new value of the field useFirstPdfAsOrder.
   */
  public void setUseFirstPdfAsOrder(java.lang.Boolean _useFirstPdfAsOrder)
  {
    useFirstPdfAsOrder = _useFirstPdfAsOrder;
  }

  /**
   * whether the subject of a mail is set to FAClanNo field or not.
   */
  @javax.persistence.Column(name="UseSubjectForRefNo")
  private java.lang.Boolean useSubjectForRefNo;

  /**
   * Gets the field useSubjectForRefNo.
   * @return the value of the field useSubjectForRefNo; may be null.
   */
  public java.lang.Boolean getUseSubjectForRefNo()
  {
    return useSubjectForRefNo;
  }

  /**
   * Sets the field useSubjectForRefNo.
   * @param _useSubjectForRefNo the new value of the field useSubjectForRefNo.
   */
  public void setUseSubjectForRefNo(java.lang.Boolean _useSubjectForRefNo)
  {
    useSubjectForRefNo = _useSubjectForRefNo;
  }

}
