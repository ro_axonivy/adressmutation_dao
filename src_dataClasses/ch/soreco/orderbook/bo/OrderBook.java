package ch.soreco.orderbook.bo;

/**
 * Auftragsbuch
 * Diese Entität steuert den Sichtbarkeitsbereich der Aufträge
 * 1:1 Beziehung zu sich selbst (verschachtelte Auftragsbücher)
 * n:n Beziehung zu Auftragstypen (Enumerator)
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class OrderBook", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_OrderBooks")
public class OrderBook extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -1199184383083747157L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer orderBookId;

  /**
   * Gets the field orderBookId.
   * @return the value of the field orderBookId; may be null.
   */
  public java.lang.Integer getOrderBookId()
  {
    return orderBookId;
  }

  /**
   * Sets the field orderBookId.
   * @param _orderBookId the new value of the field orderBookId.
   */
  public void setOrderBookId(java.lang.Integer _orderBookId)
  {
    orderBookId = _orderBookId;
  }

  /**
   * Name
   */
  private java.lang.String name;

  /**
   * Gets the field name.
   * @return the value of the field name; may be null.
   */
  public java.lang.String getName()
  {
    return name;
  }

  /**
   * Sets the field name.
   * @param _name the new value of the field name.
   */
  public void setName(java.lang.String _name)
  {
    name = _name;
  }

  /**
   * Kürzel
   */
  private java.lang.String shortname;

  /**
   * Gets the field shortname.
   * @return the value of the field shortname; may be null.
   */
  public java.lang.String getShortname()
  {
    return shortname;
  }

  /**
   * Sets the field shortname.
   * @param _shortname the new value of the field shortname.
   */
  public void setShortname(java.lang.String _shortname)
  {
    shortname = _shortname;
  }

  private transient java.util.List<ch.soreco.orderbook.enums.OrderType> orderTypeList;

  /**
   * Gets the field orderTypeList.
   * @return the value of the field orderTypeList; may be null.
   */
  public java.util.List<ch.soreco.orderbook.enums.OrderType> getOrderTypeList()
  {
    return orderTypeList;
  }

  /**
   * Sets the field orderTypeList.
   * @param _orderTypeList the new value of the field orderTypeList.
   */
  public void setOrderTypeList(java.util.List<ch.soreco.orderbook.enums.OrderType> _orderTypeList)
  {
    orderTypeList = _orderTypeList;
  }

  /**
   * Liste der Auftragsarten dieses Auftragsbuches
   */
  @javax.persistence.Column(length=2500)
  private java.lang.String orderTypes;

  /**
   * Gets the field orderTypes.
   * @return the value of the field orderTypes; may be null.
   */
  public java.lang.String getOrderTypes()
  {
    return orderTypes;
  }

  /**
   * Sets the field orderTypes.
   * @param _orderTypes the new value of the field orderTypes.
   */
  public void setOrderTypes(java.lang.String _orderTypes)
  {
    orderTypes = _orderTypes;
  }

  private transient ch.soreco.orderbook.enums.OrderType orderType;

  /**
   * Gets the field orderType.
   * @return the value of the field orderType; may be null.
   */
  public ch.soreco.orderbook.enums.OrderType getOrderType()
  {
    return orderType;
  }

  /**
   * Sets the field orderType.
   * @param _orderType the new value of the field orderType.
   */
  public void setOrderType(ch.soreco.orderbook.enums.OrderType _orderType)
  {
    orderType = _orderType;
  }

  private transient java.util.List<ch.soreco.orderbook.bo.OrderBook> orderBooks;

  /**
   * Gets the field orderBooks.
   * @return the value of the field orderBooks; may be null.
   */
  public java.util.List<ch.soreco.orderbook.bo.OrderBook> getOrderBooks()
  {
    return orderBooks;
  }

  /**
   * Sets the field orderBooks.
   * @param _orderBooks the new value of the field orderBooks.
   */
  public void setOrderBooks(java.util.List<ch.soreco.orderbook.bo.OrderBook> _orderBooks)
  {
    orderBooks = _orderBooks;
  }

  /**
   * Übergeordnetes Auftragsbuch
   */
  @javax.persistence.OneToOne(cascade={javax.persistence.CascadeType.PERSIST, javax.persistence.CascadeType.MERGE}, fetch=javax.persistence.FetchType.EAGER, orphanRemoval=false)
  private ch.soreco.orderbook.bo.OrderBook parentOrderBook;

  /**
   * Gets the field parentOrderBook.
   * @return the value of the field parentOrderBook; may be null.
   */
  public ch.soreco.orderbook.bo.OrderBook getParentOrderBook()
  {
    return parentOrderBook;
  }

  /**
   * Sets the field parentOrderBook.
   * @param _parentOrderBook the new value of the field parentOrderBook.
   */
  public void setParentOrderBook(ch.soreco.orderbook.bo.OrderBook _parentOrderBook)
  {
    parentOrderBook = _parentOrderBook;
  }

  /**
   * Auftragsbuch erscheint aus Filter-Wert (nicht aktiv)
   */
  private java.lang.Boolean filterable;

  /**
   * Gets the field filterable.
   * @return the value of the field filterable; may be null.
   */
  public java.lang.Boolean getFilterable()
  {
    return filterable;
  }

  /**
   * Sets the field filterable.
   * @param _filterable the new value of the field filterable.
   */
  public void setFilterable(java.lang.Boolean _filterable)
  {
    filterable = _filterable;
  }

  /**
   * Auftragsbuch erscheint im ManualDispatching
   */
  private java.lang.Boolean officialTeam;

  /**
   * Gets the field officialTeam.
   * @return the value of the field officialTeam; may be null.
   */
  public java.lang.Boolean getOfficialTeam()
  {
    return officialTeam;
  }

  /**
   * Sets the field officialTeam.
   * @param _officialTeam the new value of the field officialTeam.
   */
  public void setOfficialTeam(java.lang.Boolean _officialTeam)
  {
    officialTeam = _officialTeam;
  }

  /**
   * Auftragsbuch erscheint als Unbestimmt Typ im ManualDispatching
   */
  private java.lang.Boolean indefiniteType;

  /**
   * Gets the field indefiniteType.
   * @return the value of the field indefiniteType; may be null.
   */
  public java.lang.Boolean getIndefiniteType()
  {
    return indefiniteType;
  }

  /**
   * Sets the field indefiniteType.
   * @param _indefiniteType the new value of the field indefiniteType.
   */
  public void setIndefiniteType(java.lang.Boolean _indefiniteType)
  {
    indefiniteType = _indefiniteType;
  }

  /**
   * Auftragsbuch erscheint in Statistik
   */
  private java.lang.Boolean analysable;

  /**
   * Gets the field analysable.
   * @return the value of the field analysable; may be null.
   */
  public java.lang.Boolean getAnalysable()
  {
    return analysable;
  }

  /**
   * Sets the field analysable.
   * @param _analysable the new value of the field analysable.
   */
  public void setAnalysable(java.lang.Boolean _analysable)
  {
    analysable = _analysable;
  }

  /**
   * Sortiersequenz
   */
  private java.lang.Integer sortSequence;

  /**
   * Gets the field sortSequence.
   * @return the value of the field sortSequence; may be null.
   */
  public java.lang.Integer getSortSequence()
  {
    return sortSequence;
  }

  /**
   * Sets the field sortSequence.
   * @param _sortSequence the new value of the field sortSequence.
   */
  public void setSortSequence(java.lang.Integer _sortSequence)
  {
    sortSequence = _sortSequence;
  }

  /**
   * Auftragsbuch erfordert Auswahl des Auftragsvolumens
   */
  private java.lang.Boolean requiresOrderQuantityType;

  /**
   * Gets the field requiresOrderQuantityType.
   * @return the value of the field requiresOrderQuantityType; may be null.
   */
  public java.lang.Boolean getRequiresOrderQuantityType()
  {
    return requiresOrderQuantityType;
  }

  /**
   * Sets the field requiresOrderQuantityType.
   * @param _requiresOrderQuantityType the new value of the field requiresOrderQuantityType.
   */
  public void setRequiresOrderQuantityType(java.lang.Boolean _requiresOrderQuantityType)
  {
    requiresOrderQuantityType = _requiresOrderQuantityType;
  }

}
