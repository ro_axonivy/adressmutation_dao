package ch.soreco.orderbook.bo;

/**
 * Speicherort der Scanning-Shares f�r Import und Archivierung
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class ScanShare", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="btbl_ScanShares")
public class ScanShare extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -6765510944977204997L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer id;

  /**
   * Gets the field id.
   * @return the value of the field id; may be null.
   */
  public java.lang.Integer getId()
  {
    return id;
  }

  /**
   * Sets the field id.
   * @param _id the new value of the field id.
   */
  public void setId(java.lang.Integer _id)
  {
    id = _id;
  }

  /**
   * Konfigurationsname
   */
  private java.lang.String ConfigName;

  /**
   * Gets the field ConfigName.
   * @return the value of the field ConfigName; may be null.
   */
  public java.lang.String getConfigName()
  {
    return ConfigName;
  }

  /**
   * Sets the field ConfigName.
   * @param _ConfigName the new value of the field ConfigName.
   */
  public void setConfigName(java.lang.String _ConfigName)
  {
    ConfigName = _ConfigName;
  }

  /**
   * Netzlaufwerk-Pfad
   */
  @javax.persistence.Column(length=500)
  private java.lang.String NetPath;

  /**
   * Gets the field NetPath.
   * @return the value of the field NetPath; may be null.
   */
  public java.lang.String getNetPath()
  {
    return NetPath;
  }

  /**
   * Sets the field NetPath.
   * @param _NetPath the new value of the field NetPath.
   */
  public void setNetPath(java.lang.String _NetPath)
  {
    NetPath = _NetPath;
  }

  /**
   * Datum des n�chsten Scannings
   */
  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateTimeUserType")
  private ch.ivyteam.ivy.scripting.objects.DateTime NextScan;

  /**
   * Gets the field NextScan.
   * @return the value of the field NextScan; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.DateTime getNextScan()
  {
    return NextScan;
  }

  /**
   * Sets the field NextScan.
   * @param _NextScan the new value of the field NextScan.
   */
  public void setNextScan(ch.ivyteam.ivy.scripting.objects.DateTime _NextScan)
  {
    NextScan = _NextScan;
  }

  /**
   * Scanningintervall
   */
  private java.lang.Integer ScanInterval;

  /**
   * Gets the field ScanInterval.
   * @return the value of the field ScanInterval; may be null.
   */
  public java.lang.Integer getScanInterval()
  {
    return ScanInterval;
  }

  /**
   * Sets the field ScanInterval.
   * @param _ScanInterval the new value of the field ScanInterval.
   */
  public void setScanInterval(java.lang.Integer _ScanInterval)
  {
    ScanInterval = _ScanInterval;
  }

  /**
   * Deadline bis zur Archivierung
   */
  private java.lang.Integer ArchiveDeadlineDays;

  /**
   * Gets the field ArchiveDeadlineDays.
   * @return the value of the field ArchiveDeadlineDays; may be null.
   */
  public java.lang.Integer getArchiveDeadlineDays()
  {
    return ArchiveDeadlineDays;
  }

  /**
   * Sets the field ArchiveDeadlineDays.
   * @param _ArchiveDeadlineDays the new value of the field ArchiveDeadlineDays.
   */
  public void setArchiveDeadlineDays(java.lang.Integer _ArchiveDeadlineDays)
  {
    ArchiveDeadlineDays = _ArchiveDeadlineDays;
  }

  /**
   * Archivierungsverzeichnis
   */
  private java.lang.String ArchiveFolder;

  /**
   * Gets the field ArchiveFolder.
   * @return the value of the field ArchiveFolder; may be null.
   */
  public java.lang.String getArchiveFolder()
  {
    return ArchiveFolder;
  }

  /**
   * Sets the field ArchiveFolder.
   * @param _ArchiveFolder the new value of the field ArchiveFolder.
   */
  public void setArchiveFolder(java.lang.String _ArchiveFolder)
  {
    ArchiveFolder = _ArchiveFolder;
  }

  /**
   * Datum der letzten Archivierung
   */
  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateTimeUserType")
  private ch.ivyteam.ivy.scripting.objects.DateTime LastArchiveScan;

  /**
   * Gets the field LastArchiveScan.
   * @return the value of the field LastArchiveScan; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.DateTime getLastArchiveScan()
  {
    return LastArchiveScan;
  }

  /**
   * Sets the field LastArchiveScan.
   * @param _LastArchiveScan the new value of the field LastArchiveScan.
   */
  public void setLastArchiveScan(ch.ivyteam.ivy.scripting.objects.DateTime _LastArchiveScan)
  {
    LastArchiveScan = _LastArchiveScan;
  }

  /**
   * Mapping Name f�r IMTF
   */
  private java.lang.String ScanNo;

  /**
   * Gets the field ScanNo.
   * @return the value of the field ScanNo; may be null.
   */
  public java.lang.String getScanNo()
  {
    return ScanNo;
  }

  /**
   * Sets the field ScanNo.
   * @param _ScanNo the new value of the field ScanNo.
   */
  public void setScanNo(java.lang.String _ScanNo)
  {
    ScanNo = _ScanNo;
  }

  /**
   * UserId f�r Zugriff auf Netzlaufwerk
   */
  private java.lang.String NetPathUID;

  /**
   * Gets the field NetPathUID.
   * @return the value of the field NetPathUID; may be null.
   */
  public java.lang.String getNetPathUID()
  {
    return NetPathUID;
  }

  /**
   * Sets the field NetPathUID.
   * @param _NetPathUID the new value of the field NetPathUID.
   */
  public void setNetPathUID(java.lang.String _NetPathUID)
  {
    NetPathUID = _NetPathUID;
  }

  /**
   * Passwort f�r Zugriff auf Netzlaufwerk
   */
  private java.lang.String NetPathPWD;

  /**
   * Gets the field NetPathPWD.
   * @return the value of the field NetPathPWD; may be null.
   */
  public java.lang.String getNetPathPWD()
  {
    return NetPathPWD;
  }

  /**
   * Sets the field NetPathPWD.
   * @param _NetPathPWD the new value of the field NetPathPWD.
   */
  public void setNetPathPWD(java.lang.String _NetPathPWD)
  {
    NetPathPWD = _NetPathPWD;
  }

}
