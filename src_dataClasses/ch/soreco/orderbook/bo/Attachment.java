package ch.soreco.orderbook.bo;

/**
 * An attachment maps attachments of scannings to file entity. Use Process "Functional Process/common/File#show(ch.soreco.common.bo.File filter)" to display it within browser. 
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Attachment", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_Attachment")
public class Attachment extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 2524220674785346550L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer attachmentId;

  /**
   * Gets the field attachmentId.
   * @return the value of the field attachmentId; may be null.
   */
  public java.lang.Integer getAttachmentId()
  {
    return attachmentId;
  }

  /**
   * Sets the field attachmentId.
   * @param _attachmentId the new value of the field attachmentId.
   */
  public void setAttachmentId(java.lang.Integer _attachmentId)
  {
    attachmentId = _attachmentId;
  }

  /**
   * Scanning Id - definiert das zugehörige Scanning
   */
  @javax.persistence.Column(name="scanningId", nullable=false, updatable=false)
  private java.lang.Integer scanningId;

  /**
   * Gets the field scanningId.
   * @return the value of the field scanningId; may be null.
   */
  public java.lang.Integer getScanningId()
  {
    return scanningId;
  }

  /**
   * Sets the field scanningId.
   * @param _scanningId the new value of the field scanningId.
   */
  public void setScanningId(java.lang.Integer _scanningId)
  {
    scanningId = _scanningId;
  }

  /**
   * File Entity
   */
  @javax.persistence.OneToOne(cascade={javax.persistence.CascadeType.PERSIST, javax.persistence.CascadeType.MERGE, javax.persistence.CascadeType.REMOVE}, fetch=javax.persistence.FetchType.EAGER, optional=false, orphanRemoval=false)
  @javax.persistence.JoinColumn(name="fileId", nullable=false, updatable=false)
  private ch.soreco.common.bo.File file;

  /**
   * Gets the field file.
   * @return the value of the field file; may be null.
   */
  public ch.soreco.common.bo.File getFile()
  {
    return file;
  }

  /**
   * Sets the field file.
   * @param _file the new value of the field file.
   */
  public void setFile(ch.soreco.common.bo.File _file)
  {
    file = _file;
  }

}
