package ch.soreco.orderbook.bo;

/**
 * Zus�tzliche Unterlagen angefordert in Adressmutation
 * n:1 Beziehung zum Contact / Kontakt
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Letter", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_Contact_Letters")
public class Letter extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 8234356835215864060L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer id;

  /**
   * Gets the field id.
   * @return the value of the field id; may be null.
   */
  public java.lang.Integer getId()
  {
    return id;
  }

  /**
   * Sets the field id.
   * @param _id the new value of the field id.
   */
  public void setId(java.lang.Integer _id)
  {
    id = _id;
  }

  /**
   * Kontakt Id
   */
  @javax.persistence.Column(name="contactId")
  private java.lang.Integer contactId;

  /**
   * Gets the field contactId.
   * @return the value of the field contactId; may be null.
   */
  public java.lang.Integer getContactId()
  {
    return contactId;
  }

  /**
   * Sets the field contactId.
   * @param _contactId the new value of the field contactId.
   */
  public void setContactId(java.lang.Integer _contactId)
  {
    contactId = _contactId;
  }

  /**
   * Brieftyp
   */
  private java.lang.String letterType;

  /**
   * Gets the field letterType.
   * @return the value of the field letterType; may be null.
   */
  public java.lang.String getLetterType()
  {
    return letterType;
  }

  /**
   * Sets the field letterType.
   * @param _letterType the new value of the field letterType.
   */
  public void setLetterType(java.lang.String _letterType)
  {
    letterType = _letterType;
  }

  /**
   * SubTyp bei Standartvorlage
   */
  private java.lang.String letterSubType;

  /**
   * Gets the field letterSubType.
   * @return the value of the field letterSubType; may be null.
   */
  public java.lang.String getLetterSubType()
  {
    return letterSubType;
  }

  /**
   * Sets the field letterSubType.
   * @param _letterSubType the new value of the field letterSubType.
   */
  public void setLetterSubType(java.lang.String _letterSubType)
  {
    letterSubType = _letterSubType;
  }

  /**
   * Order#
   */
  private java.lang.String letterOrderNo;

  /**
   * Gets the field letterOrderNo.
   * @return the value of the field letterOrderNo; may be null.
   */
  public java.lang.String getLetterOrderNo()
  {
    return letterOrderNo;
  }

  /**
   * Sets the field letterOrderNo.
   * @param _letterOrderNo the new value of the field letterOrderNo.
   */
  public void setLetterOrderNo(java.lang.String _letterOrderNo)
  {
    letterOrderNo = _letterOrderNo;
  }

  /**
   * Brief gepr�ft
   */
  private java.lang.Boolean letterIsChecked;

  /**
   * Gets the field letterIsChecked.
   * @return the value of the field letterIsChecked; may be null.
   */
  public java.lang.Boolean getLetterIsChecked()
  {
    return letterIsChecked;
  }

  /**
   * Sets the field letterIsChecked.
   * @param _letterIsChecked the new value of the field letterIsChecked.
   */
  public void setLetterIsChecked(java.lang.Boolean _letterIsChecked)
  {
    letterIsChecked = _letterIsChecked;
  }

  /**
   * Pr�fer UserId
   */
  private java.lang.String letterIsCheckedBy;

  /**
   * Gets the field letterIsCheckedBy.
   * @return the value of the field letterIsCheckedBy; may be null.
   */
  public java.lang.String getLetterIsCheckedBy()
  {
    return letterIsCheckedBy;
  }

  /**
   * Sets the field letterIsCheckedBy.
   * @param _letterIsCheckedBy the new value of the field letterIsCheckedBy.
   */
  public void setLetterIsCheckedBy(java.lang.String _letterIsCheckedBy)
  {
    letterIsCheckedBy = _letterIsCheckedBy;
  }

}
