package ch.soreco.orderbook.bo;

/**
 * Korrespondenzcontainer
 * n:1 Beziehung zu Correspondence / Korrespondenz
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class CorrespondenceContainer", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_Recording_CorrespondenceContainer")
public class CorrespondenceContainer extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -2432206215213838354L;

  @javax.persistence.ManyToOne(cascade={javax.persistence.CascadeType.PERSIST, javax.persistence.CascadeType.MERGE}, fetch=javax.persistence.FetchType.EAGER)
  @javax.persistence.JoinColumn(name="correspondenceId")
  private ch.soreco.orderbook.bo.Correspondence correspondence;

  /**
   * Gets the field correspondence.
   * @return the value of the field correspondence; may be null.
   */
  public ch.soreco.orderbook.bo.Correspondence getCorrespondence()
  {
    return correspondence;
  }

  /**
   * Sets the field correspondence.
   * @param _correspondence the new value of the field correspondence.
   */
  public void setCorrespondence(ch.soreco.orderbook.bo.Correspondence _correspondence)
  {
    correspondence = _correspondence;
  }

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer correspondenceContainerId;

  /**
   * Gets the field correspondenceContainerId.
   * @return the value of the field correspondenceContainerId; may be null.
   */
  public java.lang.Integer getCorrespondenceContainerId()
  {
    return correspondenceContainerId;
  }

  /**
   * Sets the field correspondenceContainerId.
   * @param _correspondenceContainerId the new value of the field correspondenceContainerId.
   */
  public void setCorrespondenceContainerId(java.lang.Integer _correspondenceContainerId)
  {
    correspondenceContainerId = _correspondenceContainerId;
  }

  /**
   * Korrespondenzadresse einf�gen
   */
  private java.lang.Boolean change;

  /**
   * Gets the field change.
   * @return the value of the field change; may be null.
   */
  public java.lang.Boolean getChange()
  {
    return change;
  }

  /**
   * Sets the field change.
   * @param _change the new value of the field change.
   */
  public void setChange(java.lang.Boolean _change)
  {
    change = _change;
  }

  /**
   * Spedit/Banque Rest.
   */
  private java.lang.Boolean spedit;

  /**
   * Gets the field spedit.
   * @return the value of the field spedit; may be null.
   */
  public java.lang.Boolean getSpedit()
  {
    return spedit;
  }

  /**
   * Sets the field spedit.
   * @param _spedit the new value of the field spedit.
   */
  public void setSpedit(java.lang.Boolean _spedit)
  {
    spedit = _spedit;
  }

  /**
   * Order# des Containers
   */
  private java.lang.String containerOrderNo;

  /**
   * Gets the field containerOrderNo.
   * @return the value of the field containerOrderNo; may be null.
   */
  public java.lang.String getContainerOrderNo()
  {
    return containerOrderNo;
  }

  /**
   * Sets the field containerOrderNo.
   * @param _containerOrderNo the new value of the field containerOrderNo.
   */
  public void setContainerOrderNo(java.lang.String _containerOrderNo)
  {
    containerOrderNo = _containerOrderNo;
  }

  /**
   * Container gepr�ft
   */
  private java.lang.Boolean containerIsChecked;

  /**
   * Gets the field containerIsChecked.
   * @return the value of the field containerIsChecked; may be null.
   */
  public java.lang.Boolean getContainerIsChecked()
  {
    return containerIsChecked;
  }

  /**
   * Sets the field containerIsChecked.
   * @param _containerIsChecked the new value of the field containerIsChecked.
   */
  public void setContainerIsChecked(java.lang.Boolean _containerIsChecked)
  {
    containerIsChecked = _containerIsChecked;
  }

  /**
   * Pr�fer UserId
   */
  private java.lang.String containerCheckedBy;

  /**
   * Gets the field containerCheckedBy.
   * @return the value of the field containerCheckedBy; may be null.
   */
  public java.lang.String getContainerCheckedBy()
  {
    return containerCheckedBy;
  }

  /**
   * Sets the field containerCheckedBy.
   * @param _containerCheckedBy the new value of the field containerCheckedBy.
   */
  public void setContainerCheckedBy(java.lang.String _containerCheckedBy)
  {
    containerCheckedBy = _containerCheckedBy;
  }

}
