package ch.soreco.orderbook.bo;

/**
 * This is the container to log activities in relation of sensitive customer data - especially view of PDF's, comments and exports of lists and search results.
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class OrderAuditEvent", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_Logs_OrderAuditEvents")
public class OrderAuditEvent extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 4013154216158195917L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Long id;

  /**
   * Gets the field id.
   * @return the value of the field id; may be null.
   */
  public java.lang.Long getId()
  {
    return id;
  }

  /**
   * Sets the field id.
   * @param _id the new value of the field id.
   */
  public void setId(java.lang.Long _id)
  {
    id = _id;
  }

  /**
   * when the event occurred.
   */
  @javax.persistence.Column(nullable=false, updatable=false)
  private java.util.Date eventDate;

  /**
   * Gets the field eventDate.
   * @return the value of the field eventDate; may be null.
   */
  public java.util.Date getEventDate()
  {
    return eventDate;
  }

  /**
   * Sets the field eventDate.
   * @param _eventDate the new value of the field eventDate.
   */
  public void setEventDate(java.util.Date _eventDate)
  {
    eventDate = _eventDate;
  }

  /**
   * the type of the event.
   */
  @javax.persistence.Column(nullable=false, updatable=false, length=50)
  private java.lang.String eventType;

  /**
   * Gets the field eventType.
   * @return the value of the field eventType; may be null.
   */
  public java.lang.String getEventType()
  {
    return eventType;
  }

  /**
   * Sets the field eventType.
   * @param _eventType the new value of the field eventType.
   */
  public void setEventType(java.lang.String _eventType)
  {
    eventType = _eventType;
  }

  /**
   * the order id to which the event applies.
   */
  @javax.persistence.Column(nullable=false, updatable=false)
  private java.lang.Long orderId;

  /**
   * Gets the field orderId.
   * @return the value of the field orderId; may be null.
   */
  public java.lang.Long getOrderId()
  {
    return orderId;
  }

  /**
   * Sets the field orderId.
   * @param _orderId the new value of the field orderId.
   */
  public void setOrderId(java.lang.Long _orderId)
  {
    orderId = _orderId;
  }

  /**
   * the user which raised the event
   */
  @javax.persistence.Column(nullable=false, updatable=false, length=50)
  private java.lang.String userId;

  /**
   * Gets the field userId.
   * @return the value of the field userId; may be null.
   */
  public java.lang.String getUserId()
  {
    return userId;
  }

  /**
   * Sets the field userId.
   * @param _userId the new value of the field userId.
   */
  public void setUserId(java.lang.String _userId)
  {
    userId = _userId;
  }

  /**
   * the BP Id (Business Partner Id) applied to the order. May be null
   */
  @javax.persistence.Column(updatable=false, length=255)
  private java.lang.String bpId;

  /**
   * Gets the field bpId.
   * @return the value of the field bpId; may be null.
   */
  public java.lang.String getBpId()
  {
    return bpId;
  }

  /**
   * Sets the field bpId.
   * @param _bpId the new value of the field bpId.
   */
  public void setBpId(java.lang.String _bpId)
  {
    bpId = _bpId;
  }

}
