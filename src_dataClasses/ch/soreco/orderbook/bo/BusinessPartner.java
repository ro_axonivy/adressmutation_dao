package ch.soreco.orderbook.bo;

/**
 * Business Partner / Gesch�fts Partner in Adressmutation.
 * Felder entsprechen den Checkboxen auf dem UI
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class BusinessPartner", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_Request_BusinessPartner")
public class BusinessPartner extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 339386468622465036L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer businessPartnerId;

  /**
   * Gets the field businessPartnerId.
   * @return the value of the field businessPartnerId; may be null.
   */
  public java.lang.Integer getBusinessPartnerId()
  {
    return businessPartnerId;
  }

  /**
   * Sets the field businessPartnerId.
   * @param _businessPartnerId the new value of the field businessPartnerId.
   */
  public void setBusinessPartnerId(java.lang.Integer _businessPartnerId)
  {
    businessPartnerId = _businessPartnerId;
  }

  private java.lang.Boolean domicile;

  /**
   * Gets the field domicile.
   * @return the value of the field domicile; may be null.
   */
  public java.lang.Boolean getDomicile()
  {
    return domicile;
  }

  /**
   * Sets the field domicile.
   * @param _domicile the new value of the field domicile.
   */
  public void setDomicile(java.lang.Boolean _domicile)
  {
    domicile = _domicile;
  }

  private java.lang.Boolean taxResid;

  /**
   * Gets the field taxResid.
   * @return the value of the field taxResid; may be null.
   */
  public java.lang.Boolean getTaxResid()
  {
    return taxResid;
  }

  /**
   * Sets the field taxResid.
   * @param _taxResid the new value of the field taxResid.
   */
  public void setTaxResid(java.lang.Boolean _taxResid)
  {
    taxResid = _taxResid;
  }

  private java.lang.Boolean taxStatement;

  /**
   * Gets the field taxStatement.
   * @return the value of the field taxStatement; may be null.
   */
  public java.lang.Boolean getTaxStatement()
  {
    return taxStatement;
  }

  /**
   * Sets the field taxStatement.
   * @param _taxStatement the new value of the field taxStatement.
   */
  public void setTaxStatement(java.lang.Boolean _taxStatement)
  {
    taxStatement = _taxStatement;
  }

  private java.lang.Boolean EUSDCountry;

  /**
   * Gets the field EUSDCountry.
   * @return the value of the field EUSDCountry; may be null.
   */
  public java.lang.Boolean getEUSDCountry()
  {
    return EUSDCountry;
  }

  /**
   * Sets the field EUSDCountry.
   * @param _EUSDCountry the new value of the field EUSDCountry.
   */
  public void setEUSDCountry(java.lang.Boolean _EUSDCountry)
  {
    EUSDCountry = _EUSDCountry;
  }

  private java.lang.Boolean EUSDRetention;

  /**
   * Gets the field EUSDRetention.
   * @return the value of the field EUSDRetention; may be null.
   */
  public java.lang.Boolean getEUSDRetention()
  {
    return EUSDRetention;
  }

  /**
   * Sets the field EUSDRetention.
   * @param _EUSDRetention the new value of the field EUSDRetention.
   */
  public void setEUSDRetention(java.lang.Boolean _EUSDRetention)
  {
    EUSDRetention = _EUSDRetention;
  }

  private java.lang.Boolean telNr;

  /**
   * Gets the field telNr.
   * @return the value of the field telNr; may be null.
   */
  public java.lang.Boolean getTelNr()
  {
    return telNr;
  }

  /**
   * Sets the field telNr.
   * @param _telNr the new value of the field telNr.
   */
  public void setTelNr(java.lang.Boolean _telNr)
  {
    telNr = _telNr;
  }

  /**
   * BP. Under Observation
   */
  private java.lang.Boolean BP;

  /**
   * Gets the field BP.
   * @return the value of the field BP; may be null.
   */
  public java.lang.Boolean getBP()
  {
    return BP;
  }

  /**
   * Sets the field BP.
   * @param _BP the new value of the field BP.
   */
  public void setBP(java.lang.Boolean _BP)
  {
    BP = _BP;
  }

  /**
   * Business Partner gepr�ft
   */
  private java.lang.Boolean businessPartnerIsChecked;

  /**
   * Gets the field businessPartnerIsChecked.
   * @return the value of the field businessPartnerIsChecked; may be null.
   */
  public java.lang.Boolean getBusinessPartnerIsChecked()
  {
    return businessPartnerIsChecked;
  }

  /**
   * Sets the field businessPartnerIsChecked.
   * @param _businessPartnerIsChecked the new value of the field businessPartnerIsChecked.
   */
  public void setBusinessPartnerIsChecked(java.lang.Boolean _businessPartnerIsChecked)
  {
    businessPartnerIsChecked = _businessPartnerIsChecked;
  }

  /**
   * Pr�fer UserId
   */
  private java.lang.String businessPartnerCheckedBy;

  /**
   * Gets the field businessPartnerCheckedBy.
   * @return the value of the field businessPartnerCheckedBy; may be null.
   */
  public java.lang.String getBusinessPartnerCheckedBy()
  {
    return businessPartnerCheckedBy;
  }

  /**
   * Sets the field businessPartnerCheckedBy.
   * @param _businessPartnerCheckedBy the new value of the field businessPartnerCheckedBy.
   */
  public void setBusinessPartnerCheckedBy(java.lang.String _businessPartnerCheckedBy)
  {
    businessPartnerCheckedBy = _businessPartnerCheckedBy;
  }

  /**
   * Order#
   */
  private java.lang.String bpOrderNo;

  /**
   * Gets the field bpOrderNo.
   * @return the value of the field bpOrderNo; may be null.
   */
  public java.lang.String getBpOrderNo()
  {
    return bpOrderNo;
  }

  /**
   * Sets the field bpOrderNo.
   * @param _bpOrderNo the new value of the field bpOrderNo.
   */
  public void setBpOrderNo(java.lang.String _bpOrderNo)
  {
    bpOrderNo = _bpOrderNo;
  }

}
