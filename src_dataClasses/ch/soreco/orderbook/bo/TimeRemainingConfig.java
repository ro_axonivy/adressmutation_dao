package ch.soreco.orderbook.bo;

/**
 * Deprecated
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class TimeRemainingConfig", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="btbl_TimeRemainingConfig")
public class TimeRemainingConfig extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 5638022339798835577L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer TimeRemainingConfigId;

  /**
   * Gets the field TimeRemainingConfigId.
   * @return the value of the field TimeRemainingConfigId; may be null.
   */
  public java.lang.Integer getTimeRemainingConfigId()
  {
    return TimeRemainingConfigId;
  }

  /**
   * Sets the field TimeRemainingConfigId.
   * @param _TimeRemainingConfigId the new value of the field TimeRemainingConfigId.
   */
  public void setTimeRemainingConfigId(java.lang.Integer _TimeRemainingConfigId)
  {
    TimeRemainingConfigId = _TimeRemainingConfigId;
  }

  private java.lang.String orderType;

  /**
   * Gets the field orderType.
   * @return the value of the field orderType; may be null.
   */
  public java.lang.String getOrderType()
  {
    return orderType;
  }

  /**
   * Sets the field orderType.
   * @param _orderType the new value of the field orderType.
   */
  public void setOrderType(java.lang.String _orderType)
  {
    orderType = _orderType;
  }

  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.TimeUserType")
  private ch.ivyteam.ivy.scripting.objects.Time minRecordingTime;

  /**
   * Gets the field minRecordingTime.
   * @return the value of the field minRecordingTime; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Time getMinRecordingTime()
  {
    return minRecordingTime;
  }

  /**
   * Sets the field minRecordingTime.
   * @param _minRecordingTime the new value of the field minRecordingTime.
   */
  public void setMinRecordingTime(ch.ivyteam.ivy.scripting.objects.Time _minRecordingTime)
  {
    minRecordingTime = _minRecordingTime;
  }

  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.TimeUserType")
  private ch.ivyteam.ivy.scripting.objects.Time dayStartsAt;

  /**
   * Gets the field dayStartsAt.
   * @return the value of the field dayStartsAt; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Time getDayStartsAt()
  {
    return dayStartsAt;
  }

  /**
   * Sets the field dayStartsAt.
   * @param _dayStartsAt the new value of the field dayStartsAt.
   */
  public void setDayStartsAt(ch.ivyteam.ivy.scripting.objects.Time _dayStartsAt)
  {
    dayStartsAt = _dayStartsAt;
  }

  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.TimeUserType")
  private ch.ivyteam.ivy.scripting.objects.Time dayEndsAt;

  /**
   * Gets the field dayEndsAt.
   * @return the value of the field dayEndsAt; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Time getDayEndsAt()
  {
    return dayEndsAt;
  }

  /**
   * Sets the field dayEndsAt.
   * @param _dayEndsAt the new value of the field dayEndsAt.
   */
  public void setDayEndsAt(ch.ivyteam.ivy.scripting.objects.Time _dayEndsAt)
  {
    dayEndsAt = _dayEndsAt;
  }

  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.TimeUserType")
  private ch.ivyteam.ivy.scripting.objects.Time incomeOverflowsAt;

  /**
   * Gets the field incomeOverflowsAt.
   * @return the value of the field incomeOverflowsAt; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Time getIncomeOverflowsAt()
  {
    return incomeOverflowsAt;
  }

  /**
   * Sets the field incomeOverflowsAt.
   * @param _incomeOverflowsAt the new value of the field incomeOverflowsAt.
   */
  public void setIncomeOverflowsAt(ch.ivyteam.ivy.scripting.objects.Time _incomeOverflowsAt)
  {
    incomeOverflowsAt = _incomeOverflowsAt;
  }

  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.TimeUserType")
  private ch.ivyteam.ivy.scripting.objects.Time incomeOverflowDayEndAt;

  /**
   * Gets the field incomeOverflowDayEndAt.
   * @return the value of the field incomeOverflowDayEndAt; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Time getIncomeOverflowDayEndAt()
  {
    return incomeOverflowDayEndAt;
  }

  /**
   * Sets the field incomeOverflowDayEndAt.
   * @param _incomeOverflowDayEndAt the new value of the field incomeOverflowDayEndAt.
   */
  public void setIncomeOverflowDayEndAt(ch.ivyteam.ivy.scripting.objects.Time _incomeOverflowDayEndAt)
  {
    incomeOverflowDayEndAt = _incomeOverflowDayEndAt;
  }

  private java.lang.Integer minRecordingDuration;

  /**
   * Gets the field minRecordingDuration.
   * @return the value of the field minRecordingDuration; may be null.
   */
  public java.lang.Integer getMinRecordingDuration()
  {
    return minRecordingDuration;
  }

  /**
   * Sets the field minRecordingDuration.
   * @param _minRecordingDuration the new value of the field minRecordingDuration.
   */
  public void setMinRecordingDuration(java.lang.Integer _minRecordingDuration)
  {
    minRecordingDuration = _minRecordingDuration;
  }

  private java.lang.Integer dayStartsAtDuration;

  /**
   * Gets the field dayStartsAtDuration.
   * @return the value of the field dayStartsAtDuration; may be null.
   */
  public java.lang.Integer getDayStartsAtDuration()
  {
    return dayStartsAtDuration;
  }

  /**
   * Sets the field dayStartsAtDuration.
   * @param _dayStartsAtDuration the new value of the field dayStartsAtDuration.
   */
  public void setDayStartsAtDuration(java.lang.Integer _dayStartsAtDuration)
  {
    dayStartsAtDuration = _dayStartsAtDuration;
  }

  private java.lang.Integer dayEndsAtDuration;

  /**
   * Gets the field dayEndsAtDuration.
   * @return the value of the field dayEndsAtDuration; may be null.
   */
  public java.lang.Integer getDayEndsAtDuration()
  {
    return dayEndsAtDuration;
  }

  /**
   * Sets the field dayEndsAtDuration.
   * @param _dayEndsAtDuration the new value of the field dayEndsAtDuration.
   */
  public void setDayEndsAtDuration(java.lang.Integer _dayEndsAtDuration)
  {
    dayEndsAtDuration = _dayEndsAtDuration;
  }

  private java.lang.Integer incomeOverflowsAtDuration;

  /**
   * Gets the field incomeOverflowsAtDuration.
   * @return the value of the field incomeOverflowsAtDuration; may be null.
   */
  public java.lang.Integer getIncomeOverflowsAtDuration()
  {
    return incomeOverflowsAtDuration;
  }

  /**
   * Sets the field incomeOverflowsAtDuration.
   * @param _incomeOverflowsAtDuration the new value of the field incomeOverflowsAtDuration.
   */
  public void setIncomeOverflowsAtDuration(java.lang.Integer _incomeOverflowsAtDuration)
  {
    incomeOverflowsAtDuration = _incomeOverflowsAtDuration;
  }

  private java.lang.Integer incomeOverflowDayEndAtDuration;

  /**
   * Gets the field incomeOverflowDayEndAtDuration.
   * @return the value of the field incomeOverflowDayEndAtDuration; may be null.
   */
  public java.lang.Integer getIncomeOverflowDayEndAtDuration()
  {
    return incomeOverflowDayEndAtDuration;
  }

  /**
   * Sets the field incomeOverflowDayEndAtDuration.
   * @param _incomeOverflowDayEndAtDuration the new value of the field incomeOverflowDayEndAtDuration.
   */
  public void setIncomeOverflowDayEndAtDuration(java.lang.Integer _incomeOverflowDayEndAtDuration)
  {
    incomeOverflowDayEndAtDuration = _incomeOverflowDayEndAtDuration;
  }

}
