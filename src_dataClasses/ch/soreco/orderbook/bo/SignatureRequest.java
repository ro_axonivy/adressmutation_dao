package ch.soreco.orderbook.bo;

/**
 * Brief: Unterschrift angefordert in Adressmutation
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class SignatureRequest", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_Contact_SignatureRequest")
public class SignatureRequest extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 2212905437091226437L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer signatureRequestId;

  /**
   * Gets the field signatureRequestId.
   * @return the value of the field signatureRequestId; may be null.
   */
  public java.lang.Integer getSignatureRequestId()
  {
    return signatureRequestId;
  }

  /**
   * Sets the field signatureRequestId.
   * @param _signatureRequestId the new value of the field signatureRequestId.
   */
  public void setSignatureRequestId(java.lang.Integer _signatureRequestId)
  {
    signatureRequestId = _signatureRequestId;
  }

  /**
   * Unterschrift angefordert
   */
  private java.lang.Boolean isRequested;

  /**
   * Gets the field isRequested.
   * @return the value of the field isRequested; may be null.
   */
  public java.lang.Boolean getIsRequested()
  {
    return isRequested;
  }

  /**
   * Sets the field isRequested.
   * @param _isRequested the new value of the field isRequested.
   */
  public void setIsRequested(java.lang.Boolean _isRequested)
  {
    isRequested = _isRequested;
  }

  /**
   * Datum der Anforderung
   */
  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateUserType")
  private ch.ivyteam.ivy.scripting.objects.Date signatureRequestDate;

  /**
   * Gets the field signatureRequestDate.
   * @return the value of the field signatureRequestDate; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Date getSignatureRequestDate()
  {
    return signatureRequestDate;
  }

  /**
   * Sets the field signatureRequestDate.
   * @param _signatureRequestDate the new value of the field signatureRequestDate.
   */
  public void setSignatureRequestDate(ch.ivyteam.ivy.scripting.objects.Date _signatureRequestDate)
  {
    signatureRequestDate = _signatureRequestDate;
  }

  /**
   * Unterschrift hat Order#
   */
  private java.lang.Boolean signatureNeedsOrderNo;

  /**
   * Gets the field signatureNeedsOrderNo.
   * @return the value of the field signatureNeedsOrderNo; may be null.
   */
  public java.lang.Boolean getSignatureNeedsOrderNo()
  {
    return signatureNeedsOrderNo;
  }

  /**
   * Sets the field signatureNeedsOrderNo.
   * @param _signatureNeedsOrderNo the new value of the field signatureNeedsOrderNo.
   */
  public void setSignatureNeedsOrderNo(java.lang.Boolean _signatureNeedsOrderNo)
  {
    signatureNeedsOrderNo = _signatureNeedsOrderNo;
  }

  /**
   * Order#
   */
  private java.lang.String signatureOrderNo;

  /**
   * Gets the field signatureOrderNo.
   * @return the value of the field signatureOrderNo; may be null.
   */
  public java.lang.String getSignatureOrderNo()
  {
    return signatureOrderNo;
  }

  /**
   * Sets the field signatureOrderNo.
   * @param _signatureOrderNo the new value of the field signatureOrderNo.
   */
  public void setSignatureOrderNo(java.lang.String _signatureOrderNo)
  {
    signatureOrderNo = _signatureOrderNo;
  }

  /**
   * Unterschrift anfordern gepr�ft
   */
  private java.lang.Boolean signatureIsChecked;

  /**
   * Gets the field signatureIsChecked.
   * @return the value of the field signatureIsChecked; may be null.
   */
  public java.lang.Boolean getSignatureIsChecked()
  {
    return signatureIsChecked;
  }

  /**
   * Sets the field signatureIsChecked.
   * @param _signatureIsChecked the new value of the field signatureIsChecked.
   */
  public void setSignatureIsChecked(java.lang.Boolean _signatureIsChecked)
  {
    signatureIsChecked = _signatureIsChecked;
  }

  /**
   * Pr�fer UserId
   */
  private java.lang.String signatureCheckedBy;

  /**
   * Gets the field signatureCheckedBy.
   * @return the value of the field signatureCheckedBy; may be null.
   */
  public java.lang.String getSignatureCheckedBy()
  {
    return signatureCheckedBy;
  }

  /**
   * Sets the field signatureCheckedBy.
   * @param _signatureCheckedBy the new value of the field signatureCheckedBy.
   */
  public void setSignatureCheckedBy(java.lang.String _signatureCheckedBy)
  {
    signatureCheckedBy = _signatureCheckedBy;
  }

  /**
   * Id des Contact / Kontakt Eintrags
   */
  @javax.persistence.Column(name="contactId")
  private java.lang.Integer contactId;

  /**
   * Gets the field contactId.
   * @return the value of the field contactId; may be null.
   */
  public java.lang.Integer getContactId()
  {
    return contactId;
  }

  /**
   * Sets the field contactId.
   * @param _contactId the new value of the field contactId.
   */
  public void setContactId(java.lang.Integer _contactId)
  {
    contactId = _contactId;
  }

  /**
   * OrderId
   */
  private java.lang.Integer SignatureRequestOrderId;

  /**
   * Gets the field SignatureRequestOrderId.
   * @return the value of the field SignatureRequestOrderId; may be null.
   */
  public java.lang.Integer getSignatureRequestOrderId()
  {
    return SignatureRequestOrderId;
  }

  /**
   * Sets the field SignatureRequestOrderId.
   * @param _SignatureRequestOrderId the new value of the field SignatureRequestOrderId.
   */
  public void setSignatureRequestOrderId(java.lang.Integer _SignatureRequestOrderId)
  {
    SignatureRequestOrderId = _SignatureRequestOrderId;
  }

  /**
   * Neue Anforderung
   */
  private java.lang.String newSignatureRequest;

  /**
   * Gets the field newSignatureRequest.
   * @return the value of the field newSignatureRequest; may be null.
   */
  public java.lang.String getNewSignatureRequest()
  {
    return newSignatureRequest;
  }

  /**
   * Sets the field newSignatureRequest.
   * @param _newSignatureRequest the new value of the field newSignatureRequest.
   */
  public void setNewSignatureRequest(java.lang.String _newSignatureRequest)
  {
    newSignatureRequest = _newSignatureRequest;
  }

}
