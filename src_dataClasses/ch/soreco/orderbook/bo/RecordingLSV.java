package ch.soreco.orderbook.bo;

/**
 * LSV in Adressmutation
 * n:1 Beziehung zu Recording / Erfassung
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class RecordingLSV", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_Recording_LSV")
public class RecordingLSV extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -2094979143300761331L;

  /**
   * Id des Recording / Erfassungs Eintrags
   */
  @javax.persistence.ManyToOne(cascade={javax.persistence.CascadeType.PERSIST, javax.persistence.CascadeType.MERGE}, fetch=javax.persistence.FetchType.EAGER)
  @javax.persistence.JoinColumn(name="recordingId")
  private ch.soreco.orderbook.bo.Recording recording;

  /**
   * Gets the field recording.
   * @return the value of the field recording; may be null.
   */
  public ch.soreco.orderbook.bo.Recording getRecording()
  {
    return recording;
  }

  /**
   * Sets the field recording.
   * @param _recording the new value of the field recording.
   */
  public void setRecording(ch.soreco.orderbook.bo.Recording _recording)
  {
    recording = _recording;
  }

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer lsvId;

  /**
   * Gets the field lsvId.
   * @return the value of the field lsvId; may be null.
   */
  public java.lang.Integer getLsvId()
  {
    return lsvId;
  }

  /**
   * Sets the field lsvId.
   * @param _lsvId the new value of the field lsvId.
   */
  public void setLsvId(java.lang.Integer _lsvId)
  {
    lsvId = _lsvId;
  }

  /**
   * LSV Flag
   */
  private java.lang.Boolean isLSV;

  /**
   * Gets the field isLSV.
   * @return the value of the field isLSV; may be null.
   */
  public java.lang.Boolean getIsLSV()
  {
    return isLSV;
  }

  /**
   * Sets the field isLSV.
   * @param _isLSV the new value of the field isLSV.
   */
  public void setIsLSV(java.lang.Boolean _isLSV)
  {
    isLSV = _isLSV;
  }

  /**
   * Order#
   */
  private java.lang.String lsvOrderNo;

  /**
   * Gets the field lsvOrderNo.
   * @return the value of the field lsvOrderNo; may be null.
   */
  public java.lang.String getLsvOrderNo()
  {
    return lsvOrderNo;
  }

  /**
   * Sets the field lsvOrderNo.
   * @param _lsvOrderNo the new value of the field lsvOrderNo.
   */
  public void setLsvOrderNo(java.lang.String _lsvOrderNo)
  {
    lsvOrderNo = _lsvOrderNo;
  }

  /**
   * LSV gepr�ft
   */
  private java.lang.Boolean LSVIsChecked;

  /**
   * Gets the field LSVIsChecked.
   * @return the value of the field LSVIsChecked; may be null.
   */
  public java.lang.Boolean getLSVIsChecked()
  {
    return LSVIsChecked;
  }

  /**
   * Sets the field LSVIsChecked.
   * @param _LSVIsChecked the new value of the field LSVIsChecked.
   */
  public void setLSVIsChecked(java.lang.Boolean _LSVIsChecked)
  {
    LSVIsChecked = _LSVIsChecked;
  }

  /**
   * Pr�fer UserId
   */
  private java.lang.String LSVCheckedBy;

  /**
   * Gets the field LSVCheckedBy.
   * @return the value of the field LSVCheckedBy; may be null.
   */
  public java.lang.String getLSVCheckedBy()
  {
    return LSVCheckedBy;
  }

  /**
   * Sets the field LSVCheckedBy.
   * @param _LSVCheckedBy the new value of the field LSVCheckedBy.
   */
  public void setLSVCheckedBy(java.lang.String _LSVCheckedBy)
  {
    LSVCheckedBy = _LSVCheckedBy;
  }

}
