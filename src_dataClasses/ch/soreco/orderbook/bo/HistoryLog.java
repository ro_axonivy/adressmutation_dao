package ch.soreco.orderbook.bo;

/**
 * Deprecated
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class HistoryLog", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_History")
public class HistoryLog extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 3633780955953636852L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer id;

  /**
   * Gets the field id.
   * @return the value of the field id; may be null.
   */
  public java.lang.Integer getId()
  {
    return id;
  }

  /**
   * Sets the field id.
   * @param _id the new value of the field id.
   */
  public void setId(java.lang.Integer _id)
  {
    id = _id;
  }

  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateTimeUserType")
  @javax.persistence.Column(updatable=false, insertable=false)
  private ch.ivyteam.ivy.scripting.objects.DateTime logDate;

  /**
   * Gets the field logDate.
   * @return the value of the field logDate; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.DateTime getLogDate()
  {
    return logDate;
  }

  /**
   * Sets the field logDate.
   * @param _logDate the new value of the field logDate.
   */
  public void setLogDate(ch.ivyteam.ivy.scripting.objects.DateTime _logDate)
  {
    logDate = _logDate;
  }

  @javax.persistence.Column(updatable=false, insertable=false)
  private java.lang.Integer orderId;

  /**
   * Gets the field orderId.
   * @return the value of the field orderId; may be null.
   */
  public java.lang.Integer getOrderId()
  {
    return orderId;
  }

  /**
   * Sets the field orderId.
   * @param _orderId the new value of the field orderId.
   */
  public void setOrderId(java.lang.Integer _orderId)
  {
    orderId = _orderId;
  }

  @javax.persistence.Column(updatable=false, insertable=false)
  private java.lang.Integer typeId;

  /**
   * Gets the field typeId.
   * @return the value of the field typeId; may be null.
   */
  public java.lang.Integer getTypeId()
  {
    return typeId;
  }

  /**
   * Sets the field typeId.
   * @param _typeId the new value of the field typeId.
   */
  public void setTypeId(java.lang.Integer _typeId)
  {
    typeId = _typeId;
  }

  @javax.persistence.Column(updatable=false, insertable=false)
  private java.lang.String description;

  /**
   * Gets the field description.
   * @return the value of the field description; may be null.
   */
  public java.lang.String getDescription()
  {
    return description;
  }

  /**
   * Sets the field description.
   * @param _description the new value of the field description.
   */
  public void setDescription(java.lang.String _description)
  {
    description = _description;
  }

  @javax.persistence.Column(updatable=false, insertable=false)
  private java.lang.String newValue;

  /**
   * Gets the field newValue.
   * @return the value of the field newValue; may be null.
   */
  public java.lang.String getNewValue()
  {
    return newValue;
  }

  /**
   * Sets the field newValue.
   * @param _newValue the new value of the field newValue.
   */
  public void setNewValue(java.lang.String _newValue)
  {
    newValue = _newValue;
  }

}
