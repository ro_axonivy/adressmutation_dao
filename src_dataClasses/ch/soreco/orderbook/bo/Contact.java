package ch.soreco.orderbook.bo;

/**
 * Kontakt
 * n:1 Beziehung zum Auftrag
 * 1:n Beziehung zum Paper / Zus. Unterlagen, SignatureRequests / Unterschrift angefordert und Letters / Briefen
 * 1:1 Bezieung zum AdressCheck / Adresse gepr�ft
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Contact", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_Contacts")
public class Contact extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 5867126315630467287L;

  @javax.persistence.Column(name="orderId", updatable=false)
  private java.lang.Integer contactOrderId;

  /**
   * Gets the field contactOrderId.
   * @return the value of the field contactOrderId; may be null.
   */
  public java.lang.Integer getContactOrderId()
  {
    return contactOrderId;
  }

  /**
   * Sets the field contactOrderId.
   * @param _contactOrderId the new value of the field contactOrderId.
   */
  public void setContactOrderId(java.lang.Integer _contactOrderId)
  {
    contactOrderId = _contactOrderId;
  }

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer contactId;

  /**
   * Gets the field contactId.
   * @return the value of the field contactId; may be null.
   */
  public java.lang.Integer getContactId()
  {
    return contactId;
  }

  /**
   * Sets the field contactId.
   * @param _contactId the new value of the field contactId.
   */
  public void setContactId(java.lang.Integer _contactId)
  {
    contactId = _contactId;
  }

  /**
   * Erstelldatum des Kontaktes = Scan-Datum von File
   */
  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateTimeUserType")
  @javax.persistence.Column(updatable=false)
  private ch.ivyteam.ivy.scripting.objects.DateTime incomeDate;

  /**
   * Gets the field incomeDate.
   * @return the value of the field incomeDate; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.DateTime getIncomeDate()
  {
    return incomeDate;
  }

  /**
   * Sets the field incomeDate.
   * @param _incomeDate the new value of the field incomeDate.
   */
  public void setIncomeDate(ch.ivyteam.ivy.scripting.objects.DateTime _incomeDate)
  {
    incomeDate = _incomeDate;
  }

  /**
   * ID des mit dem Kontakt verlinkten Files
   */
  @javax.persistence.Column(updatable=false)
  private java.lang.Integer scanFileId;

  /**
   * Gets the field scanFileId.
   * @return the value of the field scanFileId; may be null.
   */
  public java.lang.Integer getScanFileId()
  {
    return scanFileId;
  }

  /**
   * Sets the field scanFileId.
   * @param _scanFileId the new value of the field scanFileId.
   */
  public void setScanFileId(java.lang.Integer _scanFileId)
  {
    scanFileId = _scanFileId;
  }

  /**
   * Typ des Kontaktes: Mail, FaxOrMail oder Post 
   */
  private java.lang.String orderIncomeType;

  /**
   * Gets the field orderIncomeType.
   * @return the value of the field orderIncomeType; may be null.
   */
  public java.lang.String getOrderIncomeType()
  {
    return orderIncomeType;
  }

  /**
   * Sets the field orderIncomeType.
   * @param _orderIncomeType the new value of the field orderIncomeType.
   */
  public void setOrderIncomeType(java.lang.String _orderIncomeType)
  {
    orderIncomeType = _orderIncomeType;
  }

  /**
   * Auftrag per Post eingegangen Typ Unterschrift: OK, DIFFERS (abweichend), MISSING (fehlt) 
   */
  private java.lang.String postalSignatureType;

  /**
   * Gets the field postalSignatureType.
   * @return the value of the field postalSignatureType; may be null.
   */
  public java.lang.String getPostalSignatureType()
  {
    return postalSignatureType;
  }

  /**
   * Sets the field postalSignatureType.
   * @param _postalSignatureType the new value of the field postalSignatureType.
   */
  public void setPostalSignatureType(java.lang.String _postalSignatureType)
  {
    postalSignatureType = _postalSignatureType;
  }

  /**
   * Auftrag per Post eingegangen Typ Original: YES (Ja), NO (Nein)
   */
  private java.lang.String postalOriginalType;

  /**
   * Gets the field postalOriginalType.
   * @return the value of the field postalOriginalType; may be null.
   */
  public java.lang.String getPostalOriginalType()
  {
    return postalOriginalType;
  }

  /**
   * Sets the field postalOriginalType.
   * @param _postalOriginalType the new value of the field postalOriginalType.
   */
  public void setPostalOriginalType(java.lang.String _postalOriginalType)
  {
    postalOriginalType = _postalOriginalType;
  }

  /**
   * Auftrag per Fax oder Mail eingegangen  Typ Revers: OK, NOK (Nicht ok, Original eingefordert)
   */
  private java.lang.String faxOrMailReversType;

  /**
   * Gets the field faxOrMailReversType.
   * @return the value of the field faxOrMailReversType; may be null.
   */
  public java.lang.String getFaxOrMailReversType()
  {
    return faxOrMailReversType;
  }

  /**
   * Sets the field faxOrMailReversType.
   * @param _faxOrMailReversType the new value of the field faxOrMailReversType.
   */
  public void setFaxOrMailReversType(java.lang.String _faxOrMailReversType)
  {
    faxOrMailReversType = _faxOrMailReversType;
  }

  /**
   * Auftrag per Fax eingegangen Typ Unterschrift: OK, DIFFERS (abweichend), MISSING (fehlt)
   */
  private java.lang.String faxSignatureType;

  /**
   * Gets the field faxSignatureType.
   * @return the value of the field faxSignatureType; may be null.
   */
  public java.lang.String getFaxSignatureType()
  {
    return faxSignatureType;
  }

  /**
   * Sets the field faxSignatureType.
   * @param _faxSignatureType the new value of the field faxSignatureType.
   */
  public void setFaxSignatureType(java.lang.String _faxSignatureType)
  {
    faxSignatureType = _faxSignatureType;
  }

  /**
   * Auftrag per Fax eingegangen  Typ Revers: OK, NOK (Nicht ok, Original eingefordert)
   */
  private java.lang.String faxReversType;

  /**
   * Gets the field faxReversType.
   * @return the value of the field faxReversType; may be null.
   */
  public java.lang.String getFaxReversType()
  {
    return faxReversType;
  }

  /**
   * Sets the field faxReversType.
   * @param _faxReversType the new value of the field faxReversType.
   */
  public void setFaxReversType(java.lang.String _faxReversType)
  {
    faxReversType = _faxReversType;
  }

  /**
   * Kontakt gepr�ft
   */
  private java.lang.Boolean contactIsChecked;

  /**
   * Gets the field contactIsChecked.
   * @return the value of the field contactIsChecked; may be null.
   */
  public java.lang.Boolean getContactIsChecked()
  {
    return contactIsChecked;
  }

  /**
   * Sets the field contactIsChecked.
   * @param _contactIsChecked the new value of the field contactIsChecked.
   */
  public void setContactIsChecked(java.lang.Boolean _contactIsChecked)
  {
    contactIsChecked = _contactIsChecked;
  }

  /**
   * Pr�fer UserId
   */
  private java.lang.String contactCheckedBy;

  /**
   * Gets the field contactCheckedBy.
   * @return the value of the field contactCheckedBy; may be null.
   */
  public java.lang.String getContactCheckedBy()
  {
    return contactCheckedBy;
  }

  /**
   * Sets the field contactCheckedBy.
   * @param _contactCheckedBy the new value of the field contactCheckedBy.
   */
  public void setContactCheckedBy(java.lang.String _contactCheckedBy)
  {
    contactCheckedBy = _contactCheckedBy;
  }

  private transient java.util.List<ch.soreco.orderbook.bo.Paper> papers;

  /**
   * Gets the field papers.
   * @return the value of the field papers; may be null.
   */
  public java.util.List<ch.soreco.orderbook.bo.Paper> getPapers()
  {
    return papers;
  }

  /**
   * Sets the field papers.
   * @param _papers the new value of the field papers.
   */
  public void setPapers(java.util.List<ch.soreco.orderbook.bo.Paper> _papers)
  {
    papers = _papers;
  }

  private transient java.util.List<ch.soreco.orderbook.bo.SignatureRequest> signatures;

  /**
   * Gets the field signatures.
   * @return the value of the field signatures; may be null.
   */
  public java.util.List<ch.soreco.orderbook.bo.SignatureRequest> getSignatures()
  {
    return signatures;
  }

  /**
   * Sets the field signatures.
   * @param _signatures the new value of the field signatures.
   */
  public void setSignatures(java.util.List<ch.soreco.orderbook.bo.SignatureRequest> _signatures)
  {
    signatures = _signatures;
  }

  /**
   * Kontakt Adresse Id
   */
  @javax.persistence.OneToOne(cascade={javax.persistence.CascadeType.PERSIST, javax.persistence.CascadeType.MERGE}, fetch=javax.persistence.FetchType.EAGER, orphanRemoval=false)
  private ch.soreco.orderbook.bo.ContactAddress address;

  /**
   * Gets the field address.
   * @return the value of the field address; may be null.
   */
  public ch.soreco.orderbook.bo.ContactAddress getAddress()
  {
    return address;
  }

  /**
   * Sets the field address.
   * @param _address the new value of the field address.
   */
  public void setAddress(ch.soreco.orderbook.bo.ContactAddress _address)
  {
    address = _address;
  }

  /**
   * ID des Auftrags aus welchem dieser Kontakt gemerged wurde
   */
  private java.lang.Integer contactMergedFromOrderId;

  /**
   * Gets the field contactMergedFromOrderId.
   * @return the value of the field contactMergedFromOrderId; may be null.
   */
  public java.lang.Integer getContactMergedFromOrderId()
  {
    return contactMergedFromOrderId;
  }

  /**
   * Sets the field contactMergedFromOrderId.
   * @param _contactMergedFromOrderId the new value of the field contactMergedFromOrderId.
   */
  public void setContactMergedFromOrderId(java.lang.Integer _contactMergedFromOrderId)
  {
    contactMergedFromOrderId = _contactMergedFromOrderId;
  }

  private transient java.util.List<ch.soreco.orderbook.bo.Letter> letters;

  /**
   * Gets the field letters.
   * @return the value of the field letters; may be null.
   */
  public java.util.List<ch.soreco.orderbook.bo.Letter> getLetters()
  {
    return letters;
  }

  /**
   * Sets the field letters.
   * @param _letters the new value of the field letters.
   */
  public void setLetters(java.util.List<ch.soreco.orderbook.bo.Letter> _letters)
  {
    letters = _letters;
  }

  private transient ch.soreco.orderbook.bo.AddressCheck addressCheck;

  /**
   * Gets the field addressCheck.
   * @return the value of the field addressCheck; may be null.
   */
  public ch.soreco.orderbook.bo.AddressCheck getAddressCheck()
  {
    return addressCheck;
  }

  /**
   * Sets the field addressCheck.
   * @param _addressCheck the new value of the field addressCheck.
   */
  public void setAddressCheck(ch.soreco.orderbook.bo.AddressCheck _addressCheck)
  {
    addressCheck = _addressCheck;
  }

}
