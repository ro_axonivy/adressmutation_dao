package ch.soreco.orderbook.bo;

/**
 * Deprecated
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class ScanLog", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_ScanLogs")
public class ScanLog extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 2578070889280641917L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer id;

  /**
   * Gets the field id.
   * @return the value of the field id; may be null.
   */
  public java.lang.Integer getId()
  {
    return id;
  }

  /**
   * Sets the field id.
   * @param _id the new value of the field id.
   */
  public void setId(java.lang.Integer _id)
  {
    id = _id;
  }

  private java.lang.Integer ShareId;

  /**
   * Gets the field ShareId.
   * @return the value of the field ShareId; may be null.
   */
  public java.lang.Integer getShareId()
  {
    return ShareId;
  }

  /**
   * Sets the field ShareId.
   * @param _ShareId the new value of the field ShareId.
   */
  public void setShareId(java.lang.Integer _ShareId)
  {
    ShareId = _ShareId;
  }

  private java.lang.String ScanType;

  /**
   * Gets the field ScanType.
   * @return the value of the field ScanType; may be null.
   */
  public java.lang.String getScanType()
  {
    return ScanType;
  }

  /**
   * Sets the field ScanType.
   * @param _ScanType the new value of the field ScanType.
   */
  public void setScanType(java.lang.String _ScanType)
  {
    ScanType = _ScanType;
  }

  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateTimeUserType")
  private ch.ivyteam.ivy.scripting.objects.DateTime ScanStart;

  /**
   * Gets the field ScanStart.
   * @return the value of the field ScanStart; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.DateTime getScanStart()
  {
    return ScanStart;
  }

  /**
   * Sets the field ScanStart.
   * @param _ScanStart the new value of the field ScanStart.
   */
  public void setScanStart(ch.ivyteam.ivy.scripting.objects.DateTime _ScanStart)
  {
    ScanStart = _ScanStart;
  }

  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateTimeUserType")
  private ch.ivyteam.ivy.scripting.objects.DateTime ScanEnd;

  /**
   * Gets the field ScanEnd.
   * @return the value of the field ScanEnd; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.DateTime getScanEnd()
  {
    return ScanEnd;
  }

  /**
   * Sets the field ScanEnd.
   * @param _ScanEnd the new value of the field ScanEnd.
   */
  public void setScanEnd(ch.ivyteam.ivy.scripting.objects.DateTime _ScanEnd)
  {
    ScanEnd = _ScanEnd;
  }

  private java.lang.Integer Errors;

  /**
   * Gets the field Errors.
   * @return the value of the field Errors; may be null.
   */
  public java.lang.Integer getErrors()
  {
    return Errors;
  }

  /**
   * Sets the field Errors.
   * @param _Errors the new value of the field Errors.
   */
  public void setErrors(java.lang.Integer _Errors)
  {
    Errors = _Errors;
  }

  private java.lang.Integer Successful;

  /**
   * Gets the field Successful.
   * @return the value of the field Successful; may be null.
   */
  public java.lang.Integer getSuccessful()
  {
    return Successful;
  }

  /**
   * Sets the field Successful.
   * @param _Successful the new value of the field Successful.
   */
  public void setSuccessful(java.lang.Integer _Successful)
  {
    Successful = _Successful;
  }

  private java.lang.Integer Warnings;

  /**
   * Gets the field Warnings.
   * @return the value of the field Warnings; may be null.
   */
  public java.lang.Integer getWarnings()
  {
    return Warnings;
  }

  /**
   * Sets the field Warnings.
   * @param _Warnings the new value of the field Warnings.
   */
  public void setWarnings(java.lang.Integer _Warnings)
  {
    Warnings = _Warnings;
  }

  private java.lang.Integer Total;

  /**
   * Gets the field Total.
   * @return the value of the field Total; may be null.
   */
  public java.lang.Integer getTotal()
  {
    return Total;
  }

  /**
   * Sets the field Total.
   * @param _Total the new value of the field Total.
   */
  public void setTotal(java.lang.Integer _Total)
  {
    Total = _Total;
  }

}
