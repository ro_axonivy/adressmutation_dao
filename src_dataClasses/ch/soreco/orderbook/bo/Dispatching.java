package ch.soreco.orderbook.bo;

/**
 * Zwischenspeicher f�r Auftrage in einem Dispatching Status
 * 1:1 Beziehung zum Order / Auftrag
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Dispatching", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_Dispatching")
public class Dispatching extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -7404312771993955581L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer id;

  /**
   * Gets the field id.
   * @return the value of the field id; may be null.
   */
  public java.lang.Integer getId()
  {
    return id;
  }

  /**
   * Sets the field id.
   * @param _id the new value of the field id.
   */
  public void setId(java.lang.Integer _id)
  {
    id = _id;
  }

  /**
   * AuftragsId
   */
  private java.lang.Integer orderId;

  /**
   * Gets the field orderId.
   * @return the value of the field orderId; may be null.
   */
  public java.lang.Integer getOrderId()
  {
    return orderId;
  }

  /**
   * Sets the field orderId.
   * @param _orderId the new value of the field orderId.
   */
  public void setOrderId(java.lang.Integer _orderId)
  {
    orderId = _orderId;
  }

  /**
   * deprecated
   */
  private java.lang.String teams;

  /**
   * Gets the field teams.
   * @return the value of the field teams; may be null.
   */
  public java.lang.String getTeams()
  {
    return teams;
  }

  /**
   * Sets the field teams.
   * @param _teams the new value of the field teams.
   */
  public void setTeams(java.lang.String _teams)
  {
    teams = _teams;
  }

  /**
   * deprecated
   */
  @javax.persistence.Column(length=8000)
  private java.lang.String orderTypes;

  /**
   * Gets the field orderTypes.
   * @return the value of the field orderTypes; may be null.
   */
  public java.lang.String getOrderTypes()
  {
    return orderTypes;
  }

  /**
   * Sets the field orderTypes.
   * @param _orderTypes the new value of the field orderTypes.
   */
  public void setOrderTypes(java.lang.String _orderTypes)
  {
    orderTypes = _orderTypes;
  }

  /**
   * Dispatcher UserId
   */
  @javax.persistence.Column(length=255)
  private java.lang.String dispatcherUserId;

  /**
   * Gets the field dispatcherUserId.
   * @return the value of the field dispatcherUserId; may be null.
   */
  public java.lang.String getDispatcherUserId()
  {
    return dispatcherUserId;
  }

  /**
   * Sets the field dispatcherUserId.
   * @param _dispatcherUserId the new value of the field dispatcherUserId.
   */
  public void setDispatcherUserId(java.lang.String _dispatcherUserId)
  {
    dispatcherUserId = _dispatcherUserId;
  }

  /**
   * Datum des Dispatchings
   */
  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateTimeUserType")
  private ch.ivyteam.ivy.scripting.objects.DateTime dispatchDateTime;

  /**
   * Gets the field dispatchDateTime.
   * @return the value of the field dispatchDateTime; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.DateTime getDispatchDateTime()
  {
    return dispatchDateTime;
  }

  /**
   * Sets the field dispatchDateTime.
   * @param _dispatchDateTime the new value of the field dispatchDateTime.
   */
  public void setDispatchDateTime(ch.ivyteam.ivy.scripting.objects.DateTime _dispatchDateTime)
  {
    dispatchDateTime = _dispatchDateTime;
  }

  /**
   * Ausgew�hlte Auftragsarten und deren Zuordnung zu den Benutzern als JSON String
   */
  @javax.persistence.Column(length=8000)
  private java.lang.String assignments;

  /**
   * Gets the field assignments.
   * @return the value of the field assignments; may be null.
   */
  public java.lang.String getAssignments()
  {
    return assignments;
  }

  /**
   * Sets the field assignments.
   * @param _assignments the new value of the field assignments.
   */
  public void setAssignments(java.lang.String _assignments)
  {
    assignments = _assignments;
  }

  /**
   * Begr�ndung falls "Kein Auftrag" gew�hlt wurde
   */
  @javax.persistence.Column(length=8000)
  private java.lang.String reason;

  /**
   * Gets the field reason.
   * @return the value of the field reason; may be null.
   */
  public java.lang.String getReason()
  {
    return reason;
  }

  /**
   * Sets the field reason.
   * @param _reason the new value of the field reason.
   */
  public void setReason(java.lang.String _reason)
  {
    reason = _reason;
  }

}
