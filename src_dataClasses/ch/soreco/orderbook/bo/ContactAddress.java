package ch.soreco.orderbook.bo;

/**
 * Kontaktadresse
 * 1:1 Beziehung zum Contact / Kontakt
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class ContactAddress", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_Contact_Address")
public class ContactAddress extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 2273798347709901929L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  @javax.persistence.Column(name="contactId")
  private java.lang.Integer addressContactId;

  /**
   * Gets the field addressContactId.
   * @return the value of the field addressContactId; may be null.
   */
  public java.lang.Integer getAddressContactId()
  {
    return addressContactId;
  }

  /**
   * Sets the field addressContactId.
   * @param _addressContactId the new value of the field addressContactId.
   */
  public void setAddressContactId(java.lang.Integer _addressContactId)
  {
    addressContactId = _addressContactId;
  }

  /**
   * Flag ob es sich um eine neue Adresse handelt
   */
  private java.lang.Boolean isNewAdress;

  /**
   * Gets the field isNewAdress.
   * @return the value of the field isNewAdress; may be null.
   */
  public java.lang.Boolean getIsNewAdress()
  {
    return isNewAdress;
  }

  /**
   * Sets the field isNewAdress.
   * @param _isNewAdress the new value of the field isNewAdress.
   */
  public void setIsNewAdress(java.lang.Boolean _isNewAdress)
  {
    isNewAdress = _isNewAdress;
  }

  /**
   * Neue Adresse, Freitext
   */
  @javax.persistence.Column(length=500)
  private java.lang.String newAdressText;

  /**
   * Gets the field newAdressText.
   * @return the value of the field newAdressText; may be null.
   */
  public java.lang.String getNewAdressText()
  {
    return newAdressText;
  }

  /**
   * Sets the field newAdressText.
   * @param _newAdressText the new value of the field newAdressText.
   */
  public void setNewAdressText(java.lang.String _newAdressText)
  {
    newAdressText = _newAdressText;
  }

  /**
   * Adresse gepr�ft
   */
  private java.lang.Boolean addressIsChecked;

  /**
   * Gets the field addressIsChecked.
   * @return the value of the field addressIsChecked; may be null.
   */
  public java.lang.Boolean getAddressIsChecked()
  {
    return addressIsChecked;
  }

  /**
   * Sets the field addressIsChecked.
   * @param _addressIsChecked the new value of the field addressIsChecked.
   */
  public void setAddressIsChecked(java.lang.Boolean _addressIsChecked)
  {
    addressIsChecked = _addressIsChecked;
  }

  /**
   * Pr�fer UserId
   */
  private java.lang.String addressCheckedBy;

  /**
   * Gets the field addressCheckedBy.
   * @return the value of the field addressCheckedBy; may be null.
   */
  public java.lang.String getAddressCheckedBy()
  {
    return addressCheckedBy;
  }

  /**
   * Sets the field addressCheckedBy.
   * @param _addressCheckedBy the new value of the field addressCheckedBy.
   */
  public void setAddressCheckedBy(java.lang.String _addressCheckedBy)
  {
    addressCheckedBy = _addressCheckedBy;
  }

}
