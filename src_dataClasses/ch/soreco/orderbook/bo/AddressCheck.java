package ch.soreco.orderbook.bo;

/**
 * Adressprüfung
 * 1:1 Beziehung zum Kontakt
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class AddressCheck", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_Contact_AddressChecks")
public class AddressCheck extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 6912916861851395605L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.Column(name="contactId")
  private java.lang.Integer addressCheckContactId;

  /**
   * Gets the field addressCheckContactId.
   * @return the value of the field addressCheckContactId; may be null.
   */
  public java.lang.Integer getAddressCheckContactId()
  {
    return addressCheckContactId;
  }

  /**
   * Sets the field addressCheckContactId.
   * @param _addressCheckContactId the new value of the field addressCheckContactId.
   */
  public void setAddressCheckContactId(java.lang.Integer _addressCheckContactId)
  {
    addressCheckContactId = _addressCheckContactId;
  }

  /**
   * OK oder DIFFERS ( Abweichend)
   */
  private java.lang.String addressCheckType;

  /**
   * Gets the field addressCheckType.
   * @return the value of the field addressCheckType; may be null.
   */
  public java.lang.String getAddressCheckType()
  {
    return addressCheckType;
  }

  /**
   * Sets the field addressCheckType.
   * @param _addressCheckType the new value of the field addressCheckType.
   */
  public void setAddressCheckType(java.lang.String _addressCheckType)
  {
    addressCheckType = _addressCheckType;
  }

  /**
   * Order#
   */
  private java.lang.String addressCheckOrderNo;

  /**
   * Gets the field addressCheckOrderNo.
   * @return the value of the field addressCheckOrderNo; may be null.
   */
  public java.lang.String getAddressCheckOrderNo()
  {
    return addressCheckOrderNo;
  }

  /**
   * Sets the field addressCheckOrderNo.
   * @param _addressCheckOrderNo the new value of the field addressCheckOrderNo.
   */
  public void setAddressCheckOrderNo(java.lang.String _addressCheckOrderNo)
  {
    addressCheckOrderNo = _addressCheckOrderNo;
  }

  /**
   * Geprüft Flag
   */
  private java.lang.Boolean addressCheckIsChecked;

  /**
   * Gets the field addressCheckIsChecked.
   * @return the value of the field addressCheckIsChecked; may be null.
   */
  public java.lang.Boolean getAddressCheckIsChecked()
  {
    return addressCheckIsChecked;
  }

  /**
   * Sets the field addressCheckIsChecked.
   * @param _addressCheckIsChecked the new value of the field addressCheckIsChecked.
   */
  public void setAddressCheckIsChecked(java.lang.Boolean _addressCheckIsChecked)
  {
    addressCheckIsChecked = _addressCheckIsChecked;
  }

}
