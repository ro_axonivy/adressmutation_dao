package ch.soreco.orderbook.bo;

/**
 * Erfassung Meta-Types in generischem Prozess
 * Types werden in Enumerator RecordingMetaType gespeichert
 * n:1 Beziehung zu Recording / Erfassung
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class RecordingMeta", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_Recording_Meta")
public class RecordingMeta extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 2109360157819197901L;

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer id;

  /**
   * Gets the field id.
   * @return the value of the field id; may be null.
   */
  public java.lang.Integer getId()
  {
    return id;
  }

  /**
   * Sets the field id.
   * @param _id the new value of the field id.
   */
  public void setId(java.lang.Integer _id)
  {
    id = _id;
  }

  /**
   * Id des Recording / Erfassungs Eintrags
   */
  @javax.persistence.ManyToOne(cascade={javax.persistence.CascadeType.PERSIST, javax.persistence.CascadeType.MERGE}, fetch=javax.persistence.FetchType.EAGER)
  @javax.persistence.JoinColumn(name="recordingId")
  private ch.soreco.orderbook.bo.Recording recording;

  /**
   * Gets the field recording.
   * @return the value of the field recording; may be null.
   */
  public ch.soreco.orderbook.bo.Recording getRecording()
  {
    return recording;
  }

  /**
   * Sets the field recording.
   * @param _recording the new value of the field recording.
   */
  public void setRecording(ch.soreco.orderbook.bo.Recording _recording)
  {
    recording = _recording;
  }

  /**
   * Erfassung Meta Typ
   */
  private java.lang.String recordingMetaType;

  /**
   * Gets the field recordingMetaType.
   * @return the value of the field recordingMetaType; may be null.
   */
  public java.lang.String getRecordingMetaType()
  {
    return recordingMetaType;
  }

  /**
   * Sets the field recordingMetaType.
   * @param _recordingMetaType the new value of the field recordingMetaType.
   */
  public void setRecordingMetaType(java.lang.String _recordingMetaType)
  {
    recordingMetaType = _recordingMetaType;
  }

  /**
   * Kommentar
   */
  private java.lang.String recordingMetaComment;

  /**
   * Gets the field recordingMetaComment.
   * @return the value of the field recordingMetaComment; may be null.
   */
  public java.lang.String getRecordingMetaComment()
  {
    return recordingMetaComment;
  }

  /**
   * Sets the field recordingMetaComment.
   * @param _recordingMetaComment the new value of the field recordingMetaComment.
   */
  public void setRecordingMetaComment(java.lang.String _recordingMetaComment)
  {
    recordingMetaComment = _recordingMetaComment;
  }

  /**
   * Order#
   */
  private java.lang.String recordingMetaOrderNo;

  /**
   * Gets the field recordingMetaOrderNo.
   * @return the value of the field recordingMetaOrderNo; may be null.
   */
  public java.lang.String getRecordingMetaOrderNo()
  {
    return recordingMetaOrderNo;
  }

  /**
   * Sets the field recordingMetaOrderNo.
   * @param _recordingMetaOrderNo the new value of the field recordingMetaOrderNo.
   */
  public void setRecordingMetaOrderNo(java.lang.String _recordingMetaOrderNo)
  {
    recordingMetaOrderNo = _recordingMetaOrderNo;
  }

  /**
   * Meta Typ gepr�ft
   */
  private java.lang.Boolean recordingMetaIsChecked;

  /**
   * Gets the field recordingMetaIsChecked.
   * @return the value of the field recordingMetaIsChecked; may be null.
   */
  public java.lang.Boolean getRecordingMetaIsChecked()
  {
    return recordingMetaIsChecked;
  }

  /**
   * Sets the field recordingMetaIsChecked.
   * @param _recordingMetaIsChecked the new value of the field recordingMetaIsChecked.
   */
  public void setRecordingMetaIsChecked(java.lang.Boolean _recordingMetaIsChecked)
  {
    recordingMetaIsChecked = _recordingMetaIsChecked;
  }

  /**
   * Pr�fer UserId
   */
  private java.lang.String recordingMetaIsCheckedBy;

  /**
   * Gets the field recordingMetaIsCheckedBy.
   * @return the value of the field recordingMetaIsCheckedBy; may be null.
   */
  public java.lang.String getRecordingMetaIsCheckedBy()
  {
    return recordingMetaIsCheckedBy;
  }

  /**
   * Sets the field recordingMetaIsCheckedBy.
   * @param _recordingMetaIsCheckedBy the new value of the field recordingMetaIsCheckedBy.
   */
  public void setRecordingMetaIsCheckedBy(java.lang.String _recordingMetaIsCheckedBy)
  {
    recordingMetaIsCheckedBy = _recordingMetaIsCheckedBy;
  }

}
