package ch.soreco.orderbook.bo;

/**
 * Scanning
 * Entit�t zur Speicherung von Scanning-Informationen
 * n:n Beziehung zu Auftrag (Kombiauftrag)
 * n:1 Beziehung zum ScanShare
 * 1:n Beziehung zu Attachments
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Scanning", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
@javax.persistence.Entity
@javax.persistence.Table(name="tbl_Scanning")
public class Scanning extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 7740527488744668840L;

  /**
   * AuftragsId
   */
  @javax.persistence.Column(name="orderId", updatable=false)
  private java.lang.Integer orderId;

  /**
   * Gets the field orderId.
   * @return the value of the field orderId; may be null.
   */
  public java.lang.Integer getOrderId()
  {
    return orderId;
  }

  /**
   * Sets the field orderId.
   * @param _orderId the new value of the field orderId.
   */
  public void setOrderId(java.lang.Integer _orderId)
  {
    orderId = _orderId;
  }

  /**
   * Identifier
   */
  @javax.persistence.Id
  @javax.persistence.GeneratedValue
  private java.lang.Integer scanningId;

  /**
   * Gets the field scanningId.
   * @return the value of the field scanningId; may be null.
   */
  public java.lang.Integer getScanningId()
  {
    return scanningId;
  }

  /**
   * Sets the field scanningId.
   * @param _scanningId the new value of the field scanningId.
   */
  public void setScanningId(java.lang.Integer _scanningId)
  {
    scanningId = _scanningId;
  }

  /**
   * Datum des Scans
   */
  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateUserType")
  @javax.persistence.Column(updatable=false)
  private ch.ivyteam.ivy.scripting.objects.Date scanDate;

  /**
   * Gets the field scanDate.
   * @return the value of the field scanDate; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Date getScanDate()
  {
    return scanDate;
  }

  /**
   * Sets the field scanDate.
   * @param _scanDate the new value of the field scanDate.
   */
  public void setScanDate(ch.ivyteam.ivy.scripting.objects.Date _scanDate)
  {
    scanDate = _scanDate;
  }

  /**
   * Zeit des Scans
   */
  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateTimeUserType")
  @javax.persistence.Column(updatable=false)
  private ch.ivyteam.ivy.scripting.objects.DateTime scanTime;

  /**
   * Gets the field scanTime.
   * @return the value of the field scanTime; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.DateTime getScanTime()
  {
    return scanTime;
  }

  /**
   * Sets the field scanTime.
   * @param _scanTime the new value of the field scanTime.
   */
  public void setScanTime(ch.ivyteam.ivy.scripting.objects.DateTime _scanTime)
  {
    scanTime = _scanTime;
  }

  /**
   * Dokumenttyp (siehe OrderMatching)
   */
  @javax.persistence.Column(updatable=false)
  private java.lang.String docType;

  /**
   * Gets the field docType.
   * @return the value of the field docType; may be null.
   */
  public java.lang.String getDocType()
  {
    return docType;
  }

  /**
   * Sets the field docType.
   * @param _docType the new value of the field docType.
   */
  public void setDocType(java.lang.String _docType)
  {
    docType = _docType;
  }

  /**
   * Flag ob es sich um ein Dossier handelt
   */
  @javax.persistence.Column(updatable=false)
  private java.lang.Boolean isDossier;

  /**
   * Gets the field isDossier.
   * @return the value of the field isDossier; may be null.
   */
  public java.lang.Boolean getIsDossier()
  {
    return isDossier;
  }

  /**
   * Sets the field isDossier.
   * @param _isDossier the new value of the field isDossier.
   */
  public void setIsDossier(java.lang.Boolean _isDossier)
  {
    isDossier = _isDossier;
  }

  /**
   * Id des Scans
   */
  @javax.persistence.Column(updatable=false)
  private java.lang.String scanId;

  /**
   * Gets the field scanId.
   * @return the value of the field scanId; may be null.
   */
  public java.lang.String getScanId()
  {
    return scanId;
  }

  /**
   * Sets the field scanId.
   * @param _scanId the new value of the field scanId.
   */
  public void setScanId(java.lang.String _scanId)
  {
    scanId = _scanId;
  }

  /**
   * Anzahl Seiten
   */
  @javax.persistence.Column(updatable=false)
  private java.lang.Integer numberOfPages;

  /**
   * Gets the field numberOfPages.
   * @return the value of the field numberOfPages; may be null.
   */
  public java.lang.Integer getNumberOfPages()
  {
    return numberOfPages;
  }

  /**
   * Sets the field numberOfPages.
   * @param _numberOfPages the new value of the field numberOfPages.
   */
  public void setNumberOfPages(java.lang.Integer _numberOfPages)
  {
    numberOfPages = _numberOfPages;
  }

  /**
   * Anzahl Seiten Netto
   */
  @javax.persistence.Column(updatable=false)
  private java.lang.Integer numberOfPagesNet;

  /**
   * Gets the field numberOfPagesNet.
   * @return the value of the field numberOfPagesNet; may be null.
   */
  public java.lang.Integer getNumberOfPagesNet()
  {
    return numberOfPagesNet;
  }

  /**
   * Sets the field numberOfPagesNet.
   * @param _numberOfPagesNet the new value of the field numberOfPagesNet.
   */
  public void setNumberOfPagesNet(java.lang.Integer _numberOfPagesNet)
  {
    numberOfPagesNet = _numberOfPagesNet;
  }

  /**
   * Seiten Nummer Start
   */
  @javax.persistence.Column(updatable=false)
  private java.lang.Integer pageNoStart;

  /**
   * Gets the field pageNoStart.
   * @return the value of the field pageNoStart; may be null.
   */
  public java.lang.Integer getPageNoStart()
  {
    return pageNoStart;
  }

  /**
   * Sets the field pageNoStart.
   * @param _pageNoStart the new value of the field pageNoStart.
   */
  public void setPageNoStart(java.lang.Integer _pageNoStart)
  {
    pageNoStart = _pageNoStart;
  }

  /**
   * Seiten Nummer Ende
   */
  @javax.persistence.Column(updatable=false)
  private java.lang.Integer pageNoEnd;

  /**
   * Gets the field pageNoEnd.
   * @return the value of the field pageNoEnd; may be null.
   */
  public java.lang.Integer getPageNoEnd()
  {
    return pageNoEnd;
  }

  /**
   * Sets the field pageNoEnd.
   * @param _pageNoEnd the new value of the field pageNoEnd.
   */
  public void setPageNoEnd(java.lang.Integer _pageNoEnd)
  {
    pageNoEnd = _pageNoEnd;
  }

  /**
   * Inhalt von DM
   */
  @javax.persistence.Column(updatable=false)
  private java.lang.String contentOfDM;

  /**
   * Gets the field contentOfDM.
   * @return the value of the field contentOfDM; may be null.
   */
  public java.lang.String getContentOfDM()
  {
    return contentOfDM;
  }

  /**
   * Sets the field contentOfDM.
   * @param _contentOfDM the new value of the field contentOfDM.
   */
  public void setContentOfDM(java.lang.String _contentOfDM)
  {
    contentOfDM = _contentOfDM;
  }

  /**
   * Id des Files
   */
  @javax.persistence.Column(updatable=false)
  private java.lang.Integer scanFileId;

  /**
   * Gets the field scanFileId.
   * @return the value of the field scanFileId; may be null.
   */
  public java.lang.Integer getScanFileId()
  {
    return scanFileId;
  }

  /**
   * Sets the field scanFileId.
   * @param _scanFileId the new value of the field scanFileId.
   */
  public void setScanFileId(java.lang.Integer _scanFileId)
  {
    scanFileId = _scanFileId;
  }

  /**
   * Id des Mono-Files
   */
  @javax.persistence.Column(updatable=false)
  private java.lang.Integer scanMonoFileId;

  /**
   * Gets the field scanMonoFileId.
   * @return the value of the field scanMonoFileId; may be null.
   */
  public java.lang.Integer getScanMonoFileId()
  {
    return scanMonoFileId;
  }

  /**
   * Sets the field scanMonoFileId.
   * @param _scanMonoFileId the new value of the field scanMonoFileId.
   */
  public void setScanMonoFileId(java.lang.Integer _scanMonoFileId)
  {
    scanMonoFileId = _scanMonoFileId;
  }

  private transient java.lang.String scanFile;

  /**
   * Gets the field scanFile.
   * @return the value of the field scanFile; may be null.
   */
  public java.lang.String getScanFile()
  {
    return scanFile;
  }

  /**
   * Sets the field scanFile.
   * @param _scanFile the new value of the field scanFile.
   */
  public void setScanFile(java.lang.String _scanFile)
  {
    scanFile = _scanFile;
  }

  private transient java.lang.String scanMonoFile;

  /**
   * Gets the field scanMonoFile.
   * @return the value of the field scanMonoFile; may be null.
   */
  public java.lang.String getScanMonoFile()
  {
    return scanMonoFile;
  }

  /**
   * Sets the field scanMonoFile.
   * @param _scanMonoFile the new value of the field scanMonoFile.
   */
  public void setScanMonoFile(java.lang.String _scanMonoFile)
  {
    scanMonoFile = _scanMonoFile;
  }

  /**
   * Flag ob es sich um das Hauptdokument des Auftrags handelt
   */
  @javax.persistence.Column(updatable=false)
  private java.lang.Boolean isMain;

  /**
   * Gets the field isMain.
   * @return the value of the field isMain; may be null.
   */
  public java.lang.Boolean getIsMain()
  {
    return isMain;
  }

  /**
   * Sets the field isMain.
   * @param _isMain the new value of the field isMain.
   */
  public void setIsMain(java.lang.Boolean _isMain)
  {
    isMain = _isMain;
  }

  /**
   * Dokumentnummer
   */
  @javax.persistence.Column(updatable=false)
  private java.lang.String docNo;

  /**
   * Gets the field docNo.
   * @return the value of the field docNo; may be null.
   */
  public java.lang.String getDocNo()
  {
    return docNo;
  }

  /**
   * Sets the field docNo.
   * @param _docNo the new value of the field docNo.
   */
  public void setDocNo(java.lang.String _docNo)
  {
    docNo = _docNo;
  }

  /**
   * Auftragstyp
   */
  @javax.persistence.Column(updatable=false)
  private java.lang.String orderType;

  /**
   * Gets the field orderType.
   * @return the value of the field orderType; may be null.
   */
  public java.lang.String getOrderType()
  {
    return orderType;
  }

  /**
   * Sets the field orderType.
   * @param _orderType the new value of the field orderType.
   */
  public void setOrderType(java.lang.String _orderType)
  {
    orderType = _orderType;
  }

  /**
   * Archivierungsstatus
   */
  private java.lang.String archivingState;

  /**
   * Gets the field archivingState.
   * @return the value of the field archivingState; may be null.
   */
  public java.lang.String getArchivingState()
  {
    return archivingState;
  }

  /**
   * Sets the field archivingState.
   * @param _archivingState the new value of the field archivingState.
   */
  public void setArchivingState(java.lang.String _archivingState)
  {
    archivingState = _archivingState;
  }

  /**
   * Datum der Bereitstellung zur Archivierung
   */
  @org.hibernate.annotations.Type(type = "ch.ivyteam.ivy.process.data.persistence.usertype.DateTimeUserType")
  private ch.ivyteam.ivy.scripting.objects.DateTime archivingPreparedDate;

  /**
   * Gets the field archivingPreparedDate.
   * @return the value of the field archivingPreparedDate; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.DateTime getArchivingPreparedDate()
  {
    return archivingPreparedDate;
  }

  /**
   * Sets the field archivingPreparedDate.
   * @param _archivingPreparedDate the new value of the field archivingPreparedDate.
   */
  public void setArchivingPreparedDate(ch.ivyteam.ivy.scripting.objects.DateTime _archivingPreparedDate)
  {
    archivingPreparedDate = _archivingPreparedDate;
  }

  private transient ch.soreco.orderbook.bo.OrderMatching orderMatching;

  /**
   * Gets the field orderMatching.
   * @return the value of the field orderMatching; may be null.
   */
  public ch.soreco.orderbook.bo.OrderMatching getOrderMatching()
  {
    return orderMatching;
  }

  /**
   * Sets the field orderMatching.
   * @param _orderMatching the new value of the field orderMatching.
   */
  public void setOrderMatching(ch.soreco.orderbook.bo.OrderMatching _orderMatching)
  {
    orderMatching = _orderMatching;
  }

  /**
   * ScanShare
   */
  @javax.persistence.ManyToOne(cascade={javax.persistence.CascadeType.PERSIST, javax.persistence.CascadeType.MERGE}, fetch=javax.persistence.FetchType.EAGER)
  @javax.persistence.JoinColumn(name="scanShareId", updatable=false)
  private ch.soreco.orderbook.bo.ScanShare scanShare;

  /**
   * Gets the field scanShare.
   * @return the value of the field scanShare; may be null.
   */
  public ch.soreco.orderbook.bo.ScanShare getScanShare()
  {
    return scanShare;
  }

  /**
   * Sets the field scanShare.
   * @param _scanShare the new value of the field scanShare.
   */
  public void setScanShare(ch.soreco.orderbook.bo.ScanShare _scanShare)
  {
    scanShare = _scanShare;
  }

  private transient ch.ivyteam.ivy.scripting.objects.DateTime archivingDate;

  /**
   * Gets the field archivingDate.
   * @return the value of the field archivingDate; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.DateTime getArchivingDate()
  {
    return archivingDate;
  }

  /**
   * Sets the field archivingDate.
   * @param _archivingDate the new value of the field archivingDate.
   */
  public void setArchivingDate(ch.ivyteam.ivy.scripting.objects.DateTime _archivingDate)
  {
    archivingDate = _archivingDate;
  }

  /**
   * Liste von Attachments welche zu einem Scanning geh�ren
   */
  private transient java.util.List<ch.soreco.orderbook.bo.Attachment> attachments;

  /**
   * Gets the field attachments.
   * @return the value of the field attachments; may be null.
   */
  public java.util.List<ch.soreco.orderbook.bo.Attachment> getAttachments()
  {
    return attachments;
  }

  /**
   * Sets the field attachments.
   * @param _attachments the new value of the field attachments.
   */
  public void setAttachments(java.util.List<ch.soreco.orderbook.bo.Attachment> _attachments)
  {
    attachments = _attachments;
  }

  /**
   * Blob File
   */
  private transient ch.soreco.webbies.common.db.BlobFile scanBlobFile;

  /**
   * Gets the field scanBlobFile.
   * @return the value of the field scanBlobFile; may be null.
   */
  public ch.soreco.webbies.common.db.BlobFile getScanBlobFile()
  {
    return scanBlobFile;
  }

  /**
   * Sets the field scanBlobFile.
   * @param _scanBlobFile the new value of the field scanBlobFile.
   */
  public void setScanBlobFile(ch.soreco.webbies.common.db.BlobFile _scanBlobFile)
  {
    scanBlobFile = _scanBlobFile;
  }

}
