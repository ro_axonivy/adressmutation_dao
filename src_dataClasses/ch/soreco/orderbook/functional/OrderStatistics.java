package ch.soreco.orderbook.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class OrderStatistics", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class OrderStatistics extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 8587372697893118730L;

  private ch.ivyteam.ivy.scripting.objects.Recordset rsOrdersStatistics;

  /**
   * Gets the field rsOrdersStatistics.
   * @return the value of the field rsOrdersStatistics; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRsOrdersStatistics()
  {
    return rsOrdersStatistics;
  }

  /**
   * Sets the field rsOrdersStatistics.
   * @param _rsOrdersStatistics the new value of the field rsOrdersStatistics.
   */
  public void setRsOrdersStatistics(ch.ivyteam.ivy.scripting.objects.Recordset _rsOrdersStatistics)
  {
    rsOrdersStatistics = _rsOrdersStatistics;
  }

  private ch.soreco.orderbook.bo.OrderBook forOrderBook;

  /**
   * Gets the field forOrderBook.
   * @return the value of the field forOrderBook; may be null.
   */
  public ch.soreco.orderbook.bo.OrderBook getForOrderBook()
  {
    return forOrderBook;
  }

  /**
   * Sets the field forOrderBook.
   * @param _forOrderBook the new value of the field forOrderBook.
   */
  public void setForOrderBook(ch.soreco.orderbook.bo.OrderBook _forOrderBook)
  {
    forOrderBook = _forOrderBook;
  }

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

  private java.lang.Integer forYear;

  /**
   * Gets the field forYear.
   * @return the value of the field forYear; may be null.
   */
  public java.lang.Integer getForYear()
  {
    return forYear;
  }

  /**
   * Sets the field forYear.
   * @param _forYear the new value of the field forYear.
   */
  public void setForYear(java.lang.Integer _forYear)
  {
    forYear = _forYear;
  }

  private java.lang.Integer forMonth;

  /**
   * Gets the field forMonth.
   * @return the value of the field forMonth; may be null.
   */
  public java.lang.Integer getForMonth()
  {
    return forMonth;
  }

  /**
   * Sets the field forMonth.
   * @param _forMonth the new value of the field forMonth.
   */
  public void setForMonth(java.lang.Integer _forMonth)
  {
    forMonth = _forMonth;
  }

  private java.lang.Integer forDay;

  /**
   * Gets the field forDay.
   * @return the value of the field forDay; may be null.
   */
  public java.lang.Integer getForDay()
  {
    return forDay;
  }

  /**
   * Sets the field forDay.
   * @param _forDay the new value of the field forDay.
   */
  public void setForDay(java.lang.Integer _forDay)
  {
    forDay = _forDay;
  }

  private java.lang.String sql;

  /**
   * Gets the field sql.
   * @return the value of the field sql; may be null.
   */
  public java.lang.String getSql()
  {
    return sql;
  }

  /**
   * Sets the field sql.
   * @param _sql the new value of the field sql.
   */
  public void setSql(java.lang.String _sql)
  {
    sql = _sql;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.enums.OrderType> forOrderTypes;

  /**
   * Gets the field forOrderTypes.
   * @return the value of the field forOrderTypes; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.enums.OrderType> getForOrderTypes()
  {
    return forOrderTypes;
  }

  /**
   * Sets the field forOrderTypes.
   * @param _forOrderTypes the new value of the field forOrderTypes.
   */
  public void setForOrderTypes(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.enums.OrderType> _forOrderTypes)
  {
    forOrderTypes = _forOrderTypes;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> columns;

  /**
   * Gets the field columns.
   * @return the value of the field columns; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getColumns()
  {
    return columns;
  }

  /**
   * Sets the field columns.
   * @param _columns the new value of the field columns.
   */
  public void setColumns(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _columns)
  {
    columns = _columns;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> aliases;

  /**
   * Gets the field aliases.
   * @return the value of the field aliases; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getAliases()
  {
    return aliases;
  }

  /**
   * Sets the field aliases.
   * @param _aliases the new value of the field aliases.
   */
  public void setAliases(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _aliases)
  {
    aliases = _aliases;
  }

  private java.lang.String additionalField;

  /**
   * Gets the field additionalField.
   * @return the value of the field additionalField; may be null.
   */
  public java.lang.String getAdditionalField()
  {
    return additionalField;
  }

  /**
   * Sets the field additionalField.
   * @param _additionalField the new value of the field additionalField.
   */
  public void setAdditionalField(java.lang.String _additionalField)
  {
    additionalField = _additionalField;
  }

}
