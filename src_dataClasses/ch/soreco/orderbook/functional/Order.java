package ch.soreco.orderbook.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Order", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class Order extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -3147756087264958973L;

  private java.lang.Integer i;

  /**
   * Gets the field i.
   * @return the value of the field i; may be null.
   */
  public java.lang.Integer getI()
  {
    return i;
  }

  /**
   * Sets the field i.
   * @param _i the new value of the field i.
   */
  public void setI(java.lang.Integer _i)
  {
    i = _i;
  }

  private java.lang.Integer j;

  /**
   * Gets the field j.
   * @return the value of the field j; may be null.
   */
  public java.lang.Integer getJ()
  {
    return j;
  }

  /**
   * Sets the field j.
   * @param _j the new value of the field j.
   */
  public void setJ(java.lang.Integer _j)
  {
    j = _j;
  }

  private ch.soreco.orderbook.bo.Order order;

  /**
   * Gets the field order.
   * @return the value of the field order; may be null.
   */
  public ch.soreco.orderbook.bo.Order getOrder()
  {
    return order;
  }

  /**
   * Sets the field order.
   * @param _order the new value of the field order.
   */
  public void setOrder(ch.soreco.orderbook.bo.Order _order)
  {
    order = _order;
  }

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

  private ch.soreco.orderbook.bo.Order filter;

  /**
   * Gets the field filter.
   * @return the value of the field filter; may be null.
   */
  public ch.soreco.orderbook.bo.Order getFilter()
  {
    return filter;
  }

  /**
   * Sets the field filter.
   * @param _filter the new value of the field filter.
   */
  public void setFilter(ch.soreco.orderbook.bo.Order _filter)
  {
    filter = _filter;
  }

  private ch.soreco.orderbook.bo.Contact currentContact;

  /**
   * Gets the field currentContact.
   * @return the value of the field currentContact; may be null.
   */
  public ch.soreco.orderbook.bo.Contact getCurrentContact()
  {
    return currentContact;
  }

  /**
   * Sets the field currentContact.
   * @param _currentContact the new value of the field currentContact.
   */
  public void setCurrentContact(ch.soreco.orderbook.bo.Contact _currentContact)
  {
    currentContact = _currentContact;
  }

  private ch.soreco.orderbook.bo.Paper currentPaper;

  /**
   * Gets the field currentPaper.
   * @return the value of the field currentPaper; may be null.
   */
  public ch.soreco.orderbook.bo.Paper getCurrentPaper()
  {
    return currentPaper;
  }

  /**
   * Sets the field currentPaper.
   * @param _currentPaper the new value of the field currentPaper.
   */
  public void setCurrentPaper(ch.soreco.orderbook.bo.Paper _currentPaper)
  {
    currentPaper = _currentPaper;
  }

  private ch.soreco.orderbook.bo.Order mergedOrder;

  /**
   * Gets the field mergedOrder.
   * @return the value of the field mergedOrder; may be null.
   */
  public ch.soreco.orderbook.bo.Order getMergedOrder()
  {
    return mergedOrder;
  }

  /**
   * Sets the field mergedOrder.
   * @param _mergedOrder the new value of the field mergedOrder.
   */
  public void setMergedOrder(ch.soreco.orderbook.bo.Order _mergedOrder)
  {
    mergedOrder = _mergedOrder;
  }

  private ch.soreco.orderbook.bo.Contact mergedContact;

  /**
   * Gets the field mergedContact.
   * @return the value of the field mergedContact; may be null.
   */
  public ch.soreco.orderbook.bo.Contact getMergedContact()
  {
    return mergedContact;
  }

  /**
   * Sets the field mergedContact.
   * @param _mergedContact the new value of the field mergedContact.
   */
  public void setMergedContact(ch.soreco.orderbook.bo.Contact _mergedContact)
  {
    mergedContact = _mergedContact;
  }

  private ch.soreco.orderbook.enums.OrderState orderState;

  /**
   * Gets the field orderState.
   * @return the value of the field orderState; may be null.
   */
  public ch.soreco.orderbook.enums.OrderState getOrderState()
  {
    return orderState;
  }

  /**
   * Sets the field orderState.
   * @param _orderState the new value of the field orderState.
   */
  public void setOrderState(ch.soreco.orderbook.enums.OrderState _orderState)
  {
    orderState = _orderState;
  }

}
