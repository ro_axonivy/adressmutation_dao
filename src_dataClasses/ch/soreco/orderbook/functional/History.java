package ch.soreco.orderbook.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class History", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class History extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -470515627215146045L;

  private java.lang.String historyTable;

  /**
   * Gets the field historyTable.
   * @return the value of the field historyTable; may be null.
   */
  public java.lang.String getHistoryTable()
  {
    return historyTable;
  }

  /**
   * Sets the field historyTable.
   * @param _historyTable the new value of the field historyTable.
   */
  public void setHistoryTable(java.lang.String _historyTable)
  {
    historyTable = _historyTable;
  }

  private java.lang.Number fetchId;

  /**
   * Gets the field fetchId.
   * @return the value of the field fetchId; may be null.
   */
  public java.lang.Number getFetchId()
  {
    return fetchId;
  }

  /**
   * Sets the field fetchId.
   * @param _fetchId the new value of the field fetchId.
   */
  public void setFetchId(java.lang.Number _fetchId)
  {
    fetchId = _fetchId;
  }

  private java.lang.String idName;

  /**
   * Gets the field idName.
   * @return the value of the field idName; may be null.
   */
  public java.lang.String getIdName()
  {
    return idName;
  }

  /**
   * Sets the field idName.
   * @param _idName the new value of the field idName.
   */
  public void setIdName(java.lang.String _idName)
  {
    idName = _idName;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset historyRS;

  /**
   * Gets the field historyRS.
   * @return the value of the field historyRS; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getHistoryRS()
  {
    return historyRS;
  }

  /**
   * Sets the field historyRS.
   * @param _historyRS the new value of the field historyRS.
   */
  public void setHistoryRS(ch.ivyteam.ivy.scripting.objects.Recordset _historyRS)
  {
    historyRS = _historyRS;
  }

  private java.lang.String redirectUrl;

  /**
   * Gets the field redirectUrl.
   * @return the value of the field redirectUrl; may be null.
   */
  public java.lang.String getRedirectUrl()
  {
    return redirectUrl;
  }

  /**
   * Sets the field redirectUrl.
   * @param _redirectUrl the new value of the field redirectUrl.
   */
  public void setRedirectUrl(java.lang.String _redirectUrl)
  {
    redirectUrl = _redirectUrl;
  }

}
