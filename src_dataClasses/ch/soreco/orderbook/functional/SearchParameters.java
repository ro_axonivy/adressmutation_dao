package ch.soreco.orderbook.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class SearchParameters", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class SearchParameters extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -7052260903993050973L;

  /**
   * BP Id: Identifier for the Customer
   */
  private transient java.lang.String bpId;

  /**
   * Gets the field bpId.
   * @return the value of the field bpId; may be null.
   */
  public java.lang.String getBpId()
  {
    return bpId;
  }

  /**
   * Sets the field bpId.
   * @param _bpId the new value of the field bpId.
   */
  public void setBpId(java.lang.String _bpId)
  {
    bpId = _bpId;
  }

  /**
   * Clan Ref Nr.: Identifier for Pension Customers
   */
  private transient java.lang.String clanRefNo;

  /**
   * Gets the field clanRefNo.
   * @return the value of the field clanRefNo; may be null.
   */
  public java.lang.String getClanRefNo()
  {
    return clanRefNo;
  }

  /**
   * Sets the field clanRefNo.
   * @param _clanRefNo the new value of the field clanRefNo.
   */
  public void setClanRefNo(java.lang.String _clanRefNo)
  {
    clanRefNo = _clanRefNo;
  }

  /**
   * Order Nr.: Identifier of Avaloq Order
   */
  private transient java.lang.String orderNo;

  /**
   * Gets the field orderNo.
   * @return the value of the field orderNo; may be null.
   */
  public java.lang.String getOrderNo()
  {
    return orderNo;
  }

  /**
   * Sets the field orderNo.
   * @param _orderNo the new value of the field orderNo.
   */
  public void setOrderNo(java.lang.String _orderNo)
  {
    orderNo = _orderNo;
  }

  /**
   * Ivy Id: Primary Key of Orders
   */
  private transient java.lang.Integer orderId;

  /**
   * Gets the field orderId.
   * @return the value of the field orderId; may be null.
   */
  public java.lang.Integer getOrderId()
  {
    return orderId;
  }

  /**
   * Sets the field orderId.
   * @param _orderId the new value of the field orderId.
   */
  public void setOrderId(java.lang.Integer _orderId)
  {
    orderId = _orderId;
  }

  /**
   * Meta Type Comment:
   */
  private transient java.lang.String metaTypeComment;

  /**
   * Gets the field metaTypeComment.
   * @return the value of the field metaTypeComment; may be null.
   */
  public java.lang.String getMetaTypeComment()
  {
    return metaTypeComment;
  }

  /**
   * Sets the field metaTypeComment.
   * @param _metaTypeComment the new value of the field metaTypeComment.
   */
  public void setMetaTypeComment(java.lang.String _metaTypeComment)
  {
    metaTypeComment = _metaTypeComment;
  }

  /**
   * Auftragsart: The orderType of an order
   */
  private transient java.lang.String orderType;

  /**
   * Gets the field orderType.
   * @return the value of the field orderType; may be null.
   */
  public java.lang.String getOrderType()
  {
    return orderType;
  }

  /**
   * Sets the field orderType.
   * @param _orderType the new value of the field orderType.
   */
  public void setOrderType(java.lang.String _orderType)
  {
    orderType = _orderType;
  }

  /**
   * Status: The orderState of an order
   */
  private transient java.lang.String orderState;

  /**
   * Gets the field orderState.
   * @return the value of the field orderState; may be null.
   */
  public java.lang.String getOrderState()
  {
    return orderState;
  }

  /**
   * Sets the field orderState.
   * @param _orderState the new value of the field orderState.
   */
  public void setOrderState(java.lang.String _orderState)
  {
    orderState = _orderState;
  }

  /**
   * Dispatcher: The Dispatcher of an Order
   */
  private transient java.lang.String dispatcherUserId;

  /**
   * Gets the field dispatcherUserId.
   * @return the value of the field dispatcherUserId; may be null.
   */
  public java.lang.String getDispatcherUserId()
  {
    return dispatcherUserId;
  }

  /**
   * Sets the field dispatcherUserId.
   * @param _dispatcherUserId the new value of the field dispatcherUserId.
   */
  public void setDispatcherUserId(java.lang.String _dispatcherUserId)
  {
    dispatcherUserId = _dispatcherUserId;
  }

  /**
   * Bearbeiter: The Editor of an Order
   */
  private transient java.lang.String editorUserId;

  /**
   * Gets the field editorUserId.
   * @return the value of the field editorUserId; may be null.
   */
  public java.lang.String getEditorUserId()
  {
    return editorUserId;
  }

  /**
   * Sets the field editorUserId.
   * @param _editorUserId the new value of the field editorUserId.
   */
  public void setEditorUserId(java.lang.String _editorUserId)
  {
    editorUserId = _editorUserId;
  }

  /**
   * Pr�fer: The 4 Eye Checker of an Order
   */
  private transient java.lang.String checkerUserId;

  /**
   * Gets the field checkerUserId.
   * @return the value of the field checkerUserId; may be null.
   */
  public java.lang.String getCheckerUserId()
  {
    return checkerUserId;
  }

  /**
   * Sets the field checkerUserId.
   * @param _checkerUserId the new value of the field checkerUserId.
   */
  public void setCheckerUserId(java.lang.String _checkerUserId)
  {
    checkerUserId = _checkerUserId;
  }

  /**
   * Kateg.: The Category of an Order (1-5)
   */
  private transient java.lang.Integer priority;

  /**
   * Gets the field priority.
   * @return the value of the field priority; may be null.
   */
  public java.lang.Integer getPriority()
  {
    return priority;
  }

  /**
   * Sets the field priority.
   * @param _priority the new value of the field priority.
   */
  public void setPriority(java.lang.Integer _priority)
  {
    priority = _priority;
  }

  /**
   * Prio: The Quantity Type of an Order (A-C), Orders with Quantity A need to be displayed on top.
   */
  private transient java.lang.String orderQuantityType;

  /**
   * Gets the field orderQuantityType.
   * @return the value of the field orderQuantityType; may be null.
   */
  public java.lang.String getOrderQuantityType()
  {
    return orderQuantityType;
  }

  /**
   * Sets the field orderQuantityType.
   * @param _orderQuantityType the new value of the field orderQuantityType.
   */
  public void setOrderQuantityType(java.lang.String _orderQuantityType)
  {
    orderQuantityType = _orderQuantityType;
  }

  /**
   * inkl. Archiv: wheter to include OrderStates Archived & Aborted
   */
  private transient java.lang.Boolean includeArchive;

  /**
   * Gets the field includeArchive.
   * @return the value of the field includeArchive; may be null.
   */
  public java.lang.Boolean getIncludeArchive()
  {
    return includeArchive;
  }

  /**
   * Sets the field includeArchive.
   * @param _includeArchive the new value of the field includeArchive.
   */
  public void setIncludeArchive(java.lang.Boolean _includeArchive)
  {
    includeArchive = _includeArchive;
  }

  /**
   * OrderStates to exclude
   */
  private transient java.util.List<ch.soreco.orderbook.enums.OrderState> excludedOrderStates;

  /**
   * Gets the field excludedOrderStates.
   * @return the value of the field excludedOrderStates; may be null.
   */
  public java.util.List<ch.soreco.orderbook.enums.OrderState> getExcludedOrderStates()
  {
    return excludedOrderStates;
  }

  /**
   * Sets the field excludedOrderStates.
   * @param _excludedOrderStates the new value of the field excludedOrderStates.
   */
  public void setExcludedOrderStates(java.util.List<ch.soreco.orderbook.enums.OrderState> _excludedOrderStates)
  {
    excludedOrderStates = _excludedOrderStates;
  }

  /**
   * Zuletzt Bearbeitet von: The Last Edited Date from defines the lower bound lastEdited date range
   */
  private transient ch.ivyteam.ivy.scripting.objects.Date lastEditFrom;

  /**
   * Gets the field lastEditFrom.
   * @return the value of the field lastEditFrom; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Date getLastEditFrom()
  {
    return lastEditFrom;
  }

  /**
   * Sets the field lastEditFrom.
   * @param _lastEditFrom the new value of the field lastEditFrom.
   */
  public void setLastEditFrom(ch.ivyteam.ivy.scripting.objects.Date _lastEditFrom)
  {
    lastEditFrom = _lastEditFrom;
  }

  /**
   * Zuletzt Bearbeitet bis: The Last Edited Date until defines the upper bound lastEdited date range
   */
  private transient ch.ivyteam.ivy.scripting.objects.Date lastEditUntil;

  /**
   * Gets the field lastEditUntil.
   * @return the value of the field lastEditUntil; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Date getLastEditUntil()
  {
    return lastEditUntil;
  }

  /**
   * Sets the field lastEditUntil.
   * @param _lastEditUntil the new value of the field lastEditUntil.
   */
  public void setLastEditUntil(ch.ivyteam.ivy.scripting.objects.Date _lastEditUntil)
  {
    lastEditUntil = _lastEditUntil;
  }

  /**
   * Select Top Results
   */
  private transient java.lang.Integer selectTop;

  /**
   * Gets the field selectTop.
   * @return the value of the field selectTop; may be null.
   */
  public java.lang.Integer getSelectTop()
  {
    return selectTop;
  }

  /**
   * Sets the field selectTop.
   * @param _selectTop the new value of the field selectTop.
   */
  public void setSelectTop(java.lang.Integer _selectTop)
  {
    selectTop = _selectTop;
  }

  /**
   * Auftrag Start von
   */
  private transient ch.ivyteam.ivy.scripting.objects.Date orderStartFrom;

  /**
   * Gets the field orderStartFrom.
   * @return the value of the field orderStartFrom; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Date getOrderStartFrom()
  {
    return orderStartFrom;
  }

  /**
   * Sets the field orderStartFrom.
   * @param _orderStartFrom the new value of the field orderStartFrom.
   */
  public void setOrderStartFrom(ch.ivyteam.ivy.scripting.objects.Date _orderStartFrom)
  {
    orderStartFrom = _orderStartFrom;
  }

  /**
   * Auftrag Start bis
   */
  private transient ch.ivyteam.ivy.scripting.objects.Date orderStartUntil;

  /**
   * Gets the field orderStartUntil.
   * @return the value of the field orderStartUntil; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Date getOrderStartUntil()
  {
    return orderStartUntil;
  }

  /**
   * Sets the field orderStartUntil.
   * @param _orderStartUntil the new value of the field orderStartUntil.
   */
  public void setOrderStartUntil(ch.ivyteam.ivy.scripting.objects.Date _orderStartUntil)
  {
    orderStartUntil = _orderStartUntil;
  }

  private transient java.lang.String fdl;

  /**
   * Gets the field fdl.
   * @return the value of the field fdl; may be null.
   */
  public java.lang.String getFdl()
  {
    return fdl;
  }

  /**
   * Sets the field fdl.
   * @param _fdl the new value of the field fdl.
   */
  public void setFdl(java.lang.String _fdl)
  {
    fdl = _fdl;
  }

}
