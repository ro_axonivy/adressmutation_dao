package ch.soreco.orderbook.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Contact", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class Contact extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -833991363064513827L;

  private ch.soreco.orderbook.bo.Contact contact;

  /**
   * Gets the field contact.
   * @return the value of the field contact; may be null.
   */
  public ch.soreco.orderbook.bo.Contact getContact()
  {
    return contact;
  }

  /**
   * Sets the field contact.
   * @param _contact the new value of the field contact.
   */
  public void setContact(ch.soreco.orderbook.bo.Contact _contact)
  {
    contact = _contact;
  }

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private ch.soreco.orderbook.bo.Contact filter;

  /**
   * Gets the field filter.
   * @return the value of the field filter; may be null.
   */
  public ch.soreco.orderbook.bo.Contact getFilter()
  {
    return filter;
  }

  /**
   * Sets the field filter.
   * @param _filter the new value of the field filter.
   */
  public void setFilter(ch.soreco.orderbook.bo.Contact _filter)
  {
    filter = _filter;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Contact> contacts;

  /**
   * Gets the field contacts.
   * @return the value of the field contacts; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Contact> getContacts()
  {
    return contacts;
  }

  /**
   * Sets the field contacts.
   * @param _contacts the new value of the field contacts.
   */
  public void setContacts(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Contact> _contacts)
  {
    contacts = _contacts;
  }

  private java.lang.Integer i;

  /**
   * Gets the field i.
   * @return the value of the field i; may be null.
   */
  public java.lang.Integer getI()
  {
    return i;
  }

  /**
   * Sets the field i.
   * @param _i the new value of the field i.
   */
  public void setI(java.lang.Integer _i)
  {
    i = _i;
  }

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

}
