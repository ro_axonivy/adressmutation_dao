package ch.soreco.orderbook.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class ScanLog", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class ScanLog extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -8227211136667194914L;

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

  private ch.soreco.orderbook.bo.ScanLog filter;

  /**
   * Gets the field filter.
   * @return the value of the field filter; may be null.
   */
  public ch.soreco.orderbook.bo.ScanLog getFilter()
  {
    return filter;
  }

  /**
   * Sets the field filter.
   * @param _filter the new value of the field filter.
   */
  public void setFilter(ch.soreco.orderbook.bo.ScanLog _filter)
  {
    filter = _filter;
  }

  private ch.soreco.orderbook.bo.ScanLog ScanLog;

  /**
   * Gets the field ScanLog.
   * @return the value of the field ScanLog; may be null.
   */
  public ch.soreco.orderbook.bo.ScanLog getScanLog()
  {
    return ScanLog;
  }

  /**
   * Sets the field ScanLog.
   * @param _ScanLog the new value of the field ScanLog.
   */
  public void setScanLog(ch.soreco.orderbook.bo.ScanLog _ScanLog)
  {
    ScanLog = _ScanLog;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.ScanLog> ScanLogs;

  /**
   * Gets the field ScanLogs.
   * @return the value of the field ScanLogs; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.ScanLog> getScanLogs()
  {
    return ScanLogs;
  }

  /**
   * Sets the field ScanLogs.
   * @param _ScanLogs the new value of the field ScanLogs.
   */
  public void setScanLogs(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.ScanLog> _ScanLogs)
  {
    ScanLogs = _ScanLogs;
  }

}
