package ch.soreco.orderbook.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class OrderBook", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class OrderBook extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 190031898955799197L;

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> orderBook;

  /**
   * Gets the field orderBook.
   * @return the value of the field orderBook; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> getOrderBook()
  {
    return orderBook;
  }

  /**
   * Sets the field orderBook.
   * @param _orderBook the new value of the field orderBook.
   */
  public void setOrderBook(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> _orderBook)
  {
    orderBook = _orderBook;
  }

  private ch.soreco.orderbook.bo.Order filter;

  /**
   * Gets the field filter.
   * @return the value of the field filter; may be null.
   */
  public ch.soreco.orderbook.bo.Order getFilter()
  {
    return filter;
  }

  /**
   * Sets the field filter.
   * @param _filter the new value of the field filter.
   */
  public void setFilter(ch.soreco.orderbook.bo.Order _filter)
  {
    filter = _filter;
  }

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset rsOrderBook;

  /**
   * Gets the field rsOrderBook.
   * @return the value of the field rsOrderBook; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRsOrderBook()
  {
    return rsOrderBook;
  }

  /**
   * Sets the field rsOrderBook.
   * @param _rsOrderBook the new value of the field rsOrderBook.
   */
  public void setRsOrderBook(ch.ivyteam.ivy.scripting.objects.Recordset _rsOrderBook)
  {
    rsOrderBook = _rsOrderBook;
  }

  private java.lang.String searchString;

  /**
   * Gets the field searchString.
   * @return the value of the field searchString; may be null.
   */
  public java.lang.String getSearchString()
  {
    return searchString;
  }

  /**
   * Sets the field searchString.
   * @param _searchString the new value of the field searchString.
   */
  public void setSearchString(java.lang.String _searchString)
  {
    searchString = _searchString;
  }

  private ch.ivyteam.ivy.process.data.persistence.IIvyQuery query;

  /**
   * Gets the field query.
   * @return the value of the field query; may be null.
   */
  public ch.ivyteam.ivy.process.data.persistence.IIvyQuery getQuery()
  {
    return query;
  }

  /**
   * Sets the field query.
   * @param _query the new value of the field query.
   */
  public void setQuery(ch.ivyteam.ivy.process.data.persistence.IIvyQuery _query)
  {
    query = _query;
  }

  private ch.soreco.orderbook.bo.OrderBook orderBookObject;

  /**
   * Gets the field orderBookObject.
   * @return the value of the field orderBookObject; may be null.
   */
  public ch.soreco.orderbook.bo.OrderBook getOrderBookObject()
  {
    return orderBookObject;
  }

  /**
   * Sets the field orderBookObject.
   * @param _orderBookObject the new value of the field orderBookObject.
   */
  public void setOrderBookObject(ch.soreco.orderbook.bo.OrderBook _orderBookObject)
  {
    orderBookObject = _orderBookObject;
  }

  private ch.soreco.orderbook.bo.OrderBook orderBookFilter;

  /**
   * Gets the field orderBookFilter.
   * @return the value of the field orderBookFilter; may be null.
   */
  public ch.soreco.orderbook.bo.OrderBook getOrderBookFilter()
  {
    return orderBookFilter;
  }

  /**
   * Sets the field orderBookFilter.
   * @param _orderBookFilter the new value of the field orderBookFilter.
   */
  public void setOrderBookFilter(ch.soreco.orderbook.bo.OrderBook _orderBookFilter)
  {
    orderBookFilter = _orderBookFilter;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.OrderBook> orderBookObjects;

  /**
   * Gets the field orderBookObjects.
   * @return the value of the field orderBookObjects; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.OrderBook> getOrderBookObjects()
  {
    return orderBookObjects;
  }

  /**
   * Sets the field orderBookObjects.
   * @param _orderBookObjects the new value of the field orderBookObjects.
   */
  public void setOrderBookObjects(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.OrderBook> _orderBookObjects)
  {
    orderBookObjects = _orderBookObjects;
  }

  private ch.soreco.common.bo.AuthorityGroup authorityGroup;

  /**
   * Gets the field authorityGroup.
   * @return the value of the field authorityGroup; may be null.
   */
  public ch.soreco.common.bo.AuthorityGroup getAuthorityGroup()
  {
    return authorityGroup;
  }

  /**
   * Sets the field authorityGroup.
   * @param _authorityGroup the new value of the field authorityGroup.
   */
  public void setAuthorityGroup(ch.soreco.common.bo.AuthorityGroup _authorityGroup)
  {
    authorityGroup = _authorityGroup;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.common.bo.AuthorityGroup> authorityGroups;

  /**
   * Gets the field authorityGroups.
   * @return the value of the field authorityGroups; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.common.bo.AuthorityGroup> getAuthorityGroups()
  {
    return authorityGroups;
  }

  /**
   * Sets the field authorityGroups.
   * @param _authorityGroups the new value of the field authorityGroups.
   */
  public void setAuthorityGroups(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.common.bo.AuthorityGroup> _authorityGroups)
  {
    authorityGroups = _authorityGroups;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.OrderBook> orderBooksTemp;

  /**
   * Gets the field orderBooksTemp.
   * @return the value of the field orderBooksTemp; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.OrderBook> getOrderBooksTemp()
  {
    return orderBooksTemp;
  }

  /**
   * Sets the field orderBooksTemp.
   * @param _orderBooksTemp the new value of the field orderBooksTemp.
   */
  public void setOrderBooksTemp(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.OrderBook> _orderBooksTemp)
  {
    orderBooksTemp = _orderBooksTemp;
  }

  private java.lang.Boolean withoutTypes;

  /**
   * Gets the field withoutTypes.
   * @return the value of the field withoutTypes; may be null.
   */
  public java.lang.Boolean getWithoutTypes()
  {
    return withoutTypes;
  }

  /**
   * Sets the field withoutTypes.
   * @param _withoutTypes the new value of the field withoutTypes.
   */
  public void setWithoutTypes(java.lang.Boolean _withoutTypes)
  {
    withoutTypes = _withoutTypes;
  }

  private java.lang.Integer lotSize;

  /**
   * Gets the field lotSize.
   * @return the value of the field lotSize; may be null.
   */
  public java.lang.Integer getLotSize()
  {
    return lotSize;
  }

  /**
   * Sets the field lotSize.
   * @param _lotSize the new value of the field lotSize.
   */
  public void setLotSize(java.lang.Integer _lotSize)
  {
    lotSize = _lotSize;
  }

  private ch.ivyteam.ivy.scripting.objects.List<Object> columnData;

  /**
   * Gets the field columnData.
   * @return the value of the field columnData; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<Object> getColumnData()
  {
    return columnData;
  }

  /**
   * Sets the field columnData.
   * @param _columnData the new value of the field columnData.
   */
  public void setColumnData(ch.ivyteam.ivy.scripting.objects.List<Object> _columnData)
  {
    columnData = _columnData;
  }

  private java.lang.String columnName;

  /**
   * Gets the field columnName.
   * @return the value of the field columnName; may be null.
   */
  public java.lang.String getColumnName()
  {
    return columnName;
  }

  /**
   * Sets the field columnName.
   * @param _columnName the new value of the field columnName.
   */
  public void setColumnName(java.lang.String _columnName)
  {
    columnName = _columnName;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> excludedOrderStates;

  /**
   * Gets the field excludedOrderStates.
   * @return the value of the field excludedOrderStates; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getExcludedOrderStates()
  {
    return excludedOrderStates;
  }

  /**
   * Sets the field excludedOrderStates.
   * @param _excludedOrderStates the new value of the field excludedOrderStates.
   */
  public void setExcludedOrderStates(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _excludedOrderStates)
  {
    excludedOrderStates = _excludedOrderStates;
  }

  private java.lang.Number i;

  /**
   * Gets the field i.
   * @return the value of the field i; may be null.
   */
  public java.lang.Number getI()
  {
    return i;
  }

  /**
   * Sets the field i.
   * @param _i the new value of the field i.
   */
  public void setI(java.lang.Number _i)
  {
    i = _i;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset tempRS;

  /**
   * Gets the field tempRS.
   * @return the value of the field tempRS; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getTempRS()
  {
    return tempRS;
  }

  /**
   * Sets the field tempRS.
   * @param _tempRS the new value of the field tempRS.
   */
  public void setTempRS(ch.ivyteam.ivy.scripting.objects.Recordset _tempRS)
  {
    tempRS = _tempRS;
  }

  private java.lang.Number i2;

  /**
   * Gets the field i2.
   * @return the value of the field i2; may be null.
   */
  public java.lang.Number getI2()
  {
    return i2;
  }

  /**
   * Sets the field i2.
   * @param _i2 the new value of the field i2.
   */
  public void setI2(java.lang.Number _i2)
  {
    i2 = _i2;
  }

}
