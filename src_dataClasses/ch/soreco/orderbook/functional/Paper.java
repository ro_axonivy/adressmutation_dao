package ch.soreco.orderbook.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Paper", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class Paper extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 1205958596408180141L;

  private ch.soreco.orderbook.bo.Paper paper;

  /**
   * Gets the field paper.
   * @return the value of the field paper; may be null.
   */
  public ch.soreco.orderbook.bo.Paper getPaper()
  {
    return paper;
  }

  /**
   * Sets the field paper.
   * @param _paper the new value of the field paper.
   */
  public void setPaper(ch.soreco.orderbook.bo.Paper _paper)
  {
    paper = _paper;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Paper> papers;

  /**
   * Gets the field papers.
   * @return the value of the field papers; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Paper> getPapers()
  {
    return papers;
  }

  /**
   * Sets the field papers.
   * @param _papers the new value of the field papers.
   */
  public void setPapers(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Paper> _papers)
  {
    papers = _papers;
  }

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private ch.soreco.orderbook.bo.Paper filter;

  /**
   * Gets the field filter.
   * @return the value of the field filter; may be null.
   */
  public ch.soreco.orderbook.bo.Paper getFilter()
  {
    return filter;
  }

  /**
   * Sets the field filter.
   * @param _filter the new value of the field filter.
   */
  public void setFilter(ch.soreco.orderbook.bo.Paper _filter)
  {
    filter = _filter;
  }

  private java.lang.Integer contactId;

  /**
   * Gets the field contactId.
   * @return the value of the field contactId; may be null.
   */
  public java.lang.Integer getContactId()
  {
    return contactId;
  }

  /**
   * Sets the field contactId.
   * @param _contactId the new value of the field contactId.
   */
  public void setContactId(java.lang.Integer _contactId)
  {
    contactId = _contactId;
  }

  private ch.soreco.orderbook.bo.Contact contact;

  /**
   * Gets the field contact.
   * @return the value of the field contact; may be null.
   */
  public ch.soreco.orderbook.bo.Contact getContact()
  {
    return contact;
  }

  /**
   * Sets the field contact.
   * @param _contact the new value of the field contact.
   */
  public void setContact(ch.soreco.orderbook.bo.Contact _contact)
  {
    contact = _contact;
  }

}
