package ch.soreco.orderbook.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class TimeRemainingConfig", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class TimeRemainingConfig extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 3499454084620816141L;

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private ch.soreco.orderbook.bo.TimeRemainingConfig TimeRemainingConfig;

  /**
   * Gets the field TimeRemainingConfig.
   * @return the value of the field TimeRemainingConfig; may be null.
   */
  public ch.soreco.orderbook.bo.TimeRemainingConfig getTimeRemainingConfig()
  {
    return TimeRemainingConfig;
  }

  /**
   * Sets the field TimeRemainingConfig.
   * @param _TimeRemainingConfig the new value of the field TimeRemainingConfig.
   */
  public void setTimeRemainingConfig(ch.soreco.orderbook.bo.TimeRemainingConfig _TimeRemainingConfig)
  {
    TimeRemainingConfig = _TimeRemainingConfig;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.TimeRemainingConfig> TimeRemainingConfigs;

  /**
   * Gets the field TimeRemainingConfigs.
   * @return the value of the field TimeRemainingConfigs; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.TimeRemainingConfig> getTimeRemainingConfigs()
  {
    return TimeRemainingConfigs;
  }

  /**
   * Sets the field TimeRemainingConfigs.
   * @param _TimeRemainingConfigs the new value of the field TimeRemainingConfigs.
   */
  public void setTimeRemainingConfigs(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.TimeRemainingConfig> _TimeRemainingConfigs)
  {
    TimeRemainingConfigs = _TimeRemainingConfigs;
  }

  private ch.soreco.orderbook.bo.TimeRemainingConfig filter;

  /**
   * Gets the field filter.
   * @return the value of the field filter; may be null.
   */
  public ch.soreco.orderbook.bo.TimeRemainingConfig getFilter()
  {
    return filter;
  }

  /**
   * Sets the field filter.
   * @param _filter the new value of the field filter.
   */
  public void setFilter(ch.soreco.orderbook.bo.TimeRemainingConfig _filter)
  {
    filter = _filter;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset Recordset;

  /**
   * Gets the field Recordset.
   * @return the value of the field Recordset; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRecordset()
  {
    return Recordset;
  }

  /**
   * Sets the field Recordset.
   * @param _Recordset the new value of the field Recordset.
   */
  public void setRecordset(ch.ivyteam.ivy.scripting.objects.Recordset _Recordset)
  {
    Recordset = _Recordset;
  }

}
