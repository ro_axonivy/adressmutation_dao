package ch.soreco.orderbook.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Dispatching", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class Dispatching extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 3962589225037892748L;

  private ch.soreco.orderbook.bo.Dispatching filter;

  /**
   * Gets the field filter.
   * @return the value of the field filter; may be null.
   */
  public ch.soreco.orderbook.bo.Dispatching getFilter()
  {
    return filter;
  }

  /**
   * Sets the field filter.
   * @param _filter the new value of the field filter.
   */
  public void setFilter(ch.soreco.orderbook.bo.Dispatching _filter)
  {
    filter = _filter;
  }

  private ch.soreco.orderbook.bo.Dispatching dispatchingData;

  /**
   * Gets the field dispatchingData.
   * @return the value of the field dispatchingData; may be null.
   */
  public ch.soreco.orderbook.bo.Dispatching getDispatchingData()
  {
    return dispatchingData;
  }

  /**
   * Sets the field dispatchingData.
   * @param _dispatchingData the new value of the field dispatchingData.
   */
  public void setDispatchingData(ch.soreco.orderbook.bo.Dispatching _dispatchingData)
  {
    dispatchingData = _dispatchingData;
  }

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset dispatchingRS;

  /**
   * Gets the field dispatchingRS.
   * @return the value of the field dispatchingRS; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getDispatchingRS()
  {
    return dispatchingRS;
  }

  /**
   * Sets the field dispatchingRS.
   * @param _dispatchingRS the new value of the field dispatchingRS.
   */
  public void setDispatchingRS(ch.ivyteam.ivy.scripting.objects.Recordset _dispatchingRS)
  {
    dispatchingRS = _dispatchingRS;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Scanning> scannings;

  /**
   * Gets the field scannings.
   * @return the value of the field scannings; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Scanning> getScannings()
  {
    return scannings;
  }

  /**
   * Sets the field scannings.
   * @param _scannings the new value of the field scannings.
   */
  public void setScannings(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Scanning> _scannings)
  {
    scannings = _scannings;
  }

}
