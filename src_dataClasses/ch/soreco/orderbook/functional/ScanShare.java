package ch.soreco.orderbook.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class ScanShare", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class ScanShare extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -501906229139504780L;

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

  private ch.soreco.orderbook.bo.ScanShare filter;

  /**
   * Gets the field filter.
   * @return the value of the field filter; may be null.
   */
  public ch.soreco.orderbook.bo.ScanShare getFilter()
  {
    return filter;
  }

  /**
   * Sets the field filter.
   * @param _filter the new value of the field filter.
   */
  public void setFilter(ch.soreco.orderbook.bo.ScanShare _filter)
  {
    filter = _filter;
  }

  private ch.soreco.orderbook.bo.ScanShare ScanShare;

  /**
   * Gets the field ScanShare.
   * @return the value of the field ScanShare; may be null.
   */
  public ch.soreco.orderbook.bo.ScanShare getScanShare()
  {
    return ScanShare;
  }

  /**
   * Sets the field ScanShare.
   * @param _ScanShare the new value of the field ScanShare.
   */
  public void setScanShare(ch.soreco.orderbook.bo.ScanShare _ScanShare)
  {
    ScanShare = _ScanShare;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.ScanShare> ScanShares;

  /**
   * Gets the field ScanShares.
   * @return the value of the field ScanShares; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.ScanShare> getScanShares()
  {
    return ScanShares;
  }

  /**
   * Sets the field ScanShares.
   * @param _ScanShares the new value of the field ScanShares.
   */
  public void setScanShares(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.ScanShare> _ScanShares)
  {
    ScanShares = _ScanShares;
  }

}
