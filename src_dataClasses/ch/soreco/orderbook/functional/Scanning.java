package ch.soreco.orderbook.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Scanning", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class Scanning extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -5457978512359291857L;

  private ch.soreco.orderbook.bo.Scanning Scan;

  /**
   * Gets the field Scan.
   * @return the value of the field Scan; may be null.
   */
  public ch.soreco.orderbook.bo.Scanning getScan()
  {
    return Scan;
  }

  /**
   * Sets the field Scan.
   * @param _Scan the new value of the field Scan.
   */
  public void setScan(ch.soreco.orderbook.bo.Scanning _Scan)
  {
    Scan = _Scan;
  }

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

  private ch.soreco.orderbook.bo.Scanning filter;

  /**
   * Gets the field filter.
   * @return the value of the field filter; may be null.
   */
  public ch.soreco.orderbook.bo.Scanning getFilter()
  {
    return filter;
  }

  /**
   * Sets the field filter.
   * @param _filter the new value of the field filter.
   */
  public void setFilter(ch.soreco.orderbook.bo.Scanning _filter)
  {
    filter = _filter;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Scanning> scannings;

  /**
   * Gets the field scannings.
   * @return the value of the field scannings; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Scanning> getScannings()
  {
    return scannings;
  }

  /**
   * Sets the field scannings.
   * @param _scannings the new value of the field scannings.
   */
  public void setScannings(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Scanning> _scannings)
  {
    scannings = _scannings;
  }

  private java.lang.Integer i;

  /**
   * Gets the field i.
   * @return the value of the field i; may be null.
   */
  public java.lang.Integer getI()
  {
    return i;
  }

  /**
   * Sets the field i.
   * @param _i the new value of the field i.
   */
  public void setI(java.lang.Integer _i)
  {
    i = _i;
  }

  private java.lang.Integer orderId;

  /**
   * Gets the field orderId.
   * @return the value of the field orderId; may be null.
   */
  public java.lang.Integer getOrderId()
  {
    return orderId;
  }

  /**
   * Sets the field orderId.
   * @param _orderId the new value of the field orderId.
   */
  public void setOrderId(java.lang.Integer _orderId)
  {
    orderId = _orderId;
  }

  private java.lang.Boolean loadFiles;

  /**
   * Gets the field loadFiles.
   * @return the value of the field loadFiles; may be null.
   */
  public java.lang.Boolean getLoadFiles()
  {
    return loadFiles;
  }

  /**
   * Sets the field loadFiles.
   * @param _loadFiles the new value of the field loadFiles.
   */
  public void setLoadFiles(java.lang.Boolean _loadFiles)
  {
    loadFiles = _loadFiles;
  }

}
