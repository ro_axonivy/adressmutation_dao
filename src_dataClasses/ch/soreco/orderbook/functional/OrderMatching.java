package ch.soreco.orderbook.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class OrderMatching", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class OrderMatching extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -328996064620687636L;

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

  private ch.soreco.orderbook.bo.OrderMatching orderMatching;

  /**
   * Gets the field orderMatching.
   * @return the value of the field orderMatching; may be null.
   */
  public ch.soreco.orderbook.bo.OrderMatching getOrderMatching()
  {
    return orderMatching;
  }

  /**
   * Sets the field orderMatching.
   * @param _orderMatching the new value of the field orderMatching.
   */
  public void setOrderMatching(ch.soreco.orderbook.bo.OrderMatching _orderMatching)
  {
    orderMatching = _orderMatching;
  }

  private ch.soreco.orderbook.bo.OrderMatching filter;

  /**
   * Gets the field filter.
   * @return the value of the field filter; may be null.
   */
  public ch.soreco.orderbook.bo.OrderMatching getFilter()
  {
    return filter;
  }

  /**
   * Sets the field filter.
   * @param _filter the new value of the field filter.
   */
  public void setFilter(ch.soreco.orderbook.bo.OrderMatching _filter)
  {
    filter = _filter;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.OrderMatching> orderMatchings;

  /**
   * Gets the field orderMatchings.
   * @return the value of the field orderMatchings; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.OrderMatching> getOrderMatchings()
  {
    return orderMatchings;
  }

  /**
   * Sets the field orderMatchings.
   * @param _orderMatchings the new value of the field orderMatchings.
   */
  public void setOrderMatchings(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.OrderMatching> _orderMatchings)
  {
    orderMatchings = _orderMatchings;
  }

}
