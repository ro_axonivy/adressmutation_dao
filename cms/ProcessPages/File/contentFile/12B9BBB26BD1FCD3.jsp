<%@ page import="ch.ivyteam.ivy.page.engine.jsp.IvyJSP"%>
<jsp:useBean id="ivy" class="ch.ivyteam.ivy.page.engine.jsp.IvyJSP" scope="session"/> 
<% ivy.setRequest(request); %>


<%@ page  import="java.io.FileInputStream" %>
<%@ page  import="java.io.BufferedInputStream"  %>
<%@ page  import="java.io.File"  %>
<%@ page import="java.io.IOException" %>

<%

String fileUrl = ivy.html.get("in.fileUrl");
String fileName = ivy.html.get("in.file.fileName");
String fileContentType = ivy.html.get("in.file.contentType");

File myfile = new File(fileUrl);
if(myfile!=null) {
	//Declare Stream
	ServletOutputStream sout=null;
	FileInputStream in=null;

	try{
	  //set response headers
		sout= response.getOutputStream();   

	  response.setContentType(fileContentType );
	  response.setContentLength( (int) myfile.length( ) );
	  response.addHeader("Content-Disposition","inline; filename="+fileName);
		response.setHeader("Cache-Control", "no-cache");


  	

	  //get Binary Data   
		in= new FileInputStream(fileUrl);
		byte[] buf = new byte[8192];
		int length;
		while ((length = in.read(buf)) != -1) {
			sout.write(buf, 0, length);
		}		
	
	} catch (IOException ioe){     
	        throw new ServletException(ioe.getMessage());
	} finally {
		if(in!=null) {
			in.close();
		}
		//close the output stream
	  if (sout != null) {
			sout.flush();
			sout.close();
		}
	}   

}
%>