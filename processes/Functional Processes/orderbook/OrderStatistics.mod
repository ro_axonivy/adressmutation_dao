[Ivy]
[>Created: Tue Oct 01 10:42:47 CEST 2013]
1314C2D77FCB5DBB 3.15 #module
>Proto >Proto Collection #zClass
Os0 OrderStatistics Big #zClass
Os0 B #cInfo
Os0 #process
Os0 @TextInP .resExport .resExport #zField
Os0 @TextInP .type .type #zField
Os0 @TextInP .processKind .processKind #zField
Os0 @AnnotationInP-0n ai ai #zField
Os0 @TextInP .xml .xml #zField
Os0 @TextInP .responsibility .responsibility #zField
Os0 @EndSub f9 '' #zField
Os0 @GridStep f10 '' #zField
Os0 @DBStep f11 '' #zField
Os0 @StartSub f12 '' #zField
Os0 @GridStep f13 '' #zField
Os0 @PushWFArc f15 '' #zField
Os0 @PushWFArc f16 '' #zField
Os0 @PushWFArc f17 '' #zField
Os0 @GridStep f0 '' #zField
Os0 @GridStep f1 '' #zField
Os0 @EndSub f2 '' #zField
Os0 @StartSub f3 '' #zField
Os0 @DBStep f4 '' #zField
Os0 @PushWFArc f5 '' #zField
Os0 @PushWFArc f6 '' #zField
Os0 @PushWFArc f7 '' #zField
Os0 @PushWFArc f8 '' #zField
Os0 @DBStep f18 '' #zField
Os0 @EndSub f19 '' #zField
Os0 @GridStep f20 '' #zField
Os0 @GridStep f21 '' #zField
Os0 @StartSub f22 '' #zField
Os0 @PushWFArc f23 '' #zField
Os0 @PushWFArc f25 '' #zField
Os0 @PushWFArc f26 '' #zField
Os0 @PushWFArc f24 '' #zField
Os0 @GridStep f27 '' #zField
Os0 @StartSub f28 '' #zField
Os0 @EndSub f29 '' #zField
Os0 @GridStep f30 '' #zField
Os0 @DBStep f31 '' #zField
Os0 @PushWFArc f32 '' #zField
Os0 @PushWFArc f33 '' #zField
Os0 @PushWFArc f34 '' #zField
Os0 @PushWFArc f35 '' #zField
Os0 @CallSub f36 '' #zField
Os0 @PushWFArc f37 '' #zField
Os0 @PushWFArc f14 '' #zField
>Proto Os0 Os0 OrderStatistics #zField
Os0 f9 type ch.soreco.orderbook.functional.OrderStatistics #txt
Os0 f9 363 299 26 26 14 0 #rect
Os0 f9 @|EndSubIcon #fIcon
Os0 f10 actionDecl 'ch.soreco.orderbook.functional.OrderStatistics out;
' #txt
Os0 f10 actionTable 'out=in;
' #txt
Os0 f10 actionCode 'import org.apache.commons.lang.StringUtils;
import ch.soreco.orderbook.enums.OrderType;
StringBuilder sql = new StringBuilder("");
sql.append("SELECT
	orderType as [Auftragstyp], 
	COUNT(orderType) as [Eingang],
	SUM(CASE WHEN orderState in (''Finished'', ''Archived'', ''Deleted'', ''Aborted'') THEN 1 ELSE 0 END) as [davon heute Erledigt],
	SUM(CASE WHEN NOT orderState in (''Finished'', ''Archived'', ''Deleted'', ''Aborted'') THEN 1 ELSE 0 END) as [davon heute Offen]
FROM
	tbl_Orders
WHERE
");
//first get the types to select
in.forOrderTypes = in.forOrderBook.getOrderTypeList();
String types = "(''"+StringUtils.join(in.forOrderTypes,"'', ''")+"'')";

List<String> clauses = new List<String>();

clauses.add("	orderType in "+types);
if(in.forYear>0){
	clauses.add("YEAR(orderStart)="+in.forYear);
	}
if(in.forMonth>0){
	clauses.add("MONTH(orderStart)="+in.forMonth);
	}
if(in.forDay>0){
	clauses.add("DAY(orderStart)="+in.forDay);
	}
sql.append(StringUtils.join(clauses,"\n	AND "));

sql.append("\nGROUP BY
	orderType
");
in.sql = sql.toString();

' #txt
Os0 f10 type ch.soreco.orderbook.functional.OrderStatistics #txt
Os0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Build Statistics statement</name>
        <nameStyle>26
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f10 358 156 36 24 20 -2 #rect
Os0 f10 @|StepIcon #fIcon
Os0 f11 actionDecl 'ch.soreco.orderbook.functional.OrderStatistics out;
' #txt
Os0 f11 actionTable 'out=in;
out.rsOrdersStatistics=recordset;
' #txt
Os0 f11 dbSql '<?xml version=""1.0"" standalone=""no""?>
<!DOCTYPE ANY_SQL SYSTEM  ""sqlStatements.dtd"">
<ANY_SQL><Verbatim quote=''false''>in.sql</Verbatim></ANY_SQL>' #txt
Os0 f11 dbUrl bzp_ivy_daten #txt
Os0 f11 cache '{/cache false /invalidation false /scope 0 /groupname ""/lifetime_group "0"/invalidation_time_group ""/identifier ""/lifetime_entry "0"/invalidation_time_entry ""}' #txt
Os0 f11 dbWizard in.sql #txt
Os0 f11 lotSize 2147483647 #txt
Os0 f11 startIdx 0 #txt
Os0 f11 type ch.soreco.orderbook.functional.OrderStatistics #txt
Os0 f11 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Get Data</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f11 358 220 36 24 20 -2 #rect
Os0 f11 @|DBStepIcon #fIcon
Os0 f12 inParamDecl '<java.lang.Integer forOrderBookId,java.lang.Integer forDay,java.lang.Integer forMonth,java.lang.Integer forYear> param;' #txt
Os0 f12 inParamTable 'out.forDay=param.forDay;
out.forMonth=param.forMonth;
out.forOrderBook.orderBookId=param.forOrderBookId;
out.forYear=param.forYear;
' #txt
Os0 f12 outParamDecl '<java.lang.Boolean success,java.lang.String error,ch.ivyteam.ivy.scripting.objects.Recordset rsOrderStatistics> result;
' #txt
Os0 f12 outParamTable 'result.success=in.success;
result.error=in.error;
result.rsOrderStatistics=in.rsOrdersStatistics;
' #txt
Os0 f12 actionDecl 'ch.soreco.orderbook.functional.OrderStatistics out;
' #txt
Os0 f12 callSignature byDay(Integer,Integer,Integer,Integer) #txt
Os0 f12 type ch.soreco.orderbook.functional.OrderStatistics #txt
Os0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>byDay(Integer,Integer,Integer,Integer)</name>
        <nameStyle>38,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f12 363 51 26 26 14 0 #rect
Os0 f12 @|StartSubIcon #fIcon
Os0 f13 actionDecl 'ch.soreco.orderbook.functional.OrderStatistics out;
' #txt
Os0 f13 actionTable 'out=in;
' #txt
Os0 f13 actionCode 'import ch.soreco.orderbook.util.Authority;
in.forOrderBook = Authority.getInstance().getBookById(in.forOrderBook.orderBookId);' #txt
Os0 f13 type ch.soreco.orderbook.functional.OrderStatistics #txt
Os0 f13 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>load OrderBook by Authority Singleton</name>
        <nameStyle>37
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f13 358 108 36 24 20 -2 #rect
Os0 f13 @|StepIcon #fIcon
Os0 f15 expr out #txt
Os0 f15 376 132 376 156 #arcP
Os0 f16 expr out #txt
Os0 f16 376 180 376 220 #arcP
Os0 f17 expr out #txt
Os0 f17 376 244 376 299 #arcP
Os0 f0 actionDecl 'ch.soreco.orderbook.functional.OrderStatistics out;
' #txt
Os0 f0 actionTable 'out=in;
' #txt
Os0 f0 actionCode 'import org.apache.commons.lang.StringUtils;
import ch.soreco.orderbook.enums.OrderType;
StringBuilder sql = new StringBuilder("");

//Build de select part
List<String> selectFields = new List<String>();
for(int i=0; i<in.columns.size(); i++){
	selectFields.add(in.columns.get(i)+" as "+in.aliases.get(i));
	}
sql.append("SET LANGUAGE German\n");
sql.append("SELECT\n");
sql.append("	"+StringUtils.join(selectFields,","));
sql.append("\nFROM\n	tbl_Orders");
//Build the WHERE Clause
String types = "(''"+StringUtils.join(in.forOrderBook.getOrderTypeList(),"'', ''")+"'')";
sql.append("\nWHERE\n	orderType in "+types);

sql.append("\nGROUP BY\n");
List<String> groupByFields = new List<String>();
List<String> aggregateFunctions = ["sum(", "count(", "avg(", "min(", "checksum_agg(", "over(", "rowcount_big(", "count_big(", "stdev(", "stdevp(", "max(", "var(", "varp("];
for(int i=0; i<in.columns.size(); i++){
	String groupBy = in.columns.get(i);
	Boolean isAggregate = false;
	for(String function:aggregateFunctions){
		if(groupBy.toLowerCase().contains(function)){
			isAggregate = true;
			break;
			}
		}
	if(!isAggregate){
		groupByFields.add(in.columns.get(i));
		}
	}
sql.append("	"+StringUtils.join(groupByFields,","));
in.sql = sql.toString();

' #txt
Os0 f0 type ch.soreco.orderbook.functional.OrderStatistics #txt
Os0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Build statement</name>
        <nameStyle>15
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f0 662 164 36 24 20 -2 #rect
Os0 f0 @|StepIcon #fIcon
Os0 f1 actionDecl 'ch.soreco.orderbook.functional.OrderStatistics out;
' #txt
Os0 f1 actionTable 'out=in;
' #txt
Os0 f1 actionCode 'import ch.soreco.orderbook.util.Authority;
in.forOrderBook = Authority.getInstance().getBookById(in.forOrderBook.orderBookId);' #txt
Os0 f1 type ch.soreco.orderbook.functional.OrderStatistics #txt
Os0 f1 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>load OrderBook by Authority Singleton</name>
        <nameStyle>37
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f1 662 116 36 24 20 -2 #rect
Os0 f1 @|StepIcon #fIcon
Os0 f2 type ch.soreco.orderbook.functional.OrderStatistics #txt
Os0 f2 667 307 26 26 14 0 #rect
Os0 f2 @|EndSubIcon #fIcon
Os0 f3 inParamDecl '<java.lang.Integer forOrderBookId,List<java.lang.String> columns,List<java.lang.String> aliases> param;' #txt
Os0 f3 inParamTable 'out.aliases=param.aliases;
out.columns=param.columns;
out.forOrderBook.orderBookId=param.forOrderBookId;
' #txt
Os0 f3 outParamDecl '<java.lang.Boolean success,java.lang.String error,ch.ivyteam.ivy.scripting.objects.Recordset rsGroupBy> result;
' #txt
Os0 f3 outParamTable 'result.success=in.success;
result.error=in.error;
result.rsGroupBy=in.rsOrdersStatistics;
' #txt
Os0 f3 actionDecl 'ch.soreco.orderbook.functional.OrderStatistics out;
' #txt
Os0 f3 callSignature getGroupBy(Integer,List<String>,List<String>) #txt
Os0 f3 type ch.soreco.orderbook.functional.OrderStatistics #txt
Os0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getGroupBy</name>
        <nameStyle>10,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f3 667 59 26 26 14 0 #rect
Os0 f3 @|StartSubIcon #fIcon
Os0 f4 actionDecl 'ch.soreco.orderbook.functional.OrderStatistics out;
' #txt
Os0 f4 actionTable 'out=in;
out.rsOrdersStatistics=recordset;
' #txt
Os0 f4 dbSql '<?xml version=""1.0"" standalone=""no""?>
<!DOCTYPE ANY_SQL SYSTEM  ""sqlStatements.dtd"">
<ANY_SQL><Verbatim quote=''false''>in.sql</Verbatim></ANY_SQL>' #txt
Os0 f4 dbUrl bzp_ivy_daten #txt
Os0 f4 cache '{/cache false /invalidation false /scope 0 /groupname ""/lifetime_group "0"/invalidation_time_group ""/identifier ""/lifetime_entry "0"/invalidation_time_entry ""}' #txt
Os0 f4 dbWizard in.sql #txt
Os0 f4 lotSize 2147483647 #txt
Os0 f4 startIdx 0 #txt
Os0 f4 type ch.soreco.orderbook.functional.OrderStatistics #txt
Os0 f4 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Get Data</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f4 662 228 36 24 20 -2 #rect
Os0 f4 @|DBStepIcon #fIcon
Os0 f5 expr out #txt
Os0 f5 680 85 680 116 #arcP
Os0 f6 expr out #txt
Os0 f6 680 140 680 164 #arcP
Os0 f7 expr out #txt
Os0 f7 680 188 680 228 #arcP
Os0 f8 expr out #txt
Os0 f8 680 252 680 307 #arcP
Os0 f18 actionDecl 'ch.soreco.orderbook.functional.OrderStatistics out;
' #txt
Os0 f18 actionTable 'out=in;
out.rsOrdersStatistics=recordset;
' #txt
Os0 f18 dbSql '<?xml version=""1.0"" standalone=""no""?>
<!DOCTYPE ANY_SQL SYSTEM  ""sqlStatements.dtd"">
<ANY_SQL><Verbatim quote=''false''>in.sql</Verbatim></ANY_SQL>' #txt
Os0 f18 dbUrl bzp_ivy_daten #txt
Os0 f18 cache '{/cache false /invalidation false /scope 0 /groupname ""/lifetime_group "0"/invalidation_time_group ""/identifier ""/lifetime_entry "0"/invalidation_time_entry ""}' #txt
Os0 f18 dbWizard in.sql #txt
Os0 f18 lotSize 2147483647 #txt
Os0 f18 startIdx 0 #txt
Os0 f18 type ch.soreco.orderbook.functional.OrderStatistics #txt
Os0 f18 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Get Data</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f18 918 236 36 24 20 -2 #rect
Os0 f18 @|DBStepIcon #fIcon
Os0 f19 type ch.soreco.orderbook.functional.OrderStatistics #txt
Os0 f19 923 315 26 26 14 0 #rect
Os0 f19 @|EndSubIcon #fIcon
Os0 f20 actionDecl 'ch.soreco.orderbook.functional.OrderStatistics out;
' #txt
Os0 f20 actionTable 'out=in;
' #txt
Os0 f20 actionCode 'import org.apache.commons.lang.StringUtils;
import ch.soreco.orderbook.enums.OrderType;

StringBuilder sql = new StringBuilder("");
sql.append("SELECT * \n");
sql.append("FROM   ( SELECT orderType, \n");
sql.append("               CONVERT(VARCHAR(10), orderStart, 104) AS [Datum] \n");
sql.append("               --,COUNT(orderType) as [Eingang] \n");
sql.append("       FROM    tbl_Orders \n");

List<String> where = new List<String>();
List<OrderType> types = in.forOrderBook.getOrderTypeList();
where.add("orderType IN (''"+StringUtils.join(types,"'', ''")+"'')");
if(in.forYear>0){
	where.add("YEAR(orderStart)="+in.forYear);
	}
if(in.forMonth>0){
	where.add("MONTH(orderStart)="+in.forMonth);
	}
if(in.forDay>0){
	where.add("DAY(orderStart)="+in.forDay);
	}

sql.append("\nWHERE "+StringUtils.join(where,"\n	AND "));
sql.append("       ) \n");
sql.append("       AS qry PIVOT ( COUNT(orderType) FOR orderType IN (["+StringUtils.join(types, "],[")+"]) ) AS pvt");

ivy.log.info(sql);
in.sql = sql.toString();' #txt
Os0 f20 type ch.soreco.orderbook.functional.OrderStatistics #txt
Os0 f20 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Build statement</name>
        <nameStyle>15
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f20 918 172 36 24 20 -2 #rect
Os0 f20 @|StepIcon #fIcon
Os0 f21 actionDecl 'ch.soreco.orderbook.functional.OrderStatistics out;
' #txt
Os0 f21 actionTable 'out=in;
' #txt
Os0 f21 actionCode 'import ch.soreco.orderbook.util.Authority;
in.forOrderBook = Authority.getInstance().getBookById(in.forOrderBook.orderBookId);' #txt
Os0 f21 type ch.soreco.orderbook.functional.OrderStatistics #txt
Os0 f21 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>load OrderBook by Authority Singleton</name>
        <nameStyle>37
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f21 918 124 36 24 20 -2 #rect
Os0 f21 @|StepIcon #fIcon
Os0 f22 inParamDecl '<java.lang.Integer forMonth,java.lang.Integer forDay,java.lang.Integer forYear,java.lang.Integer forOrderBookId> param;' #txt
Os0 f22 inParamTable 'out.forDay=param.forDay;
out.forMonth=param.forMonth;
out.forOrderBook.orderBookId=param.forOrderBookId;
out.forYear=param.forYear;
' #txt
Os0 f22 outParamDecl '<java.lang.Boolean success,java.lang.String error,ch.ivyteam.ivy.scripting.objects.Recordset rsStatistics> result;
' #txt
Os0 f22 outParamTable 'result.success=in.success;
result.error=in.error;
result.rsStatistics=in.rsOrdersStatistics;
' #txt
Os0 f22 actionDecl 'ch.soreco.orderbook.functional.OrderStatistics out;
' #txt
Os0 f22 callSignature getPivot(Integer,Integer,Integer,Integer) #txt
Os0 f22 type ch.soreco.orderbook.functional.OrderStatistics #txt
Os0 f22 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getPivot(Integer,Integer,Integer,Integer)</name>
    </language>
</elementInfo>
' #txt
Os0 f22 923 67 26 26 14 0 #rect
Os0 f22 @|StartSubIcon #fIcon
Os0 f23 expr out #txt
Os0 f23 936 93 936 124 #arcP
Os0 f25 expr out #txt
Os0 f25 936 196 936 236 #arcP
Os0 f26 expr out #txt
Os0 f26 936 260 936 315 #arcP
Os0 f24 expr out #txt
Os0 f24 936 148 936 172 #arcP
Os0 f27 actionDecl 'ch.soreco.orderbook.functional.OrderStatistics out;
' #txt
Os0 f27 actionTable 'out=in;
' #txt
Os0 f27 actionCode 'import org.apache.commons.lang.StringUtils;
import ch.soreco.orderbook.enums.OrderType;
StringBuilder sql = new StringBuilder("");
sql.append("SELECT
	orderType as [Auftragstyp], 
	"+(in.additionalField.trim().length()>0? in.additionalField+",":"")+"
	COUNT(orderType) as [Eingang],
	SUM(CASE WHEN orderState in (''Finished'', ''Archived'', ''Deleted'', ''Aborted'') THEN 1 ELSE 0 END) as [davon heute Erledigt],
	SUM(CASE WHEN NOT orderState in (''Finished'', ''Archived'', ''Deleted'', ''Aborted'') THEN 1 ELSE 0 END) as [davon heute Offen]
FROM
	tbl_Orders
WHERE
");
//first get the types to select
in.forOrderTypes = in.forOrderBook.getOrderTypeList();
String types = "(''"+StringUtils.join(in.forOrderTypes,"'', ''")+"'')";

List<String> clauses = new List<String>();

clauses.add("	orderType in "+types);
if(in.forYear>0){
	clauses.add("YEAR(orderStart)="+in.forYear);
	}
if(in.forMonth>0){
	clauses.add("MONTH(orderStart)="+in.forMonth);
	}
if(in.forDay>0){
	clauses.add("DAY(orderStart)="+in.forDay);
	}
sql.append(StringUtils.join(clauses,"\n	AND "));

//if fieldName contains " as <>" split it out!
if(in.additionalField.toLowerCase().indexOf("as")>-1) {
	in.additionalField= in.additionalField.toLowerCase().split("as").get(0);
	}
sql.append("\nGROUP BY
	orderType
	"+(in.additionalField.trim().length()>0? ","+in.additionalField:"")+"
");
in.sql = sql.toString();

' #txt
Os0 f27 type ch.soreco.orderbook.functional.OrderStatistics #txt
Os0 f27 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Build Statistics statement</name>
        <nameStyle>26
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f27 94 156 36 24 20 -2 #rect
Os0 f27 @|StepIcon #fIcon
Os0 f28 inParamDecl '<java.lang.String additionalField,java.lang.Integer forOrderBookId,java.lang.Integer forDay,java.lang.Integer forMonth,java.lang.Integer forYear> param;' #txt
Os0 f28 inParamTable 'out.additionalField=param.additionalField;
out.forDay=param.forDay;
out.forMonth=param.forMonth;
out.forOrderBook.orderBookId=param.forOrderBookId;
out.forYear=param.forYear;
' #txt
Os0 f28 outParamDecl '<java.lang.Boolean success,java.lang.String error,ch.ivyteam.ivy.scripting.objects.Recordset rsOrderStatistics> result;
' #txt
Os0 f28 outParamTable 'result.success=in.success;
result.error=in.error;
result.rsOrderStatistics=in.rsOrdersStatistics;
' #txt
Os0 f28 actionDecl 'ch.soreco.orderbook.functional.OrderStatistics out;
' #txt
Os0 f28 callSignature byDayAndField(String,Integer,Integer,Integer,Integer) #txt
Os0 f28 type ch.soreco.orderbook.functional.OrderStatistics #txt
Os0 f28 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>byDayAndField(Integer,Integer,Integer,Integer)</name>
        <nameStyle>46,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f28 99 51 26 26 14 0 #rect
Os0 f28 @|StartSubIcon #fIcon
Os0 f29 type ch.soreco.orderbook.functional.OrderStatistics #txt
Os0 f29 99 299 26 26 14 0 #rect
Os0 f29 @|EndSubIcon #fIcon
Os0 f30 actionDecl 'ch.soreco.orderbook.functional.OrderStatistics out;
' #txt
Os0 f30 actionTable 'out=in;
' #txt
Os0 f30 actionCode 'import ch.soreco.orderbook.util.Authority;
in.forOrderBook = Authority.getInstance().getBookById(in.forOrderBook.orderBookId); ' #txt
Os0 f30 type ch.soreco.orderbook.functional.OrderStatistics #txt
Os0 f30 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>load OrderBook by Authority Singleton</name>
        <nameStyle>37,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f30 94 108 36 24 20 -2 #rect
Os0 f30 @|StepIcon #fIcon
Os0 f31 actionDecl 'ch.soreco.orderbook.functional.OrderStatistics out;
' #txt
Os0 f31 actionTable 'out=in;
out.rsOrdersStatistics=recordset;
' #txt
Os0 f31 dbSql '<?xml version=""1.0"" standalone=""no""?>
<!DOCTYPE ANY_SQL SYSTEM  ""sqlStatements.dtd"">
<ANY_SQL><Verbatim quote=''false''>in.sql</Verbatim></ANY_SQL>' #txt
Os0 f31 dbUrl bzp_ivy_daten #txt
Os0 f31 cache '{/cache false /invalidation false /scope 0 /groupname ""/lifetime_group "0"/invalidation_time_group ""/identifier ""/lifetime_entry "0"/invalidation_time_entry ""}' #txt
Os0 f31 dbWizard in.sql #txt
Os0 f31 lotSize 2147483647 #txt
Os0 f31 startIdx 0 #txt
Os0 f31 type ch.soreco.orderbook.functional.OrderStatistics #txt
Os0 f31 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Get Data</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f31 94 220 36 24 20 -2 #rect
Os0 f31 @|DBStepIcon #fIcon
Os0 f32 expr out #txt
Os0 f32 112 77 112 108 #arcP
Os0 f33 expr out #txt
Os0 f33 112 132 112 156 #arcP
Os0 f34 expr out #txt
Os0 f34 112 180 112 220 #arcP
Os0 f35 expr out #txt
Os0 f35 112 244 112 299 #arcP
Os0 f36 type ch.soreco.orderbook.functional.OrderStatistics #txt
Os0 f36 processCall 'Functional Processes/orderbook/OrderStatistics:byDayAndField(String,Integer,Integer,Integer,Integer)' #txt
Os0 f36 doCall true #txt
Os0 f36 requestActionDecl '<java.lang.String additionalField,java.lang.Integer forOrderBookId,java.lang.Integer forDay,java.lang.Integer forMonth,java.lang.Integer forYear> param;
' #txt
Os0 f36 requestMappingAction 'param.additionalField="";
param.forOrderBookId=in.forOrderBook.orderBookId;
param.forDay=in.forDay;
param.forMonth=in.forMonth;
param.forYear=in.forYear;
' #txt
Os0 f36 responseActionDecl 'ch.soreco.orderbook.functional.OrderStatistics out;
' #txt
Os0 f36 responseMappingAction 'out=in;
out.error=result.error;
out.rsOrdersStatistics=result.rsOrderStatistics;
out.success=result.success;
' #txt
Os0 f36 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get By DayAnd Field</name>
        <nameStyle>19
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f36 310 180 36 24 20 -2 #rect
Os0 f36 @|CallSubIcon #fIcon
Os0 f37 expr out #txt
Os0 f37 371 76 333 180 #arcP
Os0 f14 expr out #txt
Os0 f14 333 204 371 299 #arcP
>Proto Os0 .type ch.soreco.orderbook.functional.OrderStatistics #txt
>Proto Os0 .processKind CALLABLE_SUB #txt
>Proto Os0 0 0 32 24 18 0 #rect
>Proto Os0 @|BIcon #fIcon
Os0 f13 mainOut f15 tail #connect
Os0 f15 head f10 mainIn #connect
Os0 f10 mainOut f16 tail #connect
Os0 f16 head f11 mainIn #connect
Os0 f11 mainOut f17 tail #connect
Os0 f17 head f9 mainIn #connect
Os0 f3 mainOut f5 tail #connect
Os0 f5 head f1 mainIn #connect
Os0 f1 mainOut f6 tail #connect
Os0 f6 head f0 mainIn #connect
Os0 f0 mainOut f7 tail #connect
Os0 f7 head f4 mainIn #connect
Os0 f4 mainOut f8 tail #connect
Os0 f8 head f2 mainIn #connect
Os0 f22 mainOut f23 tail #connect
Os0 f23 head f21 mainIn #connect
Os0 f20 mainOut f25 tail #connect
Os0 f25 head f18 mainIn #connect
Os0 f18 mainOut f26 tail #connect
Os0 f26 head f19 mainIn #connect
Os0 f21 mainOut f24 tail #connect
Os0 f24 head f20 mainIn #connect
Os0 f28 mainOut f32 tail #connect
Os0 f32 head f30 mainIn #connect
Os0 f30 mainOut f33 tail #connect
Os0 f33 head f27 mainIn #connect
Os0 f27 mainOut f34 tail #connect
Os0 f34 head f31 mainIn #connect
Os0 f31 mainOut f35 tail #connect
Os0 f35 head f29 mainIn #connect
Os0 f12 mainOut f37 tail #connect
Os0 f37 head f36 mainIn #connect
Os0 f36 mainOut f14 tail #connect
Os0 f14 head f9 mainIn #connect
