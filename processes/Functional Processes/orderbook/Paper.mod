[Ivy]
[>Created: Tue Feb 01 21:01:10 CET 2011]
12B78E1F18ABD25A 3.15 #module
>Proto >Proto Collection #zClass
Pr0 Paper Big #zClass
Pr0 B #cInfo
Pr0 #process
Pr0 @TextInP .resExport .resExport #zField
Pr0 @TextInP .type .type #zField
Pr0 @TextInP .processKind .processKind #zField
Pr0 @AnnotationInP-0n ai ai #zField
Pr0 @TextInP .xml .xml #zField
Pr0 @TextInP .responsibility .responsibility #zField
Pr0 @EndSub f6 '' #zField
Pr0 @StartSub f0 '' #zField
Pr0 @StartSub f5 '' #zField
Pr0 @GridStep f7 '' #zField
Pr0 @EndSub f1 '' #zField
Pr0 @GridStep f2 '' #zField
Pr0 @PushWFArc f3 '' #zField
Pr0 @PushWFArc f4 '' #zField
Pr0 @PushWFArc f8 '' #zField
Pr0 @PushWFArc f9 '' #zField
Pr0 @StartSub f10 '' #zField
Pr0 @EndSub f11 '' #zField
Pr0 @GridStep f12 '' #zField
Pr0 @PushWFArc f13 '' #zField
Pr0 @PushWFArc f14 '' #zField
Pr0 @StartSub f15 '' #zField
Pr0 @GridStep f16 '' #zField
Pr0 @EndSub f17 '' #zField
Pr0 @PushWFArc f18 '' #zField
Pr0 @PushWFArc f19 '' #zField
>Proto Pr0 Pr0 Paper #zField
Pr0 f6 type ch.soreco.orderbook.functional.Paper #txt
Pr0 f6 535 299 26 26 14 0 #rect
Pr0 f6 @|EndSubIcon #fIcon
Pr0 f0 inParamDecl '<ch.soreco.orderbook.bo.Paper filter> param;' #txt
Pr0 f0 inParamTable 'out.filter=param.filter;
' #txt
Pr0 f0 outParamDecl '<ch.soreco.orderbook.bo.Paper paper,java.lang.Boolean success> result;
' #txt
Pr0 f0 outParamTable 'result.success=in.success;
' #txt
Pr0 f0 actionDecl 'ch.soreco.orderbook.functional.Paper out;
' #txt
Pr0 f0 callSignature get(ch.soreco.orderbook.bo.Paper) #txt
Pr0 f0 type ch.soreco.orderbook.functional.Paper #txt
Pr0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get()</name>
        <nameStyle>5,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Pr0 f0 99 51 26 26 14 0 #rect
Pr0 f0 @|StartSubIcon #fIcon
Pr0 f5 inParamDecl '<ch.soreco.orderbook.bo.Paper paper> param;' #txt
Pr0 f5 inParamTable 'out.paper=param.paper;
out.success=false;
' #txt
Pr0 f5 outParamDecl '<ch.soreco.orderbook.bo.Paper paper,java.lang.Boolean success> result;
' #txt
Pr0 f5 outParamTable 'result.paper=in.paper;
result.success=in.success;
' #txt
Pr0 f5 actionDecl 'ch.soreco.orderbook.functional.Paper out;
' #txt
Pr0 f5 callSignature set(ch.soreco.orderbook.bo.Paper) #txt
Pr0 f5 type ch.soreco.orderbook.functional.Paper #txt
Pr0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set(Paper)</name>
        <nameStyle>10,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Pr0 f5 535 51 26 26 14 0 #rect
Pr0 f5 @|StartSubIcon #fIcon
Pr0 f7 actionDecl 'ch.soreco.orderbook.functional.Paper out;
' #txt
Pr0 f7 actionTable 'out=in;
' #txt
Pr0 f7 actionCode 'import ch.soreco.orderbook.bo.Paper;
import ch.soreco.webbies.common.db.EscapeHtml;
EscapeHtml.escapeHtml(in.paper);
//persist
try {
	out.paper = ivy.persistence.Adressmutation.merge(in.paper) as Paper;
	out.success = true;
	}
catch (Exception e) {
	out.success = false;
	ch.soreco.webbies.common.html.logger.HtmlLog.error(e.getMessage(),e);
	}' #txt
Pr0 f7 type ch.soreco.orderbook.functional.Paper #txt
Pr0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>merge Paper</name>
        <nameStyle>11
</nameStyle>
    </language>
</elementInfo>
' #txt
Pr0 f7 530 164 36 24 20 -2 #rect
Pr0 f7 @|StepIcon #fIcon
Pr0 f1 type ch.soreco.orderbook.functional.Paper #txt
Pr0 f1 99 299 26 26 14 0 #rect
Pr0 f1 @|EndSubIcon #fIcon
Pr0 f2 actionDecl 'ch.soreco.orderbook.functional.Paper out;
' #txt
Pr0 f2 actionTable 'out=in;
' #txt
Pr0 f2 actionCode 'import ch.soreco.orderbook.bo.Paper;
import ch.soreco.webbies.common.db.EntityFilter;
import ch.ivyteam.ivy.process.data.persistence.IIvyEntityManager;

IIvyEntityManager manager = ivy.persistence.Adressmutation;
List found = EntityFilter.getEntityResultList(in.filter,ivy.persistence.Adressmutation);

if(found!=null) {
	out.paper = found.get(0) as Paper;
	}
else {
	out.success = false;
}



' #txt
Pr0 f2 type ch.soreco.orderbook.functional.Paper #txt
Pr0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getA Paper </name>
        <nameStyle>11
</nameStyle>
    </language>
</elementInfo>
' #txt
Pr0 f2 94 164 36 24 20 -2 #rect
Pr0 f2 @|StepIcon #fIcon
Pr0 f3 expr out #txt
Pr0 f3 112 77 112 164 #arcP
Pr0 f4 expr out #txt
Pr0 f4 112 188 112 299 #arcP
Pr0 f8 expr out #txt
Pr0 f8 548 77 548 164 #arcP
Pr0 f9 expr out #txt
Pr0 f9 548 188 548 299 #arcP
Pr0 f10 inParamDecl '<ch.soreco.orderbook.bo.Paper filter> param;' #txt
Pr0 f10 inParamTable 'out.filter=param.filter;
' #txt
Pr0 f10 outParamDecl '<java.lang.Boolean success,List<ch.soreco.orderbook.bo.Paper> papers> result;
' #txt
Pr0 f10 outParamTable 'result.success=in.success;
result.papers=in.papers;
' #txt
Pr0 f10 actionDecl 'ch.soreco.orderbook.functional.Paper out;
' #txt
Pr0 f10 callSignature getList(ch.soreco.orderbook.bo.Paper) #txt
Pr0 f10 type ch.soreco.orderbook.functional.Paper #txt
Pr0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getList()</name>
        <nameStyle>9,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Pr0 f10 235 51 26 26 14 0 #rect
Pr0 f10 @|StartSubIcon #fIcon
Pr0 f11 type ch.soreco.orderbook.functional.Paper #txt
Pr0 f11 235 299 26 26 14 0 #rect
Pr0 f11 @|EndSubIcon #fIcon
Pr0 f12 actionDecl 'ch.soreco.orderbook.functional.Paper out;
' #txt
Pr0 f12 actionTable 'out=in;
' #txt
Pr0 f12 actionCode 'import ch.soreco.orderbook.bo.Paper;
import ch.soreco.webbies.common.db.EntityFilter;
import ch.ivyteam.ivy.process.data.persistence.IIvyEntityManager;

IIvyEntityManager manager = ivy.persistence.Adressmutation;
List found = EntityFilter.getEntityResultList(in.filter,ivy.persistence.Adressmutation);

if(found!=null) {
	if(found.size()>0){
		for(Paper paper : found){out.papers.add(paper);}
		}
	}
else {
	out.success = false;
}



' #txt
Pr0 f12 type ch.soreco.orderbook.functional.Paper #txt
Pr0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getList Paper </name>
        <nameStyle>14
</nameStyle>
    </language>
</elementInfo>
' #txt
Pr0 f12 230 164 36 24 20 -2 #rect
Pr0 f12 @|StepIcon #fIcon
Pr0 f13 expr out #txt
Pr0 f13 248 77 248 164 #arcP
Pr0 f14 expr out #txt
Pr0 f14 248 188 248 299 #arcP
Pr0 f15 inParamDecl '<java.lang.Integer contactId> param;' #txt
Pr0 f15 inParamTable 'out.contactId=param.contactId;
' #txt
Pr0 f15 outParamDecl '<java.lang.Boolean success,List<ch.soreco.orderbook.bo.Paper> papers> result;
' #txt
Pr0 f15 outParamTable 'result.success=in.success;
result.papers=in.papers;
' #txt
Pr0 f15 actionDecl 'ch.soreco.orderbook.functional.Paper out;
' #txt
Pr0 f15 callSignature getContactList(Integer) #txt
Pr0 f15 type ch.soreco.orderbook.functional.Paper #txt
Pr0 f15 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getContactList()</name>
        <nameStyle>16,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Pr0 f15 367 51 26 26 14 0 #rect
Pr0 f15 @|StartSubIcon #fIcon
Pr0 f16 actionDecl 'ch.soreco.orderbook.functional.Paper out;
' #txt
Pr0 f16 actionTable 'out=in;
' #txt
Pr0 f16 actionCode 'import ch.ivyteam.ivy.process.data.persistence.IIvyQuery;
import ch.soreco.orderbook.bo.Paper;
import ch.ivyteam.ivy.process.data.persistence.IIvyEntityManager;

IIvyEntityManager persistenceUnit = ivy.persistence.Adressmutation;
List found = null;

CompositeObject filter = in.filter;
String parameterName = "contactId";
Object parameterValue = in.contactId;

String whereClauses = "e."+parameterName+" =:"+parameterName;

try {
	String select = "select e from "+filter.getClass().getSimpleName()+" e where "+whereClauses;				
	IIvyQuery query = persistenceUnit.createQuery(select);
	query.setParameter(parameterName,parameterValue);
	found = query.getResultList();
	//ch.soreco.webbies.common.html.logger.HtmlLog.debug("Executed:"+query.toString());
}
catch (Exception e) {
	ch.soreco.webbies.common.html.logger.HtmlLog.debug("Error on getting "+filter.getClass().getSimpleName()+":"+e.getMessage());	
	}

if(found!=null) {
	if(found.size()>0){
		for(Paper paper : found){out.papers.add(paper);}
		}
	}
else {
	out.success = false;
}


' #txt
Pr0 f16 type ch.soreco.orderbook.functional.Paper #txt
Pr0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getList Paper </name>
        <nameStyle>14
</nameStyle>
    </language>
</elementInfo>
' #txt
Pr0 f16 362 164 36 24 20 -2 #rect
Pr0 f16 @|StepIcon #fIcon
Pr0 f17 type ch.soreco.orderbook.functional.Paper #txt
Pr0 f17 367 299 26 26 14 0 #rect
Pr0 f17 @|EndSubIcon #fIcon
Pr0 f18 expr out #txt
Pr0 f18 380 77 380 164 #arcP
Pr0 f19 expr out #txt
Pr0 f19 380 188 380 299 #arcP
>Proto Pr0 .type ch.soreco.orderbook.functional.Paper #txt
>Proto Pr0 .processKind CALLABLE_SUB #txt
>Proto Pr0 0 0 32 24 18 0 #rect
>Proto Pr0 @|BIcon #fIcon
Pr0 f0 mainOut f3 tail #connect
Pr0 f3 head f2 mainIn #connect
Pr0 f2 mainOut f4 tail #connect
Pr0 f4 head f1 mainIn #connect
Pr0 f5 mainOut f8 tail #connect
Pr0 f8 head f7 mainIn #connect
Pr0 f7 mainOut f9 tail #connect
Pr0 f9 head f6 mainIn #connect
Pr0 f10 mainOut f13 tail #connect
Pr0 f13 head f12 mainIn #connect
Pr0 f12 mainOut f14 tail #connect
Pr0 f14 head f11 mainIn #connect
Pr0 f15 mainOut f18 tail #connect
Pr0 f18 head f16 mainIn #connect
Pr0 f16 mainOut f19 tail #connect
Pr0 f19 head f17 mainIn #connect
