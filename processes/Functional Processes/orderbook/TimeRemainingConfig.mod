[Ivy]
[>Created: Tue Feb 15 19:38:46 CET 2011]
12E24B99E73C4232 3.15 #module
>Proto >Proto Collection #zClass
Tg0 TimeRemainingConfig Big #zClass
Tg0 B #cInfo
Tg0 #process
Tg0 @TextInP .resExport .resExport #zField
Tg0 @TextInP .type .type #zField
Tg0 @TextInP .processKind .processKind #zField
Tg0 @AnnotationInP-0n ai ai #zField
Tg0 @TextInP .xml .xml #zField
Tg0 @TextInP .responsibility .responsibility #zField
Tg0 @StartSub f0 '' #zField
Tg0 @EndSub f1 '' #zField
Tg0 @StartSub f3 '' #zField
Tg0 @EndSub f4 '' #zField
Tg0 @GridStep f6 '' #zField
Tg0 @GridStep f12 '' #zField
Tg0 @PushWFArc f7 '' #zField
Tg0 @PushWFArc f2 '' #zField
Tg0 @PushWFArc f8 '' #zField
Tg0 @PushWFArc f5 '' #zField
Tg0 @StartSub f9 '' #zField
Tg0 @EndSub f10 '' #zField
Tg0 @GridStep f11 '' #zField
Tg0 @PushWFArc f13 '' #zField
Tg0 @PushWFArc f14 '' #zField
>Proto Tg0 Tg0 TimeRemainingConfig #zField
Tg0 f0 inParamDecl '<ch.soreco.orderbook.bo.TimeRemainingConfig TimeRemainingConfig> param;' #txt
Tg0 f0 inParamTable 'out.filter=param.TimeRemainingConfig;
' #txt
Tg0 f0 outParamDecl '<ch.soreco.orderbook.bo.TimeRemainingConfig TimeRemainingConfig,java.lang.String error,java.lang.Boolean success> result;
' #txt
Tg0 f0 outParamTable 'result.TimeRemainingConfig=in.TimeRemainingConfig;
result.error=in.error;
result.success=in.success;
' #txt
Tg0 f0 actionDecl 'ch.soreco.orderbook.functional.TimeRemainingConfig out;
' #txt
Tg0 f0 callSignature get(ch.soreco.orderbook.bo.TimeRemainingConfig) #txt
Tg0 f0 type ch.soreco.orderbook.functional.TimeRemainingConfig #txt
Tg0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get(TimeRemainingConfig)</name>
        <nameStyle>24,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Tg0 f0 87 37 26 26 14 0 #rect
Tg0 f0 @|StartSubIcon #fIcon
Tg0 f1 type ch.soreco.orderbook.functional.TimeRemainingConfig #txt
Tg0 f1 87 287 26 26 14 0 #rect
Tg0 f1 @|EndSubIcon #fIcon
Tg0 f3 inParamDecl '<ch.soreco.orderbook.bo.TimeRemainingConfig TimeRemainingConfig> param;' #txt
Tg0 f3 inParamTable 'out.filter=param.TimeRemainingConfig;
' #txt
Tg0 f3 outParamDecl '<List<ch.soreco.orderbook.bo.TimeRemainingConfig> TimeRemainingConfigs,java.lang.String error,java.lang.Boolean success> result;
' #txt
Tg0 f3 outParamTable 'result.TimeRemainingConfigs=in.TimeRemainingConfigs;
result.error=in.error;
result.success=in.success;
' #txt
Tg0 f3 actionDecl 'ch.soreco.orderbook.functional.TimeRemainingConfig out;
' #txt
Tg0 f3 callSignature getList(ch.soreco.orderbook.bo.TimeRemainingConfig) #txt
Tg0 f3 type ch.soreco.orderbook.functional.TimeRemainingConfig #txt
Tg0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getList(TimeRemainingConfig)</name>
    </language>
</elementInfo>
' #txt
Tg0 f3 379 37 26 26 14 0 #rect
Tg0 f3 @|StartSubIcon #fIcon
Tg0 f4 type ch.soreco.orderbook.functional.TimeRemainingConfig #txt
Tg0 f4 379 287 26 26 14 0 #rect
Tg0 f4 @|EndSubIcon #fIcon
Tg0 f6 actionDecl 'ch.soreco.orderbook.functional.TimeRemainingConfig out;
' #txt
Tg0 f6 actionTable 'out=in;
' #txt
Tg0 f6 actionCode 'import ch.soreco.orderbook.bo.TimeRemainingConfig;
import ch.soreco.webbies.common.db.EntityFilter;


try {
	List found = EntityFilter.getEntityResultList(in.filter,ivy.persistence.Adressmutation);

	if(found!=null) {
		out.TimeRemainingConfig = found.get(0) as TimeRemainingConfig;
		out.success = true;
	
	}
	else {
		out.error = "No time remaining config found.";
		out.success = false;
	}
}
catch (Exception e) {
	out.success = false;
	out.error = "No time remaining config found.";
}





' #txt
Tg0 f6 type ch.soreco.orderbook.functional.TimeRemainingConfig #txt
Tg0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getA OrderMatching</name>
        <nameStyle>18
</nameStyle>
    </language>
</elementInfo>
' #txt
Tg0 f6 82 156 36 24 20 -2 #rect
Tg0 f6 @|StepIcon #fIcon
Tg0 f12 actionDecl 'ch.soreco.orderbook.functional.TimeRemainingConfig out;
' #txt
Tg0 f12 actionTable 'out=in;
' #txt
Tg0 f12 actionCode 'import ch.soreco.orderbook.bo.TimeRemainingConfig;
import ch.soreco.webbies.common.db.EntityFilter;

List found = EntityFilter.getEntityResultList(in.filter,ivy.persistence.Adressmutation);

if(found!=null) {
	if(found.size()>0){
		for(TimeRemainingConfig match : found){
			out.TimeRemainingConfigs.add(match);
		}
	}
}
else {
	out.success = false;
	out.error = "No time remaining configs found.";
}


' #txt
Tg0 f12 type ch.soreco.orderbook.functional.TimeRemainingConfig #txt
Tg0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getList Paper </name>
        <nameStyle>14
</nameStyle>
    </language>
</elementInfo>
' #txt
Tg0 f12 374 156 36 24 20 -2 #rect
Tg0 f12 @|StepIcon #fIcon
Tg0 f7 expr out #txt
Tg0 f7 100 63 100 156 #arcP
Tg0 f2 expr out #txt
Tg0 f2 100 180 100 287 #arcP
Tg0 f8 expr out #txt
Tg0 f8 392 63 392 156 #arcP
Tg0 f5 expr out #txt
Tg0 f5 392 180 392 287 #arcP
Tg0 f9 inParamDecl '<ch.ivyteam.ivy.scripting.objects.Recordset Recordset,List<ch.soreco.orderbook.bo.TimeRemainingConfig> TimeRemainingConfigs> param;' #txt
Tg0 f9 inParamTable 'out.Recordset=param.Recordset;
out.TimeRemainingConfigs=param.TimeRemainingConfigs;
' #txt
Tg0 f9 outParamDecl '<java.lang.Boolean success,java.lang.String error,ch.ivyteam.ivy.scripting.objects.Recordset Recordset> result;
' #txt
Tg0 f9 outParamTable 'result.success=in.success;
result.error=in.error;
result.Recordset=in.Recordset;
' #txt
Tg0 f9 actionDecl 'ch.soreco.orderbook.functional.TimeRemainingConfig out;
' #txt
Tg0 f9 callSignature setTimeTo(Recordset,List<ch.soreco.orderbook.bo.TimeRemainingConfig>) #txt
Tg0 f9 type ch.soreco.orderbook.functional.TimeRemainingConfig #txt
Tg0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>setTimeTo(Recordset,List&lt;TimeRemainingConfig&gt;)</name>
        <nameStyle>46,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Tg0 f9 651 43 26 26 14 0 #rect
Tg0 f9 @|StartSubIcon #fIcon
Tg0 f10 type ch.soreco.orderbook.functional.TimeRemainingConfig #txt
Tg0 f10 651 287 26 26 14 0 #rect
Tg0 f10 @|EndSubIcon #fIcon
Tg0 f11 actionDecl 'ch.soreco.orderbook.functional.TimeRemainingConfig out;
' #txt
Tg0 f11 actionTable 'out=in;
' #txt
Tg0 f11 actionCode 'import javax.xml.datatype.DatatypeFactory;
import org.apache.commons.lang.time.DurationFormatUtils;
import ch.soreco.orderbook.bo.TimeRemainingConfig;

//Declaration
Record record; TimeRemainingConfig config; List column;
DateTime start; DateTime frist;
Duration remaining; int daydiff;
ch.ivyteam.ivy.scripting.objects.Date today = new ch.ivyteam.ivy.scripting.objects.Date();
ch.ivyteam.ivy.scripting.objects.Date tomorrow = new ch.ivyteam.ivy.scripting.objects.Date(today.getYear(), today.getMonth(), today.getDay() + 1);
long showValue;

for (int i = 0; i < in.Recordset.size(); i++){
	record = in.Recordset.getAt(i);

	//get config
	for (int x = 0; x < in.TimeRemainingConfigs.size(); x++) {
		if (in.TimeRemainingConfigs.get(x).orderType == record.getField("orderType")) {
			config = in.TimeRemainingConfigs.get(x);
		}
	}
	
	//get values
	start = record.getField("orderStart") as DateTime;

	if (start.getTime().compareTo(config.incomeOverflowsAt) < 0) { 		// ? Eingang vor 15:00
		frist = new DateTime(start.getDate(), config.dayEndsAt);				// > Frist = Eingangstag um 18:00
			
		if (frist.getDate().compareTo(today) == 0) {										// ? gleicher Tag
			remaining = frist.getTime() - (new DateTime()).getTime();			// > Zeitdifferenz
			if (remaining.toNumber() < 0) { remaining = null;	}						// ? jetzt nach Arbeitssende => 0
			showValue = remaining.toNumber();
		}
		else {
			if ((new DateTime()).getTime() < config.dayStartsAt) {				// ? Jetzt nach Arbeitsende
				remaining = config.dayEndsAt-config.dayStartsAt;						// > Ganzer Tag
			}
			else {																												// ? Jetzt vor Arbeitsende
				remaining = (new DateTime()).getTime()-config.dayStartsAt; 	// > Zeit Jetzt minus Arbeitsstart
			}
			if (remaining.toNumber() < 0) { remaining = null;	}						// ? Jetzt vor Arbeitsstart => 0
			
			daydiff = (frist.getDate() - today).getDays() +1;							// > vergangene Tage - 1 (wegen Frist)
			if (daydiff > 0) {daydiff = 0;}																// ? keine verganagenen Tage => 0
			showValue = remaining.toNumber()*-1 +													// > Zeit heute plus vergangenen Tage minus First
									(daydiff * (config.dayEndsAt.toNumber() - config.dayStartsAt.toNumber()));
		}
			
	} else {
		frist = new DateTime(new ch.ivyteam.ivy.scripting.objects.Date(		// > Frist = Folgetag des Eingangs um 10:00
								start.getDate().getYear(),
								start.getDate().getMonth(),
								start.getDate().getDay() + 1),
						config.incomeOverflowDayEndAt);
		
		if (frist.getDate().compareTo(today) == 0) {											// ? gleicher Tag
			if ((new DateTime()).getTime() < config.dayEndsAt) {						// ? Jetzt vor Arbeitsende
				if ((new DateTime()).getTime() > config.dayStartsAt) {				// ? Jetzt nach Arbeitsstart
					remaining = frist.getTime() - (new DateTime()).getTime();		// > Zeitdifferenz
				}
				else {																												// ? Jetzt vor Arbeitsstart
					remaining = frist.getTime() - config.dayStartsAt;						// > Frist - Arbeitsstart
				}
			} 
			else {																													// ? Jetzt nach Arbeitsende
				remaining = config.dayEndsAt - frist.getTime();								// > Arbeitsende - Frist
			}
			showValue = remaining.toNumber();
		}
		
		else if (frist.getDate().compareTo(today) > 0) {									// ? nächster Tag
			remaining = config.dayEndsAt - (new DateTime()).getTime();			// > x = Arbeitsende minus Jetzt
			if (remaining.toNumber() < 0) { remaining = null; }							// ? jetzt nach Arbeitsende => 0
			daydiff = (frist.getTime() - config.dayStartsAt).toNumber();		// > y = Frist - Arbeitsstrat
			showValue = remaining.toNumber() + daydiff;											// > x + y
		}
		
		else {
			
			if ((new DateTime()).getTime() < config.dayEndsAt) {						// ? Jetzt vor Arbeitsende
				if ((new DateTime()).getTime() > config.dayStartsAt) {				// ? Jetzt nach Arbeitsstart
					remaining = (new DateTime()).getTime() - config.dayStartsAt;// > Zeitdifferenz
				}
				else {																												// ? Jetzt vor Arbeitsstart
					remaining = null;																						// > 0
				}
			} 
			else {																													// ? Jetzt nach Arbeitsende
				remaining = config.dayEndsAt - config.dayStartsAt;						// > ganzer Tag
			}

			daydiff = (frist.getDate() - today).getDays() +1;							// > vergangene Tage - 1 (wegen Frist)
			if (daydiff > 0) {daydiff = 0;}																// ? keine verganagenen Tage => 0
			showValue = remaining.toNumber()*-1 +													// > Zeit heute plus vergangenen Tage minus First
									(config.dayEndsAt - frist.getTime()).toNumber()*-1 +	// > Zeitdifferenz des ersten Tages
									(daydiff * (config.dayEndsAt.toNumber() - config.dayStartsAt.toNumber()));
		
		}
		
		
		
		
	}
	
	config.dayStartsAt;
	config.dayEndsAt;
	
	//set value to Recordset
	String op = "";
	if (showValue < 0) {
		op = "-";
		showValue = showValue *-1;
	}
	column.add(	//"" + start + " / " + frist + " / " + remaining.toString() + "||"
//	"" + daydiff.getDays() + " / " + remaining.toNumber() + " / "
	 op + DurationFormatUtils.formatDuration(showValue*1000,"HH:mm:ss"));

}

in.Recordset.addColumn("RemainingTime",column);' #txt
Tg0 f11 type ch.soreco.orderbook.functional.TimeRemainingConfig #txt
Tg0 f11 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set Time to Recordset</name>
        <nameStyle>21
</nameStyle>
        <desc>Should be declared in a java class!!!</desc>
    </language>
</elementInfo>
' #txt
Tg0 f11 646 156 36 24 20 -2 #rect
Tg0 f11 @|StepIcon #fIcon
Tg0 f13 expr out #txt
Tg0 f13 664 69 664 156 #arcP
Tg0 f14 expr out #txt
Tg0 f14 664 180 664 287 #arcP
>Proto Tg0 .type ch.soreco.orderbook.functional.TimeRemainingConfig #txt
>Proto Tg0 .processKind CALLABLE_SUB #txt
>Proto Tg0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language/>
</elementInfo>
' #txt
>Proto Tg0 0 0 32 24 18 0 #rect
>Proto Tg0 @|BIcon #fIcon
Tg0 f0 mainOut f7 tail #connect
Tg0 f7 head f6 mainIn #connect
Tg0 f6 mainOut f2 tail #connect
Tg0 f2 head f1 mainIn #connect
Tg0 f3 mainOut f8 tail #connect
Tg0 f8 head f12 mainIn #connect
Tg0 f12 mainOut f5 tail #connect
Tg0 f5 head f4 mainIn #connect
Tg0 f9 mainOut f13 tail #connect
Tg0 f13 head f11 mainIn #connect
Tg0 f11 mainOut f14 tail #connect
Tg0 f14 head f10 mainIn #connect
