[Ivy]
[>Created: Thu Oct 28 17:48:00 CEST 2010]
12B790A348F59ED2 3.14 #module
>Proto >Proto Collection #zClass
Ct0 Contact Big #zClass
Ct0 B #cInfo
Ct0 #process
Ct0 @TextInP .resExport .resExport #zField
Ct0 @TextInP .type .type #zField
Ct0 @TextInP .processKind .processKind #zField
Ct0 @AnnotationInP-0n ai ai #zField
Ct0 @TextInP .xml .xml #zField
Ct0 @TextInP .responsibility .responsibility #zField
Ct0 @StartSub f5 '' #zField
Ct0 @StartSub f0 '' #zField
Ct0 @GridStep f7 '' #zField
Ct0 @EndSub f6 '' #zField
Ct0 @EndSub f1 '' #zField
Ct0 @GridStep f2 '' #zField
Ct0 @PushWFArc f3 '' #zField
Ct0 @PushWFArc f4 '' #zField
Ct0 @PushWFArc f8 '' #zField
Ct0 @PushWFArc f9 '' #zField
Ct0 @EndSub f10 '' #zField
Ct0 @GridStep f11 '' #zField
Ct0 @StartSub f12 '' #zField
Ct0 @PushWFArc f13 '' #zField
Ct0 @PushWFArc f14 '' #zField
>Proto Ct0 Ct0 Contact #zField
Ct0 f5 inParamDecl '<ch.soreco.orderbook.bo.Contact contact> param;' #txt
Ct0 f5 inParamTable 'out.contact=param.contact;
out.success=false;
' #txt
Ct0 f5 outParamDecl '<java.lang.String error,ch.soreco.orderbook.bo.Contact contact,java.lang.Boolean success> result;
' #txt
Ct0 f5 outParamTable 'result.error=in.error;
result.contact=in.contact;
result.success=in.success;
' #txt
Ct0 f5 actionDecl 'ch.soreco.orderbook.functional.Contact out;
' #txt
Ct0 f5 callSignature set(ch.soreco.orderbook.bo.Contact) #txt
Ct0 f5 type ch.soreco.orderbook.functional.Contact #txt
Ct0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set(Contact)</name>
        <nameStyle>12,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ct0 f5 407 67 26 26 14 0 #rect
Ct0 f5 @|StartSubIcon #fIcon
Ct0 f0 inParamDecl '<ch.soreco.orderbook.bo.Contact filter> param;' #txt
Ct0 f0 inParamTable 'out.filter=param.filter;
' #txt
Ct0 f0 outParamDecl '<ch.soreco.orderbook.bo.Contact contact,java.lang.Boolean success> result;
' #txt
Ct0 f0 outParamTable 'result.contact=in.contact;
result.success=in.success;
' #txt
Ct0 f0 actionDecl 'ch.soreco.orderbook.functional.Contact out;
' #txt
Ct0 f0 callSignature get(ch.soreco.orderbook.bo.Contact) #txt
Ct0 f0 type ch.soreco.orderbook.functional.Contact #txt
Ct0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get()</name>
        <nameStyle>5,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ct0 f0 111 63 26 26 14 0 #rect
Ct0 f0 @|StartSubIcon #fIcon
Ct0 f7 actionDecl 'ch.soreco.orderbook.functional.Contact out;
' #txt
Ct0 f7 actionTable 'out=in;
' #txt
Ct0 f7 actionCode 'import ch.soreco.orderbook.bo.Contact;

//persist
try {
	out.contact = ivy.persistence.Adressmutation.merge(in.contact) as Contact;
	out.success = true;
	}
catch (Exception e) {
	out.success = false;
	out.error = e.getMessage();
	ch.soreco.webbies.common.html.logger.HtmlLog.error(e.getMessage(),e);
	}' #txt
Ct0 f7 type ch.soreco.orderbook.functional.Contact #txt
Ct0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>merge Contact</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
Ct0 f7 402 180 36 24 20 -2 #rect
Ct0 f7 @|StepIcon #fIcon
Ct0 f6 type ch.soreco.orderbook.functional.Contact #txt
Ct0 f6 407 315 26 26 14 0 #rect
Ct0 f6 @|EndSubIcon #fIcon
Ct0 f1 type ch.soreco.orderbook.functional.Contact #txt
Ct0 f1 111 311 26 26 14 0 #rect
Ct0 f1 @|EndSubIcon #fIcon
Ct0 f2 actionDecl 'ch.soreco.orderbook.functional.Contact out;
' #txt
Ct0 f2 actionTable 'out=in;
' #txt
Ct0 f2 actionCode 'import ch.soreco.orderbook.bo.Contact;
import ch.soreco.webbies.common.db.EntityFilter;
import ch.ivyteam.ivy.process.data.persistence.IIvyEntityManager;

IIvyEntityManager manager = ivy.persistence.Adressmutation;
List found = EntityFilter.getEntityResultList(in.filter,ivy.persistence.Adressmutation);

if(found!=null) {
	out.contact = found.get(0) as Contact;
	}
else {
	out.success = false;
}



' #txt
Ct0 f2 type ch.soreco.orderbook.functional.Contact #txt
Ct0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get A Contact </name>
        <nameStyle>14
</nameStyle>
    </language>
</elementInfo>
' #txt
Ct0 f2 106 176 36 24 20 -2 #rect
Ct0 f2 @|StepIcon #fIcon
Ct0 f3 expr out #txt
Ct0 f3 124 89 124 176 #arcP
Ct0 f4 expr out #txt
Ct0 f4 124 200 124 311 #arcP
Ct0 f8 expr out #txt
Ct0 f8 420 93 420 180 #arcP
Ct0 f9 expr out #txt
Ct0 f9 420 204 420 315 #arcP
Ct0 f10 type ch.soreco.orderbook.functional.Contact #txt
Ct0 f10 267 311 26 26 14 0 #rect
Ct0 f10 @|EndSubIcon #fIcon
Ct0 f11 actionDecl 'ch.soreco.orderbook.functional.Contact out;
' #txt
Ct0 f11 actionTable 'out=in;
' #txt
Ct0 f11 actionCode 'import ch.ivyteam.ivy.process.data.persistence.IIvyQuery;
import ch.soreco.orderbook.bo.Contact;
//import ch.soreco.webbies.common.db.EntityFilter;
//import ch.ivyteam.ivy.process.data.persistence.IIvyEntityManager;

//IIvyEntityManager manager = ivy.persistence.Adressmutation;
//List found = EntityFilter.getEntityResultList(in.filter,ivy.persistence.Adressmutation);
String select = "select e from "+in.filter.getClass().getSimpleName()+" e where e.contactOrderId =:contactOrderId";				
IIvyQuery query =ivy.persistence.Adressmutation.createQuery(select);
query.setParameter("contactOrderId",in.contact.contactOrderId);
List found = query.getResultList();
if(found!=null&&found.size()>0) {
	out.contacts.clear();
	for(Contact contact:found){
		out.contacts.add(contact);
		}
	}
else {
	out.success = false;
}



' #txt
Ct0 f11 type ch.soreco.orderbook.functional.Contact #txt
Ct0 f11 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getContacts</name>
        <nameStyle>11
</nameStyle>
    </language>
</elementInfo>
' #txt
Ct0 f11 262 176 36 24 20 -2 #rect
Ct0 f11 @|StepIcon #fIcon
Ct0 f12 inParamDecl '<java.lang.Integer orderId> param;' #txt
Ct0 f12 inParamTable 'out.contact.contactOrderId=param.orderId;
' #txt
Ct0 f12 outParamDecl '<java.lang.String error,List<ch.soreco.orderbook.bo.Contact> contacts,java.lang.Boolean success> result;
' #txt
Ct0 f12 outParamTable 'result.contacts=in.contacts;
result.success=in.success;
' #txt
Ct0 f12 actionDecl 'ch.soreco.orderbook.functional.Contact out;
' #txt
Ct0 f12 callSignature getList(Integer) #txt
Ct0 f12 type ch.soreco.orderbook.functional.Contact #txt
Ct0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getList()</name>
        <nameStyle>9,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ct0 f12 267 63 26 26 14 0 #rect
Ct0 f12 @|StartSubIcon #fIcon
Ct0 f13 expr out #txt
Ct0 f13 280 89 280 176 #arcP
Ct0 f14 expr out #txt
Ct0 f14 280 200 280 311 #arcP
>Proto Ct0 .type ch.soreco.orderbook.functional.Contact #txt
>Proto Ct0 .processKind CALLABLE_SUB #txt
>Proto Ct0 0 0 32 24 18 0 #rect
>Proto Ct0 @|BIcon #fIcon
Ct0 f0 mainOut f3 tail #connect
Ct0 f3 head f2 mainIn #connect
Ct0 f2 mainOut f4 tail #connect
Ct0 f4 head f1 mainIn #connect
Ct0 f5 mainOut f8 tail #connect
Ct0 f8 head f7 mainIn #connect
Ct0 f7 mainOut f9 tail #connect
Ct0 f9 head f6 mainIn #connect
Ct0 f12 mainOut f13 tail #connect
Ct0 f13 head f11 mainIn #connect
Ct0 f11 mainOut f14 tail #connect
Ct0 f14 head f10 mainIn #connect
