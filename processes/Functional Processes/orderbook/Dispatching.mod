[Ivy]
[>Created: Tue Jul 26 08:01:38 CEST 2011]
130317B0CAB6FB75 3.15 #module
>Proto >Proto Collection #zClass
Dg0 Dispatching Big #zClass
Dg0 B #cInfo
Dg0 #process
Dg0 @TextInP .resExport .resExport #zField
Dg0 @TextInP .type .type #zField
Dg0 @TextInP .processKind .processKind #zField
Dg0 @AnnotationInP-0n ai ai #zField
Dg0 @TextInP .xml .xml #zField
Dg0 @TextInP .responsibility .responsibility #zField
Dg0 @StartSub f0 '' #zField
Dg0 @EndSub f1 '' #zField
Dg0 @GridStep f2 '' #zField
Dg0 @PushWFArc f3 '' #zField
Dg0 @PushWFArc f4 '' #zField
Dg0 @StartSub f5 '' #zField
Dg0 @EndSub f6 '' #zField
Dg0 @GridStep f7 '' #zField
Dg0 @PushWFArc f8 '' #zField
Dg0 @PushWFArc f9 '' #zField
Dg0 @StartSub f10 '' #zField
Dg0 @EndSub f11 '' #zField
Dg0 @PushWFArc f12 '' #zField
Dg0 @StartSub f13 '' #zField
Dg0 @EndSub f14 '' #zField
Dg0 @CallSub f15 '' #zField
Dg0 @PushWFArc f16 '' #zField
Dg0 @Alternative f17 '' #zField
Dg0 @PushWFArc f18 '' #zField
Dg0 @CallSub f19 '' #zField
Dg0 @PushWFArc f20 '' #zField
Dg0 @CallSub f21 '' #zField
Dg0 @PushWFArc f22 '' #zField
Dg0 @PushWFArc f23 '' #zField
Dg0 @GridStep f24 '' #zField
Dg0 @PushWFArc f25 '' #zField
Dg0 @PushWFArc f26 '' #zField
>Proto Dg0 Dg0 Dispatching #zField
Dg0 f0 inParamDecl '<ch.soreco.orderbook.bo.Dispatching filter> param;' #txt
Dg0 f0 inParamTable 'out.filter=param.filter;
' #txt
Dg0 f0 outParamDecl '<java.lang.Boolean success,ch.soreco.orderbook.bo.Dispatching dispatchingData> result;
' #txt
Dg0 f0 outParamTable 'result.success=in.success;
result.dispatchingData=in.dispatchingData;
' #txt
Dg0 f0 actionDecl 'ch.soreco.orderbook.functional.Dispatching out;
' #txt
Dg0 f0 callSignature get(ch.soreco.orderbook.bo.Dispatching) #txt
Dg0 f0 type ch.soreco.orderbook.functional.Dispatching #txt
Dg0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get(Dispatching)</name>
        <nameStyle>16,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Dg0 f0 87 37 26 26 14 0 #rect
Dg0 f0 @|StartSubIcon #fIcon
Dg0 f1 type ch.soreco.orderbook.functional.Dispatching #txt
Dg0 f1 87 251 26 26 14 0 #rect
Dg0 f1 @|EndSubIcon #fIcon
Dg0 f2 actionDecl 'ch.soreco.orderbook.functional.Dispatching out;
' #txt
Dg0 f2 actionTable 'out=in;
' #txt
Dg0 f2 actionCode 'import ch.soreco.orderbook.bo.Dispatching;
import ch.soreco.webbies.common.db.EntityFilter;
import ch.ivyteam.ivy.process.data.persistence.IIvyEntityManager;

IIvyEntityManager manager = ivy.persistence.Adressmutation;
List found = EntityFilter.getEntityResultList(in.filter,ivy.persistence.Adressmutation);

if(found!=null) {
	if(found.size() > 0){
		out.dispatchingData = found.get(0) as Dispatching;
		out.success = true;
	}
}else {
	out.success = false;
}



' #txt
Dg0 f2 type ch.soreco.orderbook.functional.Dispatching #txt
Dg0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getDispatchingData</name>
        <nameStyle>18
</nameStyle>
    </language>
</elementInfo>
' #txt
Dg0 f2 82 124 36 24 20 -2 #rect
Dg0 f2 @|StepIcon #fIcon
Dg0 f3 expr out #txt
Dg0 f3 100 63 100 124 #arcP
Dg0 f4 expr out #txt
Dg0 f4 100 148 100 251 #arcP
Dg0 f5 inParamDecl '<ch.soreco.orderbook.bo.Dispatching dispatching> param;' #txt
Dg0 f5 inParamTable 'out.dispatchingData=param.dispatching;
' #txt
Dg0 f5 outParamDecl '<ch.soreco.orderbook.bo.Dispatching dispatchingData,java.lang.Boolean success> result;
' #txt
Dg0 f5 outParamTable 'result.dispatchingData=in.dispatchingData;
result.success=in.success;
' #txt
Dg0 f5 actionDecl 'ch.soreco.orderbook.functional.Dispatching out;
' #txt
Dg0 f5 callSignature set(ch.soreco.orderbook.bo.Dispatching) #txt
Dg0 f5 type ch.soreco.orderbook.functional.Dispatching #txt
Dg0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set(Dispatching)</name>
        <nameStyle>16,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Dg0 f5 291 35 26 26 14 0 #rect
Dg0 f5 @|StartSubIcon #fIcon
Dg0 f6 type ch.soreco.orderbook.functional.Dispatching #txt
Dg0 f6 291 243 26 26 14 0 #rect
Dg0 f6 @|EndSubIcon #fIcon
Dg0 f7 actionDecl 'ch.soreco.orderbook.functional.Dispatching out;
' #txt
Dg0 f7 actionTable 'out=in;
' #txt
Dg0 f7 actionCode 'import ch.soreco.orderbook.bo.Dispatching;
import ch.soreco.webbies.common.db.EscapeHtml;

EscapeHtml.escapeHtml(in.dispatchingData);

//persist
try {
	out.dispatchingData = ivy.persistence.Adressmutation.merge(in.dispatchingData) as Dispatching;
	out.success = true;
	}
catch (Exception e) {
	out.success = false;
	ch.soreco.webbies.common.html.logger.HtmlLog.error(e.getMessage(),e);
	}' #txt
Dg0 f7 type ch.soreco.orderbook.functional.Dispatching #txt
Dg0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>mergeDispatchingData</name>
        <nameStyle>20
</nameStyle>
    </language>
</elementInfo>
' #txt
Dg0 f7 286 124 36 24 20 -2 #rect
Dg0 f7 @|StepIcon #fIcon
Dg0 f8 expr out #txt
Dg0 f8 304 61 304 124 #arcP
Dg0 f9 expr out #txt
Dg0 f9 304 148 304 243 #arcP
Dg0 f10 inParamDecl '<ch.soreco.orderbook.bo.Dispatching filter> param;' #txt
Dg0 f10 outParamDecl '<ch.ivyteam.ivy.scripting.objects.Recordset dispatchingRS> result;
' #txt
Dg0 f10 outParamTable 'result.dispatchingRS=in.dispatchingRS;
' #txt
Dg0 f10 actionDecl 'ch.soreco.orderbook.functional.Dispatching out;
' #txt
Dg0 f10 callSignature getList(ch.soreco.orderbook.bo.Dispatching) #txt
Dg0 f10 type ch.soreco.orderbook.functional.Dispatching #txt
Dg0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getList(Dispatching)</name>
    </language>
</elementInfo>
' #txt
Dg0 f10 491 27 26 26 14 0 #rect
Dg0 f10 @|StartSubIcon #fIcon
Dg0 f11 type ch.soreco.orderbook.functional.Dispatching #txt
Dg0 f11 491 235 26 26 14 0 #rect
Dg0 f11 @|EndSubIcon #fIcon
Dg0 f12 expr out #txt
Dg0 f12 504 53 504 235 #arcP
Dg0 f13 inParamDecl '<java.lang.Integer orderId> param;' #txt
Dg0 f13 inParamTable 'out.filter.orderId=param.orderId;
' #txt
Dg0 f13 outParamDecl '<java.lang.Boolean success,java.util.List scannings> result;
' #txt
Dg0 f13 outParamTable 'result.success=in.success;
result.scannings=in.scannings;
' #txt
Dg0 f13 actionDecl 'ch.soreco.orderbook.functional.Dispatching out;
' #txt
Dg0 f13 callSignature getOriginalScannings(Integer) #txt
Dg0 f13 type ch.soreco.orderbook.functional.Dispatching #txt
Dg0 f13 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getOriginalScannings(orderId)</name>
        <nameStyle>29,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Dg0 f13 675 19 26 26 14 0 #rect
Dg0 f13 @|StartSubIcon #fIcon
Dg0 f14 type ch.soreco.orderbook.functional.Dispatching #txt
Dg0 f14 675 275 26 26 14 0 #rect
Dg0 f14 @|EndSubIcon #fIcon
Dg0 f15 type ch.soreco.orderbook.functional.Dispatching #txt
Dg0 f15 processCall 'Functional Processes/orderbook/Order:get(ch.soreco.orderbook.bo.Order)' #txt
Dg0 f15 doCall true #txt
Dg0 f15 requestActionDecl '<ch.soreco.orderbook.bo.Order filter> param;
' #txt
Dg0 f15 requestMappingAction 'param.filter.orderId=in.filter.orderId;
' #txt
Dg0 f15 responseActionDecl 'ch.soreco.orderbook.functional.Dispatching out;
' #txt
Dg0 f15 responseMappingAction 'out=in;
out.dispatchingData.id=result.order.dispatchingId;
' #txt
Dg0 f15 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get(Order)</name>
        <nameStyle>10,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Dg0 f15 670 92 36 24 20 -2 #rect
Dg0 f15 @|CallSubIcon #fIcon
Dg0 f16 expr out #txt
Dg0 f16 688 45 688 92 #arcP
Dg0 f17 type ch.soreco.orderbook.functional.Dispatching #txt
Dg0 f17 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>is ComboDoc?</name>
        <nameStyle>12
</nameStyle>
    </language>
</elementInfo>
' #txt
Dg0 f17 674 138 28 28 14 0 #rect
Dg0 f17 @|AlternativeIcon #fIcon
Dg0 f18 expr out #txt
Dg0 f18 688 116 688 138 #arcP
Dg0 f19 type ch.soreco.orderbook.functional.Dispatching #txt
Dg0 f19 processCall 'Functional Processes/orderbook/Dispatching:get(ch.soreco.orderbook.bo.Dispatching)' #txt
Dg0 f19 doCall true #txt
Dg0 f19 requestActionDecl '<ch.soreco.orderbook.bo.Dispatching filter> param;
' #txt
Dg0 f19 requestMappingAction 'param.filter.id=in.dispatchingData.id;
' #txt
Dg0 f19 responseActionDecl 'ch.soreco.orderbook.functional.Dispatching out;
' #txt
Dg0 f19 responseMappingAction 'out=in;
out.dispatchingData.orderId=result.dispatchingData.orderId;
' #txt
Dg0 f19 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get(Dispatching)</name>
        <nameStyle>16,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Dg0 f19 670 188 36 24 20 -2 #rect
Dg0 f19 @|CallSubIcon #fIcon
Dg0 f20 expr in #txt
Dg0 f20 outCond 'in.dispatchingData.id > 0' #txt
Dg0 f20 688 166 688 188 #arcP
Dg0 f21 type ch.soreco.orderbook.functional.Dispatching #txt
Dg0 f21 processCall 'Functional Processes/scanning/Scanning:getScanningList(Boolean,Integer)' #txt
Dg0 f21 doCall true #txt
Dg0 f21 requestActionDecl '<java.lang.Boolean loadFiles,java.lang.Integer OrderId> param;
' #txt
Dg0 f21 requestMappingAction 'param.loadFiles=true;
param.OrderId=in.dispatchingData.orderId;
' #txt
Dg0 f21 responseActionDecl 'ch.soreco.orderbook.functional.Dispatching out;
' #txt
Dg0 f21 responseMappingAction 'out=in;
out.scannings=result.Scannings;
out.success=result.success;
' #txt
Dg0 f21 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getScanningList(Boolean,Integer)</name>
    </language>
</elementInfo>
' #txt
Dg0 f21 670 228 36 24 20 -2 #rect
Dg0 f21 @|CallSubIcon #fIcon
Dg0 f22 expr out #txt
Dg0 f22 688 212 688 228 #arcP
Dg0 f23 expr out #txt
Dg0 f23 688 252 688 275 #arcP
Dg0 f24 actionDecl 'ch.soreco.orderbook.functional.Dispatching out;
' #txt
Dg0 f24 actionTable 'out=in;
out.success=false;
' #txt
Dg0 f24 type ch.soreco.orderbook.functional.Dispatching #txt
Dg0 f24 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>no ComboDoc</name>
        <nameStyle>11
</nameStyle>
    </language>
</elementInfo>
' #txt
Dg0 f24 878 204 36 24 20 -2 #rect
Dg0 f24 @|StepIcon #fIcon
Dg0 f25 expr in #txt
Dg0 f25 702 152 896 204 #arcP
Dg0 f25 1 896 152 #addKink
Dg0 f25 0 0.6643509735814053 0 0 #arcLabel
Dg0 f26 expr out #txt
Dg0 f26 896 228 701 288 #arcP
Dg0 f26 1 896 288 #addKink
Dg0 f26 1 0.17076378890004112 0 0 #arcLabel
>Proto Dg0 .type ch.soreco.orderbook.functional.Dispatching #txt
>Proto Dg0 .processKind CALLABLE_SUB #txt
>Proto Dg0 0 0 32 24 18 0 #rect
>Proto Dg0 @|BIcon #fIcon
Dg0 f0 mainOut f3 tail #connect
Dg0 f3 head f2 mainIn #connect
Dg0 f2 mainOut f4 tail #connect
Dg0 f4 head f1 mainIn #connect
Dg0 f5 mainOut f8 tail #connect
Dg0 f8 head f7 mainIn #connect
Dg0 f7 mainOut f9 tail #connect
Dg0 f9 head f6 mainIn #connect
Dg0 f10 mainOut f12 tail #connect
Dg0 f12 head f11 mainIn #connect
Dg0 f13 mainOut f16 tail #connect
Dg0 f16 head f15 mainIn #connect
Dg0 f15 mainOut f18 tail #connect
Dg0 f18 head f17 in #connect
Dg0 f17 out f20 tail #connect
Dg0 f20 head f19 mainIn #connect
Dg0 f19 mainOut f22 tail #connect
Dg0 f22 head f21 mainIn #connect
Dg0 f21 mainOut f23 tail #connect
Dg0 f23 head f14 mainIn #connect
Dg0 f17 out f25 tail #connect
Dg0 f25 head f24 mainIn #connect
Dg0 f24 mainOut f26 tail #connect
Dg0 f26 head f14 mainIn #connect
