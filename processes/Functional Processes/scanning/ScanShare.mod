[Ivy]
[>Created: Mon Dec 27 14:52:44 CET 2010]
12CE5DAA4CDE07EF 3.14 #module
>Proto >Proto Collection #zClass
Se0 ScanShare Big #zClass
Se0 B #cInfo
Se0 #process
Se0 @TextInP .resExport .resExport #zField
Se0 @TextInP .type .type #zField
Se0 @TextInP .processKind .processKind #zField
Se0 @AnnotationInP-0n ai ai #zField
Se0 @TextInP .xml .xml #zField
Se0 @TextInP .responsibility .responsibility #zField
Se0 @EndSub f5 '' #zField
Se0 @EndSub f1 '' #zField
Se0 @GridStep f0 '' #zField
Se0 @GridStep f4 '' #zField
Se0 @GridStep f12 '' #zField
Se0 @StartSub f3 '' #zField
Se0 @EndSub f11 '' #zField
Se0 @StartSub f10 '' #zField
Se0 @StartSub f2 '' #zField
Se0 @PushWFArc f6 '' #zField
Se0 @PushWFArc f7 '' #zField
Se0 @PushWFArc f14 '' #zField
Se0 @PushWFArc f8 '' #zField
Se0 @PushWFArc f9 '' #zField
Se0 @PushWFArc f13 '' #zField
>Proto Se0 Se0 ScanShare #zField
Se0 f5 type ch.soreco.orderbook.functional.ScanShare #txt
Se0 f5 107 315 26 26 14 0 #rect
Se0 f5 @|EndSubIcon #fIcon
Se0 f1 type ch.soreco.orderbook.functional.ScanShare #txt
Se0 f1 403 315 26 26 14 0 #rect
Se0 f1 @|EndSubIcon #fIcon
Se0 f0 actionDecl 'ch.soreco.orderbook.functional.ScanShare out;
' #txt
Se0 f0 actionTable 'out=in;
' #txt
Se0 f0 actionCode 'import ch.soreco.orderbook.bo.ScanShare;

//persist
try {
	out.ScanShare = ivy.persistence.Adressmutation.merge(in.ScanShare) as ScanShare;
	out.success = true;
	}
catch (Exception e) {
	out.success = false;
	out.error = e.getMessage();
	ch.soreco.webbies.common.html.logger.HtmlLog.error(e.getMessage(),e);
	}' #txt
Se0 f0 type ch.soreco.orderbook.functional.ScanShare #txt
Se0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set ScanShare</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
Se0 f0 398 180 36 24 20 -2 #rect
Se0 f0 @|StepIcon #fIcon
Se0 f4 actionDecl 'ch.soreco.orderbook.functional.ScanShare out;
' #txt
Se0 f4 actionTable 'out=in;
' #txt
Se0 f4 actionCode 'import ch.soreco.orderbook.bo.ScanShare;
import ch.soreco.webbies.common.db.EntityFilter;


try {
	List found = EntityFilter.getEntityResultList(in.filter,ivy.persistence.Adressmutation);

	if(found!=null) {
		out.ScanShare = found.get(0) as ScanShare;
		out.success = true;
	
	}
	else {
		out.success = false;
	}
}
catch (Exception e) {
	out.success = false;
	out.error = e.getMessage();
}





' #txt
Se0 f4 type ch.soreco.orderbook.functional.ScanShare #txt
Se0 f4 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get ScanShare</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
Se0 f4 102 180 36 24 20 -2 #rect
Se0 f4 @|StepIcon #fIcon
Se0 f12 actionDecl 'ch.soreco.orderbook.functional.ScanShare out;
' #txt
Se0 f12 actionTable 'out=in;
' #txt
Se0 f12 actionCode 'import ch.soreco.orderbook.bo.ScanShare;
import ch.soreco.webbies.common.db.EntityFilter;

List found = EntityFilter.getEntityResultList(in.filter,ivy.persistence.Adressmutation);

if(found!=null) {
	if(found.size()>0){
		for(ScanShare match : found){out.ScanShares.add(match);}
		}
	}
else {
	out.success = false;
}


' #txt
Se0 f12 type ch.soreco.orderbook.functional.ScanShare #txt
Se0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getList ScanShares </name>
        <nameStyle>19
</nameStyle>
    </language>
</elementInfo>
' #txt
Se0 f12 238 180 36 24 20 -2 #rect
Se0 f12 @|StepIcon #fIcon
Se0 f3 inParamDecl '<ch.soreco.orderbook.bo.ScanShare filter> param;' #txt
Se0 f3 inParamTable 'out.filter=param.filter;
' #txt
Se0 f3 outParamDecl '<java.lang.Boolean success,ch.soreco.orderbook.bo.ScanShare ScanShare,java.lang.String error> result;
' #txt
Se0 f3 outParamTable 'result.success=in.success;
result.ScanShare=in.ScanShare;
result.error=in.error;
' #txt
Se0 f3 actionDecl 'ch.soreco.orderbook.functional.ScanShare out;
' #txt
Se0 f3 callSignature get(ch.soreco.orderbook.bo.ScanShare) #txt
Se0 f3 type ch.soreco.orderbook.functional.ScanShare #txt
Se0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get()</name>
        <nameStyle>5,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Se0 f3 107 67 26 26 14 0 #rect
Se0 f3 @|StartSubIcon #fIcon
Se0 f11 type ch.soreco.orderbook.functional.ScanShare #txt
Se0 f11 243 315 26 26 14 0 #rect
Se0 f11 @|EndSubIcon #fIcon
Se0 f10 inParamDecl '<ch.soreco.orderbook.bo.ScanShare filter> param;' #txt
Se0 f10 inParamTable 'out.filter=param.filter;
' #txt
Se0 f10 outParamDecl '<java.lang.Boolean success,java.lang.String error,List<ch.soreco.orderbook.bo.ScanShare> ScanShares> result;
' #txt
Se0 f10 outParamTable 'result.success=in.success;
result.error=in.error;
result.ScanShares=in.ScanShares;
' #txt
Se0 f10 actionDecl 'ch.soreco.orderbook.functional.ScanShare out;
' #txt
Se0 f10 callSignature getList(ch.soreco.orderbook.bo.ScanShare) #txt
Se0 f10 type ch.soreco.orderbook.functional.ScanShare #txt
Se0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getList()</name>
        <nameStyle>9,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Se0 f10 243 67 26 26 14 0 #rect
Se0 f10 @|StartSubIcon #fIcon
Se0 f2 inParamDecl '<ch.soreco.orderbook.bo.ScanShare ScanShare> param;' #txt
Se0 f2 inParamTable 'out.ScanShare=param.ScanShare;
' #txt
Se0 f2 outParamDecl '<java.lang.Boolean success,ch.soreco.orderbook.bo.ScanShare ScanShare,java.lang.String error> result;
' #txt
Se0 f2 outParamTable 'result.success=in.success;
result.ScanShare=in.ScanShare;
result.error=in.error;
' #txt
Se0 f2 actionDecl 'ch.soreco.orderbook.functional.ScanShare out;
' #txt
Se0 f2 callSignature set(ch.soreco.orderbook.bo.ScanShare) #txt
Se0 f2 type ch.soreco.orderbook.functional.ScanShare #txt
Se0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set()</name>
        <nameStyle>5,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Se0 f2 403 67 26 26 14 0 #rect
Se0 f2 @|StartSubIcon #fIcon
Se0 f6 expr out #txt
Se0 f6 120 93 120 180 #arcP
Se0 f6 0 0.5062362574261193 0 0 #arcLabel
Se0 f7 expr out #txt
Se0 f7 120 204 120 315 #arcP
Se0 f14 expr out #txt
Se0 f14 256 204 256 315 #arcP
Se0 f8 expr out #txt
Se0 f8 416 93 416 180 #arcP
Se0 f9 expr out #txt
Se0 f9 416 204 416 315 #arcP
Se0 f13 expr out #txt
Se0 f13 256 93 256 180 #arcP
>Proto Se0 .type ch.soreco.orderbook.functional.ScanShare #txt
>Proto Se0 .processKind CALLABLE_SUB #txt
>Proto Se0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language/>
</elementInfo>
' #txt
>Proto Se0 0 0 32 24 18 0 #rect
>Proto Se0 @|BIcon #fIcon
Se0 f3 mainOut f6 tail #connect
Se0 f6 head f4 mainIn #connect
Se0 f4 mainOut f7 tail #connect
Se0 f7 head f5 mainIn #connect
Se0 f12 mainOut f14 tail #connect
Se0 f14 head f11 mainIn #connect
Se0 f2 mainOut f8 tail #connect
Se0 f8 head f0 mainIn #connect
Se0 f0 mainOut f9 tail #connect
Se0 f9 head f1 mainIn #connect
Se0 f10 mainOut f13 tail #connect
Se0 f13 head f12 mainIn #connect
