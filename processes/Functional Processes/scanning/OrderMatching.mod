[Ivy]
[>Created: Tue Apr 29 14:49:58 CEST 2014]
12C7452CD57E1535 3.17 #module
>Proto >Proto Collection #zClass
Og0 OrderMatching Big #zClass
Og0 B #cInfo
Og0 #process
Og0 @TextInP .resExport .resExport #zField
Og0 @TextInP .type .type #zField
Og0 @TextInP .processKind .processKind #zField
Og0 @AnnotationInP-0n ai ai #zField
Og0 @TextInP .xml .xml #zField
Og0 @TextInP .responsibility .responsibility #zField
Og0 @EndSub f11 '' #zField
Og0 @StartSub f3 '' #zField
Og0 @GridStep f4 '' #zField
Og0 @GridStep f12 '' #zField
Og0 @EndSub f5 '' #zField
Og0 @StartSub f10 '' #zField
Og0 @PushWFArc f6 '' #zField
Og0 @PushWFArc f7 '' #zField
Og0 @PushWFArc f13 '' #zField
Og0 @PushWFArc f14 '' #zField
Og0 @GridStep f0 '' #zField
Og0 @EndSub f1 '' #zField
Og0 @StartSub f2 '' #zField
Og0 @PushWFArc f8 '' #zField
Og0 @PushWFArc f9 '' #zField
>Proto Og0 Og0 OrderMatching #zField
Og0 f11 type ch.soreco.orderbook.functional.OrderMatching #txt
Og0 f11 227 299 26 26 14 0 #rect
Og0 f11 @|EndSubIcon #fIcon
Og0 f3 inParamDecl '<ch.soreco.orderbook.bo.OrderMatching filter> param;' #txt
Og0 f3 inParamTable 'out.filter=param.filter;
' #txt
Og0 f3 outParamDecl '<java.lang.Boolean success,ch.soreco.orderbook.bo.OrderMatching orderMatching,java.lang.String error> result;
' #txt
Og0 f3 outParamTable 'result.success=in.success;
result.orderMatching=in.orderMatching;
result.error=in.error;
' #txt
Og0 f3 actionDecl 'ch.soreco.orderbook.functional.OrderMatching out;
' #txt
Og0 f3 callSignature get(ch.soreco.orderbook.bo.OrderMatching) #txt
Og0 f3 type ch.soreco.orderbook.functional.OrderMatching #txt
Og0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get()</name>
        <nameStyle>5,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Og0 f3 91 51 26 26 14 0 #rect
Og0 f3 @|StartSubIcon #fIcon
Og0 f4 actionDecl 'ch.soreco.orderbook.functional.OrderMatching out;
' #txt
Og0 f4 actionTable 'out=in;
' #txt
Og0 f4 actionCode 'import ch.soreco.orderbook.bo.OrderMatching;
import ch.soreco.webbies.common.db.EntityFilter;

try {
	List found = EntityFilter.getEntityResultList(in.filter,ivy.persistence.Adressmutation);

	if(found!=null&&found.size()>0) {
		out.orderMatching = found.get(0) as OrderMatching;
		out.success = true;	
	}	else {
		out.error = "Cannot find OrderType for DocType <"+in.orderMatching.docType+">.";
		ivy.log.info(in.error);
		out.success = false;
	}
}
catch (Exception e) {
	out.error = "Cannot find OrderType for DocType <"+in.orderMatching.docType+">.";
	ivy.log.error(in.error,e);
	out.success = false;
}





' #txt
Og0 f4 type ch.soreco.orderbook.functional.OrderMatching #txt
Og0 f4 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getA OrderMatching</name>
        <nameStyle>18,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Og0 f4 86 164 36 24 20 -2 #rect
Og0 f4 @|StepIcon #fIcon
Og0 f12 actionDecl 'ch.soreco.orderbook.functional.OrderMatching out;
' #txt
Og0 f12 actionTable 'out=in;
' #txt
Og0 f12 actionCode 'import ch.soreco.orderbook.bo.OrderMatching;
import ch.soreco.webbies.common.db.EntityFilter;

List found = EntityFilter.getEntityResultList(in.filter,ivy.persistence.Adressmutation);

if(found!=null) {
	if(found.size()>0){
		for(OrderMatching match : found){out.orderMatchings.add(match);}
		}
	}
else {
	out.success = false;
}


' #txt
Og0 f12 type ch.soreco.orderbook.functional.OrderMatching #txt
Og0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getList Paper </name>
        <nameStyle>14
</nameStyle>
    </language>
</elementInfo>
' #txt
Og0 f12 222 164 36 24 20 -2 #rect
Og0 f12 @|StepIcon #fIcon
Og0 f5 type ch.soreco.orderbook.functional.OrderMatching #txt
Og0 f5 91 299 26 26 14 0 #rect
Og0 f5 @|EndSubIcon #fIcon
Og0 f10 inParamDecl '<ch.soreco.orderbook.bo.OrderMatching filter> param;' #txt
Og0 f10 inParamTable 'out.filter=param.filter;
' #txt
Og0 f10 outParamDecl '<java.lang.Boolean success,java.lang.String error,List<ch.soreco.orderbook.bo.OrderMatching> orderMatchings> result;
' #txt
Og0 f10 outParamTable 'result.success=in.success;
result.error=in.error;
result.orderMatchings=in.orderMatchings;
' #txt
Og0 f10 actionDecl 'ch.soreco.orderbook.functional.OrderMatching out;
' #txt
Og0 f10 callSignature getList(ch.soreco.orderbook.bo.OrderMatching) #txt
Og0 f10 type ch.soreco.orderbook.functional.OrderMatching #txt
Og0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getList()</name>
        <nameStyle>9,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Og0 f10 227 51 26 26 14 0 #rect
Og0 f10 @|StartSubIcon #fIcon
Og0 f6 expr out #txt
Og0 f6 104 77 104 164 #arcP
Og0 f7 expr out #txt
Og0 f7 104 188 104 299 #arcP
Og0 f13 expr out #txt
Og0 f13 240 77 240 164 #arcP
Og0 f14 expr out #txt
Og0 f14 240 188 240 299 #arcP
Og0 f0 actionDecl 'ch.soreco.orderbook.functional.OrderMatching out;
' #txt
Og0 f0 actionTable 'out=in;
' #txt
Og0 f0 actionCode 'import ch.soreco.orderbook.bo.OrderMatching;

//persist
try {
	out.orderMatching = ivy.persistence.Adressmutation.merge(in.orderMatching) as OrderMatching;
	out.success = true;
	}
catch (Exception e) {
	out.success = false;
	out.error = e.getMessage();
	ch.soreco.webbies.common.html.logger.HtmlLog.error(e.getMessage(),e);
	}' #txt
Og0 f0 type ch.soreco.orderbook.functional.OrderMatching #txt
Og0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>setA OrderMatching</name>
        <nameStyle>18
</nameStyle>
    </language>
</elementInfo>
' #txt
Og0 f0 374 164 36 24 20 -2 #rect
Og0 f0 @|StepIcon #fIcon
Og0 f1 type ch.soreco.orderbook.functional.OrderMatching #txt
Og0 f1 363 299 26 26 14 0 #rect
Og0 f1 @|EndSubIcon #fIcon
Og0 f2 inParamDecl '<ch.soreco.orderbook.bo.OrderMatching orderMatching> param;' #txt
Og0 f2 inParamTable 'out.orderMatching=param.orderMatching;
' #txt
Og0 f2 outParamDecl '<java.lang.Boolean success,ch.soreco.orderbook.bo.OrderMatching orderMatching,java.lang.String error> result;
' #txt
Og0 f2 outParamTable 'result.success=in.success;
result.orderMatching=in.orderMatching;
result.error=in.error;
' #txt
Og0 f2 actionDecl 'ch.soreco.orderbook.functional.OrderMatching out;
' #txt
Og0 f2 callSignature set(ch.soreco.orderbook.bo.OrderMatching) #txt
Og0 f2 type ch.soreco.orderbook.functional.OrderMatching #txt
Og0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set()</name>
        <nameStyle>5,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Og0 f2 363 51 26 26 14 0 #rect
Og0 f2 @|StartSubIcon #fIcon
Og0 f8 expr out #txt
Og0 f8 377 76 390 164 #arcP
Og0 f9 expr out #txt
Og0 f9 391 188 377 299 #arcP
>Proto Og0 .type ch.soreco.orderbook.functional.OrderMatching #txt
>Proto Og0 .processKind CALLABLE_SUB #txt
>Proto Og0 0 0 32 24 18 0 #rect
>Proto Og0 @|BIcon #fIcon
Og0 f3 mainOut f6 tail #connect
Og0 f6 head f4 mainIn #connect
Og0 f4 mainOut f7 tail #connect
Og0 f7 head f5 mainIn #connect
Og0 f10 mainOut f13 tail #connect
Og0 f13 head f12 mainIn #connect
Og0 f12 mainOut f14 tail #connect
Og0 f14 head f11 mainIn #connect
Og0 f2 mainOut f8 tail #connect
Og0 f8 head f0 mainIn #connect
Og0 f0 mainOut f9 tail #connect
Og0 f9 head f1 mainIn #connect
