[Ivy]
[>Created: Wed Mar 23 10:59:35 CET 2011]
12CE5DAE65AA8218 3.15 #module
>Proto >Proto Collection #zClass
Sg0 ScanLog Big #zClass
Sg0 B #cInfo
Sg0 #process
Sg0 @TextInP .resExport .resExport #zField
Sg0 @TextInP .type .type #zField
Sg0 @TextInP .processKind .processKind #zField
Sg0 @AnnotationInP-0n ai ai #zField
Sg0 @TextInP .xml .xml #zField
Sg0 @TextInP .responsibility .responsibility #zField
Sg0 @GridStep f12 '' #zField
Sg0 @GridStep f0 '' #zField
Sg0 @EndSub f11 '' #zField
Sg0 @StartSub f2 '' #zField
Sg0 @EndSub f5 '' #zField
Sg0 @StartSub f3 '' #zField
Sg0 @StartSub f10 '' #zField
Sg0 @EndSub f1 '' #zField
Sg0 @GridStep f4 '' #zField
Sg0 @PushWFArc f6 '' #zField
Sg0 @PushWFArc f7 '' #zField
Sg0 @PushWFArc f13 '' #zField
Sg0 @PushWFArc f14 '' #zField
Sg0 @PushWFArc f8 '' #zField
Sg0 @PushWFArc f9 '' #zField
>Proto Sg0 Sg0 ScanLog #zField
Sg0 f12 actionDecl 'ch.soreco.orderbook.functional.ScanLog out;
' #txt
Sg0 f12 actionTable 'out=in;
' #txt
Sg0 f12 actionCode 'import ch.soreco.orderbook.bo.ScanLog;
import ch.soreco.webbies.common.db.EntityFilter;

List found = EntityFilter.getEntityResultList(in.filter,ivy.persistence.Adressmutation);

if(found!=null) {
	if(found.size()>0){
		for(ScanLog match : found){out.ScanLogs.add(match);}
		}
	}
else {
	out.success = false;
}


' #txt
Sg0 f12 type ch.soreco.orderbook.functional.ScanLog #txt
Sg0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getList ScanLogs</name>
        <nameStyle>16
</nameStyle>
    </language>
</elementInfo>
' #txt
Sg0 f12 254 196 36 24 20 -2 #rect
Sg0 f12 @|StepIcon #fIcon
Sg0 f0 actionDecl 'ch.soreco.orderbook.functional.ScanLog out;
' #txt
Sg0 f0 actionTable 'out=in;
' #txt
Sg0 f0 actionCode 'import ch.soreco.orderbook.bo.ScanLog;

//persist
try {
	out.ScanLog = ivy.persistence.Adressmutation.merge(in.ScanLog) as ScanLog;
	out.success = true;
	}
catch (Exception e) {
	out.error = "Cannot set ScanLog:\n"+e.getMessage();
	ivy.log.error(in.error,e);
	out.success = false;
	}' #txt
Sg0 f0 type ch.soreco.orderbook.functional.ScanLog #txt
Sg0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set ScanLog</name>
        <nameStyle>11
</nameStyle>
    </language>
</elementInfo>
' #txt
Sg0 f0 390 196 36 24 20 -2 #rect
Sg0 f0 @|StepIcon #fIcon
Sg0 f11 type ch.soreco.orderbook.functional.ScanLog #txt
Sg0 f11 259 331 26 26 14 0 #rect
Sg0 f11 @|EndSubIcon #fIcon
Sg0 f2 inParamDecl '<ch.soreco.orderbook.bo.ScanLog ScanLog> param;' #txt
Sg0 f2 inParamTable 'out.ScanLog=param.ScanLog;
' #txt
Sg0 f2 outParamDecl '<java.lang.Boolean success,ch.soreco.orderbook.bo.ScanLog ScanLog,java.lang.String error> result;
' #txt
Sg0 f2 outParamTable 'result.success=in.success;
result.ScanLog=in.ScanLog;
result.error=in.error;
' #txt
Sg0 f2 actionDecl 'ch.soreco.orderbook.functional.ScanLog out;
' #txt
Sg0 f2 callSignature set(ch.soreco.orderbook.bo.ScanLog) #txt
Sg0 f2 type ch.soreco.orderbook.functional.ScanLog #txt
Sg0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set()</name>
        <nameStyle>5,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Sg0 f2 395 83 26 26 14 0 #rect
Sg0 f2 @|StartSubIcon #fIcon
Sg0 f5 type ch.soreco.orderbook.functional.ScanLog #txt
Sg0 f5 123 331 26 26 14 0 #rect
Sg0 f5 @|EndSubIcon #fIcon
Sg0 f3 inParamDecl '<ch.soreco.orderbook.bo.ScanLog filter> param;' #txt
Sg0 f3 inParamTable 'out.filter=param.filter;
' #txt
Sg0 f3 outParamDecl '<java.lang.Boolean success,ch.soreco.orderbook.bo.ScanLog ScanLog,java.lang.String error> result;
' #txt
Sg0 f3 outParamTable 'result.success=in.success;
result.ScanLog=in.ScanLog;
result.error=in.error;
' #txt
Sg0 f3 actionDecl 'ch.soreco.orderbook.functional.ScanLog out;
' #txt
Sg0 f3 callSignature get(ch.soreco.orderbook.bo.ScanLog) #txt
Sg0 f3 type ch.soreco.orderbook.functional.ScanLog #txt
Sg0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get()</name>
        <nameStyle>5,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Sg0 f3 123 83 26 26 14 0 #rect
Sg0 f3 @|StartSubIcon #fIcon
Sg0 f10 inParamDecl '<ch.soreco.orderbook.bo.ScanLog filter> param;' #txt
Sg0 f10 inParamTable 'out.filter=param.filter;
' #txt
Sg0 f10 outParamDecl '<java.lang.Boolean success,java.lang.String error,List<ch.soreco.orderbook.bo.ScanLog> ScanLogs> result;
' #txt
Sg0 f10 outParamTable 'result.success=in.success;
result.error=in.error;
result.ScanLogs=in.ScanLogs;
' #txt
Sg0 f10 actionDecl 'ch.soreco.orderbook.functional.ScanLog out;
' #txt
Sg0 f10 callSignature getList(ch.soreco.orderbook.bo.ScanLog) #txt
Sg0 f10 type ch.soreco.orderbook.functional.ScanLog #txt
Sg0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getList()</name>
        <nameStyle>9,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Sg0 f10 259 83 26 26 14 0 #rect
Sg0 f10 @|StartSubIcon #fIcon
Sg0 f1 type ch.soreco.orderbook.functional.ScanLog #txt
Sg0 f1 395 331 26 26 14 0 #rect
Sg0 f1 @|EndSubIcon #fIcon
Sg0 f4 actionDecl 'ch.soreco.orderbook.functional.ScanLog out;
' #txt
Sg0 f4 actionTable 'out=in;
' #txt
Sg0 f4 actionCode 'import ch.soreco.orderbook.bo.ScanLog;
import ch.soreco.webbies.common.db.EntityFilter;


try {
	List found = EntityFilter.getEntityResultList(in.filter,ivy.persistence.Adressmutation);

	if(found!=null) {
		out.ScanLog = found.get(0) as ScanLog;
		out.success = true;
	
	}
	else {
		out.success = false;
	}
}
catch (Exception e) {
	out.success = false;
	out.error = e.getMessage();
}





' #txt
Sg0 f4 type ch.soreco.orderbook.functional.ScanLog #txt
Sg0 f4 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get ScanLog</name>
        <nameStyle>11
</nameStyle>
    </language>
</elementInfo>
' #txt
Sg0 f4 118 196 36 24 20 -2 #rect
Sg0 f4 @|StepIcon #fIcon
Sg0 f6 expr out #txt
Sg0 f6 136 109 136 196 #arcP
Sg0 f7 expr out #txt
Sg0 f7 136 220 136 331 #arcP
Sg0 f13 expr out #txt
Sg0 f13 272 109 272 196 #arcP
Sg0 f14 expr out #txt
Sg0 f14 272 220 272 331 #arcP
Sg0 f8 expr out #txt
Sg0 f8 408 109 408 196 #arcP
Sg0 f9 expr out #txt
Sg0 f9 408 220 408 331 #arcP
>Proto Sg0 .type ch.soreco.orderbook.functional.ScanLog #txt
>Proto Sg0 .processKind CALLABLE_SUB #txt
>Proto Sg0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language/>
</elementInfo>
' #txt
>Proto Sg0 0 0 32 24 18 0 #rect
>Proto Sg0 @|BIcon #fIcon
Sg0 f3 mainOut f6 tail #connect
Sg0 f6 head f4 mainIn #connect
Sg0 f4 mainOut f7 tail #connect
Sg0 f7 head f5 mainIn #connect
Sg0 f10 mainOut f13 tail #connect
Sg0 f13 head f12 mainIn #connect
Sg0 f12 mainOut f14 tail #connect
Sg0 f14 head f11 mainIn #connect
Sg0 f2 mainOut f8 tail #connect
Sg0 f8 head f0 mainIn #connect
Sg0 f0 mainOut f9 tail #connect
Sg0 f9 head f1 mainIn #connect
