[Ivy]
[>Created: Thu Nov 17 09:19:09 CET 2011]
133ABC8D3542F5B7 3.15 #module
>Proto >Proto Collection #zClass
Hy0 History Big #zClass
Hy0 B #cInfo
Hy0 #process
Hy0 @TextInP .resExport .resExport #zField
Hy0 @TextInP .type .type #zField
Hy0 @TextInP .processKind .processKind #zField
Hy0 @AnnotationInP-0n ai ai #zField
Hy0 @TextInP .xml .xml #zField
Hy0 @TextInP .responsibility .responsibility #zField
Hy0 @StartSub f0 '' #zField
Hy0 @EndSub f1 '' #zField
Hy0 @DBStep f2 '' #zField
Hy0 @PushWFArc f3 '' #zField
Hy0 @PushWFArc f4 '' #zField
>Proto Hy0 Hy0 History #zField
Hy0 f0 inParamDecl '<java.lang.String idName,java.lang.Number fetchId,java.lang.String historyTable> param;' #txt
Hy0 f0 inParamTable 'out.fetchId=param.fetchId;
out.historyTable=param.historyTable;
out.idName=param.idName;
' #txt
Hy0 f0 outParamDecl '<ch.ivyteam.ivy.scripting.objects.Recordset historyData> result;
' #txt
Hy0 f0 outParamTable 'result.historyData=in.historyRS;
' #txt
Hy0 f0 actionDecl 'ch.soreco.orderbook.functional.History out;
' #txt
Hy0 f0 callSignature getHistoryDataById(String,Number,String) #txt
Hy0 f0 type ch.soreco.orderbook.functional.History #txt
Hy0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getHistoryDataById(String,Number,String)</name>
    </language>
</elementInfo>
' #txt
Hy0 f0 87 37 26 26 14 0 #rect
Hy0 f0 @|StartSubIcon #fIcon
Hy0 f1 type ch.soreco.orderbook.functional.History #txt
Hy0 f1 87 287 26 26 14 0 #rect
Hy0 f1 @|EndSubIcon #fIcon
Hy0 f2 actionDecl 'ch.soreco.orderbook.functional.History out;
' #txt
Hy0 f2 actionTable 'out=in;
out.historyRS=recordset;
' #txt
Hy0 f2 dbSql '<?xml version=""1.0"" standalone=""no""?>
<!DOCTYPE ANY_SQL SYSTEM  ""sqlStatements.dtd"">
<ANY_SQL><Verbatim quote=''false''>select * from in.historyTable where in.idName = in.fetchId AND HistoryType = &#39;INSERTED&#39; order by HistoryId asc</Verbatim></ANY_SQL>' #txt
Hy0 f2 dbUrl bzp_ivy_daten #txt
Hy0 f2 cache '{/cache false /invalidation false /scope 0 /groupname ""/lifetime_group "0"/invalidation_time_group ""/identifier ""/lifetime_entry "0"/invalidation_time_entry ""}' #txt
Hy0 f2 dbWizard 'select * from in.historyTable where in.idName = in.fetchId AND HistoryType = ''INSERTED'' order by HistoryId asc' #txt
Hy0 f2 lotSize 2147483647 #txt
Hy0 f2 startIdx 0 #txt
Hy0 f2 type ch.soreco.orderbook.functional.History #txt
Hy0 f2 82 140 36 24 20 -2 #rect
Hy0 f2 @|DBStepIcon #fIcon
Hy0 f3 expr out #txt
Hy0 f3 100 63 100 140 #arcP
Hy0 f4 expr out #txt
Hy0 f4 100 164 100 287 #arcP
>Proto Hy0 .type ch.soreco.orderbook.functional.History #txt
>Proto Hy0 .processKind CALLABLE_SUB #txt
>Proto Hy0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language/>
</elementInfo>
' #txt
>Proto Hy0 0 0 32 24 18 0 #rect
>Proto Hy0 @|BIcon #fIcon
Hy0 f0 mainOut f3 tail #connect
Hy0 f3 head f2 mainIn #connect
Hy0 f2 mainOut f4 tail #connect
Hy0 f4 head f1 mainIn #connect
