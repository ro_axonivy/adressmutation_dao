[Ivy]
[>Created: Mon Dec 27 17:03:27 CET 2010]
12D281C04E2A79D1 3.14 #module
>Proto >Proto Collection #zClass
Eg0 ErrorLog Big #zClass
Eg0 B #cInfo
Eg0 #process
Eg0 @TextInP .resExport .resExport #zField
Eg0 @TextInP .type .type #zField
Eg0 @TextInP .processKind .processKind #zField
Eg0 @AnnotationInP-0n ai ai #zField
Eg0 @TextInP .xml .xml #zField
Eg0 @TextInP .responsibility .responsibility #zField
Eg0 @GridStep f12 '' #zField
Eg0 @EndSub f3 '' #zField
Eg0 @StartSub f10 '' #zField
Eg0 @EndSub f5 '' #zField
Eg0 @StartSub f4 '' #zField
Eg0 @EndSub f11 '' #zField
Eg0 @GridStep f6 '' #zField
Eg0 @GridStep f7 '' #zField
Eg0 @StartSub f8 '' #zField
Eg0 @PushWFArc f9 '' #zField
Eg0 @PushWFArc f13 '' #zField
Eg0 @PushWFArc f14 '' #zField
Eg0 @PushWFArc f15 '' #zField
Eg0 @PushWFArc f16 '' #zField
Eg0 @PushWFArc f17 '' #zField
>Proto Eg0 Eg0 ErrorLog #zField
Eg0 f12 actionDecl 'ch.soreco.common.functional.ErrorLog out;
' #txt
Eg0 f12 actionTable 'out=in;
' #txt
Eg0 f12 actionCode 'import ch.soreco.common.bo.ErrorLog;
import ch.soreco.webbies.common.db.EntityFilter;

List found = EntityFilter.getEntityResultList(in.filter,ivy.persistence.Adressmutation);

if(found!=null) {
	if(found.size()>0){
		for(ErrorLog match : found){out.ErrorLogs.add(match);}
		}
	}
else {
	out.success = false;
}


' #txt
Eg0 f12 type ch.soreco.common.functional.ErrorLog #txt
Eg0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getList ErrorLogs</name>
        <nameStyle>17
</nameStyle>
    </language>
</elementInfo>
' #txt
Eg0 f12 214 164 36 24 20 -2 #rect
Eg0 f12 @|StepIcon #fIcon
Eg0 f3 type ch.soreco.common.functional.ErrorLog #txt
Eg0 f3 355 299 26 26 14 0 #rect
Eg0 f3 @|EndSubIcon #fIcon
Eg0 f10 inParamDecl '<ch.soreco.common.bo.ErrorLog filter> param;' #txt
Eg0 f10 inParamTable 'out.filter=param.filter;
' #txt
Eg0 f10 outParamDecl '<java.lang.Boolean success,java.lang.String error,List<ch.soreco.common.bo.ErrorLog> ErrorLogs> result;
' #txt
Eg0 f10 outParamTable 'result.success=in.success;
result.error=in.error;
result.ErrorLogs=in.ErrorLogs;
' #txt
Eg0 f10 actionDecl 'ch.soreco.common.functional.ErrorLog out;
' #txt
Eg0 f10 callSignature getList(ch.soreco.common.bo.ErrorLog) #txt
Eg0 f10 type ch.soreco.common.functional.ErrorLog #txt
Eg0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getList()</name>
        <nameStyle>9,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Eg0 f10 219 51 26 26 14 0 #rect
Eg0 f10 @|StartSubIcon #fIcon
Eg0 f5 type ch.soreco.common.functional.ErrorLog #txt
Eg0 f5 83 299 26 26 14 0 #rect
Eg0 f5 @|EndSubIcon #fIcon
Eg0 f4 inParamDecl '<ch.soreco.common.bo.ErrorLog ErrorLog> param;' #txt
Eg0 f4 inParamTable 'out.ErrorLog=param.ErrorLog;
' #txt
Eg0 f4 outParamDecl '<java.lang.Boolean success,ch.soreco.common.bo.ErrorLog ErrorLog,java.lang.String error> result;
' #txt
Eg0 f4 outParamTable 'result.success=in.success;
result.ErrorLog=in.ErrorLog;
result.error=in.error;
' #txt
Eg0 f4 actionDecl 'ch.soreco.common.functional.ErrorLog out;
' #txt
Eg0 f4 callSignature set(ch.soreco.common.bo.ErrorLog) #txt
Eg0 f4 type ch.soreco.common.functional.ErrorLog #txt
Eg0 f4 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set()</name>
        <nameStyle>5,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Eg0 f4 355 51 26 26 14 0 #rect
Eg0 f4 @|StartSubIcon #fIcon
Eg0 f11 type ch.soreco.common.functional.ErrorLog #txt
Eg0 f11 219 299 26 26 14 0 #rect
Eg0 f11 @|EndSubIcon #fIcon
Eg0 f6 actionDecl 'ch.soreco.common.functional.ErrorLog out;
' #txt
Eg0 f6 actionTable 'out=in;
' #txt
Eg0 f6 actionCode 'import ch.soreco.common.bo.ErrorLog;
import ch.soreco.webbies.common.db.EntityFilter;


try {
	List found = EntityFilter.getEntityResultList(in.filter,ivy.persistence.Adressmutation);

	if(found!=null) {
		out.ErrorLog = found.get(0) as ErrorLog;
		out.success = true;
	
	}
	else {
		out.success = false;
	}
}
catch (Exception e) {
	out.success = false;
	out.error = e.getMessage();
}





' #txt
Eg0 f6 type ch.soreco.common.functional.ErrorLog #txt
Eg0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get ErrorLog</name>
        <nameStyle>12
</nameStyle>
    </language>
</elementInfo>
' #txt
Eg0 f6 78 164 36 24 20 -2 #rect
Eg0 f6 @|StepIcon #fIcon
Eg0 f7 actionDecl 'ch.soreco.common.functional.ErrorLog out;
' #txt
Eg0 f7 actionTable 'out=in;
' #txt
Eg0 f7 actionCode 'import ch.soreco.common.bo.ErrorLog;

//persist
try {
	out.ErrorLog = ivy.persistence.Adressmutation.merge(in.ErrorLog) as ErrorLog;
	out.success = true;
	}
catch (Exception e) {
	out.success = false;
	out.error = e.getMessage();
	ch.soreco.webbies.common.html.logger.HtmlLog.error(e.getMessage(),e);
	}' #txt
Eg0 f7 type ch.soreco.common.functional.ErrorLog #txt
Eg0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set ErrorLog</name>
        <nameStyle>12
</nameStyle>
    </language>
</elementInfo>
' #txt
Eg0 f7 350 164 36 24 20 -2 #rect
Eg0 f7 @|StepIcon #fIcon
Eg0 f8 inParamDecl '<ch.soreco.common.bo.ErrorLog filter> param;' #txt
Eg0 f8 inParamTable 'out.filter=param.filter;
' #txt
Eg0 f8 outParamDecl '<java.lang.Boolean success,ch.soreco.common.bo.ErrorLog ErrorLog,java.lang.String error> result;
' #txt
Eg0 f8 outParamTable 'result.success=in.success;
result.ErrorLog=in.ErrorLog;
result.error=in.error;
' #txt
Eg0 f8 actionDecl 'ch.soreco.common.functional.ErrorLog out;
' #txt
Eg0 f8 callSignature get(ch.soreco.common.bo.ErrorLog) #txt
Eg0 f8 type ch.soreco.common.functional.ErrorLog #txt
Eg0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get()</name>
        <nameStyle>5,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Eg0 f8 83 51 26 26 14 0 #rect
Eg0 f8 @|StartSubIcon #fIcon
Eg0 f9 expr out #txt
Eg0 f9 96 77 96 164 #arcP
Eg0 f13 expr out #txt
Eg0 f13 96 188 96 299 #arcP
Eg0 f14 expr out #txt
Eg0 f14 232 77 232 164 #arcP
Eg0 f15 expr out #txt
Eg0 f15 232 188 232 299 #arcP
Eg0 f16 expr out #txt
Eg0 f16 368 77 368 164 #arcP
Eg0 f17 expr out #txt
Eg0 f17 368 188 368 299 #arcP
>Proto Eg0 .type ch.soreco.common.functional.ErrorLog #txt
>Proto Eg0 .processKind CALLABLE_SUB #txt
>Proto Eg0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language/>
</elementInfo>
' #txt
>Proto Eg0 0 0 32 24 18 0 #rect
>Proto Eg0 @|BIcon #fIcon
Eg0 f8 mainOut f9 tail #connect
Eg0 f9 head f6 mainIn #connect
Eg0 f6 mainOut f13 tail #connect
Eg0 f13 head f5 mainIn #connect
Eg0 f10 mainOut f14 tail #connect
Eg0 f14 head f12 mainIn #connect
Eg0 f12 mainOut f15 tail #connect
Eg0 f15 head f11 mainIn #connect
Eg0 f4 mainOut f16 tail #connect
Eg0 f16 head f7 mainIn #connect
Eg0 f7 mainOut f17 tail #connect
Eg0 f17 head f3 mainIn #connect
