[Ivy]
[>Created: Thu Oct 07 11:18:59 CEST 2010]
12B85DC82D0A98F7 3.14 #module
>Proto >Proto Collection #zClass
Pe0 Persistence Big #zClass
Pe0 B #cInfo
Pe0 #process
Pe0 @TextInP .resExport .resExport #zField
Pe0 @TextInP .type .type #zField
Pe0 @TextInP .processKind .processKind #zField
Pe0 @AnnotationInP-0n ai ai #zField
Pe0 @TextInP .xml .xml #zField
Pe0 @TextInP .responsibility .responsibility #zField
Pe0 @StartSub f0 '' #zField
Pe0 @EndSub f1 '' #zField
Pe0 @GridStep f2 '' #zField
Pe0 @PushWFArc f3 '' #zField
Pe0 @PushWFArc f4 '' #zField
>Proto Pe0 Pe0 Persistence #zField
Pe0 f0 inParamDecl '<ch.ivyteam.ivy.scripting.objects.CompositeObject filter> param;' #txt
Pe0 f0 inParamTable 'out.filter=param.filter;
' #txt
Pe0 f0 outParamDecl '<java.lang.Object object,List<java.lang.Object> objectList,java.lang.String error,java.lang.Boolean success> result;
' #txt
Pe0 f0 outParamTable 'result.object=in.object;
result.objectList=in.objectList;
result.error=in.error;
result.success=in.success;
' #txt
Pe0 f0 actionDecl 'ch.soreco.common.functional.Persistence out;
' #txt
Pe0 f0 callSignature call(ch.ivyteam.ivy.scripting.objects.CompositeObject) #txt
Pe0 f0 type ch.soreco.common.functional.Persistence #txt
Pe0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(CompositeObject)</name>
        <nameStyle>21,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Pe0 f0 87 37 26 26 14 0 #rect
Pe0 f0 @|StartSubIcon #fIcon
Pe0 f1 type ch.soreco.common.functional.Persistence #txt
Pe0 f1 87 287 26 26 14 0 #rect
Pe0 f1 @|EndSubIcon #fIcon
Pe0 f2 actionDecl 'ch.soreco.common.functional.Persistence out;
' #txt
Pe0 f2 actionTable 'out=in;
' #txt
Pe0 f2 actionCode 'import ch.soreco.webbies.common.db.EntityFilter;

List found = EntityFilter.getEntityResultList(in.filter,in.persistenceUnit);

if(found!=null) {
	out.object = found.get(0);
	out.objectList = found;
	}
else {
	out.success = false;
}
' #txt
Pe0 f2 type ch.soreco.common.functional.Persistence #txt
Pe0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Object</name>
        <nameStyle>10
</nameStyle>
    </language>
</elementInfo>
' #txt
Pe0 f2 78 160 36 24 20 -2 #rect
Pe0 f2 @|StepIcon #fIcon
Pe0 f3 expr out #txt
Pe0 f3 99 62 96 160 #arcP
Pe0 f4 expr out #txt
Pe0 f4 96 184 99 287 #arcP
>Proto Pe0 .type ch.soreco.common.functional.Persistence #txt
>Proto Pe0 .processKind CALLABLE_SUB #txt
>Proto Pe0 0 0 32 24 18 0 #rect
>Proto Pe0 @|BIcon #fIcon
Pe0 f0 mainOut f3 tail #connect
Pe0 f3 head f2 mainIn #connect
Pe0 f2 mainOut f4 tail #connect
Pe0 f4 head f1 mainIn #connect
