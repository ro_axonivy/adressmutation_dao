[Ivy]
[>Created: Fri May 13 06:59:55 CEST 2016]
12B5543117A96C6C 3.17 #module
>Proto >Proto Collection #zClass
Fe0 File Big #zClass
Fe0 B #cInfo
Fe0 #process
Fe0 @TextInP .resExport .resExport #zField
Fe0 @TextInP .type .type #zField
Fe0 @TextInP .processKind .processKind #zField
Fe0 @AnnotationInP-0n ai ai #zField
Fe0 @TextInP .xml .xml #zField
Fe0 @TextInP .responsibility .responsibility #zField
Fe0 @StartSub f0 '' #zField
Fe0 @EndSub f1 '' #zField
Fe0 @GridStep f2 '' #zField
Fe0 @PushWFArc f3 '' #zField
Fe0 @EndSub f5 '' #zField
Fe0 @GridStep f6 '' #zField
Fe0 @StartSub f7 '' #zField
Fe0 @GridStep f12 '' #zField
Fe0 @PushWFArc f14 '' #zField
Fe0 @Alternative f18 '' #zField
Fe0 @PushWFArc f19 '' #zField
Fe0 @PushWFArc f21 '' #zField
Fe0 @Alternative f24 '' #zField
Fe0 @Alternative f27 '' #zField
Fe0 @EndSub f28 '' #zField
Fe0 @GridStep f29 '' #zField
Fe0 @StartSub f31 '' #zField
Fe0 @PushWFArc f33 '' #zField
Fe0 @PushWFArc f34 '' #zField
Fe0 @PushWFArc f36 '' #zField
Fe0 @PushWFArc f25 '' #zField
Fe0 @StartSub f45 '' #zField
Fe0 @EndSub f46 '' #zField
Fe0 @GridStep f48 '' #zField
Fe0 @Page f52 '' #zField
Fe0 @PushWFArc f50 '' #zField
Fe0 @PushWFArc f56 '' #zField
Fe0 @GridStep f55 '' #zField
Fe0 @PushWFArc f57 '' #zField
Fe0 @GridStep f58 '' #zField
Fe0 @PushWFArc f30 '' #zField
Fe0 @GridStep f20 '' #zField
Fe0 @PushWFArc f22 '' #zField
Fe0 @PushWFArc f15 '' #zField
Fe0 @PushWFArc f4 '' #zField
Fe0 @Alternative f8 '' #zField
Fe0 @PushWFArc f9 '' #zField
Fe0 @PushWFArc f11 '' #zField
Fe0 @GridStep f13 '' #zField
Fe0 @PushWFArc f17 '' #zField
Fe0 @EndSub f23 '' #zField
Fe0 @GridStep f26 '' #zField
Fe0 @GridStep f32 '' #zField
Fe0 @GridStep f38 '' #zField
Fe0 @GridStep f40 '' #zField
Fe0 @Alternative f41 '' #zField
Fe0 @Alternative f42 '' #zField
Fe0 @StartSub f43 '' #zField
Fe0 @PushWFArc f44 '' #zField
Fe0 @PushWFArc f47 '' #zField
Fe0 @PushWFArc f49 '' #zField
Fe0 @PushWFArc f51 '' #zField
Fe0 @PushWFArc f53 '' #zField
Fe0 @PushWFArc f54 '' #zField
Fe0 @PushWFArc f62 '' #zField
Fe0 @Alternative f63 '' #zField
Fe0 @PushWFArc f60 '' #zField
Fe0 @PushWFArc f61 '' #zField
Fe0 @PushWFArc f16 '' #zField
Fe0 @PushWFArc f10 '' #zField
Fe0 @Page f66 '' #zField
Fe0 @PushWFArc f37 '' #zField
Fe0 @Page f68 '' #zField
Fe0 @PushWFArc f69 '' #zField
Fe0 @PushWFArc f65 '' #zField
Fe0 @Page f35 '' #zField
Fe0 @PushWFArc f67 '' #zField
Fe0 @PushWFArc f39 '' #zField
Fe0 @GridStep f59 '' #zField
Fe0 @PushWFArc f71 '' #zField
Fe0 @PushWFArc f70 '' #zField
>Proto Fe0 Fe0 File #zField
Fe0 f0 inParamDecl '<ch.soreco.common.bo.File filter> param;' #txt
Fe0 f0 inParamTable 'out.filter=param.filter;
out.success=false;
' #txt
Fe0 f0 outParamDecl '<ch.soreco.common.bo.File file> result;
' #txt
Fe0 f0 outParamTable 'result.file=in.file;
' #txt
Fe0 f0 actionDecl 'ch.soreco.common.functional.File out;
' #txt
Fe0 f0 callSignature get(ch.soreco.common.bo.File) #txt
Fe0 f0 type ch.soreco.common.functional.File #txt
Fe0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get()</name>
        <nameStyle>5,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f0 87 37 26 26 14 0 #rect
Fe0 f0 @|StartSubIcon #fIcon
Fe0 f1 type ch.soreco.common.functional.File #txt
Fe0 f1 87 351 26 26 14 0 #rect
Fe0 f1 @|EndSubIcon #fIcon
Fe0 f2 actionDecl 'ch.soreco.common.functional.File out;
' #txt
Fe0 f2 actionTable 'out=in;
' #txt
Fe0 f2 actionCode 'import ch.soreco.webbies.common.db.EntityFilter;
import ch.ivyteam.ivy.process.data.persistence.IIvyEntityManager;

IIvyEntityManager manager = ivy.persistence.Adressmutation;
List found = EntityFilter.getEntityResultList(in.filter,manager);

if(found!=null) {
	out.file = found.get(0) as ch.soreco.common.bo.File;
	out.success = true;
	}
else {
	out.success = false;
}



' #txt
Fe0 f2 type ch.soreco.common.functional.File #txt
Fe0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get a File</name>
        <nameStyle>10
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f2 82 100 36 24 20 -2 #rect
Fe0 f2 @|StepIcon #fIcon
Fe0 f3 expr out #txt
Fe0 f3 100 63 100 100 #arcP
Fe0 f5 type ch.soreco.common.functional.File #txt
Fe0 f5 235 427 26 26 14 0 #rect
Fe0 f5 @|EndSubIcon #fIcon
Fe0 f6 actionDecl 'ch.soreco.common.functional.File out;
' #txt
Fe0 f6 actionTable 'out=in;
' #txt
Fe0 f6 actionCode '//persist to get an id
try {
	java.io.File file = in.file.file;
	out.file = ivy.persistence.Adressmutation.merge(in.file) as ch.soreco.common.bo.File;
	out.file.file = file;
	out.success = true;
	}
catch (Exception e) {
	out.success = false;
	out.error = e.getMessage();
	ivy.log.error(e);
	}' #txt
Fe0 f6 type ch.soreco.common.functional.File #txt
Fe0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>merge File</name>
        <nameStyle>10
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f6 230 196 36 24 20 -2 #rect
Fe0 f6 @|StepIcon #fIcon
Fe0 f7 inParamDecl '<java.lang.Boolean keepFile,java.lang.Boolean deleteFromFileSystem,ch.ivyteam.ivy.scripting.objects.File file> param;' #txt
Fe0 f7 inParamTable 'out.deleteFromFileSystem=(param.deleteFromFileSystem==null)? true: param.deleteFromFileSystem;
out.file.file=param.file.getJavaFile();
out.success=false;
' #txt
Fe0 f7 outParamDecl '<java.lang.String error,java.lang.Boolean success,ch.soreco.common.bo.File file> result;
' #txt
Fe0 f7 outParamTable 'result.error=in.error;
result.success=in.success;
result.file=in.file;
' #txt
Fe0 f7 actionDecl 'ch.soreco.common.functional.File out;
' #txt
Fe0 f7 callSignature set(Boolean,Boolean,File) #txt
Fe0 f7 type ch.soreco.common.functional.File #txt
Fe0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set(Boolean,Boolean,File)</name>
    </language>
</elementInfo>
' #txt
Fe0 f7 235 43 26 26 14 0 #rect
Fe0 f7 @|StartSubIcon #fIcon
Fe0 f12 actionDecl 'ch.soreco.common.functional.File out;
' #txt
Fe0 f12 actionTable 'out=in;
out.file.createDate=new DateTime();
out.file.createUserId=ivy.session.getSessionUserName();
out.file.fileExtension=in.file.file.getName().substring(in.file.file.getName().lastIndexOf(".")+1).toLowerCase();
out.file.fileName=in.file.file.getName();
out.file.fileSize=in.file.file.length();
' #txt
Fe0 f12 actionCode 'import java.net.URLConnection;

if(in.file.file.exists()) {
	try {
		in.file.contentType = URLConnection.guessContentTypeFromName(in.file.file.getAbsolutePath());
	}
	catch (Exception e){
		ivy.log.debug("Could not Guess the content type of "+in.file.file.getAbsolutePath(), e);
		}
	}
else {
	out.success = false;
	out.error ="File not exists";
	}

String upperExtension = in.file.file.getName().substring(in.file.file.getName().lastIndexOf(".")+1);
String lowerExtension = in.file.file.getName().substring(in.file.file.getName().lastIndexOf(".")+1).toLowerCase();
out.file.fileName.replace(upperExtension,lowerExtension);' #txt
Fe0 f12 type ch.soreco.common.functional.File #txt
Fe0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>guess ContentType</name>
        <nameStyle>17
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f12 230 100 36 24 20 -2 #rect
Fe0 f12 @|StepIcon #fIcon
Fe0 f14 expr out #txt
Fe0 f14 248 69 248 100 #arcP
Fe0 f18 type ch.soreco.common.functional.File #txt
Fe0 f18 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>successful?</name>
        <nameStyle>11
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f18 86 154 28 28 14 0 #rect
Fe0 f18 @|AlternativeIcon #fIcon
Fe0 f19 expr out #txt
Fe0 f19 100 124 100 154 #arcP
Fe0 f21 expr in #txt
Fe0 f21 100 182 100 351 #arcP
Fe0 f24 type ch.soreco.common.functional.File #txt
Fe0 f24 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>succesful?</name>
        <nameStyle>10
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f24 234 250 28 28 14 0 #rect
Fe0 f24 @|AlternativeIcon #fIcon
Fe0 f27 type ch.soreco.common.functional.File #txt
Fe0 f27 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>found file?</name>
        <nameStyle>11,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f27 594 162 28 28 17 -19 #rect
Fe0 f27 @|AlternativeIcon #fIcon
Fe0 f28 type ch.soreco.common.functional.File #txt
Fe0 f28 595 459 26 26 14 0 #rect
Fe0 f28 @|EndSubIcon #fIcon
Fe0 f29 actionDecl 'ch.soreco.common.functional.File out;
' #txt
Fe0 f29 actionTable 'out=in;
' #txt
Fe0 f29 actionCode 'import ch.soreco.webbies.common.db.EntityFilter;
import ch.ivyteam.ivy.process.data.persistence.IIvyEntityManager;

IIvyEntityManager manager = ivy.persistence.Adressmutation;
List found = EntityFilter.getEntityResultList(in.filter,manager);

if(found!=null) {
	out.file = found.get(0) as ch.soreco.common.bo.File;
	out.success = true;
	}
else {
	out.success = false;
}



' #txt
Fe0 f29 type ch.soreco.common.functional.File #txt
Fe0 f29 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get File by Filter</name>
        <nameStyle>18,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f29 590 108 36 24 20 -2 #rect
Fe0 f29 @|StepIcon #fIcon
Fe0 f31 inParamDecl '<ch.soreco.common.bo.File filter> param;' #txt
Fe0 f31 inParamTable 'out.filter=param.filter;
out.success=false;
' #txt
Fe0 f31 outParamDecl '<ch.soreco.common.bo.File file> result;
' #txt
Fe0 f31 outParamTable 'result.file=in.file;
' #txt
Fe0 f31 actionDecl 'ch.soreco.common.functional.File out;
' #txt
Fe0 f31 callSignature show(ch.soreco.common.bo.File) #txt
Fe0 f31 type ch.soreco.common.functional.File #txt
Fe0 f31 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>show(File)</name>
    </language>
</elementInfo>
' #txt
Fe0 f31 595 51 26 26 14 0 #rect
Fe0 f31 @|StartSubIcon #fIcon
Fe0 f33 expr out #txt
Fe0 f33 608 77 608 108 #arcP
Fe0 f34 expr out #txt
Fe0 f34 608 132 608 162 #arcP
Fe0 f36 expr in #txt
Fe0 f36 608 190 608 459 #arcP
Fe0 f25 expr out #txt
Fe0 f25 248 220 248 250 #arcP
Fe0 f45 inParamDecl '<java.io.File file> param;' #txt
Fe0 f45 inParamTable 'out.file.file=param.file;
' #txt
Fe0 f45 outParamDecl '<> result;
' #txt
Fe0 f45 actionDecl 'ch.soreco.common.functional.File out;
' #txt
Fe0 f45 callSignature show(java.io.File) #txt
Fe0 f45 type ch.soreco.common.functional.File #txt
Fe0 f45 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>show(File)</name>
    </language>
</elementInfo>
' #txt
Fe0 f45 1059 51 26 26 14 0 #rect
Fe0 f45 @|StartSubIcon #fIcon
Fe0 f46 type ch.soreco.common.functional.File #txt
Fe0 f46 1059 331 26 26 14 0 #rect
Fe0 f46 @|EndSubIcon #fIcon
Fe0 f48 actionDecl 'ch.soreco.common.functional.File out;
' #txt
Fe0 f48 actionTable 'out=in;
out.file.contentType="application/vnd.ms-excel";
out.file.fileName=in.file.file.getName();
out.fileUrl=in.file.file.getAbsolutePath();
' #txt
Fe0 f48 actionCode '
' #txt
Fe0 f48 type ch.soreco.common.functional.File #txt
Fe0 f48 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Path</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f48 1054 156 36 24 20 -2 #rect
Fe0 f48 @|StepIcon #fIcon
Fe0 f52 outTypes "ch.soreco.common.functional.File" #txt
Fe0 f52 outLinks "LinkA.ivp" #txt
Fe0 f52 template "/ProcessPages/File/contentFile.ivc" #txt
Fe0 f52 type ch.soreco.common.functional.File #txt
Fe0 f52 skipLink skip.ivp #txt
Fe0 f52 sortLink sort.ivp #txt
Fe0 f52 templateWizard '#
#Thu Oct 03 16:19:36 CEST 2013
' #txt
Fe0 f52 pageArchivingActivated false #txt
Fe0 f52 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Content File</name>
        <nameStyle>16
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f52 @C|.responsibility Everybody #txt
Fe0 f52 1054 228 36 24 20 -2 #rect
Fe0 f52 @|PageIcon #fIcon
Fe0 f50 expr data #txt
Fe0 f50 outCond ivp=="LinkA.ivp" #txt
Fe0 f50 1072 252 1072 331 #arcP
Fe0 f56 expr out #txt
Fe0 f56 1072 180 1072 228 #arcP
Fe0 f56 0 0.484342148949783 0 0 #arcLabel
Fe0 f55 actionDecl 'ch.soreco.common.functional.File out;
' #txt
Fe0 f55 actionTable 'out=in;
' #txt
Fe0 f55 actionCode 'import ch.soreco.webbies.common.db.BlobHelper;

BlobHelper uploader = new BlobHelper("bzp_ivy_daten");
uploader.fileUpload(in.file.file.getAbsolutePath(),in.file.fileId);' #txt
Fe0 f55 type ch.soreco.common.functional.File #txt
Fe0 f55 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set Blob</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f55 302 292 36 24 20 -2 #rect
Fe0 f55 @|StepIcon #fIcon
Fe0 f57 expr in #txt
Fe0 f57 outCond in.file.fileId>0 #txt
Fe0 f57 257 269 302 294 #arcP
Fe0 f58 actionDecl 'ch.soreco.common.functional.File out;
' #txt
Fe0 f58 actionTable 'out=in;
' #txt
Fe0 f58 actionCode 'import ch.soreco.webbies.common.cms.CMSUtils;
import ch.soreco.webbies.common.db.BlobHelper;

BlobHelper blob = new BlobHelper("bzp_ivy_daten");
File file;// = blob.fileDownload(in.file.fileId);

try {
	if(!CMSUtils.exists("/fileDownload/"+in.file.fileName)){

		file = blob.fileDownload(in.file.fileId);
		if(file!=null){
			in.fileUrl = file.getAbsolutePath();
			ivy.log.debug("Download path in CMS exists: "+CMSUtils.exists("/fileDownload").toString());
			if(!CMSUtils.exists("/fileDownload/"+file.getPath())){
				ivy.log.debug("create file "+file.getPath()+" in CMS");
				if(file.readBinary().length() > 0){
					ivy.log.debug("Content: try append to /fileDownload/"+file.getPath());
					CMSUtils.setContent("/fileDownload",file.getPath(),file.getJavaFile());
					ivy.log.debug("Content is appended to /fileDownload/"+file.getPath());
					in.success = true;
				} else {
					ivy.log.debug("no filesize");
					in.success = false;
				}
			}
			out.fileUrl = ivy.cms.cr("/fileDownload/"+file.getPath());
			ivy.log.debug(ivy.cms.getContentObject("/fileDownload/"+file.getPath()).getUri());
			file.delete();
		}
		else {
			in.error = "Could not Download File";
			in.success = false;
		}
		
	} else {
		out.fileUrl = ivy.cms.cr("/fileDownload/"+in.file.fileName);
		ivy.log.debug("load existing cms file: "+out.fileUrl);
	}
}catch(Exception e){
			in.error = "Could not Download File:"+e.getMessage();
			ivy.log.error(e);
			in.success = false;
}




' #txt
Fe0 f58 type ch.soreco.common.functional.File #txt
Fe0 f58 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getBlob</name>
        <nameStyle>7,7
</nameStyle>
        <desc>import ch.soreco.webbies.common.cms.CMSUtils;
import ch.soreco.webbies.common.db.BlobHelper;

BlobHelper blob = new BlobHelper(&quot;bzp_ivy_daten&quot;);
File file;//  = blob.fileDownload(in.file.fileId);

if(CMSUtils.exists(&quot;/fileDownload/&quot;+in.file.fileName)){
	ivy.log.debug(&quot;Use existing file: &quot;+&quot;/fileDownload/&quot;+in.file.fileName);
	out.fileUrl = ivy.cms.cr(&quot;/fileDownload/&quot;+in.file.fileName);
} else {
	file = blob.fileDownload(in.file.fileId);
	if(file!=null){
		in.fileUrl = file.getAbsolutePath();
		ivy.log.debug(&quot;Download path in CMS exists: &quot;+CMSUtils.exists(&quot;/fileDownload&quot;).toString());
		if(!CMSUtils.exists(&quot;/fileDownload/&quot;+file.getPath())){
			ivy.log.debug(&quot;create file &quot;+file.getPath()+&quot; in CMS&quot;);
			CMSUtils.setContent(&quot;/fileDownload&quot;,file.getPath(),file.getJavaFile());
		}
		out.fileUrl = ivy.cms.cr(&quot;/fileDownload/&quot;+file.getPath());
		//ivy.log.debug(ivy.cms.getContentObject(&quot;/fileDownload/&quot;+file.getPath()).getUri());
	}
	else {
		in.error = &quot;Could not Download File&quot;;
		in.success = false;
	}
}



</desc>
    </language>
</elementInfo>
' #txt
Fe0 f58 790 164 36 24 20 -2 #rect
Fe0 f58 @|StepIcon #fIcon
Fe0 f30 expr out #txt
Fe0 f30 1072 77 1072 156 #arcP
Fe0 f20 actionDecl 'ch.soreco.common.functional.File out;
' #txt
Fe0 f20 actionTable 'out=in;
' #txt
Fe0 f20 actionCode 'import ch.soreco.webbies.common.db.BlobHelper;

try{
	BlobHelper uploader = new BlobHelper("bzp_ivy_daten");

	in.file.fileIvy = uploader.fileDownload(in.file.fileId);
	in.file.file = in.file.fileIvy.getJavaFile();
} catch(Exception e){
	ivy.log.error(e.getMessage());
}' #txt
Fe0 f20 type ch.soreco.common.functional.File #txt
Fe0 f20 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getBlob</name>
        <nameStyle>7,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f20 142 244 36 24 20 -2 #rect
Fe0 f20 @|StepIcon #fIcon
Fe0 f22 expr in #txt
Fe0 f22 outCond in.success #txt
Fe0 f22 106 176 152 244 #arcP
Fe0 f15 expr out #txt
Fe0 f15 153 268 106 352 #arcP
Fe0 f4 expr out #txt
Fe0 f4 248 124 248 196 #arcP
Fe0 f8 type ch.soreco.common.functional.File #txt
Fe0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>delete From File System</name>
        <nameStyle>23
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f8 234 330 28 28 14 0 #rect
Fe0 f8 @|AlternativeIcon #fIcon
Fe0 f9 expr in #txt
Fe0 f9 248 278 248 330 #arcP
Fe0 f11 expr out #txt
Fe0 f11 302 314 257 339 #arcP
Fe0 f13 actionDecl 'ch.soreco.common.functional.File out;
' #txt
Fe0 f13 actionTable 'out=in;
' #txt
Fe0 f13 actionCode 'try {

	if (!in.file.file.delete()){
		out.error = "Could not delete File from FileSystem: \n"+in.file.file.getAbsolutePath();
		ivy.log.debug(in.error);
	}

}
catch (Exception e){
	out.error = "Could not delete File from FileSystem: \n"+in.file.file.getAbsolutePath();
	ivy.log.debug(in.error,e);
}' #txt
Fe0 f13 type ch.soreco.common.functional.File #txt
Fe0 f13 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>delete File</name>
        <nameStyle>11
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f13 302 372 36 24 20 -2 #rect
Fe0 f13 @|StepIcon #fIcon
Fe0 f17 expr out #txt
Fe0 f17 305 396 258 432 #arcP
Fe0 f23 type ch.soreco.common.functional.File #txt
Fe0 f23 419 427 26 26 14 0 #rect
Fe0 f23 @|EndSubIcon #fIcon
Fe0 f26 actionDecl 'ch.soreco.common.functional.File out;
' #txt
Fe0 f26 actionTable 'out=in;
' #txt
Fe0 f26 actionCode 'try {

	if (!in.file.file.delete()){
		out.error = "Could not delete File from FileSystem: \n"+in.file.file.getAbsolutePath();
		ivy.log.debug(in.error);
	}

}
catch (Exception e){
	out.error = "Could not delete File from FileSystem: \n"+in.file.file.getAbsolutePath();
	ivy.log.debug(in.error,e);
}' #txt
Fe0 f26 type ch.soreco.common.functional.File #txt
Fe0 f26 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>delete File</name>
        <nameStyle>11
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f26 486 372 36 24 20 -2 #rect
Fe0 f26 @|StepIcon #fIcon
Fe0 f32 actionDecl 'ch.soreco.common.functional.File out;
' #txt
Fe0 f32 actionTable 'out=in;
' #txt
Fe0 f32 actionCode 'import ch.soreco.webbies.common.db.BlobHelper;

BlobHelper uploader = new BlobHelper("bzp_ivy_daten");
uploader.fileUpload(in.file.file.getAbsolutePath(),in.file.fileId);' #txt
Fe0 f32 type ch.soreco.common.functional.File #txt
Fe0 f32 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set Blob</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f32 486 292 36 24 20 -2 #rect
Fe0 f32 @|StepIcon #fIcon
Fe0 f38 actionDecl 'ch.soreco.common.functional.File out;
' #txt
Fe0 f38 actionTable 'out=in;
out.file.createDate=new DateTime();
out.file.createUserId=ivy.session.getSessionUserName();
out.file.file=new java.io.File(in.fileUrl);
out.file.fileExtension=in.file.file.getName().substring(in.file.file.getName().lastIndexOf(".")+1);
out.file.fileName=in.file.file.getName();
out.file.fileSize=in.file.file.length();
' #txt
Fe0 f38 actionCode 'import java.net.URLConnection;

if(in.file.file.exists()) {
	try {
		in.file.contentType = URLConnection.guessContentTypeFromName(in.fileUrl);
	}
	catch (Exception e){
		ivy.log.debug("Could not Guess the content type of "+in.fileUrl, e);
		}
	}
else {
	out.success = false;
	out.error ="File not exists";
	}
String upperExtension = in.file.file.getName().substring(in.file.file.getName().lastIndexOf(".")+1);
String lowerExtension = in.file.file.getName().substring(in.file.file.getName().lastIndexOf(".")+1).toLowerCase();
out.file.fileName.replace(upperExtension,lowerExtension);' #txt
Fe0 f38 type ch.soreco.common.functional.File #txt
Fe0 f38 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>guess ContentType</name>
        <nameStyle>17
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f38 414 100 36 24 20 -2 #rect
Fe0 f38 @|StepIcon #fIcon
Fe0 f40 actionDecl 'ch.soreco.common.functional.File out;
' #txt
Fe0 f40 actionTable 'out=in;
' #txt
Fe0 f40 actionCode '//persist to get an id
try {
	java.io.File file = in.file.file;
	out.file = ivy.persistence.Adressmutation.merge(in.file) as ch.soreco.common.bo.File;
	out.file.file = file;
	out.success = true;
	}
catch (Exception e) {
	out.success = false;
	out.error = e.getMessage();
	ivy.log.error(e);
	}' #txt
Fe0 f40 type ch.soreco.common.functional.File #txt
Fe0 f40 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>merge File</name>
        <nameStyle>10
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f40 414 196 36 24 20 -2 #rect
Fe0 f40 @|StepIcon #fIcon
Fe0 f41 type ch.soreco.common.functional.File #txt
Fe0 f41 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>delete From File System</name>
        <nameStyle>23
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f41 418 330 28 28 14 0 #rect
Fe0 f41 @|AlternativeIcon #fIcon
Fe0 f42 type ch.soreco.common.functional.File #txt
Fe0 f42 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>succesful?</name>
        <nameStyle>10
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f42 418 250 28 28 14 0 #rect
Fe0 f42 @|AlternativeIcon #fIcon
Fe0 f43 inParamDecl '<java.lang.Boolean keepFile,java.lang.Boolean deleteFromFileSystem,java.lang.String filePath> param;' #txt
Fe0 f43 inParamTable 'out.deleteFromFileSystem=(param.deleteFromFileSystem==null)? true: param.deleteFromFileSystem;
out.fileUrl=param.filePath;
out.success=false;
' #txt
Fe0 f43 outParamDecl '<java.lang.String error,java.lang.Boolean success,ch.soreco.common.bo.File file> result;
' #txt
Fe0 f43 outParamTable 'result.error=in.error;
result.success=in.success;
result.file=in.file;
' #txt
Fe0 f43 actionDecl 'ch.soreco.common.functional.File out;
' #txt
Fe0 f43 callSignature set(Boolean,Boolean,String) #txt
Fe0 f43 type ch.soreco.common.functional.File #txt
Fe0 f43 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set(Boolean,Boolean,String)</name>
    </language>
</elementInfo>
' #txt
Fe0 f43 419 43 26 26 14 0 #rect
Fe0 f43 @|StartSubIcon #fIcon
Fe0 f44 expr out #txt
Fe0 f44 432 69 432 100 #arcP
Fe0 f47 expr out #txt
Fe0 f47 432 220 432 250 #arcP
Fe0 f49 expr in #txt
Fe0 f49 outCond in.file.fileId>0 #txt
Fe0 f49 441 269 486 294 #arcP
Fe0 f51 expr out #txt
Fe0 f51 432 124 432 196 #arcP
Fe0 f53 expr in #txt
Fe0 f53 432 278 432 330 #arcP
Fe0 f54 expr out #txt
Fe0 f54 486 314 441 339 #arcP
Fe0 f62 expr out #txt
Fe0 f62 489 396 442 432 #arcP
Fe0 f63 type ch.soreco.common.functional.File #txt
Fe0 f63 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>successful?</name>
        <nameStyle>11
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f63 658 290 28 28 14 0 #rect
Fe0 f63 @|AlternativeIcon #fIcon
Fe0 f60 expr in #txt
Fe0 f60 441 349 486 374 #arcP
Fe0 f61 expr in #txt
Fe0 f61 outCond in.keepFile #txt
Fe0 f61 432 358 432 427 #arcP
Fe0 f16 expr in #txt
Fe0 f16 257 349 302 374 #arcP
Fe0 f10 expr in #txt
Fe0 f10 outCond in.keepFile #txt
Fe0 f10 248 358 248 427 #arcP
Fe0 f10 0 0.5285863943713797 0 0 #arcLabel
Fe0 f66 outTypes "ch.soreco.common.functional.File" #txt
Fe0 f66 outLinks "LinkA.ivp" #txt
Fe0 f66 template "redirect.jsp" #txt
Fe0 f66 type ch.soreco.common.functional.File #txt
Fe0 f66 skipLink skip.ivp #txt
Fe0 f66 sortLink sort.ivp #txt
Fe0 f66 templateWizard '#
#Thu Oct 03 15:56:41 CEST 2013
' #txt
Fe0 f66 pageArchivingActivated false #txt
Fe0 f66 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>redirect to PDF</name>
        <nameStyle>15,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f66 @C|.responsibility Everybody #txt
Fe0 f66 782 348 36 24 20 -2 #rect
Fe0 f66 @|PageIcon #fIcon
Fe0 f37 expr data #txt
Fe0 f37 outCond ivp=="LinkA.ivp" #txt
Fe0 f37 800 372 621 472 #arcP
Fe0 f37 1 800 472 #addKink
Fe0 f37 1 0.24221569246662358 0 0 #arcLabel
Fe0 f68 outTypes "ch.soreco.common.functional.File" #txt
Fe0 f68 outLinks "LinkA.ivp" #txt
Fe0 f68 template "/ProcessPages/File/noDocument.ivc" #txt
Fe0 f68 type ch.soreco.common.functional.File #txt
Fe0 f68 skipLink skip.ivp #txt
Fe0 f68 sortLink sort.ivp #txt
Fe0 f68 templateWizard '#
#Mon Oct 29 11:43:48 CET 2012
' #txt
Fe0 f68 pageArchivingActivated false #txt
Fe0 f68 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>no document</name>
        <nameStyle>11,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f68 @C|.responsibility Everybody #txt
Fe0 f68 654 364 36 24 20 -2 #rect
Fe0 f68 @|PageIcon #fIcon
Fe0 f69 expr in #txt
Fe0 f69 672 318 672 364 #arcP
Fe0 f65 expr data #txt
Fe0 f65 outCond ivp=="LinkA.ivp" #txt
Fe0 f65 672 388 619 465 #arcP
Fe0 f65 1 672 432 #addKink
Fe0 f65 1 0.15035819936246717 0 0 #arcLabel
Fe0 f35 outTypes "ch.soreco.common.functional.File" #txt
Fe0 f35 outLinks "LinkA.ivp" #txt
Fe0 f35 template "/ProcessPages/File/redirectToCMSFile.ivc" #txt
Fe0 f35 type ch.soreco.common.functional.File #txt
Fe0 f35 skipLink skip.ivp #txt
Fe0 f35 sortLink sort.ivp #txt
Fe0 f35 templateWizard '#
#Thu May 12 08:55:06 CEST 2016
' #txt
Fe0 f35 pageArchivingActivated false #txt
Fe0 f35 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>HTML redirect to fileUrl</name>
        <nameStyle>24,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f35 @C|.responsibility Everybody #txt
Fe0 f35 918 364 36 24 20 -2 #rect
Fe0 f35 @|PageIcon #fIcon
Fe0 f67 expr in #txt
Fe0 f67 outCond in.success #txt
Fe0 f67 686 304 936 364 #arcP
Fe0 f67 1 936 304 #addKink
Fe0 f67 0 0.7710215855700752 0 0 #arcLabel
Fe0 f39 expr data #txt
Fe0 f39 outCond ivp=="LinkA.ivp" #txt
Fe0 f39 936 388 621 472 #arcP
Fe0 f39 1 936 472 #addKink
Fe0 f39 1 0.35323463865321353 0 0 #arcLabel
Fe0 f59 actionDecl 'ch.soreco.common.functional.File out;
' #txt
Fe0 f59 actionTable 'out=in;
' #txt
Fe0 f59 actionCode 'import java.util.Calendar;
import ch.soreco.webbies.common.db.BlobHelper;

BlobHelper blob = new BlobHelper("bzp_ivy_daten");
File file;
try {
	file = blob.downloadToSessionFile(in.file.fileId);
	if(file!=null){
		Binary binary = file.readBinary();
		if(binary!=null&&binary.length()>0){
			out.fileUrl = ivy.html.fileref(file);
			if(!out.fileUrl.contains("?")){
				out.fileUrl=out.fileUrl+"?";
			}
			out.fileUrl = out.fileUrl+ "&timeStamp="+String.valueOf(Calendar.getInstance().getTimeInMillis());
			in.success = true;
			ivy.log.debug("downloaded file id {0} to {1}", in.file.fileId, out.fileUrl);
		} else {
			ivy.log.debug("no filesize");
			in.success = false;
		}
	}	else {
		in.error = "Could not Download File";
		in.success = false;
	}
} catch(Exception e){
			in.error = "Could not Download File: "+e.getMessage();
			ivy.log.error(in.error, e);
			in.success = false;
}' #txt
Fe0 f59 type ch.soreco.common.functional.File #txt
Fe0 f59 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Download File to Session Files folder</name>
        <nameStyle>37,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Fe0 f59 654 228 36 24 20 -2 #rect
Fe0 f59 @|StepIcon #fIcon
Fe0 f71 expr in #txt
Fe0 f71 outCond in.success #txt
Fe0 f71 622 176 672 228 #arcP
Fe0 f71 1 672 176 #addKink
Fe0 f71 0 0.9020113168155675 0 0 #arcLabel
Fe0 f70 expr out #txt
Fe0 f70 672 252 672 290 #arcP
Fe0 f70 0 0.9020113168155675 0 0 #arcLabel
>Proto Fe0 .type ch.soreco.common.functional.File #txt
>Proto Fe0 .processKind CALLABLE_SUB #txt
>Proto Fe0 0 0 32 24 18 0 #rect
>Proto Fe0 @|BIcon #fIcon
Fe0 f0 mainOut f3 tail #connect
Fe0 f3 head f2 mainIn #connect
Fe0 f7 mainOut f14 tail #connect
Fe0 f14 head f12 mainIn #connect
Fe0 f2 mainOut f19 tail #connect
Fe0 f19 head f18 in #connect
Fe0 f21 head f1 mainIn #connect
Fe0 f31 mainOut f33 tail #connect
Fe0 f33 head f29 mainIn #connect
Fe0 f29 mainOut f34 tail #connect
Fe0 f34 head f27 in #connect
Fe0 f36 head f28 mainIn #connect
Fe0 f6 mainOut f25 tail #connect
Fe0 f25 head f24 in #connect
Fe0 f52 out f50 tail #connect
Fe0 f50 head f46 mainIn #connect
Fe0 f48 mainOut f56 tail #connect
Fe0 f56 head f52 mainIn #connect
Fe0 f24 out f57 tail #connect
Fe0 f57 head f55 mainIn #connect
Fe0 f45 mainOut f30 tail #connect
Fe0 f30 head f48 mainIn #connect
Fe0 f18 out f22 tail #connect
Fe0 f22 head f20 mainIn #connect
Fe0 f18 out f21 tail #connect
Fe0 f20 mainOut f15 tail #connect
Fe0 f15 head f1 mainIn #connect
Fe0 f12 mainOut f4 tail #connect
Fe0 f4 head f6 mainIn #connect
Fe0 f24 out f9 tail #connect
Fe0 f9 head f8 in #connect
Fe0 f55 mainOut f11 tail #connect
Fe0 f11 head f8 in #connect
Fe0 f13 mainOut f17 tail #connect
Fe0 f17 head f5 mainIn #connect
Fe0 f43 mainOut f44 tail #connect
Fe0 f44 head f38 mainIn #connect
Fe0 f40 mainOut f47 tail #connect
Fe0 f47 head f42 in #connect
Fe0 f42 out f49 tail #connect
Fe0 f49 head f32 mainIn #connect
Fe0 f38 mainOut f51 tail #connect
Fe0 f51 head f40 mainIn #connect
Fe0 f42 out f53 tail #connect
Fe0 f53 head f41 in #connect
Fe0 f32 mainOut f54 tail #connect
Fe0 f54 head f41 in #connect
Fe0 f26 mainOut f62 tail #connect
Fe0 f62 head f23 mainIn #connect
Fe0 f60 head f26 mainIn #connect
Fe0 f41 out f61 tail #connect
Fe0 f61 head f23 mainIn #connect
Fe0 f41 out f60 tail #connect
Fe0 f16 head f13 mainIn #connect
Fe0 f8 out f10 tail #connect
Fe0 f10 head f5 mainIn #connect
Fe0 f8 out f16 tail #connect
Fe0 f66 out f37 tail #connect
Fe0 f37 head f28 mainIn #connect
Fe0 f69 head f68 mainIn #connect
Fe0 f68 out f65 tail #connect
Fe0 f65 head f28 mainIn #connect
Fe0 f63 out f67 tail #connect
Fe0 f67 head f35 mainIn #connect
Fe0 f63 out f69 tail #connect
Fe0 f35 out f39 tail #connect
Fe0 f39 head f28 mainIn #connect
Fe0 f27 out f71 tail #connect
Fe0 f71 head f59 mainIn #connect
Fe0 f27 out f36 tail #connect
Fe0 f59 mainOut f70 tail #connect
Fe0 f70 head f63 in #connect
