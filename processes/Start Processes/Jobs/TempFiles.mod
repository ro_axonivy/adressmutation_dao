[Ivy]
[>Created: Thu Mar 15 17:08:07 CET 2012]
13616E10BF176917 3.15 #module
>Proto >Proto Collection #zClass
Ts0 TempFiles Big #zClass
Ts0 B #cInfo
Ts0 #process
Ts0 @TextInP .resExport .resExport #zField
Ts0 @TextInP .type .type #zField
Ts0 @TextInP .processKind .processKind #zField
Ts0 @AnnotationInP-0n ai ai #zField
Ts0 @TextInP .xml .xml #zField
Ts0 @TextInP .responsibility .responsibility #zField
Ts0 @StartEvent f21 '' #zField
Ts0 @GridStep f63 '' #zField
Ts0 @EndTask f57 '' #zField
Ts0 @PushWFArc f70 '' #zField
Ts0 @PushWFArc f110 '' #zField
Ts0 @StartRequest f0 '' #zField
Ts0 @GridStep f2 '' #zField
Ts0 @PushWFArc f3 '' #zField
Ts0 @EndTask f4 '' #zField
Ts0 @PushWFArc f5 '' #zField
>Proto Ts0 Ts0 TempFiles #zField
Ts0 f21 outerBean "ch.ivyteam.ivy.process.eventstart.beans.AutoProcessStarterEventBean" #txt
Ts0 f21 beanConfig "1800" #txt
Ts0 f21 outLink eventLink.ivp #txt
Ts0 f21 type adressmutation_dao.Data #txt
Ts0 f21 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>delete temp files</name>
        <nameStyle>17,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ts0 f21 @C|.responsibility Everybody #txt
Ts0 f21 91 43 26 26 14 0 #rect
Ts0 f21 @|StartEventIcon #fIcon
Ts0 f63 actionDecl 'adressmutation_dao.Data out;
' #txt
Ts0 f63 actionTable 'out=in;
' #txt
Ts0 f63 actionCode 'import ch.ivyteam.ivy.cm.IContentObject;
import ch.soreco.webbies.common.cms.CMSUtils;

DateTime dt = new DateTime();

if(dt.getHours() == 23){
	ivy.log.debug("deleting tempfiles in CMS");	
	CMSUtils.deleteContentOfDirectory("/fileDownload");
}' #txt
Ts0 f63 type adressmutation_dao.Data #txt
Ts0 f63 86 116 36 24 20 -2 #rect
Ts0 f63 @|StepIcon #fIcon
Ts0 f57 type adressmutation_dao.Data #txt
Ts0 f57 91 195 26 26 14 0 #rect
Ts0 f57 @|EndIcon #fIcon
Ts0 f70 expr out #txt
Ts0 f70 104 69 104 116 #arcP
Ts0 f110 expr out #txt
Ts0 f110 104 140 104 195 #arcP
Ts0 f0 outLink deleteTempFiles.ivp #txt
Ts0 f0 type adressmutation_dao.Data #txt
Ts0 f0 inParamDecl '<> param;' #txt
Ts0 f0 actionDecl 'adressmutation_dao.Data out;
' #txt
Ts0 f0 guid 13616E1C193AD6EC #txt
Ts0 f0 requestEnabled true #txt
Ts0 f0 triggerEnabled false #txt
Ts0 f0 callSignature deleteTempFiles() #txt
Ts0 f0 persist false #txt
Ts0 f0 taskData '#
#Thu Mar 15 17:08:05 CET 2012
TaskTriggered.ROL=Everybody
TaskTriggered.EXTYPE=0
TaskTriggered.EXPRI=2
TaskTriggered.TYPE=0
TaskTriggered.PRI=2
TaskTriggered.EXROL=Everybody
' #txt
Ts0 f0 caseData '#
#Thu Mar 15 17:08:05 CET 2012
businessCreator.user=
businessMilestone.timestamp=
businessObject.code=
businessObject.docDb.code=
businessObject.folder.id=
businessObject.name=
businessPriority=
businessStart.timestamp=
case.description=
case.name=
correspondent.id=
mainContact.docDb.code=
mainContact.folder.id=
mainContact.id=
mainContact.name=
mainContact.type=
process.code=
process.name=
processCategory.code=
processCategory.name=
subType.code=
subType.name=
type.code=
type.name=
' #txt
Ts0 f0 showInStartList 1 #txt
Ts0 f0 roleExceptionId '>> Ignore Exception' #txt
Ts0 f0 taskAndCaseSetupAction 'import ch.ivyteam.ivy.workflow.TaskUpdateDefinition;
ch.ivyteam.ivy.workflow.TaskUpdateDefinition taskUpdDef = new ch.ivyteam.ivy.workflow.TaskUpdateDefinition();taskUpdDef.setPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
taskUpdDef.setExpiryActivator("Everybody");
taskUpdDef.setExpiryPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
engine.updateCurrentTask(taskUpdDef);
' #txt
Ts0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>deleteTempFiles.ivp</name>
        <nameStyle>19,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ts0 f0 @C|.responsibility Administrator #txt
Ts0 f0 235 43 26 26 14 0 #rect
Ts0 f0 @|StartRequestIcon #fIcon
Ts0 f2 actionDecl 'adressmutation_dao.Data out;
' #txt
Ts0 f2 actionTable 'out=in;
' #txt
Ts0 f2 actionCode 'import ch.soreco.webbies.common.cms.CMSUtils;
CMSUtils.deleteContentOfDirectory("/fileDownload");
' #txt
Ts0 f2 type adressmutation_dao.Data #txt
Ts0 f2 230 116 36 24 20 -2 #rect
Ts0 f2 @|StepIcon #fIcon
Ts0 f3 expr out #txt
Ts0 f3 248 69 248 116 #arcP
Ts0 f4 type adressmutation_dao.Data #txt
Ts0 f4 235 195 26 26 14 0 #rect
Ts0 f4 @|EndIcon #fIcon
Ts0 f5 expr out #txt
Ts0 f5 248 140 248 195 #arcP
>Proto Ts0 .type adressmutation_dao.Data #txt
>Proto Ts0 .processKind NORMAL #txt
>Proto Ts0 0 0 32 24 18 0 #rect
>Proto Ts0 @|BIcon #fIcon
Ts0 f21 mainOut f70 tail #connect
Ts0 f70 head f63 mainIn #connect
Ts0 f63 mainOut f110 tail #connect
Ts0 f110 head f57 mainIn #connect
Ts0 f0 mainOut f3 tail #connect
Ts0 f3 head f2 mainIn #connect
Ts0 f2 mainOut f5 tail #connect
Ts0 f5 head f4 mainIn #connect
