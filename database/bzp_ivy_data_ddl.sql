USE [bzp_ivy_data]
GO
/****** Object:  Table [dbo].[btbl_TimeRemainingConfig]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[btbl_TimeRemainingConfig](
	[TimeRemainingConfigId] [int] IDENTITY(1,1) NOT NULL,
	[dayEndsAt] [time](7) NULL,
	[dayEndsAtDuration] [int] NULL,
	[dayStartsAt] [time](7) NULL,
	[dayStartsAtDuration] [int] NULL,
	[incomeOverflowDayEndAt] [time](7) NULL,
	[incomeOverflowDayEndAtDuration] [int] NULL,
	[incomeOverflowsAt] [time](7) NULL,
	[incomeOverflowsAtDuration] [int] NULL,
	[minRecordingDuration] [int] NULL,
	[minRecordingTime] [time](7) NULL,
	[orderType] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[TimeRemainingConfigId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[btbl_ScanShares]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[btbl_ScanShares](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ArchiveDeadlineDays] [int] NULL,
	[ArchiveFolder] [varchar](255) NULL,
	[ConfigName] [varchar](255) NULL,
	[LastArchiveScan] [datetime2](7) NULL,
	[NetPath] [varchar](500) NULL,
	[NetPathPWD] [varchar](255) NULL,
	[NetPathUID] [varchar](255) NULL,
	[NextScan] [datetime2](7) NULL,
	[ScanInterval] [int] NULL,
	[ScanNo] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[btbl_OrderMachings]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[btbl_OrderMachings](
	[orderMatchId] [int] IDENTITY(1,1) NOT NULL,
	[archiveDuration] [int] NULL,
	[docType] [varchar](255) NULL,
	[mapToDocType] [varchar](255) NULL,
	[orderPriority] [int] NULL,
	[orderType] [varchar](255) NULL,
	[priority] [int] NULL,
	[stateAfterCreate] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[orderMatchId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[btbl_AuthorityGroupUsers]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[btbl_AuthorityGroupUsers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[authorityGroupId] [int] NOT NULL,
	[orderBookId] [int] NOT NULL,
	[userSign] [nvarchar](50) NOT NULL,
	[canRead] [smallint] NOT NULL,
	[canWrite] [smallint] NOT NULL,
	[canDispatch] [smallint] NOT NULL,
	[canTeamDisp] [smallint] NOT NULL,
 CONSTRAINT [PK_btbl_AuthorityGroupUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Contact_SignatureRequest]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Contact_SignatureRequest](
	[signatureRequestId] [int] IDENTITY(1,1) NOT NULL,
	[SignatureRequestOrderId] [int] NULL,
	[contactId] [int] NULL,
	[isRequested] [bit] NULL,
	[newSignatureRequest] [varchar](255) NULL,
	[signatureCheckedBy] [varchar](255) NULL,
	[signatureIsChecked] [bit] NULL,
	[signatureNeedsOrderNo] [bit] NULL,
	[signatureOrderNo] [varchar](255) NULL,
	[signatureRequestDate] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[signatureRequestId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Contact_Letters]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Contact_Letters](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[contactId] [int] NULL,
	[letterIsChecked] [bit] NULL,
	[letterIsCheckedBy] [varchar](255) NULL,
	[letterOrderNo] [varchar](255) NULL,
	[letterSubType] [varchar](255) NULL,
	[letterType] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Contact_AddressChecks]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Contact_AddressChecks](
	[contactId] [int] NOT NULL,
	[addressCheckIsChecked] [bit] NULL,
	[addressCheckOrderNo] [varchar](255) NULL,
	[addressCheckType] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[contactId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Contact_Address]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Contact_Address](
	[contactId] [int] IDENTITY(1,1) NOT NULL,
	[addressCheckedBy] [varchar](255) NULL,
	[addressIsChecked] [bit] NULL,
	[isNewAdress] [bit] NULL,
	[newAdressText] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[contactId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  UserDefinedFunction [dbo].[getSplittedTextTable]    Script Date: 10/22/2014 17:55:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/**
* Description: this function splits varchar(8000) text by given delimiter
* Author: Jeff Moden
* Have a look at:http://www.sqlservercentral.com/articles/Tally+Table/72993/
*/
CREATE FUNCTION [dbo].[getSplittedTextTable]
--===== Define I/O parameters
        (@text VARCHAR(8000), @delimiter CHAR(1))
--WARNING!!! DO NOT USE MAX DATA-TYPES HERE!  IT WILL KILL PERFORMANCE!
RETURNS TABLE WITH SCHEMABINDING AS
 RETURN
--===== "Inline" CTE Driven "Tally Table" produces values from 1 up to 10,000...
     -- enough to cover VARCHAR(8000)
  WITH E1(N) AS (
                 SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL
                 SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL
                 SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1
                ),                          --10E+1 or 10 rows
       E2(N) AS (SELECT 1 FROM E1 a, E1 b), --10E+2 or 100 rows
       E4(N) AS (SELECT 1 FROM E2 a, E2 b), --10E+4 or 10,000 rows max
 cteTally(N) AS (--==== This provides the "base" CTE and limits the number of rows right up front
                     -- for both a performance gain and prevention of accidental "overruns"
                 SELECT TOP (ISNULL(DATALENGTH(@text),0)) ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) FROM E4
                ),
cteStart(N1) AS (--==== This returns N+1 (starting position of each "element" just once for each delimiter)
                 SELECT 1 UNION ALL
                 SELECT t.N+1 FROM cteTally t WHERE SUBSTRING(@text,t.N,1) = @delimiter
                ),
cteLen(N1,L1) AS(--==== Return start and length (for use in substring)
                 SELECT s.N1,
                        ISNULL(NULLIF(CHARINDEX(@delimiter,@text,s.N1),0)-s.N1,8000)
                   FROM cteStart s
                )
--===== Do the actual split. The ISNULL/NULLIF combo handles the length for the final element when no delimiter is found.
 SELECT ItemNumber = ROW_NUMBER() OVER(ORDER BY l.N1),
        Item       = SUBSTRING(@text, l.N1, l.L1)
   FROM cteLen l
;
GO
/****** Object:  Table [dbo].[tbl_OrderBookTypes]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_OrderBookTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[orderBookId] [int] NOT NULL,
	[orderType] [varchar](255) NOT NULL,
 CONSTRAINT [PK_tbl_OrderBookTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_OrderBookRelations]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_OrderBookRelations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderBookId] [int] NOT NULL,
	[RelatedOrderBookId] [int] NOT NULL,
	[RelatedParentOrderBookId] [int] NULL,
	[IsHerited] [bit] NOT NULL,
	[RelatedOrderBookLevel] [int] NOT NULL,
 CONSTRAINT [PK_tbl_OrderBookRelations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Logs_OrderAuditEvents]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Logs_OrderAuditEvents](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[bpId] [varchar](255) NULL,
	[eventDate] [datetime2](7) NOT NULL,
	[eventType] [varchar](50) NOT NULL,
	[orderId] [bigint] NOT NULL,
	[userId] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_IssueLogs]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_IssueLogs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[conceptReference] [varchar](800) NULL,
	[description] [varchar](800) NULL,
	[expectedResult] [varchar](800) NULL,
	[issueDate] [date] NULL,
	[issuePriority] [varchar](1) NULL,
	[issueRole] [varchar](255) NULL,
	[issueSolvedDate] [date] NULL,
	[issueState] [varchar](255) NULL,
	[issuedBy] [varchar](255) NULL,
	[providerComment] [varchar](800) NULL,
	[providerState] [varchar](255) NULL,
	[release] [varchar](10) NULL,
	[screen] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_History]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_History](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [varchar](255) NULL,
	[logDate] [datetime2](7) NULL,
	[newValue] [varchar](255) NULL,
	[orderId] [int] NULL,
	[typeId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Files]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Files](
	[fileId] [int] IDENTITY(1,1) NOT NULL,
	[contentType] [varchar](255) NULL,
	[createDate] [datetime2](7) NULL,
	[createUserId] [varchar](255) NULL,
	[createUserName] [varchar](255) NULL,
	[fileContent] [varchar](8000) NULL,
	[fileExtension] [varchar](255) NULL,
	[fileGroupId] [int] NULL,
	[fileName] [varchar](255) NULL,
	[fileSize] [int] NULL,
	[hostName] [varchar](255) NULL,
	[isDeleted] [bit] NULL,
	[processModelName] [varchar](255) NULL,
	[processModelVersion] [varchar](255) NULL,
	[webAppName] [varchar](255) NULL,
	[fileBlob] [image] NULL,
PRIMARY KEY CLUSTERED 
(
	[fileId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_FileGroups]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_FileGroups](
	[fileGroupId] [int] IDENTITY(1,1) NOT NULL,
	[createDate] [datetime2](7) NULL,
	[createUserId] [varchar](255) NULL,
	[createUserName] [varchar](255) NULL,
	[fileGroupDescription] [varchar](255) NULL,
	[fileGroupName] [varchar](255) NULL,
	[hostName] [varchar](255) NULL,
	[modifyDate] [datetime2](7) NULL,
	[modifyUserId] [varchar](255) NULL,
	[modifyUserName] [varchar](255) NULL,
	[processModelName] [varchar](255) NULL,
	[processModelVersion] [varchar](255) NULL,
	[security] [varchar](255) NULL,
	[webAppName] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[fileGroupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_ErrorLog]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ErrorLog](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ErrorDetail] [varchar](255) NULL,
	[ErrorMassage] [varchar](255) NULL,
	[ExtId] [int] NULL,
	[LogLevel] [int] NULL,
	[LogObject] [varchar](255) NULL,
	[LogTime] [datetime2](7) NULL,
	[LogType] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Dispatching]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Dispatching](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[assignments] [varchar](8000) NULL,
	[dispatchDateTime] [datetime2](7) NULL,
	[dispatcherUserId] [varchar](255) NULL,
	[orderId] [int] NULL,
	[orderTypes] [varchar](8000) NULL,
	[reason] [varchar](8000) NULL,
	[teams] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_ScanLogs]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ScanLogs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Errors] [int] NULL,
	[ScanEnd] [datetime2](7) NULL,
	[ScanStart] [datetime2](7) NULL,
	[ScanType] [varchar](255) NULL,
	[ShareId] [int] NULL,
	[Successful] [int] NULL,
	[Total] [int] NULL,
	[Warnings] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Request_BusinessPartner]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Request_BusinessPartner](
	[businessPartnerId] [int] IDENTITY(1,1) NOT NULL,
	[BP] [bit] NULL,
	[EUSDCountry] [bit] NULL,
	[EUSDRetention] [bit] NULL,
	[bpOrderNo] [varchar](255) NULL,
	[businessPartnerCheckedBy] [varchar](255) NULL,
	[businessPartnerIsChecked] [bit] NULL,
	[domicile] [bit] NULL,
	[taxResid] [bit] NULL,
	[taxStatement] [bit] NULL,
	[telNr] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[businessPartnerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Recording_Correspondence]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Recording_Correspondence](
	[correspondenceId] [int] IDENTITY(1,1) NOT NULL,
	[changeAddressCheckedBy] [varchar](255) NULL,
	[changeAddressIsChecked] [bit] NULL,
	[changeAddressOrderNo] [varchar](255) NULL,
	[correspondenceType] [varchar](255) NULL,
	[newAddressIsChecked] [bit] NULL,
	[newAddressOrderNo] [varchar](255) NULL,
	[newAdressCheckedBy] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[correspondenceId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Recording_Affirmation]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Recording_Affirmation](
	[affirmationId] [int] IDENTITY(1,1) NOT NULL,
	[affirmationCheckedBy] [varchar](255) NULL,
	[affirmationIsChecked] [bit] NULL,
	[isAffirmed] [bit] NULL,
	[orderNo] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[affirmationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Papers]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Papers](
	[paperId] [int] IDENTITY(1,1) NOT NULL,
	[contactId] [int] NULL,
	[individualPaperType] [varchar](255) NULL,
	[isCRMIssued] [bit] NULL,
	[isPaper] [bit] NULL,
	[paperCheckedBy] [varchar](255) NULL,
	[paperIsChecked] [bit] NULL,
	[orderId] [int] NULL,
	[paperRequestDate] [date] NULL,
	[paperType] [varchar](255) NULL,
	[standardPaperType] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[paperId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Orders_History]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Orders_History](
	[orderId] [int] NOT NULL,
	[BPId] [varchar](255) NULL,
	[FAClanId] [varchar](255) NULL,
	[archiveId] [int] NULL,
	[bpIdIsChecked] [tinyint] NULL,
	[bpNotMatchable] [tinyint] NULL,
	[checkerUserId] [varchar](255) NULL,
	[comment] [varchar](8000) NULL,
	[discard] [tinyint] NULL,
	[dispatcherUserId] [varchar](255) NULL,
	[dispatchingId] [int] NULL,
	[doubletteIsChecked] [tinyint] NULL,
	[doubletteType] [varchar](255) NULL,
	[editorUserId] [varchar](255) NULL,
	[isDouble] [tinyint] NULL,
	[matchedToOrder] [int] NULL,
	[orderEnd] [datetime] NULL,
	[orderStart] [datetime] NULL,
	[orderState] [varchar](255) NULL,
	[orderType] [varchar](255) NULL,
	[priority] [int] NULL,
	[scanFileId] [int] NULL,
	[scanId] [varchar](255) NULL,
	[scanMonoFileId] [int] NULL,
	[scanningId] [int] NULL,
	[teams] [varchar](8000) NULL,
	[timeRemaining] [datetime] NULL,
	[recordingId] [int] NULL,
	[nextDueDate] [datetime] NULL,
	[orderQuantityType] [varchar](1) NULL,
	[HistoryDate] [datetime] NOT NULL,
	[HistoryYear] [int] NOT NULL,
	[HistoryMonth] [int] NOT NULL,
	[HistoryDay] [int] NOT NULL,
	[HistoryType] [nvarchar](10) NOT NULL,
	[TriggerType] [nvarchar](10) NOT NULL,
	[HistoryId] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[v_Authored_OrderTypesPerUser]    Script Date: 10/22/2014 17:55:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_Authored_OrderTypesPerUser]
AS
SELECT 
	authoredUsers.userSign
	, relatedOrderTypes.orderType
	, MAX(authoredUsers.canRead) as canRead
	, MAX(authoredUsers.canWrite) as canWrite
	, MAX(authoredUsers.canDispatch) as canDispatch
	, MAX(authoredUsers.canTeamDisp) as canTeamDisp
FROM 
	btbl_AuthorityGroupUsers authoredUsers
	INNER JOIN tbl_orderBookRelations relatedBooks ON authoredUsers.orderBookId=relatedBooks.orderBookId
	INNER JOIN tbl_OrderBookTypes relatedOrderTypes ON relatedBooks.RelatedOrderBookId=relatedOrderTypes.OrderBookId
GROUP BY
	authoredUsers.userSign
	, relatedOrderTypes.orderType
GO
/****** Object:  Table [dbo].[tbl_Scanning]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Scanning](
	[scanningId] [int] IDENTITY(1,1) NOT NULL,
	[archivingPreparedDate] [datetime2](7) NULL,
	[archivingState] [varchar](255) NULL,
	[contentOfDM] [varchar](255) NULL,
	[docNo] [varchar](255) NULL,
	[docType] [varchar](255) NULL,
	[isDossier] [bit] NULL,
	[isMain] [bit] NULL,
	[numberOfPages] [int] NULL,
	[numberOfPagesNet] [int] NULL,
	[orderId] [int] NULL,
	[orderType] [varchar](255) NULL,
	[pageNoEnd] [int] NULL,
	[pageNoStart] [int] NULL,
	[scanDate] [date] NULL,
	[scanFileId] [int] NULL,
	[scanId] [varchar](255) NULL,
	[scanMonoFileId] [int] NULL,
	[scanTime] [datetime2](7) NULL,
	[scanShareId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[scanningId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Recording_CorrespondenceContainer]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Recording_CorrespondenceContainer](
	[correspondenceContainerId] [int] IDENTITY(1,1) NOT NULL,
	[change] [bit] NULL,
	[containerCheckedBy] [varchar](255) NULL,
	[containerIsChecked] [bit] NULL,
	[containerOrderNo] [varchar](255) NULL,
	[spedit] [bit] NULL,
	[correspondenceId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[correspondenceContainerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Recordings]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Recordings](
	[recordingId] [int] IDENTITY(1,1) NOT NULL,
	[affirmationId] [int] NULL,
	[businessPartner_businessPartnerId] [int] NULL,
	[correspondence_correspondenceId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[recordingId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Contacts]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Contacts](
	[contactId] [int] IDENTITY(1,1) NOT NULL,
	[contactCheckedBy] [varchar](255) NULL,
	[contactIsChecked] [bit] NULL,
	[contactMergedFromOrderId] [int] NULL,
	[orderId] [int] NULL,
	[faxOrMailReversType] [varchar](255) NULL,
	[faxReversType] [varchar](255) NULL,
	[faxSignatureType] [varchar](255) NULL,
	[incomeDate] [datetime2](7) NULL,
	[orderIncomeType] [varchar](255) NULL,
	[postalOriginalType] [varchar](255) NULL,
	[postalSignatureType] [varchar](255) NULL,
	[scanFileId] [int] NULL,
	[address_contactId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[contactId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_OrderBooks]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_OrderBooks](
	[orderBookId] [int] IDENTITY(1,1) NOT NULL,
	[analysable] [bit] NULL,
	[filterable] [bit] NULL,
	[indefiniteType] [bit] NULL,
	[name] [varchar](255) NULL,
	[officialTeam] [bit] NULL,
	[orderTypes] [varchar](2500) NULL,
	[requiresOrderQuantityType] [bit] NULL,
	[shortname] [varchar](255) NULL,
	[sortSequence] [int] NULL,
	[parentOrderBook_orderBookId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[orderBookId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Attachment]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Attachment](
	[attachmentId] [int] IDENTITY(1,1) NOT NULL,
	[scanningId] [int] NOT NULL,
	[fileId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[attachmentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_bgk1qj0wl9byw6km6bj7va1uu] UNIQUE NONCLUSTERED 
(
	[fileId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[btbl_AuthorityGroup]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[btbl_AuthorityGroup](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1500) NULL,
	[GroupName] [varchar](255) NULL,
	[GroupRole] [varchar](255) NULL,
	[Users] [varchar](2500) NULL,
	[OrderBook_orderBookId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Recording_Meta]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Recording_Meta](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[recordingMetaComment] [varchar](255) NULL,
	[recordingMetaIsChecked] [bit] NULL,
	[recordingMetaIsCheckedBy] [varchar](255) NULL,
	[recordingMetaOrderNo] [varchar](255) NULL,
	[recordingMetaType] [varchar](255) NULL,
	[recordingId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Recording_LSV]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Recording_LSV](
	[lsvId] [int] IDENTITY(1,1) NOT NULL,
	[LSVCheckedBy] [varchar](255) NULL,
	[LSVIsChecked] [bit] NULL,
	[isLSV] [bit] NULL,
	[lsvOrderNo] [varchar](255) NULL,
	[recordingId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[lsvId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Recording_DomicileOrOwner]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Recording_DomicileOrOwner](
	[domicileOrOwnerId] [int] IDENTITY(1,1) NOT NULL,
	[bpLevel] [varchar](255) NULL,
	[domicileOrOwnerCheckedBy] [varchar](255) NULL,
	[domicileOrOwnerIsChecked] [bit] NULL,
	[isAddressmutation] [bit] NULL,
	[orderNo] [varchar](255) NULL,
	[recordingId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[domicileOrOwnerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Orders]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Orders](
	[orderId] [int] IDENTITY(1,1) NOT NULL,
	[BPId] [varchar](255) NULL,
	[FAClanId] [varchar](255) NULL,
	[archiveId] [int] NULL,
	[bpIdIsChecked] [bit] NULL,
	[bpNotMatchable] [bit] NULL,
	[checkerUserId] [varchar](255) NULL,
	[comment] [varchar](8000) NULL,
	[discard] [bit] NULL,
	[dispatcherUserId] [varchar](255) NULL,
	[dispatchingId] [int] NULL,
	[doubletteIsChecked] [bit] NULL,
	[doubletteType] [varchar](255) NULL,
	[editorUserId] [varchar](255) NULL,
	[isDouble] [bit] NULL,
	[lastEdit] [datetime2](7) NULL,
	[matchedToOrder] [int] NULL,
	[nextDueDate] [date] NULL,
	[orderEnd] [datetime2](7) NULL,
	[orderQuantityType] [varchar](1) NULL,
	[orderStart] [datetime2](7) NULL,
	[orderState] [varchar](255) NULL,
	[orderType] [varchar](255) NULL,
	[pendencyTicketType] [varchar](255) NULL,
	[priority] [int] NULL,
	[scanFileId] [int] NULL,
	[scanId] [varchar](255) NULL,
	[scanMonoFileId] [int] NULL,
	[scanningId] [int] NULL,
	[teams] [varchar](8000) NULL,
	[timeRemaining] [datetime2](7) NULL,
	[recordingId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[orderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_PendencyTickets]    Script Date: 10/22/2014 17:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_PendencyTickets](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[CRMIssueOrderNo] [varchar](255) NULL,
	[allowManualRemindAt] [bit] NULL,
	[createDateTime] [datetime2](7) NULL,
	[createUser] [varchar](255) NULL,
	[delegateTo] [varchar](255) NULL,
	[department] [varchar](255) NULL,
	[dueDate] [date] NULL,
	[isWaitingOnResponse] [bit] NULL,
	[pendencyTicketType] [varchar](255) NULL,
	[remindAt] [date] NULL,
	[remindInDays] [int] NULL,
	[respondUser] [varchar](255) NULL,
	[solveDateTime] [datetime2](7) NULL,
	[solved] [bit] NULL,
	[orderId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  UserDefinedFunction [dbo].[GetRelatedOrders]    Script Date: 10/22/2014 17:55:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetRelatedOrders]
(
	@OrderId int
)
RETURNS 
@Matches TABLE 
(
	OrderId int not null
)
AS
BEGIN
	WITH Parents(orderId, matchedToOrder, dispatchingId)
	AS
	(
		--Anchor
		SELECT 
			orders.orderId,
			CASE WHEN orders.matchedToOrder IS NULL THEN 
				orders.orderId --may matched by dispatchingId
			ELSE 
				orders.matchedToOrder
			END as matchedToOrder, 
			orders.dispatchingId
		FROM 
			tbl_Orders orders
		WHERE 
			orderId = @OrderId
			OR dispatchingId IN (SELECT dispatchingId FROM tbl_Orders WHERE orderId = @OrderId)
		UNION ALL
		--Top Down Childs
		SELECT 
			siblings.orderId,
			siblings.matchedToOrder, 
			siblings.dispatchingId
		FROM tbl_Orders as siblings INNER JOIN Parents 
			ON Parents.matchedToOrder =siblings.orderId
		WHERE 
			siblings.orderId <> siblings.matchedToOrder
			AND NOT siblings.matchedToOrder IS NULL
	)
	INSERT @Matches
	SELECT
		matchedToOrder
	FROM 
		Parents;

	INSERT 
		@Matches
	SELECT 
		dispatches.orderId 
	FROM 
		tbl_Orders orders
		INNER JOIN tbl_Orders dispatches ON orders.dispatchingId = dispatches.dispatchingId
		INNER JOIN @Matches matches ON orders.orderId = matches.orderId;


	WITH ChildsOfParents(OrderId)
	AS (
		SELECT orderId FROM @Matches as parents
		UNION ALL
		--Top Down Childs
		SELECT 
			siblings.orderId as ParentOrderId
		FROM tbl_Orders as siblings INNER JOIN ChildsOfParents 
			ON ChildsOfParents.OrderId =siblings.matchedToOrder
		WHERE 
			siblings.orderId <> siblings.matchedToOrder	
	)
	INSERT @Matches
	SELECT
		OrderId
	FROM 
		ChildsOfParents;

	-- add the linked dispatchingIds
	INSERT @Matches
	SELECT
		OrderId
	FROM 
		tbl_Orders 
	WHERE 
		dispatchingId in (SELECT dispatchingId from tbl_Orders where orderId in (select orderId from @Matches))

	RETURN 
END
GO
/****** Object:  UserDefinedFunction [dbo].[getOrderIdsWithOrderNo]    Script Date: 10/22/2014 17:55:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Streuli
-- Create date: 18.12.2013
-- Description:	Returns Order Id's having underlying given orderNo
-- =============================================
CREATE FUNCTION [dbo].[getOrderIdsWithOrderNo] 
(	
	@OrderNo nvarchar(255)
)
RETURNS TABLE 
AS
RETURN 
(
SELECT DISTINCT
	o.orderId 
FROM    
	tbl_Orders o 
	--ContactSignature 
	LEFT OUTER JOIN dbo.tbl_Contact_SignatureRequest signR ON o.orderId = signR.SignatureRequestOrderId 
		AND signR.signatureOrderNo = @OrderNo 
	--Contacts  
	LEFT OUTER JOIN dbo.tbl_Contacts contact ON o.orderId = contact.orderId  
		--Letters  
		LEFT OUTER JOIN dbo.tbl_Contact_Letters lettr ON contact.contactId = lettr.contactId 
			AND lettr.letterOrderNo = @OrderNo  
		--PendencyTickets  
		LEFT OUTER JOIN dbo.tbl_PendencyTickets ticket ON o.orderId = ticket.orderId 
			AND ticket.CRMIssueOrderNo = @OrderNo  
	--Recordings 
	LEFT OUTER JOIN tbl_Recordings rec ON o.recordingId = rec.recordingId 
		LEFT OUTER JOIN tbl_Recording_Correspondence cor ON rec.correspondence_correspondenceId = cor.correspondenceId 
			--Correspondence has No Order no 
			LEFT OUTER JOIN tbl_Recording_CorrespondenceContainer cont ON cont.correspondenceId = cor.correspondenceId 
				AND 
						( 
								cor.changeAddressOrderNo = @OrderNo 
						OR      cor.newAddressOrderNo    = @OrderNo 
						) 
		LEFT OUTER JOIN dbo.tbl_Recording_DomicileOrOwner doo ON o.recordingId = doo.recordingId 
			AND doo.orderNo = @OrderNo 
		LEFT OUTER JOIN tbl_Request_BusinessPartner bp ON rec.businessPartner_businessPartnerId = bp.businessPartnerId 
			AND bp.bpOrderNo = @OrderNo 
		LEFT OUTER JOIN dbo.tbl_Recording_Affirmation aff ON rec.affirmationId = aff.affirmationId 
			AND aff.orderNo = @OrderNo 
		LEFT OUTER JOIN dbo.tbl_Recording_LSV lsv ON rec.recordingId = lsv.recordingId 
			AND lsv.lsvOrderNo  = @OrderNo 
		--MetaTypes 
		LEFT OUTER JOIN dbo.tbl_Recording_Meta meta  ON  o.recordingId = meta.recordingId 
			AND meta.recordingMetaOrderNo = @OrderNo 
WHERE
	NOT (CASE 
	WHEN signr.signatureOrderNo<>'' THEN signr.signatureOrderNo 
	WHEN cor.changeAddressOrderNo= @OrderNo THEN cor.changeAddressOrderNo 
	WHEN cor.newAddressOrderNo = @OrderNo THEN cor.newAddressOrderNo 
	WHEN cont.containerOrderNo<>'' THEN cont.containerOrderNo 
	WHEN doo.orderNo<>'' THEN doo.orderNo 
	WHEN aff.orderNo<>'' THEN aff.orderNo 
	WHEN bp.bpOrderNo<>'' THEN bp.bpOrderNo 
	WHEN lsv.lsvOrderNo<>'' THEN lsv.lsvOrderNo 
	WHEN meta.recordingMetaOrderNo<>'' THEN meta.recordingMetaOrderNo 
	ELSE NULL END) IS NULL
)
GO
/****** Object:  ForeignKey [FK_qtlc04d53emleqxwx259nt4qc]    Script Date: 10/22/2014 17:55:12 ******/
ALTER TABLE [dbo].[btbl_AuthorityGroup]  WITH CHECK ADD  CONSTRAINT [FK_qtlc04d53emleqxwx259nt4qc] FOREIGN KEY([OrderBook_orderBookId])
REFERENCES [dbo].[tbl_OrderBooks] ([orderBookId])
GO
ALTER TABLE [dbo].[btbl_AuthorityGroup] CHECK CONSTRAINT [FK_qtlc04d53emleqxwx259nt4qc]
GO
/****** Object:  ForeignKey [FK_bgk1qj0wl9byw6km6bj7va1uu]    Script Date: 10/22/2014 17:55:12 ******/
ALTER TABLE [dbo].[tbl_Attachment]  WITH CHECK ADD  CONSTRAINT [FK_bgk1qj0wl9byw6km6bj7va1uu] FOREIGN KEY([fileId])
REFERENCES [dbo].[tbl_Files] ([fileId])
GO
ALTER TABLE [dbo].[tbl_Attachment] CHECK CONSTRAINT [FK_bgk1qj0wl9byw6km6bj7va1uu]
GO
/****** Object:  ForeignKey [FK_c2oxqdtmpeh3k6d6wigeoyjbe]    Script Date: 10/22/2014 17:55:12 ******/
ALTER TABLE [dbo].[tbl_Contacts]  WITH CHECK ADD  CONSTRAINT [FK_c2oxqdtmpeh3k6d6wigeoyjbe] FOREIGN KEY([address_contactId])
REFERENCES [dbo].[tbl_Contact_Address] ([contactId])
GO
ALTER TABLE [dbo].[tbl_Contacts] CHECK CONSTRAINT [FK_c2oxqdtmpeh3k6d6wigeoyjbe]
GO
/****** Object:  ForeignKey [FK_9qq54c908w9nlw32717nkfbep]    Script Date: 10/22/2014 17:55:12 ******/
ALTER TABLE [dbo].[tbl_OrderBooks]  WITH CHECK ADD  CONSTRAINT [FK_9qq54c908w9nlw32717nkfbep] FOREIGN KEY([parentOrderBook_orderBookId])
REFERENCES [dbo].[tbl_OrderBooks] ([orderBookId])
GO
ALTER TABLE [dbo].[tbl_OrderBooks] CHECK CONSTRAINT [FK_9qq54c908w9nlw32717nkfbep]
GO
/****** Object:  ForeignKey [FK_be7regalh0wbakxrrytsgrmrq]    Script Date: 10/22/2014 17:55:12 ******/
ALTER TABLE [dbo].[tbl_Orders]  WITH CHECK ADD  CONSTRAINT [FK_be7regalh0wbakxrrytsgrmrq] FOREIGN KEY([recordingId])
REFERENCES [dbo].[tbl_Recordings] ([recordingId])
GO
ALTER TABLE [dbo].[tbl_Orders] CHECK CONSTRAINT [FK_be7regalh0wbakxrrytsgrmrq]
GO
/****** Object:  ForeignKey [FK_65lvg5cekv1nd8x8du45r36ie]    Script Date: 10/22/2014 17:55:12 ******/
ALTER TABLE [dbo].[tbl_PendencyTickets]  WITH CHECK ADD  CONSTRAINT [FK_65lvg5cekv1nd8x8du45r36ie] FOREIGN KEY([orderId])
REFERENCES [dbo].[tbl_Orders] ([orderId])
GO
ALTER TABLE [dbo].[tbl_PendencyTickets] CHECK CONSTRAINT [FK_65lvg5cekv1nd8x8du45r36ie]
GO
/****** Object:  ForeignKey [FK_hfh8ohm0b8w7dwl9n8prp3u0o]    Script Date: 10/22/2014 17:55:12 ******/
ALTER TABLE [dbo].[tbl_Recording_CorrespondenceContainer]  WITH CHECK ADD  CONSTRAINT [FK_hfh8ohm0b8w7dwl9n8prp3u0o] FOREIGN KEY([correspondenceId])
REFERENCES [dbo].[tbl_Recording_Correspondence] ([correspondenceId])
GO
ALTER TABLE [dbo].[tbl_Recording_CorrespondenceContainer] CHECK CONSTRAINT [FK_hfh8ohm0b8w7dwl9n8prp3u0o]
GO
/****** Object:  ForeignKey [FK_jbo2klcj95lpj6o16hdwogj51]    Script Date: 10/22/2014 17:55:12 ******/
ALTER TABLE [dbo].[tbl_Recording_DomicileOrOwner]  WITH CHECK ADD  CONSTRAINT [FK_jbo2klcj95lpj6o16hdwogj51] FOREIGN KEY([recordingId])
REFERENCES [dbo].[tbl_Recordings] ([recordingId])
GO
ALTER TABLE [dbo].[tbl_Recording_DomicileOrOwner] CHECK CONSTRAINT [FK_jbo2klcj95lpj6o16hdwogj51]
GO
/****** Object:  ForeignKey [FK_96kljnwq8wci2bsp5dihauaf5]    Script Date: 10/22/2014 17:55:12 ******/
ALTER TABLE [dbo].[tbl_Recording_LSV]  WITH CHECK ADD  CONSTRAINT [FK_96kljnwq8wci2bsp5dihauaf5] FOREIGN KEY([recordingId])
REFERENCES [dbo].[tbl_Recordings] ([recordingId])
GO
ALTER TABLE [dbo].[tbl_Recording_LSV] CHECK CONSTRAINT [FK_96kljnwq8wci2bsp5dihauaf5]
GO
/****** Object:  ForeignKey [FK_7jgn6rayc52sq4p59brfhtj64]    Script Date: 10/22/2014 17:55:12 ******/
ALTER TABLE [dbo].[tbl_Recording_Meta]  WITH CHECK ADD  CONSTRAINT [FK_7jgn6rayc52sq4p59brfhtj64] FOREIGN KEY([recordingId])
REFERENCES [dbo].[tbl_Recordings] ([recordingId])
GO
ALTER TABLE [dbo].[tbl_Recording_Meta] CHECK CONSTRAINT [FK_7jgn6rayc52sq4p59brfhtj64]
GO
/****** Object:  ForeignKey [FK_lrs57tbco8rr5n00yli3e8cow]    Script Date: 10/22/2014 17:55:12 ******/
ALTER TABLE [dbo].[tbl_Recordings]  WITH CHECK ADD  CONSTRAINT [FK_lrs57tbco8rr5n00yli3e8cow] FOREIGN KEY([affirmationId])
REFERENCES [dbo].[tbl_Recording_Affirmation] ([affirmationId])
GO
ALTER TABLE [dbo].[tbl_Recordings] CHECK CONSTRAINT [FK_lrs57tbco8rr5n00yli3e8cow]
GO
/****** Object:  ForeignKey [FK_mo3m9dg0x2nntj8cvevivndkq]    Script Date: 10/22/2014 17:55:12 ******/
ALTER TABLE [dbo].[tbl_Recordings]  WITH CHECK ADD  CONSTRAINT [FK_mo3m9dg0x2nntj8cvevivndkq] FOREIGN KEY([businessPartner_businessPartnerId])
REFERENCES [dbo].[tbl_Request_BusinessPartner] ([businessPartnerId])
GO
ALTER TABLE [dbo].[tbl_Recordings] CHECK CONSTRAINT [FK_mo3m9dg0x2nntj8cvevivndkq]
GO
/****** Object:  ForeignKey [FK_trp34afu4vmjcceea4cs3x9ax]    Script Date: 10/22/2014 17:55:12 ******/
ALTER TABLE [dbo].[tbl_Recordings]  WITH CHECK ADD  CONSTRAINT [FK_trp34afu4vmjcceea4cs3x9ax] FOREIGN KEY([correspondence_correspondenceId])
REFERENCES [dbo].[tbl_Recording_Correspondence] ([correspondenceId])
GO
ALTER TABLE [dbo].[tbl_Recordings] CHECK CONSTRAINT [FK_trp34afu4vmjcceea4cs3x9ax]
GO
/****** Object:  ForeignKey [FK_kqvauv9yntha2pamna0tv2bqy]    Script Date: 10/22/2014 17:55:12 ******/
ALTER TABLE [dbo].[tbl_Scanning]  WITH CHECK ADD  CONSTRAINT [FK_kqvauv9yntha2pamna0tv2bqy] FOREIGN KEY([scanShareId])
REFERENCES [dbo].[btbl_ScanShares] ([id])
GO
ALTER TABLE [dbo].[tbl_Scanning] CHECK CONSTRAINT [FK_kqvauv9yntha2pamna0tv2bqy]
GO
