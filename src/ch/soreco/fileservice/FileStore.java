package ch.soreco.fileservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


@Deprecated
public class FileStore {
	@Deprecated
	public static byte[] getBytesFromFile(File file) throws IOException {
		FileInputStream fileInputStream = new FileInputStream(file);
		byte[] data = new byte[(int) file.length()];
		fileInputStream.read(data);
		fileInputStream.close();
		return data;
	}
}
