package ch.soreco.orderbook.view.dao;

public class SqlNameAlias extends SqlExpression {
	private String alias;
	protected SqlNameAlias(String qualifier, String name, String alias) {
		super(new SqlName(qualifier, name));
		this.alias = alias;
	}
	/**
	 * Is the expression with an alias of an SQL Object to return, e.g. <code>o.orderId AS IvyId</code>.<br> 
	 * @return the name and alias of an SQL Object
	 */
	@Override
	protected String getExpression() {
		String fullQualifiedName = super.getExpression();
		if(this.alias!=null&&!this.alias.equals("")){
			fullQualifiedName += " AS "+alias;
		}
		return fullQualifiedName;
	}
}
