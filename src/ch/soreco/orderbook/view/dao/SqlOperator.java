package ch.soreco.orderbook.view.dao;

public enum SqlOperator {
	EQUAL("="),
	NOT_EQUAL("<>"), 
	GREATER_OR_EQUAL(">="),
	LESS_OR_EQUAL("<="),
	AND("AND"),
	OR("OR"),
	LIKE("LIKE")
	;
	private String operator;
	SqlOperator(String operator){
		this.operator = operator;
	}
	public String getOperator(){
		return operator;
	}
}
