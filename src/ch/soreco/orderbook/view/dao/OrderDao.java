package ch.soreco.orderbook.view.dao;

import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.process.data.persistence.IIvyQuery;
import ch.ivyteam.ivy.scripting.objects.Date;
import ch.soreco.orderbook.bo.Order;
import ch.soreco.orderbook.dao.AddressmutationManagedDao;
import ch.soreco.orderbook.enums.OrderState;
import ch.soreco.orderbook.functional.SearchParameters;
/**
 * Use this Dao to find Orders by {@link SearchParameters}
 */
public class OrderDao extends AddressmutationManagedDao {
	private SqlDateFormatter sqlDateFormat = new SqlDateFormatter();//The Date format for MS SQL Server
	public OrderDao(){
	}
	/**
	 * Find Orders queried by {@link SearchParameters}.
	 * A native SQL query is built by following conditions:
	 * <ul>
	 * <li>If a string attribute is null or is an empty string the attribute is ignored.</li>
	 * <li>All Strings are escaped by {@link StringEscapeUtils#escapeSql}</li>
	 * <li>{@link SearchParameters#getSelectTop()} is used to select given top rows.<br>If 0 the result will contain also 0 rows.</li>
	 * <li>The user argument is used to determine which orderTypes the user is allowed to see.<br>Check the Table v_Authored_OrderTypesPerUser.</li>
	 * <li>{@link SearchParameters#getExcludedOrderStates()} are only excluded if {@link SearchParameters#getOrderState()} parameter is not specified.</li>
	 * <li>If {@link SearchParameters#getCheckerUserId}, {@link SearchParameters#getDispatcherUserId} or {@link SearchParameters#getEditorUserId} equals to a hyphen (-) the resulting query clause will contain a "null or empty" clause.<br> e.g. <code>checkerUserId IS NULL OR checkerUserId=''</code></li>
	 * </ul>
	 * Tables which may be queried:
	 * <table border="1">
	 * <tr><td>tbl_Orders</td><td>the orders from which the List<Order> is built.</td></tr>
	 * <tr><td>v_Authored_OrderTypesPerUser</td><td>orderTypes the user is allowed to see:<br>
	 * This view depends on three tables:
	 * <ul>
	 * <li>btbl_AuthorityGroupUsers: derived by Trigger tr_btbl_AuthorityGroup_Normalisation</li>
	 * <li>tbl_OrderBookRelations: derived by Trigger tr_tbl_OrderBooks_Normalisation</li>
	 * <li>tbl_OrderBookTypes: derived by Trigger tr_tbl_OrderBooks_Normalisation</li>
	 * </ul>
	 * These tables are filled by two triggers which normalise data of btbl_AuthorityGroup (users & rights) and tbl_OrderBooks (rights and orderTypes)
	 * @see <a href="/deployment/SQL/ivy5/49/Interactive SQL Compare Report_from_v46_to_v49.html">Sql Compare Report for delivery of Version 49</a>
	 * </td></tr>
	 * <tr><td>tbl_Recording_Meta</td><td>if {@link SearchParameters#getMetaTypeComment} is set</td></tr>
	 * <tr><td>getOrderIdsWithOrderNo(orderNo)</td><td>This is a Table Valued Function which returns orders containing given orderNo</td></tr>
	 * </table>
	 * @param user required, the user for which the query is executed
	 * @param parameters the parameters which need to be applied to query
	 * @return OrderDaoResult containing found orders and a count of results which the query clauses meet.
	 */
	public OrderDaoResult findBySearchParameters(String user, SearchParameters parameters) {
		SqlQueryBuilder queryBuilder = getSelectQuery(user, parameters);
		String sql = queryBuilder.getQuery();
		IIvyQuery query = this.getEntityManager().createNativeQuery(sql, Order.class);
		Ivy.log().info("--Execute findBySearchParameters query:\n"+sql);
		List<Order> orders = (List<Order>) query.getResultList();
		OrderDaoResult result = new OrderDaoResult();
		result.setOrders(orders);
		if(orders.size()<parameters.getSelectTop()){
			result.setCountOfQueryResults(orders.size());
		} else {
			result.setCountOfQueryResults(getQueryResultsCount(queryBuilder));
		}
		return result;
	}
	/**
	 * Get the Count of Results the query would have, if select top would not be set.<br>
	 * To get the count the select fields and order by fields are cleared from sql. 
	 * Then a count(*) field is added and the resulting query is executed - which returns the count of results the query would have. 
	 * @param queryBuilder the Query Builder
	 * @return count of results of the query
	 */
	private Integer getQueryResultsCount(SqlQueryBuilder queryBuilder){
		queryBuilder = queryBuilder.clearSelect().clearOrderBy();
		String sql = queryBuilder.select(new SqlNameAlias("", "Count(*)", "countOfQueryResults")).getQuery();			
		Ivy.log().info("--Execute findBySearchParameters#getQueryResultsCount query:\n"+sql);
		IIvyQuery resultCountQuery = this.getEntityManager().createNativeQuery(sql);
		return (Integer) resultCountQuery.getSingleResult();
	}
	/**
	 * Builds the Query using a {@link SqlQueryBuilder} to 
	 * @param user the users for which the query is executed for
	 * @param parameters the parameters to build the query clauses from
	 * @return a query builder, containing the fields, tables and clauses to execute, never null
	 */
	private SqlQueryBuilder getSelectQuery(String user, SearchParameters parameters) {
		SqlQueryBuilder queryBuilder = new SqlQueryBuilder();
		
		//instantiate v_Authored_OrderTypesPerUser join by user and orderType
		SqlNameAlias authTable = new SqlNameAlias("dbo", "v_Authored_OrderTypesPerUser", "auth");
		SqlClause authUserSign = new SqlClause(new SqlName("auth", "userSign"), SqlOperator.EQUAL, user);
		SqlClause authOrderType = new SqlClause(new SqlName("auth", "orderType"), SqlOperator.EQUAL, new SqlName("o", "orderType"));

		queryBuilder
			.top(parameters.getSelectTop())
			.select(new SqlName("o", "*"))
			.from(new SqlNameAlias("dbo", "tbl_Orders", "o"))
			.innerJoin(authTable, SqlOperator.AND,authUserSign, authOrderType)
			.where(new SqlClause(new SqlName("o", "orderState"), SqlOperator.NOT_EQUAL, ""))
			.orderBy(new SqlName("CASE o.orderQuantityType WHEN 'A' THEN 0 ELSE 3 END"),true)
			.orderBy(new SqlName("o", "orderId"), false)
		;
		if(parameters.getBpId()!=null&&!parameters.getBpId().equals("")){
			queryBuilder.where(new SqlClause(new SqlName("o", "BPId"), SqlOperator.EQUAL, parameters.getBpId()));
		}
		if(parameters.getCheckerUserId()!=null&&!parameters.getCheckerUserId().equals("")){
			if(parameters.getCheckerUserId().equals("-")){
				queryBuilder.whereStringFieldIsEmpty(new SqlName("o", "checkerUserId"));
			} else {
				queryBuilder.where(new SqlClause(new SqlName("o", "checkerUserId"), SqlOperator.EQUAL, parameters.getCheckerUserId()));				
			}
		}
		if(parameters.getClanRefNo()!=null&&!parameters.getClanRefNo().equals("")){
			queryBuilder.where(new SqlClause(new SqlName("o", "FAClanId"), SqlOperator.EQUAL, parameters.getClanRefNo()));
		}
		if(parameters.getDispatcherUserId()!=null&&!parameters.getDispatcherUserId().equals("")){
			if(parameters.getDispatcherUserId().equals("-")){
				queryBuilder.whereStringFieldIsEmpty(new SqlName("o", "dispatcherUserId"));
			} else {
				queryBuilder.where(new SqlClause(new SqlName("o", "dispatcherUserId"), SqlOperator.EQUAL, parameters.getDispatcherUserId()));				
			}
		}
		if(parameters.getEditorUserId()!=null&&!parameters.getEditorUserId().equals("")){
			if(parameters.getEditorUserId().equals("-")){
				queryBuilder.whereStringFieldIsEmpty(new SqlName("o", "editorUserId"));
			} else {
				queryBuilder.where(new SqlClause(new SqlName("o", "editorUserId"), SqlOperator.EQUAL, parameters.getEditorUserId()));				
			}
		}
		if (parameters.getMetaTypeComment() != null
				&& !parameters.getMetaTypeComment().equals("")) {
			queryBuilder
				.innerJoin(new SqlNameAlias("dbo", "tbl_Recording_Meta", "c"), SqlOperator.AND, new SqlClause(new SqlName("o", "recordingId"), SqlOperator.EQUAL, new SqlName("c", "recordingId")))
				.where(new SqlClause(new SqlName("c", "recordingMetaComment"), SqlOperator.LIKE, parameters.getMetaTypeComment()));
		}
		if(parameters.getOrderId()!=null&&!parameters.getOrderId().equals(0)){
			queryBuilder.where(new SqlClause(new SqlName("o", "orderId"), SqlOperator.EQUAL, parameters.getOrderId()));
		}
		if(parameters.getOrderNo()!=null&&!parameters.getOrderNo().equals("")){
			queryBuilder
			.innerJoin(new SqlNameAlias("dbo", "getOrderIdsWithOrderNo('"+StringEscapeUtils.escapeSql(parameters.getOrderNo())+"')", "orderNoOrders"), SqlOperator.AND, new SqlClause(new SqlName("o", "orderId"), SqlOperator.EQUAL, new SqlName("orderNoOrders", "orderId")));
		}
		if(parameters.getOrderQuantityType()!=null&&!parameters.getOrderQuantityType().equals("")){
			queryBuilder.where(new SqlClause(new SqlName("o", "orderQuantityType"), SqlOperator.EQUAL, parameters.getOrderQuantityType()));
		}
		
		if(parameters.getOrderState()!=null&&!parameters.getOrderState().equals("")){
			queryBuilder.where(new SqlClause(new SqlName("o", "orderState"), SqlOperator.EQUAL, parameters.getOrderState()));
		}

		//If IncludeArchive is set to true
		//we need to make sure, that Aborted & Archived States are not excluded
		if(parameters.getIncludeArchive()!=null
				&&parameters.getIncludeArchive()
				&&parameters.getExcludedOrderStates()!=null){
			if(parameters.getExcludedOrderStates().contains(OrderState.Aborted)){
				parameters.getExcludedOrderStates().remove(OrderState.Aborted);
			}
			if(parameters.getExcludedOrderStates().contains(OrderState.Archived)){
				parameters.getExcludedOrderStates().remove(OrderState.Archived);
			}
		}
		//If IncludeArchive is set to false
		//we need to exclude orders which where finished earlier than two work days before
		//if they aren't in state Merged or Archived
		if(parameters.getIncludeArchive()==null
				||!parameters.getIncludeArchive()){
			String dateBeforeTwoWorkDays = sqlDateFormat.getConvertToBeginOfDayExpression(getStartOfPreviousWorkDay(2));
			String orderEndClauseBeforeTwoWorkDays = "(DATEDIFF(dd,o.orderEnd, "+dateBeforeTwoWorkDays+")<3 OR (o.orderState<>'"+OrderState.Merged.name()+"' AND o.orderState<>'"+OrderState.Aborted.name()+"'))";
			queryBuilder.where(new SqlExpression(orderEndClauseBeforeTwoWorkDays));
		}
		// Order States are only excluded if no OrderState parameter is selected
		if ((parameters.getOrderState() == null||parameters.getOrderState().equals(""))
				&& parameters.getExcludedOrderStates() != null
				&& parameters.getExcludedOrderStates().size() > 0) {
			for (OrderState state : parameters.getExcludedOrderStates()) {
				queryBuilder.where(new SqlClause(new SqlName("o", "orderState"), SqlOperator.NOT_EQUAL, state.name()));
			}
		}
		if(parameters.getOrderType()!=null&&!parameters.getOrderType().equals("")){
			queryBuilder.where(new SqlClause(new SqlName("o", "orderType"), SqlOperator.EQUAL, parameters.getOrderType()));
		}
		if(parameters.getFdl()!=null&&!parameters.getFdl().equals("")){
			queryBuilder.where(new SqlClause(new SqlName("o", "fdl"), SqlOperator.EQUAL, parameters.getFdl()));
		}
		if(parameters.getPriority()!=null&&!parameters.getPriority().equals(0)){
			queryBuilder.where(new SqlClause(new SqlName("o", "priority"), SqlOperator.EQUAL, parameters.getPriority()));
		}
		if(parameters.getLastEditFrom()!=null&&!parameters.getLastEditFrom().equals(Date.UNINITIALIZED_DATE)){
			String from = sqlDateFormat.getConvertToBeginOfDayExpression(parameters.getLastEditFrom().toJavaDate());
			//queryBuilder.where(new SqlClause(new SqlName("o", "lastEdit"), SqlOperator.GREATER_OR_EQUAL, from));
			queryBuilder.where(new SqlExpression("o.lastEdit>="+from));
		}
		if(parameters.getLastEditUntil()!=null&&!parameters.getLastEditUntil().equals(Date.UNINITIALIZED_DATE)){
			String until = sqlDateFormat.getConvertToEndOfDayExpression(parameters.getLastEditUntil().toJavaDate());
			//queryBuilder.where(new SqlClause(new SqlName("o", "lastEdit"), SqlOperator.LESS_OR_EQUAL, until));
			queryBuilder.where(new SqlExpression("o.lastEdit<="+until));
		}
		if(parameters.getOrderStartFrom()!=null&&!parameters.getOrderStartFrom().equals(Date.UNINITIALIZED_DATE)){
			String from = sqlDateFormat.getConvertToBeginOfDayExpression(parameters.getOrderStartFrom().toJavaDate());
			//queryBuilder.where(new SqlClause(new SqlName("o", "orderStart"), SqlOperator.GREATER_OR_EQUAL, from));
			queryBuilder.where(new SqlExpression("o.orderStart>="+from));
		}
		if(parameters.getOrderStartUntil()!=null&&!parameters.getOrderStartUntil().equals(Date.UNINITIALIZED_DATE)){
			String until = sqlDateFormat.getConvertToEndOfDayExpression(parameters.getOrderStartUntil().toJavaDate());
			//queryBuilder.where(new SqlClause(new SqlName("o", "orderStart"), SqlOperator.LESS_OR_EQUAL, until));
			queryBuilder.where(new SqlExpression("o.orderStart<="+until));
		}
		return queryBuilder;
	}
	private java.util.Date getStartOfPreviousWorkDay(int offset) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);

		cal.add(Calendar.DAY_OF_WEEK, (offset * -1));
		// step back 2 days
		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			cal.add(Calendar.DAY_OF_WEEK, -2);
		}
		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
			cal.add(Calendar.DAY_OF_WEEK, -1);
		}

		return cal.getTime();
	}
	public class OrderDaoResult {
		private List<Order> orders;
		private int countOfQueryResults;
		/**
		 * The list of Orders which meet the given {@link SearchParameters}.
		 * @return List<Order> orders
		 */
		public List<Order> getOrders(){
			return orders;
		}
		private void setOrders(List<Order> orders){
			this.orders = orders;
		}
		/**
		 * The Count of Results which meet the given {@link SearchParameters}.
		 * This may does not meet the size of the Orders List, as the {@link SearchParameters#getSelectTop()} limits the size of resulting list of Orders.
		 * @return count of Query Results
		 */
		public int getCountOfQueryResults(){
			return this.countOfQueryResults;
		}
		private void setCountOfQueryResults(int countOfQueryResults){
			this.countOfQueryResults = countOfQueryResults;
		}
	}
}
