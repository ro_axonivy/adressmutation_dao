package ch.soreco.orderbook.view.dao;

public class SqlExpression {
	private String expression;
	protected SqlExpression(String expression){
		this.expression = expression;
	}
	protected SqlExpression(SqlExpression expression){
		this.expression = expression.getExpression();
	}
	protected String getExpression(){
		return this.expression;
	};
}
