package ch.soreco.orderbook.view.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * Provides a thread save Simple Date Format (yyyy-MM-dd) and some convenience methods.
 */
public class SqlDateFormatter {
	private static final ThreadLocal<SimpleDateFormat> sdf = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd");
		}
	};
	/**
	 * Formats a Date into a yyyy-MM-dd formatted string.
	 * @param date the date value to be formatted.
	 * @return the formatted date string.
	 * @see SimpleDateFormat#format(Date)
	 */
	public String format(Date date){
		return sdf.get().format(date);
	}
	/**
	 * Formats a Date into a yyyy-MM-dd formatted string and wraps it with MS SQL Convert function 
	 * <pre>convert(datetime, '[date] 00:00:00.000',121)</pre> to be used within a native SQL.
	 * @param date the date value to be formatted.
	 * @return the convert datetime expression.
	 */	
	public String getConvertToBeginOfDayExpression(Date date){
		return "convert(datetime, '"+format(date)+" 00:00:00.000',  121)";
	}
	/**
	 * Formats a Date into a yyyy-MM-dd formatted string and wraps it with MS SQL Convert function.
	 * <pre>convert(datetime, '[date] 23:59:59.999',121)</pre> to be used within a native SQL.
	 * @param date the date value to be formatted.
	 * @return the convert datetime expression.
	 */	
	public String getConvertToEndOfDayExpression(Date date){
		return "convert(datetime, '"+format(date)+" 23:59:59.999',  121)";
	}

}
