package ch.soreco.orderbook.view.dao;

import org.apache.commons.lang.StringEscapeUtils;
/**
 * SqlClause represents an SQL Clause which has the structure [SQLObject][operator][SQLObject]
 * while SQL Objects can be an expression a column etc. 
 */
public class SqlClause {
	private SqlName left;
	private String operator;
	private SqlName right;
	protected SqlClause(SqlName leftName, SqlOperator operator, SqlName rightName){
		this.left = leftName;
		this.operator = operator.getOperator();
		this.right = rightName;
	}
	protected SqlClause(SqlName leftName, SqlOperator operator, String value){
		this.left = leftName;
		this.operator = operator.getOperator();
		this.right = new SqlName("'"+StringEscapeUtils.escapeSql(value)+"'");
	}
	protected SqlClause(SqlName leftName, SqlOperator operator, Integer value){
		this.left = leftName;
		this.operator = operator.getOperator();
		this.right = new SqlName(String.valueOf(value));
	}
	protected String getExpression() {
		StringBuilder operation = new StringBuilder("");
		operation.append(left.getExpression());
		operation.append(" "+this.operator+" ");
		operation.append(right.getExpression());
		return operation.toString();
	}
}
