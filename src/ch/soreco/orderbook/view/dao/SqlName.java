package ch.soreco.orderbook.view.dao;
/**
 * SqlName is used within {@link SqlQueryBuilder}.
 * It represents an Sql Object Expression which is used within SQL Native Queries<br>
 */
public class SqlName extends SqlExpression {
	private String qualifier;
	/**
	 * Instantiate a simple name, e.g. <code>orderId</code> or <code>CASE WHEN orderId IS NULL THEN 0 ELSE orderId END</code>.
	 * Qualify the Name with a qualifier by {@link #SqlName(qualifier, name)} and use {@link #getQualifiedName()} 
	 * to prevent an ambiguous reference, such as occurs when two tables in the FROM clause have columns with duplicate names.
	 * @param name the SQL Object name or expression
	 */
	protected SqlName(String name){
		super(name);
		this.qualifier = null;
	}
	/**
	 * Instantiate a qualified name, e.g. <code>o.orderId</code>. Where o is the alias of the table which contains the column orderId.
	 * Use {@link #getQualifiedName()} to prevent an ambiguous reference, such as occurs when two tables in the FROM clause have columns with duplicate names.
	 * @param name the SQL Object name or expression
	 */
	protected SqlName(String qualifier, String name){
		super(name);
		this.qualifier = qualifier;
	}
	/**
	 * Is the qualified name of an SQL Object to return, e.g. <code>o.orderId</code>.<br> 
	 * Qualify the Name with a qualifier by {@link #SqlName(qualifier, name)} to prevent an ambiguous reference, 
	 * such as occurs when two tables in the FROM clause have columns with duplicate names. 
	 * @return the name of an SQL Object
	 */
	@Override
	protected String getExpression() {
		String qualifiedName = "";
		if(qualifier!=null&&!qualifier.equals("")){
			qualifiedName = qualifier+".";
		}
		qualifiedName +=super.getExpression();
		return qualifiedName;
	}
}
