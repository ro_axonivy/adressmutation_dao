package ch.soreco.orderbook.view.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
/**
 * Use the {@link SqlQueryBuilder} to build a native SQL Query
 * All methods of this class return the {@link SqlQueryBuilder} instance, to allow to chain up the query methods.
 * Example:<br><br>
 * <code>
 * {@link SqlQueryBuilder} query = new {@link SqlQueryBuilder}();<br>
 * query.top(200)<br>
 *			.select(new SqlName("o", "*"))<br>
 *			.from(new SqlName("dbo", "tbl_Orders", "o"))<br>
 *System.out.print(query.toString());
 * </code></br>
 * <br>
 * The {@link SqlQueryBuilder#getQuery()} method would return:<br>
 * <code>SELECT TOP 200 o.* FROM dbo.tbl_Orders o</code><br>
 * See {@link SqlQueryBuilderTest} for further examples.
 */
public class SqlQueryBuilder {
	private int top=0;
	private List<SqlExpression> select= new ArrayList<SqlExpression>();
	private SqlNameAlias from;
	private List<String> joins = new ArrayList<String>();
	private List<String> wheres = new ArrayList<String>();
	private List<String> orderBys = new ArrayList<String>();
	protected SqlQueryBuilder(){}
	protected SqlQueryBuilder top(int top){
		this.top = top;
		return this;
	}
	protected SqlQueryBuilder select(SqlExpression field){
		this.select.add(field);
		return this;
	}
	protected SqlQueryBuilder clearSelect(){
		this.select.clear();
		return this;
	}
	protected SqlQueryBuilder from(SqlNameAlias fromTable){
		from = fromTable;
		return this;
	}
	protected SqlQueryBuilder innerJoin(SqlNameAlias joinTable, SqlOperator joinOperator, SqlClause... joinClauses){
		StringBuilder join = new StringBuilder("");
		join.append("INNER JOIN ");
		join.append(joinTable.getExpression());
		join.append(" ON ");
		join.append(joinClauses[0].getExpression());
		
		for(int i=1;i<joinClauses.length;i++){
			join.append(" "+joinOperator.getOperator()+" ");
			join.append(joinClauses[i].getExpression());
		}
		joins.add(join.toString());
		return this;
	}
	protected SqlQueryBuilder where(SqlClause clause){
		wheres.add(clause.getExpression());
		return this;
	}
	protected SqlQueryBuilder where(SqlExpression expression){
		if(expression!=null&&!expression.getExpression().equals("")){
			wheres.add(expression.getExpression());
		}
		return this;
	}
	protected SqlQueryBuilder whereStringFieldIsEmpty(SqlName field){
		StringBuilder where = new StringBuilder("");
		where.append("(");
		where.append(field.getExpression());
		where.append(" IS NULL OR ");
		where.append(field.getExpression());
		where.append(" ='' ");
		where.append(")");
		wheres.add(where.toString());
		return this;
	}
	protected SqlQueryBuilder orderBy(SqlName field, boolean ascending){
		String sortDirection = ascending? "ASC" : "DESC";
		orderBys.add(field.getExpression()+" "+sortDirection);
		return this;
	}
	protected SqlQueryBuilder clearOrderBy(){
		this.orderBys.clear();
		return this;
	}

	protected String getQuery(){
		StringBuilder sql = new StringBuilder("");
		sql.append("SELECT ");
		if(top>0){
			sql.append("TOP "+top+" ");
		}
		//Loop fields an add the full qualified name to the sql
		if(select.size()>0){
			sql.append("\n\t");
			sql.append(select.get(0).getExpression());
			for(int i=1;i<select.size();i++){
				sql.append("\n\t, ");
				sql.append(select.get(i).getExpression());				
			}
		} else {
			sql.append("*");
		}
		if(this.from!=null){
			sql.append("\nFROM ");
			sql.append("\n\t");
			sql.append(this.from.getExpression());			
		}
		if(this.joins.size()>0){
			sql.append("\n\t");
			sql.append(StringUtils.join(this.joins, "\n\t"));			
		}
		if(this.wheres.size()>0){
			sql.append("\nWHERE ");
			sql.append("\n\t");
			sql.append(StringUtils.join(this.wheres, "\n\tAND "));			
		}
		if(this.orderBys.size()>0){
			sql.append("\nORDER BY ");
			sql.append("\n\t");
			sql.append(StringUtils.join(this.orderBys, "\n\t, "));			
		}
		
		return sql.toString();		
	}
	public String toString(){
		return getQuery();
	}
}
