package ch.soreco.orderbook.dao;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.process.data.persistence.IIvyEntityManager;
/**
 * Provides the Ivy Entity Manager for persistence unit "Adressmutation"
 */
public abstract class AddressmutationManagedDao {
	public static final String PERSISTENCE_UNIT="Adressmutation";
	public static final String DB_CONFIG_NAME="bzp_ivy_daten";
	private IIvyEntityManager entityManager=null;
	/**
	 * Gets an {@link IIvyEntityManager} of Persistence Unit {@link #PERSISTENCE_UNIT} for managing entity classes of the persistence unit. 
	 * @return IIvyEntityManager of {@link #PERSISTENCE_UNIT}
	 */
	protected IIvyEntityManager getEntityManager(){
		if(entityManager==null){
			entityManager =Ivy.persistence().get(PERSISTENCE_UNIT);
		}
		return entityManager;
	}
}