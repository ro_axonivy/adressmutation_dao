package ch.soreco.orderbook.mail.dao;

import ch.ivyteam.ivy.process.data.persistence.IIvyQuery;
import ch.soreco.orderbook.bo.OrderMatching;
import ch.soreco.orderbook.dao.AddressmutationManagedDao;

public class OrderMatchingDao extends AddressmutationManagedDao {
	/**
	 * Finds the first OrderMatching by given docType
	 * @param docType: the docType to find, e.g. "IL"
	 * @return first item matching the docType, may be null
	 */
	public OrderMatching findFirstByDocType(String docType){
		IIvyQuery query = this.getEntityManager().createQuery("select e from "+OrderMatching.class.getSimpleName()+" e where docType=:docType");
		query.setParameter("docType", docType);
		return (OrderMatching) query.getSingleResult();
	}
}
