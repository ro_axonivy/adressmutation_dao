package ch.soreco.orderbook.mail.dao;

import java.util.List;

import ch.ivyteam.ivy.process.data.persistence.IIvyQuery;
import ch.soreco.orderbook.bo.Scanning;
import ch.soreco.orderbook.dao.AddressmutationManagedDao;

public class ScanningDao extends AddressmutationManagedDao {
	public Scanning merge(Scanning scanning){
		return this.getEntityManager().merge(scanning);
	}
	/**
	 * Delete scannings with given order id, including may applied attachments.
	 * @param orderId the order containing the scanning
	 */
	public void deleteByOrderId(Integer orderId){
		List<Scanning> scannings = findByOrderId(orderId);
		if(scannings!=null){
			for(Scanning scanning:scannings){
				delete(scanning);
			}			
		}
	}
	private List<Scanning> findByOrderId(Integer orderId){
		IIvyQuery query = this.getEntityManager().createQuery("select s from Scanning s where s.orderId = :orderId");
		query.setParameter("orderId", orderId);
		return (List<Scanning>) query.getResultList();
	}
	private void delete(Scanning scanning){
		AttachmentDao attachmentDao = new AttachmentDao();
		attachmentDao.removeByScanningId(scanning.getScanningId());
		FileDao fileDao = new FileDao();
		fileDao.removeById(scanning.getScanFileId());
		fileDao.removeById(scanning.getScanMonoFileId());
		
		this.getEntityManager().remove(scanning);
	}
}
