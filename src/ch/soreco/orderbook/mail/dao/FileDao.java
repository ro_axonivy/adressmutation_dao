package ch.soreco.orderbook.mail.dao;

import java.sql.SQLException;

import ch.ivyteam.ivy.persistence.PersistencyException;
import ch.soreco.common.bo.File;
import ch.soreco.orderbook.dao.AddressmutationManagedDao;
import ch.soreco.webbies.common.db.BlobFile;
import ch.soreco.webbies.common.db.BlobHelper;

public class FileDao extends AddressmutationManagedDao {
	private BlobHelper blobHelper;
	public FileDao(){
		super();
		blobHelper = new BlobHelper(AddressmutationManagedDao.DB_CONFIG_NAME);
	}
	public File saveFromBlobFile(BlobFile blob) throws PersistencyException, SQLException, Exception{
		if(blob.getBytes()==null){
			throw new UnsupportedOperationException("Save of a BlobFile without content is not supported!");
		}
		return blobHelper.fileUpload(blob, AddressmutationManagedDao.PERSISTENCE_UNIT);
	}
	public void remove(File file){
		if(file!=null){
			this.getEntityManager().remove(file);
		}
	}
	public void removeById(Integer fileId){
		File file = findById(fileId);
		remove(file);			
	}
	private File findById(Integer fileId){
		File file = null;
		if(fileId!=null){
			file =  this.getEntityManager().find(File.class, fileId);			
		}
		return file;
	}
}
