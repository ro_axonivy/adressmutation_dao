package ch.soreco.orderbook.mail.dao;

import ch.soreco.orderbook.bo.Order;
import ch.soreco.orderbook.dao.AddressmutationManagedDao;
/**
 * Save, remove or find an Order with this Class
 */
public class OrderDao extends AddressmutationManagedDao {
	/**
	 * Merge the state of the given {@link Order} into the database 
	 * @param order the order to save
	 * @return the saved order
	 */
	public Order merge(Order order){
		return this.getEntityManager().merge(order);
	}
	/**
	 * Remove the given Order instance from the database 
	 * including may existing {@link Scanning} applied to the Order.
	 * @param order the Order to remove
	 */
	public void remove(Order order){
		ScanningDao scanningDao = new ScanningDao();
		scanningDao.deleteByOrderId(order.getOrderId());
		this.getEntityManager().remove(order);
	}
	/**
	 * Find an entity by the given primary key on the database 
	 * @param orderId the primary key of Order to find
	 * @return the order
	 */
	public Order findById(Integer orderId){
		return this.getEntityManager().find(Order.class, orderId);
	}
}
