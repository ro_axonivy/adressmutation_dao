package ch.soreco.orderbook.mail.dao;

import java.util.List;

import ch.ivyteam.ivy.process.data.persistence.IIvyQuery;
import ch.soreco.orderbook.bo.Attachment;
import ch.soreco.orderbook.dao.AddressmutationManagedDao;

public class AttachmentDao extends AddressmutationManagedDao {
	/**
	 * Merge the state of the given {@link Attachment} into the database
	 * 
	 * @param attachment
	 *            the attachment to save
	 * @return the saved attachment
	 */
	public Attachment merge(Attachment attachment) {
		return this.getEntityManager().merge(attachment);
	}

	public void removeByScanningId(Integer scanningId) {
		List<Attachment> attachments = findByScanningId(scanningId);
		for (Attachment attachment : attachments) {
			remove(attachment);
		}
	}

	@SuppressWarnings("unchecked")
	public List<Attachment> findByScanningId(Integer scanningId) {
		IIvyQuery query = this.getEntityManager().createQuery(
				"select e from Attachment e where e.scanningId = :scanningId");
		query.setParameter("scanningId", scanningId);
		return (List<Attachment>) query.getResultList();
	}

	private void remove(Attachment attachment) {
		Integer fileId = null;
		if (attachment.getFile() != null) {
			fileId = attachment.getFile().getFileId();
			// detach file before removing attachment
			// as attachment files are mapped bi-directional
			attachment.setFile(null);
		}
		this.getEntityManager().remove(attachment);
		if (fileId != null) {
			// now remove attachment file
			FileDao fileDao = new FileDao();
			fileDao.removeById(fileId);
		}

	}
}
