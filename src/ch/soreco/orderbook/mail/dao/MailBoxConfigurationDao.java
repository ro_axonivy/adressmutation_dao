package ch.soreco.orderbook.mail.dao;

import java.util.List;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.scripting.objects.DateTime;
import ch.ivyteam.ivy.scripting.objects.Duration;
import ch.soreco.orderbook.bo.MailBoxConfiguration;
import ch.soreco.orderbook.dao.AddressmutationManagedDao;

public class MailBoxConfigurationDao extends AddressmutationManagedDao {
	/**
	 * Merge the state of the given {@link MailBoxConfiguration} into the
	 * database
	 * 
	 * @param mailBoxConfiguration
	 *            the MailBoxConfiguration to save
	 * @return the saved configuration
	 */
	public MailBoxConfiguration merge(MailBoxConfiguration mailBoxConfiguration) {
		return this.getEntityManager().merge(mailBoxConfiguration);
	}

	/**
	 * Refreshes the state of the given {@link MailBoxConfiguration} from the
	 * database
	 * 
	 * @param mailBoxConfiguration
	 *            the MailBoxConfiguration to refresh
	 * @return the refreshed configuration
	 */
	public MailBoxConfiguration refresh(MailBoxConfiguration mailBoxConfiguration) {
		return this.getEntityManager().refresh(mailBoxConfiguration);
	}

	/**
	 * Set all {@link MailBoxConfiguration} to the given runnable state. This to
	 * prevent the system to send error mails for the same reason. When set as
	 * runnable the next scan time will be set according to the scan interval
	 * literal from now. This to give some time to configure properly.
	 */
	public void setAllToRunnable(boolean runnable) {
		List<MailBoxConfiguration> configs = this.getEntityManager().findAll(MailBoxConfiguration.class);
		if (runnable) {
			for (MailBoxConfiguration config : configs) {
				config.setIsRunnable(true);
				config = saveWithNextScanAt(config);
			}
		} else {
			for (MailBoxConfiguration config : configs) {
				config.setIsRunnable(false);
				config = merge(config);
			}
		}
	}

	private MailBoxConfiguration saveWithNextScanAt(MailBoxConfiguration config) {
		if (config.getScanIntervalLiteral() != null && !config.getScanIntervalLiteral().isEmpty()) {
			String literal = config.getScanIntervalLiteral();
			DateTime nextScanAt = new DateTime();
			try {
				nextScanAt = fromNowIn(literal);
				config.setNextScanAt(nextScanAt);
				Ivy.log().debug("MailBoxConfiguration set next scan at:" + nextScanAt);
			} catch (Exception e) {
				config.setScanIntervalLiteral(null);
				config.setNextScanAt(null);
				Ivy.log().error("Error during calculation of next MailBoxConfiguration scan time.", e);
			}
		} else {
			config.setScanIntervalLiteral(null);
			config.setNextScanAt(null);
		}
		return merge(config);
	}

	/**
	 * Adds a duration to given date time.
	 * 
	 * @param durationLiteral
	 *            A duration literal has the following format:
	 *            P[n]Y[n]M[n]DT[n]H[n]M[n]S. Elements may be omitted if their
	 *            value is zero.<br>
	 *            You enter a duration in the ISO 8601 time period format such
	 *            as: <code>12h20m</code> or <code>12h20m30s</code>. An example
	 *            for the full format is: <code>P3Y6M4DT12h30m10s</code>
	 *            <ul>
	 *            <li>P stands for Period</li>
	 *            <li>Y for setting the years.</li>
	 *            <li>M for setting the months.</li>
	 *            <li>D for setting the days.</li>
	 *            <li>T is the separator between the date fields and the time
	 *            fields.</li>
	 *            <li>H or h for setting the hours.</li>
	 *            <li>M or m for setting the minutes.</li>
	 *            <li>S or s for setting the seconds.</li>
	 *            </ul>
	 *            * @return A new date object translated by the duration.
	 */
	private static DateTime fromNowIn(String durationLiteral) {
		DateTime newDate = null;
		if (durationLiteral != null && !durationLiteral.trim().isEmpty()) {
			Duration duration = new Duration(durationLiteral);
			newDate = new DateTime().add(duration);
		}
		return newDate;
	}
}
