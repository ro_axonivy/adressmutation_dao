package ch.soreco.orderbook.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.Callable;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.process.data.persistence.IIvyEntityManager;
import ch.ivyteam.ivy.scripting.objects.List;
import ch.ivyteam.ivy.security.IRole;
import ch.ivyteam.ivy.security.IUser;
import ch.soreco.common.bo.AuthorityGroup;
import ch.soreco.orderbook.bo.OrderBook;
import ch.soreco.orderbook.enums.GroupRole;
import ch.soreco.orderbook.enums.OrderType;

public class Authority {
	private static final GroupRole[] ROLES_OF_TEAMLEADERS = new GroupRole[] { GroupRole.TeamDisp, GroupRole.Dispatch, GroupRole.Write };

	// Singleton instance
	private static Authority instance = null;

	// Data fields
	private IIvyEntityManager persistenceUnit;
	private ArrayList<OrderBook> books;
	private ArrayList<AuthorityGroup> groups;

	// private Map<AuthorityGroup, ArrayList> groups
	// private IIvyEntityManager;

	private Authority() {
		persistenceUnit = Ivy.getInstance().persistence.get("Adressmutation");
		getOrderBooks();
		if(Ivy.log().isDebugEnabled()&&books!=null){
			Ivy.log().debug("Got {0} Order Books", books.size());			
		}
		getAuthorityGroups();
		if(Ivy.log().isDebugEnabled()&&groups!=null){
			Ivy.log().debug("Got {0} Authority Groups", groups.size());			
		}
	}

	/**
	 * Returns the Authority instance
	 * 
	 * @return instance of Authority
	 */
	public static Authority getInstance() {
		if (instance == null) {
			instance = new Authority();
		}
		return instance;
	}

	/**
	 * Generates a new (Singleton) Instance
	 */
	public static void buildInstance() {
		instance = new Authority();
	}

	/**
	 * Returns all Orderbooks
	 * 
	 * @return Orderbooks
	 */
	public ArrayList<OrderBook> getBooks() {
		return this.books;
	}

	/**
	 * Finds a team leader.
	 * 
	 * @param teams
	 *            the comma separated list of {@link OrderBook#getShortname()}
	 *            to find the user owning the role TeamLeader and having
	 *            GroupRole TeamDisp, Dispatch or Write.
	 * @param orderType
	 *            the orderType which may is used to find the appropriate team
	 *            when no teams are set.
	 * @param excludedUserName
	 *            the userName to exclude.
	 * @param defaultUserName
	 *            the userName to return when no team leader has been found.
	 * @return the team leader or the defaultUserName when no team leader has
	 *         been found.
	 */
	public String findTeamLeaderOf(String teams, String orderType, String excludedUserName, String defaultUserName) {
		java.util.List<String> trimmedNames = null;
		if (teams != null) {
			trimmedNames = new ArrayList<String>();
			String[] teamNames = teams.split(",");
			for (String teamName : teamNames) {
				String trimmedTeamName = teamName.trim();
				if (trimmedTeamName.length() > 0) {
					trimmedNames.add(trimmedTeamName);
				}
			}
		}
		String excludeUser = excludedUserName != null ? excludedUserName.trim() : null;
		return executeFindTeamLeaderOf(teams, trimmedNames, orderType, excludeUser, defaultUserName);
	}

	private String executeFindTeamLeaderOf(String teams,final java.util.List<String> teamNames, final String orderType,
			final String excludeUser, String defaultUserName) {
		try {
			String teamLeader = Ivy.wf().getSecurityContext().executeAsSystemUser(new Callable<String>() {
				@Override
				public String call() throws Exception {
					for (GroupRole groupRole : ROLES_OF_TEAMLEADERS) {
						java.util.List<IUser> teamLeaders = findTeamLeadersOf(teamNames, orderType,
								groupRole.name(), excludeUser);
						if (teamLeaders != null && teamLeaders.size() > 0) {
							return teamLeaders.get(0).getName();
						}
					}
					return null;
				}
			});
			if(teamLeader==null){
				teamLeader = defaultUserName;
				Ivy.log().warn("Could not find Team Leader by teams:{0}, orderType:{1}, editor:{2} using the dispatcher {3} instead.", teams, orderType, excludeUser, defaultUserName);
			} else {
				Ivy.log().debug("Could find Team Leader by teams:{0}, orderType:{1}, editor:{2} using the TeamLeader {3} as checker.", teams, orderType, excludeUser, teamLeader);	
			}
			return teamLeader;
		} catch (Exception e) {
			Ivy.log().error("Could not find Team Leader using the default {0} instead.", e, defaultUserName);
			return defaultUserName;
		}
	}

	private java.util.List<IUser> filterTeamLeaders(java.util.List<IUser> groupMembers, String excludedUserName) {
		java.util.List<IUser> teamLeaders = new ArrayList<IUser>();
		if (groupMembers != null) {
			for (IUser user : groupMembers) {
				if (excludedUserName == null || !user.getName().equalsIgnoreCase(excludedUserName)) {
					java.util.List<IRole> rolesOfUser = user.getRoles();
					for (IRole role : rolesOfUser) {
						if (role.getName().equalsIgnoreCase("TeamLeader")) {
							teamLeaders.add(user);
						}
					}
				}
			}
		}
		return teamLeaders;
	}

	private java.util.List<IUser> findTeamLeadersOf(java.util.List<String> teams, String orderType, String groupRole,
			String excludedUserName) {
		for (AuthorityGroup group : groups) {
			if (group.getGroupRole().equalsIgnoreCase(groupRole)) {
				OrderBook book = group.getOrderBook();
				if (book != null && book.getOrderTypes() != null && book.getOrderTypes().contains(orderType)) {
					if (teams == null || teams.size() == 0) {
						java.util.List<IUser> users = group.getUserList();
						return filterTeamLeaders(users, excludedUserName);
					}
					String shortname = book.getShortname();
					if (shortname != null && teams.contains(shortname.trim())) {
						java.util.List<IUser> users = group.getUserList();
						return filterTeamLeaders(users, excludedUserName);
					}
				}
			}
		}
		return null;
	}

	/**
	 * Returns the Orderbook with the given id
	 * 
	 * @param id
	 *            OrderBookId
	 * @return Orderbook
	 */
	public OrderBook getBookById(Integer id) {
		for (OrderBook book : books) {
			if (book.getOrderBookId().equals(id)) {
				return book;
			}
		}
		return null;
	}

	/**
	 * Returns all AuthorityGroup
	 * 
	 * @return AuthorityGroup
	 */
	public ArrayList<AuthorityGroup> getGroups() {
		return this.groups;
	}

	/**
	 * 
	 * @param user
	 *            the user name
	 * @param groupRole
	 *            the GroupRole of Authority
	 * @return list of OrderTypes never null.
	 */
	public ArrayList<OrderType> getTypes(IUser user, String groupRole) {
		ArrayList<OrderType> types = new ArrayList<OrderType>();

		for (AuthorityGroup group : groups) {
			if (group.getGroupRole().equals(groupRole) && group.getUserList().contains(user)) {
				Ivy.log().debug("User {0} is granted with <{2}>-Role by AuthorityGroup <{1}>({3}) and OrderBook <{4}>", user.getName(), group.getGroupName(), group.getGroupRole(), group.getId(), group.getOrderBook().getName());
				for (OrderType type : group.getOrderBook().getOrderTypeList()) {
					if (!types.contains(type)) {
						types.add(type);
					}
				}
			}
		}

		return types;
	}

	/**
	 * Get an ivy list of {@link OrderType} granted for a User and given list of
	 * GroupRoles.
	 * 
	 * @param user
	 *            the user name
	 * @param groupRoles
	 *            the GroupRoles of Authority
	 * @return list of OrderTypes never null.
	 */
	public List<OrderType> getOrderTypesByGroupRoles(IUser user, List<String> groupRoles) {
		List<OrderType> orderTypes = List.create(OrderType.class);
		if (user != null && groupRoles != null) {
			for (AuthorityGroup group : groups) {
				if (group.getGroupRole() != null && group.getUserList() != null
						&& groupRoles.indexOf(group.getGroupRole()) > -1 && group.getUserList().indexOf(user) > -1) {
					Ivy.log().debug("User {0} is in AuthorityGroup {1} ({2})", user.getName(), group.getGroupName(), group.getId());
					for (OrderType orderType : group.getOrderBook().getOrderTypeList()) {
						if (!orderTypes.contains(orderType)) {
							orderTypes.add(orderType);
						}
					}
				}
			}
		}
		return orderTypes;
	}

	@SuppressWarnings("unchecked")
	private void getAuthorityGroups() {
		String select = "select e from " + AuthorityGroup.class.getSimpleName() + " e";
		groups = (ArrayList<AuthorityGroup>) persistenceUnit.createQuery(select).getResultList();
		OrderBook book;

		for (AuthorityGroup group : groups) {
			book = getBookById(group.getOrderBook().getOrderBookId());
			group.setOrderBook(book);
			group.setUserList(usersToList(group.getUsers()));
		}
	}

	private ArrayList<OrderBook> getOrderBookChildren(OrderBook parentOrderBook) {
		ArrayList<OrderBook> children = new ArrayList<OrderBook>();
		children.add(parentOrderBook);
		for (OrderBook book : books) {
			if (book.getParentOrderBook() != null && book.getParentOrderBook().equals(parentOrderBook)) {
				children.addAll(getOrderBookChildren(book));
			}
		}
		return children;
	}

	@SuppressWarnings("unchecked")
	private void getOrderBooks() {

		String select = "select e from " + OrderBook.class.getSimpleName() + " e ORDER BY e.sortSequence";
		books = (ArrayList<OrderBook>) persistenceUnit.createQuery(select).getResultList();

		for (OrderBook book : books) {
			ArrayList<OrderBook> childs = getOrderBookChildren(book);
			ArrayList<OrderType> types = new ArrayList<OrderType>();
			if (childs != null) {
				for (OrderBook child : childs) {
					ArrayList<OrderType> childTypes = typesToList(child.getOrderTypes());
					if (childTypes != null) {
						for (OrderType type : childTypes) {
							if (!types.contains(type)) {
								types.add(type);
							}
						}
					}
				}
			}
			book.setOrderTypeList(types);
		}
	}

	/**
	 * Get Users granted with {@link GroupRole#Write} or
	 * {@link GroupRole#TeamDisp} within {@link AuthorityGroup#getUsers()}
	 * entities.
	 * 
	 * @return list of users having Write or TeamDisp Role, never null.
	 */
	public ArrayList<String> getEditorUsers() {
		String select = "select Users from " + AuthorityGroup.class.getName()
				+ " where GroupRole = 'Write' OR GroupRole = 'TeamDisp'";
		@SuppressWarnings("unchecked")
		ArrayList<String> userGroups = (ArrayList<String>) persistenceUnit.createQuery(select).getResultList();
		ArrayList<String> editors = new ArrayList<String>();
		if (userGroups != null) {
			for (String usersOfGroup : userGroups) {
				if (!usersOfGroup.equals("")) {
					String[] users = usersOfGroup.toString().replace("[", "").replace("]", "").replace(" ", "")
							.split(",");
					for (String user : users) {
						if (!user.equals("") && !editors.contains(user)) {
							editors.add(user);
						}
					}
				}
			}
		}
		return editors;
	}

	/**
	 * Get {@link #getEditorUsers()} list sorted by
	 * {@link String#CASE_INSENSITIVE_ORDER}.
	 * 
	 * @return List of Editors, never null
	 * @see #getEditorUsers()
	 */
	public List<String> getEditorUsersSorted() {
		ArrayList<String> editors = getEditorUsers();
		Collections.sort(editors, String.CASE_INSENSITIVE_ORDER);
		return List.create(String.class, editors);
	}

	/**
	 * Build a List of OrderTypes from given comma separated text.
	 * 
	 * @param orderTypes
	 *            comma separated string containing @link {@link OrderType}
	 *            names.
	 * @return list of OrderTypes, never null.
	 */
	public static ArrayList<OrderType> typesToList(String orderTypes) {
		if (orderTypes == null || orderTypes.length() < 1) {
			return new ArrayList<OrderType>();
		}
		String[] orderTypeNames = orderTypes.replace("[", "").replace("]", "").split(",");
		ArrayList<OrderType> typeList = new ArrayList<OrderType>();

		for (String orderTypeName : orderTypeNames) {
			try {
				typeList.add(OrderType.valueOf(orderTypeName.trim()));
			} catch (IllegalArgumentException e) {
				// If value isn't a defined OrderType
				Ivy.log().info("The value {0} is not a member of OrderType enum.", orderTypeName);
			} catch (Exception e) {
				Ivy.log().error(e);
			}
		}
		return typeList;
	}

	/**
	 * Use {@link #typesToList(String)} instead.
	 * 
	 * @param orderTypes
	 *            comma separated string containing @link {@link OrderType}
	 *            names.
	 * @return list of OrderTypes, never null.
	 */
	@Deprecated
	public static List<OrderType> getOrderTypes(String orderTypes) {
		List<OrderType> typeList = List.create(OrderType.class);
		String[] values = orderTypes.replace("[", "").replace("]", "").split(",");

		for (String value : values) {
			try {
				typeList.add(OrderType.valueOf(value.trim()));
				// If value isn't a defined OrderType
			} catch (IllegalArgumentException e) {
				// If value isn't a defined OrderType
				Ivy.log().info("The value {0} is not a member of OrderType enum.", value);
			} catch (Exception e) {
				Ivy.log().error(e);
			}
		}

		return typeList;
	}

	public static ArrayList<IUser> usersToList(String users) {
		ArrayList<IUser> userList = new ArrayList<IUser>();
		String[] values = users.replace("[", "").replace("]", "").replace(" ", "").split(",");

		for (String value : values) {
			try {
				IUser user = Ivy.getInstance().session.getSecurityContext().findUser(value);
				if (user != null) {
					userList.add(user);
				}
				// Value is not a defined IvyUser
			} catch (Exception e) {
			}
		}
		return userList;
	}

	public Boolean typeAllowed(String type, GroupRole role) {
		if (role == null || type == null) {
			return false;
		}

		// get all allowed types
		ArrayList<OrderType> myTypes;
		try {
			myTypes = getTypes(Ivy.session().getSessionUser(), role.name());
		} catch (Exception e) {
			return false;
		}

		// check given type
		for (OrderType myType : myTypes) {
			if (myType.name().equalsIgnoreCase(type)) {
				return true;
			}
		}

		return false;
	}

}
