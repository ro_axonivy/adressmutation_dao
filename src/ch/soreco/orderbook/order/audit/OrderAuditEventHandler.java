package ch.soreco.orderbook.order.audit;

import java.text.SimpleDateFormat;
import java.util.Date;

import ch.ivyteam.log.Logger;
import org.apache.log4j.MDC;

import ch.soreco.orderbook.bo.OrderAuditEvent;
import ch.soreco.orderbook.dao.AddressmutationManagedDao;

/**
 * Log activities in relation of sensitive customer data - especially view of
 * PDF's, comments and exports of lists and search results.
 * 
 */
public class OrderAuditEventHandler extends AddressmutationManagedDao {
	private static Logger logger = Logger.getLogger("OrderAuditLogger");
	private static final ThreadLocal<SimpleDateFormat> sdf = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		}
	};

	/**
	 * Log the {@link OrderAuditEvent} to configured loggers and save the event
	 * to database.
	 * 
	 * @param event
	 */
	public void handle(OrderAuditEvent event) {
		if(event.getBpId()!=null&&event.getBpId().equals("")){
			event.setBpId(null);			
		}
		log(event);
		persist(event);
	}

	/**
	 * Formats a Date into a yyyy-MM-dd HH:mm:ss formatted string.
	 * 
	 * @param date
	 *            the date value to be formatted.
	 * @return the formatted date string.
	 * @see SimpleDateFormat#format(Date)
	 */
	private String format(Date date) {
		return sdf.get().format(date);
	}

	/**
	 * Logs the OrderAudit Event to configured loggers.
	 * 
	 * @param event
	 *            the event to log
	 */
	private void log(OrderAuditEvent event) {
		MDC.put("orderId", String.valueOf(event.getOrderId()));
		if(event.getBpId()!=null){
			MDC.put("bpId", event.getBpId());			
		} else {
			MDC.put("bpId", "");			
		}
		MDC.put("userId", event.getUserId());
		MDC.put("eventType", event.getEventType());
		MDC.put("eventDate", format(event.getEventDate()));
		logger.info("");
		MDC.remove("orderId");
		MDC.remove("bpId");
		MDC.remove("userId");
		MDC.remove("eventType");
		MDC.remove("eventDate");
	}

	/**
	 * Persists the OrderAudit Entity.
	 * 
	 * @param event
	 *            the event to log
	 */
	private void persist(OrderAuditEvent event) {
		getEntityManager().persist(event);
	}
}
