package ch.soreco.orderbook.enums;

/**
 * List of possible roles for specifics OrderBooks.
 * @author aim
 */
public enum GroupRole {
	Read,
	Write,
	Dispatch,
	TeamDisp
}
