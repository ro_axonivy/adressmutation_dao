package ch.soreco.orderbook.enums;

public enum RecordingMetaType {
	DEFAULT,
	BP, 
	Address,
	Container,
	FISellOff,
	FIDisInvestment,
	FIInvestment,
	FIFirstInvestment,
	FITransfer, 
	FIReba, 
	FISwitch, 
	LSV,
	Payment,
	IncomingPayment,
	Letter,
	MoneyTransfer, 
	Transfer2,
	Adressmutation, 
	CRM, 
	ServiceCharges, 
	Collection,
	Other,
	FeeTransfer,
	Interest
}
