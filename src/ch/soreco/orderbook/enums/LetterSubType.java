package ch.soreco.orderbook.enums;

public enum LetterSubType {
	UNDEFINED,
	MoveToEU,
	MoveToUSA
}
