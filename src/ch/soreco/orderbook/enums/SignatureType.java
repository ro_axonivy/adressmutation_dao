package ch.soreco.orderbook.enums;

public enum SignatureType {
OK,
DIFFERS,
MISSING
}
