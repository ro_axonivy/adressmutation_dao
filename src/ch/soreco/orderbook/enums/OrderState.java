package ch.soreco.orderbook.enums;

public enum OrderState {
	Default,
	InDispatchingNew, 
	InDispatchingOpen,
	InDispatchingAbortCheck,
	Dispatched,
	Recording,
	InIdCheck,
	InCheck,
	In4eCheck,
	Finished,
	InPendencyCheck,
	InPendencyRecording,
	InPendencyRecordingCheck,
	Pendant,
	InAbortCheck,
	Aborted, 
	Unidentified, 
	Deleted,
	Merged,
	Archived,
	Rejected
}
