package ch.soreco.orderbook.enums;

public enum StandardPaperType {
	MoveToUSA, 
	MoveToEU,
	Default
}
