package ch.soreco.orderbook.enums;

public enum PendencyTicketType {
	UNDEFINED,
	SignatureRequested, 
	InfosRequestedFromCustomer, 
	RemindOrder,
	InternalDelegation,
	Transaction,
	LSV,
	REBA,
	SWV,
	INV,
	KiSpa,
	TA,
	CRM
}
