package ch.soreco.orderbook.enums;

/**
 * @author bst
 * 
 */
public enum OrderType {
	/** Unzugeordnet */
	Default,
	// Passport,
	// Investmentprofile,
	/** Adressänderung */
	Addressmutation,
	/** Auftrag */
	Order,
	/** Static Data unbestimmt */
	OrderSD,
	/** TRX unbestimmt */
	OrderTRX,
	/** PS unbestimmt */
	OrderPS,
	/** PE unbestimmt */
	OrderPE,
	/** AO unbestimmt */
	OrderAO,
	/** Kontoeröffnung */
	Accountopening,
	/** Risikoprofil */
	RiskProfile,
	/** Elektr. Kundeneröffnung */
	EClientOnboarding,
	/** Vollmacht */
	Authority,
	/** Widerruf */
	Cancelation,
	/** Verpfändung */
	Pawning,
	/** Revers (Fax, Mail, Telefon. Aufträge) */
	Acknowledgment,
	/** EUZ (Ermächtigung Meldeverfahren) */
	Accreditation,
	/** Erbfall */
	Inheritance,
	/** E-Service Vereinbarung */
	EService,
	/** LSV */
	LSV,
	/** Korrespondenz */
	Correspondence,
	/** Payments */
	Payment,
	/** Konto/Depotauszug */
	Statement,
	/** Selloff Saldierung */
	Selloff,
	/** Desinvestment */
	Disinvestment,
	/** Investitionen */
	Investment,
	/** Transfer (intern+extern) */
	Transfer,
	/** Switch */
	Switch,
	/** Individuelles REBA */
	IndivREBA,
	/** Laufzeit-/Zielsummenred. */
	SumReduction,
	/** Umschreibung TRX */
	CircumlocutionTRX,
	/** Rückabwicklung */
	Reversal,
	/** ENT-Planverwaltung */
	Planadministration,
	/** Rücklastschriften */
	ReturnDebitNote,
	/** REBA */
	REBA,
	/** Strategieänderungen */
	Strategychanges,
	/** Unwind Buchungen */
	Unwind,
	/** Sonstiges TRX */
	OthersTRX,
	/** Namensänderungen */
	NameChanges,
	/** NV-Bescheinigung */
	NVCertification,
	/** Volljährige */
	FullAge,
	/** Bestellung */
	Ordering,
	/** Einzugsrelevante Aufträge */
	CatchmentRelevance,
	/** Steuerinformationen */
	Taxinformation,
	/** CRM-Umschlüsselung */
	CRMChangement,
	/** Gebührenbelastung */
	FeesContamination,
	/** Umschreibungen SD */
	CircumlocutionSD,
	/** Sonstiges SD */
	OthersSD,
	/** Korrespondenz SD */
	CorrespondenceSD,
	/** Korrespondenz TRX */
	CorrespondenceTRX,
	/** Verkauf unbestimmt */
	SaleTRX,
	/** Invest unbestimmt */
	InvestTRX,
	/** Sonstige TRX unbestimmt */
	RemainingTRX,
	/** Special Tasks */
	SpecialTasks,
	/** Direct Client Order */
	DirectClientOrder,
	/** Sonstiges MT */
	OthersMT,
	/** LeCo */
	LeCo,
	/** NoOrder */
	NoOrder,

	/** Pension Order */
	PensionOrder(true),
	/** PE-Eröffnung */
	PEOpening(true),
	/** PE-Passkopie */
	PEPassportCopy(true),
	/** PE-Vollmacht */
	PEAuthorization(true),
	/** PE-Adressmutation */
	PEAdressmutation(true),
	/** PE-Korrespondenz */
	PECorrespondence(true),
	/** PE-Auftrag */
	PEOrder(true),
	/** PE-Saldierung */
	PENetting(true),
	/** PE-Anlageprofil */
	PEInvestmentProfile(true),
	/** PE-Heiratsurkunde */
	PEMarriageCertificate(true),
	/** PE-Sterbeurkunde */
	PEDeathCertificate(true),
	/** PE-Pfandbestellung */
	PEPledge(true),
	/** PE-Reserve 1 */
	PEReserve1(true),
	/** PE-Reserve 2 */
	PEReserve2(true),
	/** PE-Reserve 3 */
	PEReserve3(true),
	/** PE-Reserve 4 */
	PEReserve4(true),

	/** Zeichnungsschein */
	SubscriptionForm,
	/** Sonstiges EAM */
	OthersEAM,
	/** Sonstiges AO */
	OthersAO,

	/** Banking Line Auftrag */
	OrderBL,
	/** Reserve BL */
	ReserveBL,

	/** EAM Auftrag */
	OrderEAM,
	/** Reserve EAM1 */
	ReserveEAM1,
	/** Reserve EAM2 */
	ReserveEAM2,
	/** Reserve EAM3 */
	ReserveEAM3,
	/** Team Schweiz Auftrag */
	OrderTS,
	/** Reserve TS */
	ReserveTS,
	
	
	/** Nachbearbeitung FDL CH */
	PostProcessingFdlCH,
	/** Nachbearbeitung FDL DE */
	PostProcessingFdlDE,
	/** Nachbearbeitung FDL CASH */
	PostProcessingFdlCASH;
	
	
	
	
	
	private boolean requiresFAClanId;

	private OrderType() {
		this.requiresFAClanId = false;
	}

	private OrderType(boolean requiresFAClanId) {
		this.requiresFAClanId = requiresFAClanId;
	}

	public boolean requiresFAClanId() {
		return requiresFAClanId;
	}
}
