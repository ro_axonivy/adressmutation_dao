package ch.soreco.orderbook.enums;

public enum ArchivingState 
{
	PREPARED,
	IVY_ARCHIVED,
	IMTF_ARCHIVED,
	IN_REPAIR,
	REPAIRED
	
}
