package ch.soreco.orderbook.enums;

public enum LetterType {
	UNDEFINED,
	Letter,
	CRM, 
	LetterAvaloq, 
	LetterWord, 
	Others
}
