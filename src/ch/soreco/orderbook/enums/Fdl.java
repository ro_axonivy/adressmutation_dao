package ch.soreco.orderbook.enums;

public enum Fdl {
	ZIAGStruki("ZIAG Struki"),
	ZIAG("ZIAG"),
	Zugerberg("Zugerberg"), 
	OVB("OVB"),
	DE("DE"),
	Telis("Telis"),
	CH("CH"),
	CASH("CASH");
	
	private final String fdlName;

    Fdl(String fdlName) {
        this.fdlName = fdlName;
    }
    
    public String getFdlName() {
        return this.fdlName;
    }
}
