package ch.soreco.webbies.common.eclipse;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

import ch.ivyteam.ivy.application.ActivityState;
import ch.ivyteam.ivy.application.IProcessModel;
import ch.ivyteam.ivy.application.IProcessModelVersion;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.persistence.PersistencyException;
import ch.soreco.webbies.common.util.ClassFinder;

public class Classes {
	private static final String DATA_CLASSES_DIR = "classes";
	private static List<Class<?>> classes = null;
	private static List<Class<?>> enums = null;
	private static Classes instance = null;
	private static List<String> projectDirs = null;
	private static List<File> codeSources = null;
	/**
	* Constructor
	*/
	private Classes(){
		
	}
	/**
	 * get the instance
	 * @return the singleton
	 */
	public static Classes getInstance(){
	    if (instance == null)
	    {
	    	instance = new Classes();
	    }
	    return instance;
	  }
	/**
	 * get the list of classes within present workspace projects
	 * which are located in the "classes" root folder
	 * @return
	 */
	public static List<Class<?>> getAllClasses(){
		if(classes==null){
			List<File> dirs = Classes.getProjectCodeSources();
			ArrayList<Class<?>> classList = new ArrayList<Class<?>>();				
			for(File dir:dirs){
				String packageName = null;
				Class<?> classSearched = null;
				classList.addAll(ClassFinder.getClassesFromDir(dir, packageName, classSearched));
			}
			classes = classList;
		}

		return classes;
	}
	private static List<IProcessModelVersion> getProcessModelVersions() throws Exception{
		return Ivy.session().getSecurityContext().executeAsSystemUser(
				new Callable<List<IProcessModelVersion>>(){
					public List<IProcessModelVersion> call() throws PersistencyException {
						List<IProcessModelVersion> pmvs = new ArrayList<IProcessModelVersion>();
						List<String> names = new ArrayList<String>();
						pmvs.add(Ivy.wfCase().getProcessModelVersion());
						names.add(Ivy.wfCase().getProcessModelVersion().getName());
						try {
							List<IProcessModel> models = Ivy.wfCase().getApplication().getProcessModels();
							for(IProcessModel model:models){
								int version = -1;
								IProcessModelVersion currentPmv = null;
								for(IProcessModelVersion pmv:model.getProcessModelVersions()){
									if(pmv.getActivityState().equals(ActivityState.ACTIVE)
											&&pmv.getVersionNumber()>version){
										currentPmv = pmv;
									}
								}								
								if(currentPmv!=null
										&&names.indexOf(currentPmv.getName())==-1){
									Ivy.log().info(Classes.class.getSimpleName()+":Set Application Process Model:"+currentPmv.getName());										
									names.add(currentPmv.getName());
									pmvs.add(currentPmv);
								}
							}
						} catch (Exception e){
							Ivy.log().error(Classes.class.getSimpleName()+" Error getProcessModels:"+e.getMessage(), e);							
						}
						return Collections.unmodifiableList(pmvs);
					}
				}
			);		
	}
	private static List<String> getProjectDirectoryPaths(){
		if(projectDirs==null){
			projectDirs = new ArrayList<String>();
			List<IProcessModelVersion> pmvs;
			try {
				pmvs = Classes.getProcessModelVersions();
				for(IProcessModelVersion pmv:pmvs){
					try {
						projectDirs.add(pmv.getProjectDirectory());
					} catch (PersistencyException e) {
						Ivy.log().error(Classes.class.getSimpleName()+" Error getProjectDirectoryPaths:"+e.getMessage(), e);							
					}
				}			
			} catch (Exception e1) {
				Ivy.log().error(Classes.class.getSimpleName()+" Error getProcessModelVersions:"+e1.getMessage(), e1);							
			}
		}
		return projectDirs;
	}
	/**
	 * Gets the List of available Projects within a Server or Workspace
	 * @return
	 * @throws Exception
	 */
	private static List<String> getProjectPaths() throws Exception{	
		if(projectDirs==null){
			projectDirs = Ivy.session().getSecurityContext().executeAsSystemUser(
					new Callable<List<String>>(){
						public List<String> call() throws PersistencyException {
							List<String> libs = new ArrayList<String>();
							String thisProjectDir = Ivy.wfCase().getProcessModelVersion().getProjectDirectory();
							//Add This project directory to Project Directory List
							Ivy.log().info(Classes.class.getSimpleName()+":Set Process Model path:"+thisProjectDir);										
							libs.add(thisProjectDir);
							try {
								List<IProcessModel> models = Ivy.wfCase().getApplication().getProcessModels();
								for(IProcessModel model:models){
									int version = -1;
									String path = "";
									for(IProcessModelVersion pmv:model.getProcessModelVersions()){
										if(pmv.getActivityState().equals(ActivityState.ACTIVE)
												&&pmv.getVersionNumber()>version){
											path = pmv.getProjectDirectory();
										}
									}								
									if(!path.equals("")
											&&libs.indexOf(path)==-1){
										Ivy.log().info(Classes.class.getSimpleName()+":Set Application Process Model path:"+path);										
										libs.add(path);									
									}
								}
							} catch (Exception e){
								Ivy.log().error(Classes.class.getSimpleName()+" Error getProcessModels:"+e.getMessage(), e);							
							}
							return Collections.unmodifiableList(libs);
						}
					}
				);			
		}		
		return projectDirs;
	}
	public static List<File> getProjectCodeSources(){
		List<String> libs = new ArrayList<String>();
		if(codeSources==null){
			codeSources = new ArrayList<File>();
			try {
				libs = Classes.getProjectPaths();
				for(String lib:libs){
					File libFile = new File(lib);
					for(File dir:libFile.listFiles()){
						if(dir.getName().equals(DATA_CLASSES_DIR)){
							codeSources.add(dir);
						}
					}
				}
			} catch (Exception e) {
				Ivy.log().error(Classes.class.getSimpleName()+" Error on getting libraries.",e);
			}			
		}
		return codeSources;
	}
	
	/**
	 * get the list of enum's within present workspace projects
	 * which are located in the "classes" root folder
	 * @return
	 */
	public static List<Class<?>> getAllEnums(){
		if(enums==null&&getAllClasses()!=null){
			ArrayList<Class<?>> classList = new ArrayList<Class<?>>();				
			for(Class<?> clazz:Classes.getAllClasses()){
				if(clazz.isEnum()){
					classList.add(clazz);
				}
			}
			enums = classList;
		}
		return enums;
	}

}
