package ch.soreco.webbies.common.db;

/**
 * Container for File content having a byte array which is ready to be uploaded by
 * {@link BlobHelper#fileUpload(BlobFile)}
 */
public class BlobFile {
	private String contentType;
	private String extension;
	private String fileName;
	private String originalFileName;
	private byte[] bytes;

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}
}
