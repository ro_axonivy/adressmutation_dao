package ch.soreco.webbies.common.db;

public enum EntityAssociationType {
	UNDEFINED,
	OneToOne,
	OneToMany,
	ManyToOne, 
	ManyToMany
}
