package ch.soreco.webbies.common.db;

import java.lang.reflect.Field;

import ch.ivyteam.ivy.environment.Ivy;

public class EntityAssociation implements Comparable<EntityAssociation> {
	public Class<?> referencedFrom = null;
	public EntityAssociationType associationType = EntityAssociationType.UNDEFINED;
	public Class<?> referencedTo = null;
	//TODO: Add Mapped By Field
	private PersistenceMetaField associatedBy = null;
	EntityAssociation(PersistenceMetaField entityField){
		this.associatedBy = entityField;
		this.associationType = this.associatedBy.getAssociationType();
		this.referencedFrom = this.associatedBy.getReferencedFrom();
		this.referencedTo = this.associatedBy.getReferencedTo();
	}
	public Boolean isAssociationOf(Class<?> clazz){
		return clazz.getName().equals(this.referencedFrom.getName())
				||clazz.getName().equals(this.referencedTo.getName());
	}
	@Override
	public int compareTo(EntityAssociation o) {
		int from = 0;
		int to = 0;
		from = this.referencedFrom.getName().compareTo(o.referencedFrom.getName());
		to = this.referencedFrom.getName().compareTo(o.referencedFrom.getName());

	    return from*100+to;
	}
	public String toString(){
		StringBuilder info = new StringBuilder("");
		Field[] fields = this.getClass().getDeclaredFields();
		for ( Field field : fields  ) {
			String name = field.getName();
			String value = "";
			//try to get out the value directly
			try {
				Object obj = field.get(this);
				if(obj instanceof String){
					value = (String) obj;
				} else if(obj instanceof Integer){
					value = obj.toString();
				} else if (obj instanceof Boolean){
					Boolean bool = (Boolean) obj;
					if(bool){
						value = "true";
					}
					else {
						value = "false";
					}
				} else if(obj instanceof Class<?>){
					Class<?> clazz = (Class<?>) obj;
					value = clazz.getName();
				} else if(name.equals("associationType")){
					value = this.associationType.toString();
				}
			} catch (IllegalArgumentException e) {
				Ivy.log().info("Can't access "+field.getName(),e);
			} catch (IllegalAccessException e) {
				Ivy.log().info("Can't access "+field.getName(),e);
			}
			if(!value.equals("")){
				if(info.length()>1){
					info.append(",");
				}
				info.append(name+":"+value);				
			}
		}
		info.insert(0, this.getClass().getSimpleName()+"(");
		info.append(")");
		return info.toString();
	}
}
