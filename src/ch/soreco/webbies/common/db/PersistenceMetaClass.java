package ch.soreco.webbies.common.db;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;


public class PersistenceMetaClass{
	private List<EntityAssociation> entityAssociations = new ArrayList<EntityAssociation>();
	private PersistenceMetaField idField = null;
	private Class<?> clazz = null;
	private Table table = null;
	private Entity entity = null;
	private LinkedHashMap<String, PersistenceMetaField> fieldsMap = new LinkedHashMap<String, PersistenceMetaField>(); 
	private List<PersistenceMetaField> fieldList = new ArrayList<PersistenceMetaField>();
	private Boolean isAssociated = null;
	public PersistenceMetaClass(Class<?> clazz){
		this.clazz = clazz;
	}
	private void loadClassAnnotations(){
		for(java.lang.annotation.Annotation annotation: this.clazz.getAnnotations()){
			if(annotation instanceof Table){
				this.table = (Table) annotation;
			}
			if(annotation instanceof Entity){
				this.entity = (Entity) annotation;
			}
		}							
	}
	private void loadClassFieldsAnnotations(){
		//Loop fields to gather Persistence Meta fields
		for(Field field:clazz.getDeclaredFields()){
			if(!field.getName().equals("serialVersionUID")){
				PersistenceMetaField entityField = new PersistenceMetaField(field);
				this.fieldsMap.put(field.getName(), entityField);
				this.fieldList.add(entityField);
				if(entityField.isId()){
					this.idField = entityField;
				}
				if(entityField.isAssociated()){
					this.isAssociated = true;
					EntityAssociation association = new EntityAssociation(entityField);
					this.entityAssociations.add(association);
				}
			}
		}
		if(this.isAssociated==null){
			this.isAssociated = false;
		}
	}
	public Table getTable(){
		if(this.table==null){
			this.loadClassAnnotations();
		}
		return this.table;
	}
	public String getEntityName(){
		if(this.entity==null){
			this.loadClassAnnotations();
		}
		return this.entity.name();
	}
	public PersistenceMetaField getPrimaryKeyField(){
		if(this.idField==null){
			this.loadClassFieldsAnnotations();
		}
		return this.idField;
		
	}
	public LinkedHashMap<String, PersistenceMetaField> getFieldMap(){
		if(this.fieldsMap.size()==0){
			loadClassFieldsAnnotations();
		}
		return this.fieldsMap;
	}
	public List<PersistenceMetaField> getFieldList(){
		if(this.fieldList.size()==0){
			loadClassFieldsAnnotations();
		}
		return this.fieldList;
	}
	public PersistenceMetaField getMetaField(String name){
		return this.getFieldMap().get(name);
	}
	public Boolean isAssociated(){
		if(this.fieldList.size()==0){
			loadClassFieldsAnnotations();
		}
		return this.isAssociated;
	}
	public List<EntityAssociation> getAssociations(){
		if(this.fieldList.size()==0){
			loadClassFieldsAnnotations();
		}
		return this.entityAssociations;
	}
}	

