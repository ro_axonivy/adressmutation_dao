package ch.soreco.webbies.common.db;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import ch.soreco.webbies.common.eclipse.Classes;

public class EntityClasses {
	private static List<Class<?>> entityClasses = null;
	private static List<EntityAssociation> entityAssociations = null;
	/**
	 * Get the classes within all projects of present workspace, which are annotated as javax.persistence.Entity
	 * @return List<Class<?>> or null
	 */
	public static List<Class<?>> getEntityClasses(){
		if(entityClasses==null&&Classes.getAllClasses()!=null){
			entityClasses = new ArrayList<Class<?>>();
			for(Class<?> clazz:Classes.getAllClasses()){
				if(PersistenceUtils.isEntityClass(clazz)){
					entityClasses.add(clazz);
				}
			}
		}
		return entityClasses;
	}
	public static List<EntityAssociation> getAssociations(){
		if(getEntityClasses()!=null&&entityAssociations==null){
			entityAssociations = new ArrayList<EntityAssociation>();			
			for(Class<?> clazz:getEntityClasses()){
				PersistenceMetaClass entityMetaClass= new PersistenceMetaClass(clazz);
				entityAssociations.addAll(entityMetaClass.getAssociations());
			}
			Collections.sort(entityAssociations);
		}
		return entityAssociations;
	}
	public static List<EntityAssociation> getAssociationsOf(Class<?> clazz){
		List<EntityAssociation> associations = new ArrayList<EntityAssociation>();
		if(getAssociations()!=null){
			for(EntityAssociation association:getAssociations()){
				if(association.isAssociationOf(clazz)){
					associations.add(association);
				}
			}
			Collections.sort(associations);
		}
		return associations;
	}
	public static List<Class<?>> getAssociatedClassesOf(Class<?> clazz){
		List<Class<?>> classes = new ArrayList<Class<?>>();
		List<EntityAssociation> associations = getAssociationsOf(clazz);
		for(EntityAssociation association :associations){
			if(!clazz.getName().equals(association.referencedFrom.getName())){
				classes.add(association.referencedFrom);
			}
			if(!clazz.getName().equals(association.referencedTo.getName())){
				classes.add(association.referencedTo);
			}
		}
		return classes;
	}
}
