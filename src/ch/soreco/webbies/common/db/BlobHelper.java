package ch.soreco.webbies.common.db;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.Callable;

import org.apache.commons.io.IOUtils;

import ch.ivyteam.db.jdbc.DatabaseUtil;
import ch.ivyteam.ivy.db.IExternalDatabase;
import ch.ivyteam.ivy.db.IExternalDatabaseApplicationContext;
import ch.ivyteam.ivy.db.IExternalDatabaseRuntimeConnection;
import ch.ivyteam.ivy.environment.EnvironmentNotAvailableException;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.persistence.PersistencyException;
import ch.ivyteam.ivy.process.data.persistence.IIvyEntityManager;
import ch.ivyteam.ivy.scripting.objects.DateTime;
import ch.soreco.common.bo.File;

/**
 * Use BlobHelper to upload Files into {@link File} Table
 */
public class BlobHelper {
	IExternalDatabase database = null;
	private String ivyDBConnectionName = null; // the user friendly connection
												// name to Database in Ivy

	/**
	 * Use BlobHelper to upload Files into tbl_Files
	 * 
	 * @param dbConfigName
	 *            : the user friendly name of Ivy Database configuration
	 */
	public BlobHelper(String dbConfigName) {
		this.ivyDBConnectionName = dbConfigName;
	}

	/**
	 * used to get Ivy IExternalDatabase object with given user friendly name of
	 * Ivy Database configuration
	 * 
	 * @return the IExternalDatabase object
	 * @throws Exception
	 * @throws EnvironmentNotAvailableException
	 */
	private IExternalDatabase getDatabase() throws Exception {
		if (database == null) {
			final String _nameOfTheDatabaseConnection = this.ivyDBConnectionName;
			database = Ivy.session().getSecurityContext().executeAsSystemUser(new Callable<IExternalDatabase>() {
				public IExternalDatabase call() throws Exception {
					IExternalDatabaseApplicationContext context = (IExternalDatabaseApplicationContext) Ivy.wf()
							.getApplication().getAdapter(IExternalDatabaseApplicationContext.class);
					return context.getExternalDatabase(_nameOfTheDatabaseConnection);
				}
			});
		}
		return database;
	}

	/**
	 * Upload a {@link BlobFile} to given persistence unit. The BlobFile object
	 * is mapped to the appropriate {@link File} fields. Unmapped(Depreceated)
	 * fields are:
	 * <ul>
	 * <li>{@link File#setIsDeleted(Boolean)}</li>
	 * <li>{@link File#setFileGroupId(Integer)}</li>
	 * <li>{@link File#setCreateUserName(String)}</li>
	 * <li>{@link File#setHostName(String)}</li>
	 * <li>{@link File#setProcessModelName(String)}</li>
	 * <li>{@link File#setProcessModelVersion(String)}</li>
	 * <li>{@link File#setWebAppName(String)}</li>
	 * </ul>
	 * 
	 * @param blobFile
	 *            : the {@link BlobFile} to be uploaded.
	 * @param persistenceUnit
	 *            : the persistence unit name where {@link File} belongs to.
	 * @throws PersistencyException
	 * @throws SQLException
	 * @throws Exception
	 */
	public File fileUpload(BlobFile blobFile, String persistenceUnit) throws PersistencyException, SQLException,
			Exception {
		IIvyEntityManager manager = Ivy.persistence().get(persistenceUnit);
		File fileEntity = new File();
		// Map BlobFile values to File
		fileEntity.setContentType(blobFile.getContentType());
		fileEntity.setFileExtension(blobFile.getExtension());
		fileEntity.setFileName(blobFile.getFileName());
		fileEntity.setFileSize(blobFile.getBytes().length);

		// set additional {@link File} values
		fileEntity.setIsDeleted(false);
		fileEntity.setCreateDate(new DateTime());
		fileEntity.setCreateUserId(Ivy.session().getSessionUserName());
		fileEntity = manager.persist(fileEntity);
		this.byteUpload(blobFile.getBytes(), fileEntity.getFileId());

		return fileEntity;

	}

	private void byteUpload(byte[] blob, Integer fileId) throws PersistencyException, SQLException, Exception {
		ByteArrayInputStream bais = new ByteArrayInputStream(blob);
		try {
			this.fileUpload(bais, fileId);
		} finally {
			bais.close();
		}
	}

	private void fileUpload(InputStream in, Integer fileId) throws PersistencyException, SQLException, Exception {
		String query = "UPDATE tbl_Files SET fileBlob = ? WHERE fileId=" + fileId;

		// get File Input Stream
		BufferedInputStream bis = new BufferedInputStream(in);
		// get a connection from the environment
		IExternalDatabaseRuntimeConnection conn = null;
		try {
			conn = getDatabase().getAndLockConnection();
			Connection jdbcConnection = conn.getDatabaseConnection();

			// now Prepare File Update Statement
			PreparedStatement statement = null;
			try {
				statement = jdbcConnection.prepareStatement(query);
				// set Blob
				statement.setBinaryStream(1, bis, bis.available());
				statement.executeUpdate();
			} finally {
				DatabaseUtil.close(statement);
			}

		} finally {
			if (conn != null) {
				database.giveBackAndUnlockConnection(conn);
			}
			bis.close();
		}
	}

	/**
	 * Updates [fileBlob] field of table tbl_files with File at given FileUrl
	 * Consider to refactor this method to use
	 * {@link #fileUpload(InputStream, Integer)}
	 * 
	 * @param fileUrl
	 *            the url to the file which need to be updated into database
	 * @param fileId
	 *            the file id to be updated
	 * @throws PersistencyException
	 * @throws SQLException
	 * @throws Exception
	 */
	public void fileUpload(String fileUrl, Integer fileId) throws PersistencyException, SQLException, Exception {
		String query = "UPDATE tbl_Files SET fileBlob = ? WHERE fileId=" + fileId;

		// get File Input Stream
		BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(fileUrl));
		// get a connection from the environment
		IExternalDatabaseRuntimeConnection connection = null;
		try {
			connection = getDatabase().getAndLockConnection();
			Connection jdbcConnection = connection.getDatabaseConnection();

			// now Prepare File Update Statement
			PreparedStatement statement = null;
			try {
				statement = jdbcConnection.prepareStatement(query);
				// set Blob
				statement.setBinaryStream(1, inputStream, inputStream.available());
				statement.executeUpdate();
			} finally {
				DatabaseUtil.close(statement);
			}

		} finally {
			inputStream.close();
			if (connection != null) {
				database.giveBackAndUnlockConnection(connection);
			}
		}
	}

	public ch.ivyteam.ivy.scripting.objects.File downloadToSessionFile(Integer fileId) throws PersistencyException,
			SQLException, Exception {
		return downloadFile(fileId, true);
	}

	private ch.ivyteam.ivy.scripting.objects.File downloadFile(Integer fileId, boolean temporary)
			throws PersistencyException, SQLException, Exception {
		String query = "SELECT fileBlob, fileName FROM  tbl_Files WHERE fileId=" + fileId;
		ch.ivyteam.ivy.scripting.objects.File file = null;
		// get a connection from the environment
		IExternalDatabaseRuntimeConnection connection = null;
		try {
			connection = getDatabase().getAndLockConnection();
			Connection jdbcConnection = connection.getDatabaseConnection();
			
			// now Prepare File Update Statement
			PreparedStatement statement = null;
			try {
				statement = jdbcConnection.prepareStatement(query);
				// get Blob
				ResultSet rs = statement.executeQuery();
				if (rs.next()) {
					String fileName = rs.getString(2);
					file = new ch.ivyteam.ivy.scripting.objects.File(fileName, temporary);
					// Get Bytes
					InputStream in = rs.getBinaryStream(1);
					FileOutputStream out = new FileOutputStream(file.getJavaFile());
					IOUtils.copy(in, out);
				}
			} catch(Exception e){
				Ivy.log().error("Error on fetching file id {0} with query {1}", e, fileId, query);
			} finally {
				DatabaseUtil.close(statement);
			}

		} catch(Exception e){
			Ivy.log().error("Error on downloading file id {0}", e, fileId);
		} finally {
			if (connection != null) {
				database.giveBackAndUnlockConnection(connection);
			}
		}
		return file;
	}

	public ch.ivyteam.ivy.scripting.objects.File fileDownload(Integer fileId) throws PersistencyException,
			SQLException, Exception {
		return downloadFile(fileId, false);
	}
}
