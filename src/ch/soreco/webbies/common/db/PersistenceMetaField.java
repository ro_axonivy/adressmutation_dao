package ch.soreco.webbies.common.db;

import java.lang.reflect.Field;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import ch.ivyteam.ivy.environment.Ivy;

public class PersistenceMetaField {
	private Field field= null;
	private Class<?> columnType = null;
	private Boolean isAnnotatedColumn = false;
	private Boolean isId = false;

	private Boolean isAssociated = false;
	private EntityAssociationType associationType = EntityAssociationType.UNDEFINED;
	private Class<?> referencedTo = null;
	private Class<?> referencedFrom = null;
	
	private ManyToOne manyToOne = null;
	private OneToMany oneToMany = null;
	private OneToOne oneToOne = null;
	private ManyToMany manyToMany = null;
	
	private Column column = null;
	private String columnName = "";
	private String columnTable = "";
	
	private Boolean isGenerated = false;
	private Boolean isInsertable = true;
	private Boolean isNullable = true;
	private Boolean isUnique = true;
	private Boolean isUpdateable = true;
	
	private Integer length = 0;
	private Integer precision = 0;
	private Integer scale = 0;
	
	public PersistenceMetaField(Field classField){
		this.field = classField;
		this.columnType = this.field.getType();
		
		for(java.lang.annotation.Annotation annotation: this.field.getDeclaredAnnotations()){
			if(annotation instanceof Id){
				this.isId = true;
			}
			if(annotation instanceof GeneratedValue){
				this.isGenerated = true;
			}
			if(annotation instanceof Column){
				this.column = (Column) annotation;

				this.columnName = this.column.name();
				this.columnTable = this.column.table();

				
				this.isAnnotatedColumn = true;
				this.isInsertable = this.column.insertable();
				this.isNullable = this.column.nullable();
				this.isUnique = this.column.unique();
				this.isUpdateable = this.column.updatable();
				this.isUpdateable = this.column.updatable();
				//may have awareness to default value
				this.length =  this.column.length();
				this.precision = this.column.precision();
				this.scale = this.column.scale();
				
			}
			if(annotation instanceof ManyToOne){
				this.manyToOne = (ManyToOne) annotation;
				this.referencedFrom = this.field.getDeclaringClass();
				this.referencedTo = this.field.getType();
				this.isAssociated = true;
				this.associationType = EntityAssociationType.ManyToOne;
			}
			if(annotation instanceof OneToMany){
				this.oneToMany = (OneToMany) annotation;
				this.referencedFrom = this.field.getType();
				this.referencedTo = this.field.getDeclaringClass();
				this.isAssociated = true;
				this.associationType = EntityAssociationType.OneToMany;
			}
			if(annotation instanceof OneToOne){
				this.oneToOne = (OneToOne) annotation;
				this.referencedFrom = this.field.getDeclaringClass();
				this.referencedTo = this.field.getType();
				this.isAssociated = true;
				this.associationType = EntityAssociationType.OneToOne;
			}
			if(annotation instanceof ManyToMany){
				this.manyToMany = (ManyToMany) annotation;
				this.referencedFrom = this.field.getDeclaringClass();
				this.referencedTo = this.field.getType();
				this.isAssociated = true;
				this.associationType = EntityAssociationType.ManyToMany;
			}
		}			
		//adjust Column lenght to default on Strings
		if(this.length==0&&String.class.isAssignableFrom(this.field.getType())){
			this.length = 255;
		}

	}
	public Field getJavaField(){
		return this.field;
	}
	public Boolean isId(){
		return this.isId;
	}
	public Boolean isGenerated(){
		return this.isGenerated;
	}
	public Boolean isAnnotatedColumn(){
		return this.isAnnotatedColumn;
	}
	public Column getEntityColumn(){
		return this.column;
	}
	public Boolean isAssociated(){
		return this.isAssociated;
	}
	public Class<?> getReferencedTo(){
		return this.referencedTo;
	}
	public Class<?> getReferencedFrom(){
		return this.referencedFrom;
	}
	public ManyToOne getManyToOne(){
		return this.manyToOne;
	}
	public OneToMany getOneToMany(){
		return this.oneToMany;
	}
	public OneToOne getOneToOne(){
		return this.oneToOne;
	}
	public ManyToMany getManyToMany(){
		return this.manyToMany;
	}
	public EntityAssociationType getAssociationType(){
		return this.associationType;
	}
	public String getColumnName(){
		return this.columnName;
	}	
	public String getColumnTable(){
		return this.columnTable;
	}	
	public Boolean isInsertable(){
		return this.isInsertable;
	}	
	public Boolean isNullable(){
		return this.isNullable;
	}	
	public Boolean isUnique(){
		return this.isUnique;
	}
	public Boolean isUpdateable(){
		return this.isUpdateable;
	}
	public Integer getLength(){
		return this.length;
	}
	public Integer getPrecision(){
		return this.precision;
	}
	public Integer getScale(){
		return this.scale;
	}
	public Class<?> getColumnType(){
		return this.columnType;
	}
	public String toString(){
		StringBuilder info = new StringBuilder("");
		Field[] fields = this.getClass().getDeclaredFields();
		for ( Field field : fields  ) {
			String name = field.getName();
			String value = "";
			//try to get out the value directly
			try {
				Object obj = field.get(this);
				if(obj instanceof String){
					value = (String) obj;
				} else if(obj instanceof Integer){
					value = obj.toString();
				} else if (obj instanceof Boolean){
					Boolean bool = (Boolean) obj;
					if(bool){
						value = "true";
					}
					else {
						value = "false";
					}
				} else if(obj instanceof Class<?>){
					Class<?> clazz = (Class<?>) obj;
					value = clazz.getName();
				} else if(name.equals("associationType")&&this.isAssociated){
					value = this.associationType.toString();
				}
			} catch (IllegalArgumentException e) {
				Ivy.log().info(this.getClass().getSimpleName()+" Exception: Can't access "+field.getName(),e);
			} catch (IllegalAccessException e) {
				Ivy.log().info(this.getClass().getSimpleName()+" Exception: Can't access "+field.getName(),e);
			}
			if(!value.equals("")){
				if(info.length()>1){
					info.append(",");
				}
				info.append(name+":"+value);				
			}
		}
		info.insert(0, this.getClass().getSimpleName()+"(");
		info.append(")");
		return info.toString();
	}
}
