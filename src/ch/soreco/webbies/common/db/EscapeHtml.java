package ch.soreco.webbies.common.db;
import java.lang.reflect.Field;

import org.apache.commons.lang.StringEscapeUtils;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.scripting.objects.CompositeObject;
public class EscapeHtml {
	public static CompositeObject escapeHtml(CompositeObject object){
		Field[] fields = object.getClass().getDeclaredFields();
		for(int i=0;i<fields.length;i++){
			String fieldName = fields[i].getName();
			String fieldType = fields[i].getType().getName();
			if(fieldType.equals("java.lang.String")){
				try {
					String value = (String) object.get(fieldName);
					value = StringEscapeUtils.escapeHtml(value);
					object.set(fieldName, value);				
				} catch (NoSuchFieldException e) {
					Ivy.log().debug("Could not Escape "+fieldName, e);
				}
			}
		}
		return object;
	}
	public static CompositeObject unescapeHtml(CompositeObject object){
		Field[] fields = object.getClass().getDeclaredFields();
		for(int i=0;i<fields.length;i++){
			String fieldName = fields[i].getName();
			String fieldType = fields[i].getType().getName();
			if(fieldType.equals("java.lang.String")){
				try {
					String value = (String) object.get(fieldName);
					value = StringEscapeUtils.unescapeHtml(value);
					object.set(fieldName, value);				
				} catch (NoSuchFieldException e) {
					Ivy.log().debug("Could not Escape "+fieldName, e);
				}
			}
		}
		return object;
	}}
