package ch.soreco.webbies.common.db;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import ch.ivyteam.ivy.environment.EnvironmentNotAvailableException;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.persistence.PersistencyException;
import ch.ivyteam.ivy.process.data.persistence.IIvyEntityManager;
import ch.ivyteam.ivy.process.data.persistence.IIvyQuery;
import ch.ivyteam.ivy.scripting.objects.CompositeObject;
import ch.soreco.webbies.common.util.CompositeUtils;

public class PersistenceUtils {
	public final static String PERSISTENCE_RESOURCE= "\\META-INF\\persistence.xml";
	private static List<String> persistenceUnits = null;
	private static HashMap<String, String> persistenceUnitMap = new HashMap<String, String>();
	public static List<?> searchBy(CompositeObject filter, Integer lotSize) throws EnvironmentNotAvailableException, PersistencyException{
		List<?> entities = new ArrayList<CompositeObject>();
		Class<?> clazz = filter.getClass();
		String unit = PersistenceUtils.getPersistenceUnitNameOf(clazz);
		HashMap<String, Object> parameters = new HashMap<String, Object>();

		//gather filled fields
		for(Field field:clazz.getDeclaredFields()){
			Object value = null;
			try {
				value = filter.get(field.getName());
			} catch (NoSuchFieldException e) {
				Ivy.log().info(CompositeUtils.class.getSimpleName()+" Exception",e);
			}
			if(!CompositeUtils.getValue(value).equals("")){
				parameters.put(field.getName(), value);
			}
		}
		if(!unit.equals("")&&parameters.size()==0){
			//as no filters are passed
			//get all of them.
			entities = Ivy.persistence().get(unit).findAll(clazz);	
			}
		if(!unit.equals("")&&parameters.size()>0){
			List<String> clauses = new ArrayList<String>();
			for(String key:parameters.keySet()){
				clauses.add("e."+key+" = :"+key);
			}
			String where = StringUtils.join(clauses, " AND ");
			String select = "select e from "+filter.getClass().getSimpleName()+" e where "+where;				
			IIvyQuery query = Ivy.persistence().get(unit).createQuery(select);
			for(String key:parameters.keySet()){
				query.setParameter(key,parameters.get(key));				
			}
			if(lotSize>0){
				query.setMaxResults(lotSize);
			}
			entities = query.getResultList();
			}
		return entities;
	}
	/**
	 * Remove All Entities of this class
	 * @param entityClass Class of its entities need to be removed
	 * @param persistenceUnit
	 */
	public static void removeAll(Class<?> entityClass, IIvyEntityManager persistenceUnit){
		List<?> found = persistenceUnit.findAll(entityClass);
		for(int i=0;i<found.size();i++){
			persistenceUnit.remove(found.get(i));
		}
	}
	public static PersistenceMetaClass getMetaClass(Class<?> clazz){
		return new PersistenceMetaClass(clazz);
	}
	/**
	 * Get's the available Persistence Unit Names
	 * @return List<String> units or empty list.
	 */
	public static List<String> getPersistenceUnitNames(){
		if(persistenceUnits==null){
			List<String> names = new ArrayList<String>();
//			List<File> codeSources = Classes.getProjectCodeSources();
//			for(File codeSource:codeSources){
//				File persistenceResource = new File(codeSource.getAbsolutePath()+PERSISTENCE_RESOURCE);
//				if(persistenceResource.exists()){
//					URL url=null;
//					try {			
//						url = persistenceResource.toURI().toURL();
//						EJB3DTDEntityResolver entityResolver = new EJB3DTDEntityResolver();
//						HashMap<String, String> overloads = new HashMap<String, String>();
//						List<PersistenceMetadata> pmetas = PersistenceXmlLoader.deploy(url, overloads , entityResolver);
//						for(PersistenceMetadata pmeta:pmetas){
//							int len = pmeta.getClasses().size();
//							Ivy.log().info(PersistenceUtils.class.getSimpleName()+" Added Persistence Unit:"+pmeta.getName()+" having "+len+" listed classes.");
//							names.add(pmeta.getName());
//						}
//					} catch (MalformedURLException e1) {
//						Ivy.log().error(PersistenceUtils.class.getSimpleName()+" Error reading File\n"+persistenceResource.getAbsolutePath()+" \nCause:"+e1.getCause(), e1);
//					} catch (Exception e) {
//						Ivy.log().error(PersistenceUtils.class.getSimpleName()+" Error loading File\n"+url.toString()+" \nCause:"+e.getCause(), e);
//					}							
//				}
//				else {
//					Ivy.log().error(PersistenceUtils.class.getSimpleName()+" No Persistence xml found at "+persistenceResource.getAbsolutePath());				
//				}
//			}
			//names.add("Planning");
			names.add("Adressmutation");
			persistenceUnits = names;
		}
		return persistenceUnits;
	}
	
	/**
	 * Gets the unit Name out of a Class
	 * @return String unit Name or empty String if class is not an entity
	 * @throws PersistencyException 
	 * @throws EnvironmentNotAvailableException 
	 */
	public static String getPersistenceUnitNameOf(Class<?> clazz) throws EnvironmentNotAvailableException, PersistencyException{
		String unitName = "";
		if(persistenceUnitMap.containsKey(clazz.getName())){
			unitName = persistenceUnitMap.get(clazz.getName());
		}
		else {
			if(isEntityClass(clazz)){
				List<String> names = PersistenceUtils.getPersistenceUnitNames();
				Ivy ivy = Ivy.getInstance();
				for(String name:names){
					IIvyEntityManager persistenceUnit = ivy.persistence.get(name);
					Boolean isContaining = PersistenceUtils.contains(persistenceUnit , clazz);
					if(isContaining){
						unitName = name;
						Ivy.log().info(PersistenceUtils.class.getSimpleName()+" Class:"+clazz.getName()+" belongs to unit:"+unitName);
						persistenceUnitMap.put(clazz.getName(), unitName);
						break;
					}
				}
				//XmlProperties props = new XmlProperties(clazz);		
				//unitName = props.getProperty(ClassProperties.PERSISTENCEUNIT.toString());			

			}
		}
		
		return unitName;
	}
	/**
	 * Checks if the class is Annotated to javax.persistence.Entity
	 * @param clazz
	 * @return
	 */
	public static boolean isEntityClass(Class<?> clazz){
		return clazz.isAnnotationPresent(javax.persistence.Entity.class);
	}
	/**
	 * Checks if present entity encloses fields with a ManyToOne association
	 * executes a refresh against those to prevent an error and merges the entity. 
	 * @param entity
	 * @return merged entity
	 * @throws EnvironmentNotAvailableException
	 * @throws PersistencyException
	 */
	public static CompositeObject merge(CompositeObject entity) throws EnvironmentNotAvailableException, PersistencyException{
		HashMap<String, CompositeObject> entities = PersistenceUtils.getAssociatedEntitiesMap(entity, EntityAssociationType.ManyToOne);
		String unit = PersistenceUtils.getPersistenceUnitNameOf(entity.getClass());
		IIvyEntityManager manager = Ivy.persistence().get(unit);
		for(String fieldName:entities.keySet()){
			CompositeObject enclosed=null;
			try {
				enclosed = (CompositeObject) entity.get(fieldName);
			} catch (NoSuchFieldException e1) {
				Ivy.log().info("Excecute refresh before merge for "+fieldName+" failed");
			}
			if(enclosed!=null){
				Object id = PersistenceUtils.getIdFieldValue(enclosed);
				//Ivy.log().info("Excecute find for enclosed entity "+enclosed.toString()+" with id:"+id.toString());					
				
				if(id!=null&&!id.toString().equals("")){
					enclosed= manager.find(enclosed.getClass(), id);
					}
				}
				else {
					//reset the enclosed value to prevent refresh in merge.
					enclosed = null;				
					}
				
				try {
					entity.set(fieldName, enclosed);
				} catch (NoSuchFieldException e) {
					Ivy.log().debug(PersistenceUtils.class.getSimpleName()+": Error "+e.getCause(),e);
				}
			}
		//obvisually there is the possibility that a parent child relationship
		//can be covered by OneToOne Relations
		for(Field field:entity.getClass().getDeclaredFields()){
			if(field.getType().getName().equals(entity.getClass().getName())){
				CompositeObject enclosedParent = null;
				try {
					enclosedParent = (CompositeObject) entity.get(field.getName());
				} catch (NoSuchFieldException e) {
					Ivy.log().debug(PersistenceUtils.class.getSimpleName()+": Error on merge"+e.getCause(),e);
				}
				if(enclosedParent!=null){
					Object id = PersistenceUtils.getIdFieldValue(enclosedParent);					
					if(id!=null&&!id.toString().equals("")){
						enclosedParent= manager.find(enclosedParent.getClass(), id);
					}
					else {
						enclosedParent = null;
					}
					try {
						entity.set(field.getName(), enclosedParent);
					} catch (NoSuchFieldException e) {
						Ivy.log().debug(PersistenceUtils.class.getSimpleName()+": Error "+e.getCause(),e);
					}
					
				}
			}
		}
		//Ivy.log().info("Excecute Merge for "+entity.toString());
		entity = manager.merge(entity);
		return entity;
	}
	/**
	 * Gets the enclosed fields having given EntityAssociationType
	 * @param composite Entity class to check
	 * @param type EntityAssociationType to look for.
	 * @return List<CompositeObject>
	 */
	public static List<CompositeObject> getAssociatedEntities(CompositeObject composite, EntityAssociationType type){
		List<CompositeObject> entities = new ArrayList<CompositeObject>();
		if(isEntityClass(composite.getClass())){
			PersistenceMetaClass entityMeta = new PersistenceMetaClass(composite.getClass());
						
			for(PersistenceMetaField entityField:entityMeta.getFieldList()){
				if(entityField.getAssociationType().equals(type)){
					CompositeObject entity = null;
					try {
						entity = (CompositeObject) composite.get(entityField.getJavaField().getName());
					} catch (NoSuchFieldException e) {
						Ivy.log().info(PersistenceUtils.class.getSimpleName()+" Error", e);
					}
					if(entity!=null){
						entities.add(entity);
					}										
				}
			}
		}
		return entities;
	}
	/**
	 * Gets the enclosed fields having given EntityAssociationType
	 * @param composite Entity class to check
	 * @param type EntityAssociationType to look for.
	 * @return List<CompositeObject>
	 */
	public static HashMap<String, CompositeObject> getAssociatedEntitiesMap(CompositeObject composite, EntityAssociationType type){
		HashMap<String, CompositeObject> entities = new HashMap<String, CompositeObject>();
		//Ivy.log().debug("PersistenceUtils: Getting associated Entities of "+composite+" with type "+type);
		if(isEntityClass(composite.getClass())){
			PersistenceMetaClass entityMeta = new PersistenceMetaClass(composite.getClass());						
			for(PersistenceMetaField entityField:entityMeta.getFieldList()){
				if(entityField.getAssociationType().equals(type)){
					CompositeObject entity = null;
					String fieldName = entityField.getJavaField().getName();
					//Ivy.log().debug("PersistenceUtils: Found "+fieldName+" as Associated Entity of "+composite+" with type "+type);
					try {
						entity = (CompositeObject) composite.get(entityField.getJavaField().getName());
					} catch (NoSuchFieldException e) {
						Ivy.log().info(PersistenceUtils.class.getSimpleName()+" Error", e);
					}
					if(entity!=null){
						entities.put(fieldName, entity);
					}										
				}
			}
		}
		return entities;
	}	
	public static List<CompositeObject> getEnclosedEntities(CompositeObject composite){
		List<CompositeObject> entities = new ArrayList<CompositeObject>();
		for(Field field:composite.getClass().getDeclaredFields()){
			if(isEntityClass(field.getType())){
				CompositeObject entity = null;
				try {
					entity = (CompositeObject) composite.get(field.getName());
				} catch (NoSuchFieldException e) {
					Ivy.log().info(PersistenceUtils.class.getSimpleName()+" Error", e);
				}
				if(entity!=null){
					entities.add(entity);
				}
			}
		}
		return entities;
	}
	public static String getIdFieldName(Class<?> clazz){
		String idFieldName = "";
		for(Field field:clazz.getDeclaredFields()){
			if(field.isAnnotationPresent(javax.persistence.Id.class)){
				idFieldName = field.getName();
			}
		}
		return idFieldName;
	}
	public static Object getIdFieldValue(CompositeObject entity){
		Object value=null;
		String idFieldName = PersistenceUtils.getIdFieldName(entity.getClass());
		
		if(!idFieldName.equals("")&&entity!=null){
			try {
				value = entity.get(idFieldName);
			} catch (NoSuchFieldException e) {
				Ivy.log().info(PersistenceUtils.class.getSimpleName()+" Exception",e);
			}
		}
		//Ivy.log().info("Mapped Value of"+this.content.field.getType().getName()+" to "+value.toString());
		return value;	
	}
	
	/**
	 * Check if the instance belongs to the current persistence context
	 * @param persistenceUnit Pass the ivy Persistence Unit (ivy.persistence.<unit>)
	 * @param obj Pass Object to be checked
	 * @return true if the object belongs to present EntityManager
	 */
	public static boolean contains(IIvyEntityManager persistenceUnit, Class<?> entityClass){
		boolean isContaining = false;
		String statement = "Select e From "+entityClass.getSimpleName()+" e";

		try {
			@SuppressWarnings("unused")
			IIvyQuery query = persistenceUnit.createQuery(statement);
			isContaining = true;
		} catch(javax.persistence.PersistenceException e){
			//as the present unit is not obtained by the deployment descriptor
			//an PersistenceException is thrown.
			//Ivy.log().info(PersistenceUtils.class.getSimpleName()+" "+e.getClass()+": "+entityClass.getSimpleName()+" is not mapped by "+persistenceUnit.toString(),e);
		
		} catch(java.lang.IllegalArgumentException e){
			//as the class is not mapped by present unit
			//an IllegalArgumentException is thrown.
			//Ivy.log().info(PersistenceUtils.class.getSimpleName()+" "+e.getClass()+": "+entityClass.getSimpleName()+" is not mapped by "+persistenceUnit.toString(),e);
		}
		
		return isContaining;
	}
}
