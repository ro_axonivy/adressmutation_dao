package ch.soreco.webbies.common.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class MapUtils {
	/**
	 * Sorts the given map by it's values.
	 * 
	 * @param map
	 *            the map of which the values are to sort.
	 * @return the sorted map. never null.
	 */
	public static <K, V extends Comparable<V>> Map<K, V> sortedMapByValue(
			Map<K, V> map) {
		Map<K, V> result = new LinkedHashMap<K, V>();
		List<Entry<K, V>> entries = new ArrayList<Entry<K, V>>(map.entrySet());
		Collections.sort(entries, new ByValueComparator<K, V>());
		
		for (Map.Entry<K, V> entry : entries) {
			result.put(entry.getKey(), entry.getValue());
		}
		return result;

	}
	private static class ByValueComparator<K, V extends Comparable<V>> implements
			Comparator<Entry<K, V>> {
		public int compare(Entry<K, V> o1, Entry<K, V> o2) {
			return o1.getValue().compareTo(o2.getValue());
		}
	}
}