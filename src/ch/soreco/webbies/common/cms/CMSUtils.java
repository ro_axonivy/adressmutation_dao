package ch.soreco.webbies.common.cms;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import ch.ivyteam.ivy.cm.CoType;
import ch.ivyteam.ivy.cm.IContentObject;
import ch.ivyteam.ivy.cm.IContentObjectValue;
import ch.ivyteam.ivy.environment.EnvironmentNotAvailableException;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.persistence.PersistencyException;
import ch.ivyteam.ivy.scripting.objects.List;
import ch.ivyteam.ivy.scripting.objects.Record;
import ch.ivyteam.ivy.scripting.objects.Recordset;

public class CMSUtils {
	private static final String SEPARATORCMS = "/";

	private static String getCurrentUser() throws EnvironmentNotAvailableException, PersistencyException {
		return Ivy.session().getSessionUserName();
	}

	/**
	 * Use this method to create a content object parent folders.
	 * 
	 * @param contentUri
	 * @return IContentObject of contentUri
	 * @throws EnvironmentNotAvailableException
	 * @throws PersistencyException
	 */
	public static IContentObject mkdirs(String contentUri) throws EnvironmentNotAvailableException,
			PersistencyException {
		IContentObject co = null;

		String currentUri = "";
		String user = getCurrentUser();
		try {
			// try to find attribute content->name object directly
			co = Ivy.cms().getContentObject(contentUri);
			if (co == null && !user.equals("") && !contentUri.equals("")) {
				// no name content object
				// -> assume that current field is new and its attribute content
				// has to be added to CMS

				String[] uriList = contentUri.substring(1).split(SEPARATORCMS);
				IContentObject parentFolder = null;
				// explore content hierarchy and add needed CMS folders to meet
				// attributes declaringClass hierarchy
				for (int i = 0; i < uriList.length; i++) {
					StringBuilder coUri = new StringBuilder("");
					String coName = uriList[i];
					for (int j = 0; j <= i; j++) {
						coUri.append(SEPARATORCMS);
						coUri.append(uriList[j]);
					}
					currentUri = coUri.toString();
					IContentObject folder = Ivy.cms().getContentObject(currentUri);
					if (folder == null) {
						if (i == 0) {
							parentFolder = Ivy.cms().getRootContentObject().addChild(coName, "", CoType.FOLDER, user);
						} else if (parentFolder != null) {
							parentFolder = parentFolder.addChild(coName, "", CoType.FOLDER, user);
						}
					} else {
						parentFolder = folder;
					}
				}
				co = Ivy.cms().getContentObject(contentUri);
			}

		} catch (Exception e) {
			Ivy.log().error("Error on getContentObject " + currentUri + ": " + e.getMessage(), e);
		}
		return co;
	}

	/**
	 * Deletes the content of the specified directory
	 * 
	 * @param dir
	 */
	public static void deleteContentOfDirectory(String dir) {
		try {
			if (Ivy.cms().findContentObject(dir) != null) {
				java.util.List<IContentObject> files = Ivy.cms().findContentObject(dir).getChildren();
				for (IContentObject cFile : files) {
					cFile.delete();
				}
			} else {
				Ivy.log().error("Error on deleteContentOfDirectory: directory not found: " + dir);
			}
		} catch (EnvironmentNotAvailableException e) {
			Ivy.log().error("Error on deleteContentOfDirectory(" + dir + "): " + e.getMessage());
		} catch (PersistencyException e) {
			Ivy.log().error("Error on deleteContentOfDirectory(" + dir + "): " + e.getMessage());
		}
	}

	/**
	 * Add a content object to CMS, if the parent folders do not exist, they
	 * will be created. TODO: Make this Method work for other types than Source
	 * and String
	 * 
	 * @param contentType
	 *            CoType: Type to add (source and string only)
	 * @param contentUri
	 *            ParentFolder Path
	 * @param contentName
	 *            Name of the ContentObject
	 * @param content
	 *            String to set to the content
	 * @throws EnvironmentNotAvailableException
	 * @throws PersistencyException
	 */
	public static void setContent(CoType contentType, String contentUri, String contentName, String content)
			throws EnvironmentNotAvailableException, PersistencyException {
		String user = getCurrentUser();
		if (!contentUri.equals("") && !user.equals("")) {
			IContentObjectValue coValue = CMSUtils.getValueToSet(contentType, contentUri, contentName);
			if (coValue != null) {
				coValue.setContent(content, user);
				Ivy.cms().getContentObject(contentUri + SEPARATORCMS + contentName).touch(user);
			}
		}
	}

	public static void setContent(String contentUri, String contentName, File contentFile)
			throws EnvironmentNotAvailableException, PersistencyException, FileNotFoundException {
		String user = getCurrentUser();
		if (!contentUri.equals("") && !user.equals("")) {
			CoType contentType = CMSUtils.guessCoType(contentFile.getName());
			IContentObjectValue coValue = CMSUtils.getValueToSet(contentType, contentUri, contentName);
			if (coValue != null) {
				InputStream content = new FileInputStream(contentFile);
				coValue.setContent(content, 0, user);
				Ivy.cms().getContentObject(contentUri + SEPARATORCMS + contentName).touch(user);
			}
		}
	}

	public static boolean exists(String contentUri) {
		try {
			return Ivy.cms().getContentObject(contentUri) != null;
		} catch (EnvironmentNotAvailableException e) {
			return false;
		} catch (PersistencyException e) {
			return false;
		}
	}

	private static IContentObjectValue getValueToSet(CoType contentType, String contentFolderUri, String contentName)
			throws EnvironmentNotAvailableException, PersistencyException {
		String user = getCurrentUser();
		IContentObjectValue coValueToSet = null;
		if (!contentFolderUri.equals("") && !user.equals("")) {
			try {
				IContentObject coFolder = Ivy.cms().getContentObject(contentFolderUri);
				if (coFolder == null) {
					coFolder = mkdirs(contentFolderUri);
				}
				if (coFolder != null) {
					IContentObject contentObject = coFolder.getChild(contentName);
					if (contentObject == null) {
						contentObject = coFolder.addChild(contentName, "", contentType, user);
					}
					if (contentObject != null) {
						if (contentObject.hasValues()) {
							for (IContentObjectValue coValue : contentObject.getValues()) {
								if (coValue.isDefault()) {
									coValueToSet = coValue;
									break;
								}
							}
						} else {
							coValueToSet = contentObject.addValue("", new java.util.Date(), null, Ivy.cms()
									.getDefaultLanguage(), user, true, "");
						}
					}
				}
			} catch (Exception e) {
				Ivy.log().error(e);
			}
		}
		return coValueToSet;
	}

	private static CoType guessCoType(String fileName) {
		String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
		CoType contentType = CoType.UNKNOWN;
		if (extension.toLowerCase().equals("pdf")) {
			contentType = CoType.PDF;
		} else if (extension.toLowerCase().equals("png")) {
			contentType = CoType.PNG;
		} else if (extension.toLowerCase().equals("gif")) {
			contentType = CoType.GIF;
		} else if (extension.toLowerCase().equals("jpg")) {
			contentType = CoType.JPEG;
		} else if (extension.toLowerCase().equals("xls")) {
			contentType = CoType.EXCEL;
		} else if (extension.toLowerCase().equals("html") || extension.toLowerCase().equals("htm")) {
			contentType = CoType.HTML;
		}

		return contentType;
	}

	/**
	 * Get the Makro string of a content object, returns empty String if content
	 * object does not exist
	 * 
	 * @param contentUri
	 * @return String:jsp Makro
	 */
	public static String getContentMakro(String contentUri) {
		String makro = "";
		try {
			IContentObject co = Ivy.cms().getContentObject(contentUri);
			if (co != null && co.hasValues()) {
				makro = "<%=ivy.cms.co(\"" + contentUri + "\")%>";
			}
		} catch (Exception e) {
			Ivy.log().debug(e);
		}
		return makro;
	}

	public static Recordset getCMSRecordset(List<String> rootPaths) {
		Recordset rsCMSAll = new Recordset();
		for (String rootPath : rootPaths) {
			Recordset rsCMS = getCMSRecordset(rootPath);
			if (rsCMS != null) {
				rsCMSAll.addAll(rsCMS);
			}
		}
		return rsCMSAll;
	}

	public static Recordset getCMSRecordset(String rootPath) {
		IContentObject coRoot = null;
		Recordset rsCMS = new Recordset();
		try {
			coRoot = Ivy.cms().getContentObject(rootPath);
			if (coRoot != null) {
				rsCMS.clear();
				rsCMS = exploreCMSContent(coRoot, rsCMS);
			} else {
				Ivy.log().debug("Can not instantiate " + rootPath);
			}
		} catch (Exception e) {
			Ivy.log().debug("Error finding " + rootPath, e);
		}
		return rsCMS;
	}

	private static Recordset exploreCMSContent(IContentObject co, Recordset rsCMS) throws PersistencyException {
		Record record = null;
		try {
			record = getCMSRecord(co);
		} catch (Exception e) {
			Ivy.log().debug("Error on get CMS Record", e);
		}
		try {
			rsCMS = addRecord(record, rsCMS);
		} catch (Exception e) {
			Ivy.log().debug("Error on add CMS Record", e);
		}

		for (IContentObject child : co.getChildren()) {
			try {
				exploreCMSContent(child, rsCMS);
			} catch (Exception e) {
				Ivy.log().debug("Error on exploring CMS Record", e);
			}
		}
		return rsCMS;
	}

	private static Recordset addRecord(Record record, Recordset rs) {
		if (record != null) {
			for (String key : record.getKeys()) {
				if (rs.getKeys().indexOf(key) == -1) {
					java.util.List<String> column = new ArrayList<String>();
					// add empty column
					rs.addColumn(key, column);
				}
			}
			rs.add(record);
		}
		return rs;
	}

	private static Record getCMSRecord(IContentObject co) {
		Record record = null;
		if (co != null) {
			try {
				if (co.getContentObjectType().getCoType().equals(CoType.SOURCE)
						|| co.getContentObjectType().getCoType().equals(CoType.STRING)
						|| co.getContentObjectType().getCoType().equals(CoType.HTML)
						|| co.getContentObjectType().getCoType().equals(CoType.TEXT)) {
					record = new Record();
					record.putField("coUri", co.getUri());
					record.putField("coUriJSP", "<%=ivy.cms.co(\"" + co.getUri() + "\")%>");
					record.putField("coName", co.getName());
					record.putField("coType", co.getContentObjectType().getCoType().getName());
					// record.putField("coValue", Ivy.cms().co(co.getUri()));
					if (co.hasValues()) {
						IContentObjectValue defaultValue = null;
						for (IContentObjectValue value : co.getValues()) {
							if (value.isDefault()) {
								defaultValue = value;
							}
						}
						if (defaultValue != null) {
							record.putField("coValue", defaultValue.getContentAsString());
						}
					}
				}

			} catch (PersistencyException e) {
				Ivy.log().debug("Error on building CMS record", e);
			}

		}
		return record;
	}
}
