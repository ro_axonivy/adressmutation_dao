package ch.soreco.webbies.common.cms;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.Field;
import java.util.Properties;

import ch.ivyteam.ivy.cm.CoType;
import ch.ivyteam.ivy.cm.IContentObject;
import ch.ivyteam.ivy.environment.EnvironmentNotAvailableException;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.persistence.PersistencyException;

public class PropertiesToCMS {
	public static final String CONTENTPREFIX = "0_";
	public static final String SEPARATORCMS = "/";
	public static final String SEPARATORCLASS = ".";
	
	private final String META_NAME = "properties";
	private Properties properties = new Properties();
	private IContentObject content;
	private String view = "0_";
	private String comment = "";
	
	private PropertiesToCMS(IContentObject content, String view) throws 
															EnvironmentNotAvailableException
															, PersistencyException{
		this.content = content;
		if(!view.equals("")){
			this.view = view;			
		}
		this.loadProperties();
	}
	private PropertiesToCMS(Class<?> clazz) throws 
															EnvironmentNotAvailableException
															, PersistencyException{
		IContentObject content = Ivy.cms().getContentObject(getUri(clazz));
		this.content = content;
		this.view = CONTENTPREFIX;

		this.loadProperties();
	}
	private PropertiesToCMS(Field field) throws 
															EnvironmentNotAvailableException
															, PersistencyException{
		IContentObject content = Ivy.cms().getContentObject(getUri(field));
		this.content = content;
		this.view = CONTENTPREFIX;
		this.loadProperties();
	}
	private String getUri(Field field){
		String uri = SEPARATORCLASS
					+field.getType().getName();
		uri = uri
				+SEPARATORCLASS
				+field.getName();
		uri = uri.replace(SEPARATORCLASS, SEPARATORCMS);

		return uri;
	}
	private String getUri(Class<?> clazz){
		String uri = SEPARATORCLASS
						+clazz.getName();
		return uri.replace(SEPARATORCLASS, SEPARATORCMS);
	}
	/**
	 * Load Properties XML as is stored within content childs named "view"+properties
	 * @throws PersistencyException 
	 */
	private void loadProperties() throws PersistencyException
	{
		properties = new Properties();
		String propertyXML = getPropertyXml();
		if(propertyXML.trim().length()>0){
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(propertyXML.getBytes());
			try
			{
				properties.loadFromXML(byteArrayInputStream);
			}
			catch(Exception e)
			{
				Ivy.log().debug("Error on Loading CMS Properties from "+this.content.toString(), e);
			}			
		}
	}
	private String getPropertyXml() throws PersistencyException{
		String uri = this.content.getUri()+"/"+this.getName();
		return Ivy.cms().co(uri);
	}
	public Properties getProperties(){
		return properties;
	}
	public void setProperties(Properties properties){
		this.properties = properties;
	}
	/**
	 * Get a property by key
	 * @param key
	 * @return String, empty String if not in property map
	 */
	public String getProperty(String key){
		String property = "";
		if(this.properties.containsKey(key)){
			property = (String) this.properties.get(key);
		}
		return property;
	}
	public void setProperty(String key, String value){
		this.properties.put(key, value);
	}
	public void save(){
		if(this.properties.size()>0){
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			try
			{
				this.properties.storeToXML(byteArrayOutputStream, this.getComment());
				String xmlString = byteArrayOutputStream.toString();
				//this.content.setDescription(xmlString, this.user);
				this.saveToContent(xmlString);
			}
			catch(Exception e)
			{
				Ivy.log().error("AttributeContent: storeProperties failed", e);
			}					
		}
		else {
			this.saveToContent("");
		}
	}
	private String getName(){
		return this.view+this.META_NAME;		
	}
	private void saveToContent(String content){
		try {
			CMSUtils.setContent(CoType.SOURCE, this.content.getUri(), this.getName(), content);
		} catch (EnvironmentNotAvailableException e) {
			Ivy.log().error(this.getClass().getSimpleName()+" Store Properties failed!");
		} catch (PersistencyException e) {
			Ivy.log().error(this.getClass().getSimpleName()+" Store Properties failed!");
		}
	}
	/**
	 * Define the comment to set to the Beginning of properties xml
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	/**
	 * @return the comment
	 */
	public String getComment() {
		if(this.comment.equals("")){
			this.comment = "AttributeContent Properties - these values are saved programmatically - do not change this.";
		}
		return comment;
	}
	public String toString(){
		StringBuilder text = new StringBuilder("");
		for(String key:this.properties.stringPropertyNames()){
			text.append(key);
			text.append("=");
			text.append(this.properties.getProperty(key));
			text.append("\n");
		}
		return text.toString();
	}
}
