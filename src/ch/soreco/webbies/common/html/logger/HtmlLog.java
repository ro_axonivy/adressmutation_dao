package ch.soreco.webbies.common.html.logger;

import org.apache.log4j.Logger;
import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.Appender;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import ch.ivyteam.ivy.environment.Ivy;

public class HtmlLog  {
	private static String LOGGER_NAME = "HtmlLogger";
	private static String APPENDER_NAME = "HtmlLogAppender";
	private static String appenderFileRoot = "";
	private static List<List<String>> logRecords = null;
	private static Logger logger = Logger.getLogger( LOGGER_NAME );
	private static java.io.File appenderFile = null;
	private static String PATTERN_DELIMITER = ";";
	private static String CONVERSION_PATTERN = "%r;%d{ISO8601};%-5p;%t;%x;%c;%m;%n";
	private static HashMap<String, String> patternMap = null;
	private static List<String> patternList = null;
	
	public static void debug(Object message){
		logger.debug(message);
		Ivy.log().debug(message);
	}
	public static void debug(Object message, Throwable t){
		logger.debug(message);
		Ivy.log().debug(message,t);
	}
	
	public static void error(Object message){
		logger.error(message);
		Ivy.log().error(message);
	}
	public static void error(Object message, Throwable t){
		logger.error(message);
		Ivy.log().error(message,t);
	}
	
	public static void fatal(Object message){
		logger.fatal(message);
		Ivy.log().fatal(message);
	}
	public static void fatal(Object message, Throwable t){
		logger.fatal(message);
		Ivy.log().fatal(message,t);
	}
	
	public static void info(Object message){
		logger.info(message);
		Ivy.log().info(message);
	}
	public static void info(Object message, Throwable t){
		logger.info(message);
		Ivy.log().info(message,t);
	}
	
	public static Logger getLogger(){
		return logger;
	}

	
	public static String HtmlLogFileName(){
		String fileNameRoot = "";
		Appender appender = logger.getAppender(APPENDER_NAME);
        if (appender == null) {
            Ivy.log().debug("no appender named '" + APPENDER_NAME + "'");
            return appenderFileRoot;
        }
        if (!(appender instanceof DailyRollingFileAppender)) {
        	Ivy.log().debug("appender named '" + APPENDER_NAME
                    + "' is not a DailyRollingFileAppender");
            return appenderFileRoot;

        }
        
        DailyRollingFileAppender drfa = (DailyRollingFileAppender) appender;
        String datePattern = drfa.getDatePattern();
        appenderFileRoot = drfa.getFile();
               
        Ivy.log().debug("OK, got DailyRollingFileAppender with " + datePattern + " and " + fileNameRoot);

		return appenderFileRoot;
	}
	public static java.io.File HtmlLogFile(){
		String filePath ="";
		
		if(appenderFileRoot.equals("")) 
			filePath = HtmlLogFileName();
		else
			filePath = appenderFileRoot;

		if(!filePath.equals("")) appenderFile = new File(filePath);
		return appenderFile;
	}
	public static List<String> getPatternList(){
		if(patternList==null){
			List<String> result = new ArrayList<String>();
			if(CONVERSION_PATTERN!=null){
				Scanner patterns = new Scanner(CONVERSION_PATTERN);
				patterns.useDelimiter(PATTERN_DELIMITER);
				while(patterns.hasNext()){
					String pattern = patterns.next();
					String name = getPatternName(pattern);
					result.add(name);
				}
			}
			
			patternList = result;
			return result;			
		}
		else {
			return patternList;
		}

	}
	private static String getPatternName(String pattern){
		String name = pattern.replace("%", "").trim();
		HashMap<String, String> patternMappings = getPatternMap();
		if(patternMappings.containsKey(name)){
			name = patternMappings.get(name);
		}
		else {
			for(String key : patternMappings.keySet()) {
				if(name.indexOf(key)>-1){
					name = patternMappings.get(key);
					break;
				}
			}
		}
		
		return name;
	}
	public static String getLogJSONList(){
		StringBuilder json = new StringBuilder();
		json.append("[\n");
		try {
			getLogRecords();
			List<String> listHeaders = getPatternList();
			
			for(int i=0;i<logRecords.size();i++){
				json.append("{");
				List<String> record = logRecords.get(i);
				//String line = logRecords.get(i).toString();
				String line = "";
				for(int j=0;j<record.size();j++){
					if(j>0) json.append(",");
					if(j<listHeaders.size()){
						json.append(listHeaders.get(j)+":");	
					}
					else {
						json.append("Column"+j+":");
					}
					json.append(record.get(j));
				}
				

				//line = line.substring(1,line.length()-1);
				json.append(line);
				json.append("}\n");
				if(i<logRecords.size()-1) json.append(",");
			}
		} catch (FileNotFoundException e) {
			Ivy.log().debug("getLogJSONList: Log File not found", e);
		} catch (Exception e){
			Ivy.log().debug("getLogJSONList: "+e.getMessage(),e);			
		}
		json.append("]");
		Ivy.log().debug("getLogJSONList from "+appenderFile+" was successful");
		return json.toString();
	}
	/** Template method that calls {@link #getRecord(String)}.  */
	public static List<List<String>> getLogRecords() throws FileNotFoundException {
		List<List<String>> logRecordList = new ArrayList<List<String>>();
		if(appenderFile==null||!appenderFile.exists()){
			HtmlLogFile();
		}
		if(appenderFile.exists()){
		    Scanner scanner = new Scanner(appenderFile);
		    try {
		    	//first use a Scanner to get each line
		    	StringBuilder completeRecord = new StringBuilder("");
		    	while ( scanner.hasNextLine() ){
		    		String line = scanner.nextLine();
	    			completeRecord.append(line);
		    		if(line.endsWith(PATTERN_DELIMITER)){
		    			logRecordList.add(getRecord(completeRecord.toString()));
		    			completeRecord = new StringBuilder("");
		    			}
		    		}
		    	}
		    catch (Exception e){
		    	Ivy.log().debug("getLogRecords: "+e.getMessage(),e);
		    }
		    finally {
		      //ensure the underlying stream is always closed
		      scanner.close();
		    }			
		}
		else {
			Ivy.log().debug("getLogRecords: appender File does not exist");
		}
		logRecords = logRecordList;
		return logRecordList;
	}
	private static List<String> getRecord(String line){
	  //use a second Scanner to parse the content of each line 
	  Scanner scanner = new Scanner(line);
	  scanner.useDelimiter(PATTERN_DELIMITER);
	  
	  List<String> result = new ArrayList<String>();
	  try {
		  while (scanner.hasNext()) {
			  String value = quote(scanner.next());
			  result.add(value);
		  }		  
	  }
	  catch (Exception e){
		  Ivy.log().info("getRecord: Log File Scan Error on getRecord",e);
	  }
	  finally {
		  //(no need for finally here, since String is source)
		  scanner.close();		  
	  }
	  return result;
	}
	private static String quote(String text){
		String QUOTE = "\"";
		return QUOTE + text.replace("\"", "&quot;") + QUOTE;
	}
	private static HashMap<String, String> getPatternMap(){
		if(patternMap==null){
			HashMap<String, String> hashMap = new HashMap<String, String>();
			hashMap.put("c", "Category");
			hashMap.put("C", "ClassName");
			// 	Used to output the fully qualified class name of the caller issuing the logging request. This conversion specifier can be optionally followed by precision specifier, that is a decimal constant in brackets.
			//If a precision specifier is given, then only the corresponding number of right most components of the class name will be printed. By default the class name is output in fully qualified form.
			//For example, for the class name "org.apache.xyz.SomeClass", the pattern %C{1} will output "SomeClass".
			//WARNING Generating the caller class information is slow. Thus, it's use should be avoided unless execution speed is not an issue. 
			hashMap.put("d", "Date");
			//Used to output the date of the logging event. The date conversion specifier may be followed by a set of braces containing a date and time pattern strings SimpleDateFormat, ABSOLUTE, DATE or ISO8601  and a set of braces containing a time zone id per TimeZone.getTimeZone(String). For example, %d{HH:mm:ss,SSS}, %d{dd MMM yyyy HH:mm:ss,SSS}, %d{DATE} or %d{HH:mm:ss}{GMT+0}. If no date format specifier is given then ISO8601 format is assumed. 
			hashMap.put("F", "FileName");
			// 	Used to output the file name where the logging request was issued.
			//WARNING Generating caller location information is extremely slow. Its use should be avoided unless execution speed is not an issue. 
			hashMap.put("l", "Location");
			//Used to output location information of the caller which generated the logging event.
			//The location information depends on the JVM implementation but usually consists of the fully qualified name of the calling method followed by the callers source the file name and line number between parentheses.
			//The location information can be very useful. However, it's generation is extremely slow. It's use should be avoided unless execution speed is not an issue. 
			hashMap.put("L", "LineNumber");		
			//Used to output the line number from where the logging request was issued.
			//WARNING Generating caller location information is extremely slow. It's use should be avoided unless execution speed is not an issue. 
			hashMap.put("m", "Message");
			//Used to output the application supplied message associated with the logging event.
			hashMap.put("M", "MethodName");		
			//Used to output the method name where the logging request was issued.
			//WARNING Generating caller location information is extremely slow. It's use should be avoided unless execution speed is not an issue. 
			hashMap.put("p", "Priority");	
			//Used to output the priority of the logging event.
			hashMap.put("r", "MillisecondsElapsed");	
			//Used to output the number of milliseconds elapsed since the construction of the layout until the creation of the logging event.
			hashMap.put("t", "ThreadName");	
			//Used to output the name of the thread that generated the logging event.
			hashMap.put("x", "NDC");			
			//Used to output the NDC (nested diagnostic context) associated with the thread that generated the logging event. 
			hashMap.put("X", "MDC");	
			//Used to output the MDC (mapped diagnostic context) associated with the thread that generated the logging event. The X  conversion character can be followed by the key for the map placed between braces, as in %X{clientNumber} where clientNumber is the key. The value in the MDC corresponding to the key will be output. If no additional sub-option is specified, then the entire contents of the MDC key value pair set is output using a format {{key1,val1},{key2,val2}}
			//See MDC class for more details. 
			hashMap.put("properties", "Properties");
			//Used to output the Properties associated with the logging event. The properties  conversion word can be followed by the key for the map placed between braces, as in %properties{application} where application is the key. The value in the Properties bundle corresponding to the key will be output. If no additional sub-option is specified, then the entire contents of the Properties key value pair set is output using a format {{key1,val1},{key2,val2}}
			hashMap.put("throwable", "Throwable");	
			//Used to output the Throwable trace that has been bound to the LoggingEvent, by default this will output the full trace as one would normally find by a call to Throwable.printStackTrace(). The throwable conversion word can be followed by an option in the form %throwable{short}  which will only output the first line of the ThrowableInformation.
			hashMap.put("n", "LineSeparator");		
			//Outputs the platform dependent line separator character or characters.
			//This conversion character offers practically the same performance as using non-portable line separator strings such as "\n", or "\r\n". Thus, it is the preferred way of specifying a line separator. 
			patternMap = hashMap;
			return hashMap;
			
		}
		else {
			return patternMap;
		}
	}
}
