package ch.ivyteam.ivy.ext;

import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.Callable;

import ch.ivyteam.ivy.application.IApplication;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.persistence.PersistencyException;
import ch.ivyteam.ivy.security.ISession;

/**
 * Manager to lock own ID-s for sessions. If case a session expires, then the lock will be released.<br>
 * WARN: This code work only for 1 application. In case you try to use from multiple application, you will
 * experience some problems!! (see {@link #releaseExpiredSessionLocks(IApplication)})
 * 
 * @author tkis
 * @since 14.04.2011
 */
public class SessionLockManager
{
  /** Instance */
  private static SessionLockManager fInstance = null;

  /** Lock map for ID-s pro session */
  private HashMap<String, ISession> fLockMap = new HashMap<String, ISession>();

  /**
   * Constructor
   */
  private SessionLockManager()
  {
  }

  /**
   * get the instance
   * @return the singleton
   */
  public static SessionLockManager getInstance()
  {
    if (fInstance == null)
    {
      fInstance = new SessionLockManager();
    }
    return fInstance;
  }

  /**
   * lock the given ID for the given session.
   * @param id must not be null!
   * @param session must not be null!
   * @return true if ID could be locked.
   */
  public synchronized boolean lockIdForSession(String id, ISession session)
  {
	if (isLockOwner(id, session))
	{
	  return true; // is already locked by present session
	}  
    if (isIdLocked(id))
    {
      return false; // could not be locked
    }
    if (id == null || id.length() == 0 || session == null)
    {
      return false;
    }
    fLockMap.put(id, session);
    return true;
  }

  /**
   * gets if the given ID is already locked for any sessions.
   * @param id ID to check for
   * @return true if already locked
   */
  public synchronized boolean isIdLocked(String id)
  {
    return fLockMap.containsKey(id);
  }
  /**
   * checks if current session already owns lock for current id
   * @param id ID to check for
   * @return true if session owns the lock
   */
  private synchronized boolean isLockOwner(String id, ISession session){
	  if(fLockMap.containsKey(id)){
		  ISession lockingSession = fLockMap.get(id);
		  return lockingSession.getIdentifier()==session.getIdentifier();
	  }
	  else {
		  return false;
	  }
  }
  /**
   * Get the locking User if any, else empty String is returned
   * @param id
   * @return UserName if any, else empty String
   * @throws Exception 
   */
  public synchronized String getLockingUser(String id) throws Exception{
	 String userName="";
	 if(isIdLocked(id)){
		 ISession lockingSession = fLockMap.get(id);
		 userName = getUserNameOfSession(lockingSession);
	 }
	 return userName;
  }
  private synchronized String getUserNameOfSession(final ISession session) throws Exception{
	  String userName= "";
	  userName =Ivy.session().getSecurityContext().executeAsSystemUser(
				new Callable<String>() {
					public String call() throws PersistencyException {
						return session.getSessionUserName();
					}
				});
	  return userName;
  }
  /**
   * releases given ID (if already registered)
   * @param id ID to release
   */
  public synchronized void releaseLockId(String id)
  {
    fLockMap.remove(id);
  }

  /**
   * WARN: This must be executed with some special rights! <br><br>
   * 
   * releases all locked ID-s for all expired sessions. (ONLY ONE APPLICATION IS SUPPORTED). <br>
   * Therefore only expired sessions of this application are considered.
   * @param application Xpert.ivy IApplication
   * @throws PersistencyException if case application cannot access session
   */
  public synchronized void releaseExpiredSessionLocks(IApplication application) throws PersistencyException
  {
    Iterator<String> keys = fLockMap.keySet().iterator();
    while (keys.hasNext())
    {
      String key = keys.next();
      ISession session = fLockMap.get(key);
      //Ivy.log().info("Look for Session "+session.getIdentifier()+" within App Context of "+application.getName());
      ISession existingSession = application.getSecurityContext().findSession(session.getIdentifier());
      if (existingSession == null)
      {
    	  //Ivy.log().info("Released Session lock for "+key+" by session "+session.getIdentifier()+" in application "+application.getName());
    	  fLockMap.remove(key);
      }
      else {
    	  //Ivy.log().info("Keeping locks of session "+session.getIdentifier()+" alive.");
      }
    }
  }
}